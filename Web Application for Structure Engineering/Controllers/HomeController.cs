﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PagedList;
using PagedList.Mvc;
using Web_Application_for_Structure_Engineering.Models;

namespace Web_Application_for_Structure_Engineering.Controllers
{
    public class HomeController : Controller
    {
        public int Error;

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult MyProject(int? page)
        {
            List<Project> list1 = new List<Project>();
            if (Session["uid"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
                SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
                sqlconn.Open();
                string sqlquery3 = "select * from project where Uid = " + Session["uid"];
                SqlCommand sqlcomm1 = new SqlCommand(sqlquery3);
                sqlcomm1.Connection = sqlconn;
                SqlDataReader dr = sqlcomm1.ExecuteReader();
                while (dr.Read())
                {
                    list1.Add(new Project
                    {
                        projectID = Convert.ToInt32(dr["projectID"]),
                        projectName = dr["projectName"].ToString()
                    });
                }
                sqlconn.Close();
            }
            return View(list1.ToList().ToPagedList(page ?? 1, 9));
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult DeletePFile(int A_ID)
        {
            string fileName1 = "";
            accessFile file = new accessFile();
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            string sqlquery7 = "select accessFileName from accessFile WHERE accessFileID = " + A_ID;
            SqlCommand sqlcomm7 = new SqlCommand(sqlquery7);
            sqlcomm7.Connection = sqlconn;
            SqlDataReader dr = sqlcomm7.ExecuteReader();
            while (dr.Read())
            {
                fileName1 = dr["accessFileName"].ToString();
            }
            dr.Close();
            string sqlquery1 = "DELETE FROM Stress WHERE accessFileID = " + A_ID;
            SqlCommand sqlcomm1 = new SqlCommand(sqlquery1);
            sqlcomm1.Connection = sqlconn;
            sqlcomm1.ExecuteNonQuery();
            string sqlquery2 = "DELETE FROM RCBeam WHERE accessFileID = " + A_ID;
            SqlCommand sqlcomm2 = new SqlCommand(sqlquery2);
            sqlcomm2.Connection = sqlconn;
            sqlcomm2.ExecuteNonQuery();
            string sqlquery3 = "DELETE FROM ElementForcesFrames WHERE accessFileID = " + A_ID;
            SqlCommand sqlcomm3 = new SqlCommand(sqlquery3);
            sqlcomm3.Connection = sqlconn;
            sqlcomm3.ExecuteNonQuery();
            string sqlquery4 = "DELETE FROM FrameSectionAssignments WHERE accessFileID = " + A_ID;
            SqlCommand sqlcomm4 = new SqlCommand(sqlquery4);
            sqlcomm4.Connection = sqlconn;
            sqlcomm4.ExecuteNonQuery();
            string sqlquery5 = "DELETE FROM ConnectivityFrame WHERE accessFileID = " + A_ID;
            SqlCommand sqlcomm5 = new SqlCommand(sqlquery5);
            sqlcomm5.Connection = sqlconn;
            sqlcomm5.ExecuteNonQuery();
            string sqlquery6 = "DELETE FROM accessFile WHERE accessFileID = " + A_ID;
            SqlCommand sqlcomm6 = new SqlCommand(sqlquery6);
            sqlcomm6.Connection = sqlconn;
            sqlcomm6.ExecuteNonQuery();
            string fileName2 = Path.Combine(Server.MapPath("~/img/"), fileName1);
            System.IO.File.Delete(fileName2);
            return Json(new { data = "Sucsess" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteProject(int P_ID)
        {
            List<string> fileName1 = new List<string>();
            accessFile file = new accessFile();
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            string sqlquery7 = "select accessFileName from accessFile WHERE projectID = " + P_ID;
            SqlCommand sqlcomm7 = new SqlCommand(sqlquery7);
            sqlcomm7.Connection = sqlconn;
            SqlDataReader dr = sqlcomm7.ExecuteReader();
            while (dr.Read())
            {
                fileName1.Add(dr["accessFileName"].ToString());
            }
            dr.Close();
            string sqlquery1 = "DELETE FROM Stress WHERE projectID = " + P_ID;
            SqlCommand sqlcomm1 = new SqlCommand(sqlquery1);
            sqlcomm1.Connection = sqlconn;
            sqlcomm1.ExecuteNonQuery();
            string sqlquery2 = "DELETE FROM RCBeam WHERE projectID = " + P_ID;
            SqlCommand sqlcomm2 = new SqlCommand(sqlquery2);
            sqlcomm2.Connection = sqlconn;
            sqlcomm2.ExecuteNonQuery();
            string sqlquery3 = "DELETE FROM ElementForcesFrames WHERE projectID = " + P_ID;
            SqlCommand sqlcomm3 = new SqlCommand(sqlquery3);
            sqlcomm3.Connection = sqlconn;
            sqlcomm3.ExecuteNonQuery();
            string sqlquery4 = "DELETE FROM FrameSectionAssignments WHERE projectID = " + P_ID;
            SqlCommand sqlcomm4 = new SqlCommand(sqlquery4);
            sqlcomm4.Connection = sqlconn;
            sqlcomm4.ExecuteNonQuery();
            string sqlquery5 = "DELETE FROM ConnectivityFrame WHERE projectID = " + P_ID;
            SqlCommand sqlcomm5 = new SqlCommand(sqlquery5);
            sqlcomm5.Connection = sqlconn;
            sqlcomm5.ExecuteNonQuery();
            string sqlquery6 = "DELETE FROM accessFile WHERE projectID = " + P_ID;
            SqlCommand sqlcomm6 = new SqlCommand(sqlquery6);
            sqlcomm6.Connection = sqlconn;
            sqlcomm6.ExecuteNonQuery();
            string sqlquery8 = "DELETE FROM Project WHERE projectID = " + P_ID;
            SqlCommand sqlcomm8 = new SqlCommand(sqlquery8);
            sqlcomm8.Connection = sqlconn;
            sqlcomm8.ExecuteNonQuery();
            for (int i = 0; i < fileName1.Count; i++)
            {
                string fileName2 = Path.Combine(Server.MapPath("~/img/"), fileName1[i]);
                System.IO.File.Delete(fileName2);
            }

            return Json(new { data = "Sucsess" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PFile(int? page, int PID)
        {
            List<accessFile> list2 = new List<accessFile>();
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            string sqlquery3 = "select * from accessFile where projectID = @PID";
            SqlCommand sqlcomm1 = new SqlCommand(sqlquery3);
            sqlcomm1.Connection = sqlconn;
            sqlcomm1.Parameters.AddWithValue("@PID", PID);
            SqlDataReader dr = sqlcomm1.ExecuteReader();
            while (dr.Read())
            {
                list2.Add(new accessFile
                {
                    accessFileID = Convert.ToInt32(dr["accessFileID"]),
                    accessFileName = dr["accessFileName"].ToString(),
                    projectID = Convert.ToInt32(dr["projectID"]),
                });
            }
            Session["PProject"] = PID;
            sqlconn.Close();
            return View(list2.ToList().ToPagedList(page ?? 1, 9));
        }
        public ActionResult InsertaccessFile()
        {
            return View();
        }

        [HttpPost, ActionName("InsertaccessFile")]
        public ActionResult InsertaccessFile(accessFile isN)
        {
            try
            {
                int projectID = Convert.ToInt32(Session["PProject"]);
                String fileName = Path.GetFileNameWithoutExtension(isN.ImgFile1.FileName);
                string extension1 = Path.GetExtension(isN.ImgFile1.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension1;
                isN.Front = "/img/" + fileName;
                string fileName2 = Path.Combine(Server.MapPath("~/img/"), fileName);
                isN.ImgFile1.SaveAs(fileName2);

                //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
                SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
                sqlconn.Open();
                string sqlquery2 = "INSERT INTO accessFile(accessFileName,projectID)  VALUES(@filename,@pid)";
                SqlCommand sqlcomm2 = new SqlCommand(sqlquery2, sqlconn);
                sqlcomm2.Parameters.AddWithValue("@filename", fileName);
                sqlcomm2.Parameters.AddWithValue("@pid", projectID);
                sqlcomm2.ExecuteNonQuery();

                //return RedirectToAction("ShowAccess", "Home", new { fileName1 = isN.Front });                

                return RedirectToAction("ShowAccess", "Home", new { fileName1 = fileName2, file1 = fileName });
            }
            catch
            {
                ViewBag.Message = "File upload failed!!";
                return View();
            }
            //return RedirectToAction("ShowAccess", "ImportAccess", new { fileName1 = isN.Front });

        }

        public ActionResult ShowAccess(string fileName1, string file1)
        {
            //string file = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName1;
            string file = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName1;
            List<Element_Forces___Frames> list1 = new List<Element_Forces___Frames>();
            List<Frame_Section_Assignments> list2 = new List<Frame_Section_Assignments>();

            List<Connectivity___Frame> list3 = new List<Connectivity___Frame>();
            string connectionString = file;

            string strSQL1 = " SELECT [Element Forces - Frames].* FROM [Element Forces - Frames] INNER JOIN [Frame Section Assignments] ON [Element Forces - Frames].[Frame] = [Frame Section Assignments].[Frame]  Where [Frame Section Assignments].[AnalSect] LIKE 'B %' ";
            string strSQL2 = " SELECT * FROM [Frame Section Assignments]  Where [AnalSect] LIKE 'B %' ";
            string strSQL3 = " SELECT [Connectivity - Frame].* FROM [Connectivity - Frame] INNER JOIN [Frame Section Assignments] ON [Connectivity - Frame].Frame = [Frame Section Assignments].Frame  Where [Frame Section Assignments].[AnalSect] LIKE 'B %' ";
            OleDbConnection connection = new OleDbConnection(connectionString);
            connection.Open();
            OleDbCommand command = new OleDbCommand(strSQL1, connection);
            OleDbCommand command2 = new OleDbCommand(strSQL2, connection);
            OleDbCommand command3 = new OleDbCommand(strSQL3, connection);
            OleDbDataReader reader = command.ExecuteReader();
            OleDbDataReader reader2 = command2.ExecuteReader();
            OleDbDataReader reader3 = command3.ExecuteReader();
            while (reader.Read())
            {
                list1.Add(new Element_Forces___Frames
                {
                    Frame = Convert.ToInt32(reader["Frame"]),
                    Station = float.Parse(reader["Station"].ToString()),
                    OutputCase = reader["OutputCase"].ToString(),
                    CaseType = reader["CaseType"].ToString(),
                    P = float.Parse(reader["P"].ToString()),
                    V2 = float.Parse(reader["V2"].ToString()),
                    V3 = float.Parse(reader["V3"].ToString()),
                    T = float.Parse(reader["T"].ToString()),
                    M2 = float.Parse(reader["M2"].ToString()),
                    M3 = float.Parse(reader["M3"].ToString())
                });
            }
            TempData["myData1"] = list1;
            while (reader2.Read())
            {
                list2.Add(new Frame_Section_Assignments
                {
                    Frame = Convert.ToInt32(reader2["Frame"]),
                    SectionType = reader2["SectionType"].ToString(),
                    AutoSelect = reader2["AutoSelect"].ToString(),
                    AnalSect = reader2["AnalSect"].ToString(),
                    DesignSect = reader2["DesignSect"].ToString(),
                    MatProp = reader2["MatProp"].ToString()
                });
            }
            TempData["myData2"] = list2;
            while (reader3.Read())
            {
                list3.Add(new Connectivity___Frame
                {
                    Frame = Convert.ToInt32(reader3["Frame"]),
                    JointI = float.Parse(reader3["JointI"].ToString()),
                    JointJ = float.Parse(reader3["JointJ"].ToString()),
                    IsCurved = reader3["IsCurved"].ToString(),
                    Length = float.Parse(reader3["Length"].ToString()),
                    CentroidX = float.Parse(reader3["CentroidX"].ToString()),
                    CentroidY = float.Parse(reader3["CentroidY"].ToString()),
                    CentroidZ = float.Parse(reader3["CentroidZ"].ToString()),
                    GUID = reader3["GUID"].ToString()
                });
            }
            TempData["myData3"] = list3;
            connection.Close();
            //return View();
            return RedirectToAction("seAid", "Home", new { fileName = file1 });
        }
        public ActionResult seAid(string fileName)
        {
            string a = " accessFileName LIKE '%" + fileName + "%' ";
            List<accessFile> accessFiles1 = new List<accessFile>();
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            string q1 = "SELECT accessFileID  FROM accessFile where " + a;
            SqlCommand sqlc1 = new SqlCommand(q1, sqlconn);
            SqlDataReader rrr = sqlc1.ExecuteReader();
            while (rrr.Read())
            {
                accessFiles1.Add(new accessFile
                {
                    accessFileID = Convert.ToInt32(rrr["accessFileID"])
                });
            }
            TempData["aid"] = accessFiles1;
            return RedirectToAction("InsertAccess", "Home");
        }
        public ActionResult InsertAccess()
        {
            int pid = Convert.ToInt32(Session["PProject"]);
            List<accessFile> a1 = (List<accessFile>)TempData["aid"];
            List<Element_Forces___Frames> data = (List<Element_Forces___Frames>)TempData["myData1"];
            List<Frame_Section_Assignments> data2 = (List<Frame_Section_Assignments>)TempData["myData2"];
            List<Connectivity___Frame> data3 = (List<Connectivity___Frame>)TempData["myData3"];
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            foreach (var n in a1)
            {

                int aid = n.accessFileID;

                foreach (var details in data)
                {
                    int Frame = details.Frame;
                    float Station = details.Station;
                    string OutputCase = details.OutputCase;
                    string CaseType = details.CaseType;
                    float P = details.P;
                    float V2 = details.V2;
                    float V3 = details.V3;
                    float T = details.T;
                    float M2 = details.M2;
                    float M3 = details.M3;
                    string sqlquery = "insert into ElementForcesFrames(projectID,accessFileID,Frame,Station,OutputCase,CaseType,P,V2,V3,T,M2,M3) values(@pid,@aid,@Frame,@Station,@OutputCase,@CaseType,@P,@V2,@V3,@T,@M2,@M3)";
                    SqlCommand sqlcomm = new SqlCommand(sqlquery, sqlconn);
                    sqlcomm.Parameters.AddWithValue("@pid", pid);
                    sqlcomm.Parameters.AddWithValue("@aid", aid);
                    sqlcomm.Parameters.AddWithValue("@Frame", Frame);
                    sqlcomm.Parameters.AddWithValue("@Station", Station);
                    sqlcomm.Parameters.AddWithValue("@OutputCase", OutputCase);
                    sqlcomm.Parameters.AddWithValue("@CaseType", CaseType);
                    sqlcomm.Parameters.AddWithValue("@P", P);
                    sqlcomm.Parameters.AddWithValue("@V2", V2);
                    sqlcomm.Parameters.AddWithValue("@V3", V3);
                    sqlcomm.Parameters.AddWithValue("@T", T);
                    sqlcomm.Parameters.AddWithValue("@M2", M2);
                    sqlcomm.Parameters.AddWithValue("@M3", M3);
                    sqlcomm.ExecuteNonQuery();

                }
                foreach (var details2 in data2)
                {

                    int Frame = details2.Frame;
                    string SectionType = details2.SectionType;
                    string AutoSelect = details2.AutoSelect;
                    string AnalSect = details2.AnalSect;
                    string DesignSect = details2.DesignSect;
                    string MatProp = details2.MatProp;
                    string sqlquery = "insert into FrameSectionAssignments(projectID,accessFileID,Frame,SectionType,AutoSelect,AnalSect,DesignSect,MatProp) values(@pid,@aid,@Frame,@SectionType,@AutoSelect,@AnalSect,@DesignSect,@MatProp)";
                    SqlCommand sqlcomm = new SqlCommand(sqlquery, sqlconn);
                    sqlcomm.Parameters.AddWithValue("@pid", pid);
                    sqlcomm.Parameters.AddWithValue("@aid", aid);
                    sqlcomm.Parameters.AddWithValue("@Frame", Frame);
                    sqlcomm.Parameters.AddWithValue("@SectionType", SectionType);
                    sqlcomm.Parameters.AddWithValue("@AutoSelect", AutoSelect);
                    sqlcomm.Parameters.AddWithValue("@AnalSect", AnalSect);
                    sqlcomm.Parameters.AddWithValue("@DesignSect", DesignSect);
                    sqlcomm.Parameters.AddWithValue("@MatProp", MatProp);
                    sqlcomm.ExecuteNonQuery();

                }
                foreach (var details3 in data3)
                {

                    int Frame = details3.Frame;
                    float JointI = details3.JointI;
                    float JointJ = details3.JointJ;
                    string IsCurved = details3.IsCurved;
                    float Length = details3.Length;
                    float CentroidX = details3.CentroidX;
                    float CentroidY = details3.CentroidY;
                    float CentroidZ = details3.CentroidZ;
                    string GUID = details3.GUID;
                    string sqlquery = "insert into ConnectivityFrame(projectID,accessFileID,Frame,JointI,JointJ,IsCurved,Length,CentroidX,CentroidY,CentroidZ,GUID) values(@pid,@aid,@Frame,@JointI,@JointJ,@IsCurved,@Length,@CentroidX,@CentroidY,@CentroidZ,@GUID)";
                    SqlCommand sqlcomm = new SqlCommand(sqlquery, sqlconn);
                    sqlcomm.Parameters.AddWithValue("@pid", pid);
                    sqlcomm.Parameters.AddWithValue("@aid", aid);
                    sqlcomm.Parameters.AddWithValue("@Frame", Frame);
                    sqlcomm.Parameters.AddWithValue("@JointI", JointI);
                    sqlcomm.Parameters.AddWithValue("@JointJ", JointJ);
                    sqlcomm.Parameters.AddWithValue("@IsCurved", IsCurved);
                    sqlcomm.Parameters.AddWithValue("@Length", Length);
                    sqlcomm.Parameters.AddWithValue("@CentroidX", CentroidX);
                    sqlcomm.Parameters.AddWithValue("@CentroidY", CentroidY);
                    sqlcomm.Parameters.AddWithValue("@CentroidZ", CentroidZ);
                    sqlcomm.Parameters.AddWithValue("@GUID", GUID);
                    sqlcomm.ExecuteNonQuery();

                }

            }
            ViewBag.Message = "Insert Succesfully";
            return RedirectToAction("PFile", "Home", new { PID = Session["PProject"] });
        }
        public ActionResult subProject(int AID)
        {
            Session["aid"] = AID;
            return View();
        }

        public ActionResult RCBeamDesign()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RCBeamDesign(String submit)
        {

            int aid = Convert.ToInt32(Session["aid"]);
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            switch (submit)
            {
                case "datafile":
                    List<RC_Beam> RC_Beam = new List<RC_Beam>();
                    String q1 = "select Id from RCBeam where accessFileID = " + aid + " ORDER BY Mem ASC";
                    SqlCommand sqlc1 = new SqlCommand(q1, sqlconn);
                    SqlDataReader rrr = sqlc1.ExecuteReader();
                    while (rrr.Read())
                    {
                        RC_Beam.Add(new RC_Beam
                        {
                            ID = Convert.ToInt32(rrr["Id"])
                        });
                    }
                    rrr.Close();
                    TempData["Rid"] = RC_Beam;
                    List<RC_Beam> r1 = (List<RC_Beam>)TempData["Rid"];
                    if (RC_Beam.Count == 0)
                    {
                        ViewBag.Message = "Count0";
                        String query1 = "INSERT INTO RCBeam (projectID,accessFileID,Mem,Name,L,LT,B,T,Ct,Cb,CLT,CRT,CL,CR,CLH,CRH,AXL,FL,FR,TL,TR,N1,D1,N2,D2,L2L,L2R,N3,D3,L3L,L3R,N4,D4,L4L,L4R,N5,D5,L5L,L5R,LN1,LD1,LN2,LD2,LL2,LN3,LD3,LL3,LN4,LD4,LL4,LN5,LD5,LL5,RN2,RD2,RL2,RN3,RD3,RL3,RN4,RD4,RL4,RN5,RD5,RL5,N11,D11,RS2,Cot2,Cot3,Cot4,Cot5,Cob2,Cob3,Cob4,Cob5,Ds,Nso,Nsi,Nslo,Nsli,Nsro,Nsri,Stln,Stls,Sts,Strn,Strs,MEMLIST,M3X0,TX0,SX0,V2XV0,XV0,M3XM,TXM,SXM,M3X10,TX10,SX10,V2XV10,XV10) SELECT DISTINCT ElementForcesFrames.projectID,ElementForcesFrames.accessFileID,ElementForcesFrames.Frame,FrameSectionAssignments.AnalSect,ConnectivityFrame.Length,ConnectivityFrame.Length,SUBSTRING(FrameSectionAssignments.AnalSect, 3, 2),SUBSTRING(FrameSectionAssignments.AnalSect, 6, 2) AS ExtractString,0,0,'F','F','F','F','F','F','F',' ',' ',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,' ',' ',' ',' ',' ',' ',' ',' ',0,0,0,0,0,0,0,0,0,0,0,0,' ',0,0,0,0,0,0,0,0,0,0,0,0,0 FROM ElementForcesFrames INNER JOIN FrameSectionAssignments ON ElementForcesFrames.Frame = FrameSectionAssignments.Frame INNER JOIN ConnectivityFrame ON ElementForcesFrames.Frame = ConnectivityFrame.Frame where ElementForcesFrames.accessFileID = " + aid + " ORDER BY ElementForcesFrames.Frame ASC";

                        SqlCommand sqlcomm1 = new SqlCommand(query1, sqlconn);
                        sqlcomm1.ExecuteNonQuery();

                    }
                    else
                    {
                        ViewBag.Message = "Count!0";
                        foreach (var r in r1)
                        {
                            int rid = r.ID;
                            string query2 = "DELETE FROM RCBeam WHERE accessFileID = @aid";
                            SqlCommand sqlcomm2 = new SqlCommand(query2, sqlconn);
                            sqlcomm2.Parameters.AddWithValue("@aid", aid);
                            String query3 = "INSERT INTO RCBeam (projectID,accessFileID,Mem,Name,L,LT,B,T,Ct,Cb,CLT,CRT,CL,CR,CLH,CRH,AXL,FL,FR,TL,TR,N1,D1,N2,D2,L2L,L2R,N3,D3,L3L,L3R,N4,D4,L4L,L4R,N5,D5,L5L,L5R,LN1,LD1,LN2,LD2,LL2,LN3,LD3,LL3,LN4,LD4,LL4,LN5,LD5,LL5,RN2,RD2,RL2,RN3,RD3,RL3,RN4,RD4,RL4,RN5,RD5,RL5,N11,D11,RS2,Cot2,Cot3,Cot4,Cot5,Cob2,Cob3,Cob4,Cob5,Ds,Nso,Nsi,Nslo,Nsli,Nsro,Nsri,Stln,Stls,Sts,Strn,Strs,MEMLIST,M3X0,TX0,SX0,V2XV0,XV0,M3XM,TXM,SXM,M3X10,TX10,SX10,V2XV10,XV10) SELECT DISTINCT ElementForcesFrames.projectID,ElementForcesFrames.accessFileID,ElementForcesFrames.Frame,FrameSectionAssignments.AnalSect,ConnectivityFrame.Length,ConnectivityFrame.Length,SUBSTRING(FrameSectionAssignments.AnalSect, 3, 2),SUBSTRING(FrameSectionAssignments.AnalSect, 6, 2) AS ExtractString,0,0,'F','F','F','F','F','F','F',' ',' ',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,' ',' ',' ',' ',' ',' ',' ',' ',0,0,0,0,0,0,0,0,0,0,0,0,' ',0,0,0,0,0,0,0,0,0,0,0,0,0 FROM ElementForcesFrames INNER JOIN FrameSectionAssignments ON ElementForcesFrames.Frame = FrameSectionAssignments.Frame INNER JOIN ConnectivityFrame ON ElementForcesFrames.Frame = ConnectivityFrame.Frame where ElementForcesFrames.accessFileID = " + aid + " ORDER BY ElementForcesFrames.Frame ASC";
                            SqlCommand sqlcomm3 = new SqlCommand(query3, sqlconn);
                            sqlcomm2.ExecuteNonQuery();
                            sqlcomm3.ExecuteNonQuery();

                        }

                    }

                    TempData["buttonval"] = "datafile";
                    break;
                case "RCBeam":
                    return RedirectToAction("RCBeam", "Home");
                default:
                    TempData["buttonval"] = "ไม่ได้";
                    break;
            }
            sqlconn.Close();

            return RedirectToAction("RCBeamDesign", "Home");
        }
        public ActionResult RCBeam()
        {
            int aid = Convert.ToInt32(Session["aid"]);
            List<RC_Beam> list1 = new List<RC_Beam>();
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            String query2 = "SELECT * FROM RCBeam where accessFileID = " + aid + " ORDER BY Mem ASC";
            SqlCommand sqlcomm2 = new SqlCommand(query2, sqlconn);
            SqlDataReader rd = sqlcomm2.ExecuteReader();
            while (rd.Read())
            {
                if (DBNull.Value.Equals(rd["Ct"]))
                {
                    list1.Add(new RC_Beam
                    {
                        Mem = Convert.ToInt32(rd["Mem"]),
                        Mark = rd["Mark"].ToString(),
                        B = rd["B"].ToString(),
                        T = rd["T"].ToString(),
                        L = float.Parse(rd["L"].ToString()),
                        LT = float.Parse(rd["LT"].ToString()),
                        Name = rd["Name"].ToString(),

                    });

                }
                else
                {
                    list1.Add(new RC_Beam
                    {
                        Mem = Convert.ToInt32(rd["Mem"]),
                        Mark = rd["Mark"].ToString(),
                        B = rd["B"].ToString(),
                        T = rd["T"].ToString(),
                        L = float.Parse(rd["L"].ToString()),
                        LT = float.Parse(rd["LT"].ToString()),
                        Name = rd["Name"].ToString(),
                        Ct = Convert.ToInt32(rd["Ct"]),
                        Cb = Convert.ToInt32(rd["Cb"]),
                        CLT = Convert.ToChar(rd["CLT"]),
                        CRT = Convert.ToChar(rd["CRT"]),
                        CL = Convert.ToChar(rd["CL"]),
                        CR = Convert.ToChar(rd["CR"]),
                        CLH = Convert.ToChar(rd["CLH"]),
                        CRH = Convert.ToChar(rd["CRH"]),
                        AXL = Convert.ToChar(rd["AXL"]),
                        FL = Convert.ToChar(rd["FL"]),
                        FR = Convert.ToChar(rd["FR"]),
                        TL = Convert.ToInt32(rd["TL"]),
                        TR = Convert.ToInt32(rd["TR"]),
                        N1 = Convert.ToInt32(rd["N1"]),
                        D1 = Convert.ToInt32(rd["D1"]),
                        N2 = Convert.ToInt32(rd["N2"]),
                        D2 = Convert.ToInt32(rd["D2"]),
                        L2L = Convert.ToInt32(rd["L2L"]),
                        L2R = Convert.ToInt32(rd["L2R"]),
                        N3 = Convert.ToInt32(rd["N3"]),
                        D3 = Convert.ToInt32(rd["D3"]),
                        L3L = Convert.ToInt32(rd["L3L"]),
                        L3R = Convert.ToInt32(rd["L3R"]),
                        N4 = Convert.ToInt32(rd["N4"]),
                        D4 = Convert.ToInt32(rd["D4"]),
                        L4L = Convert.ToInt32(rd["L4L"]),
                        L4R = Convert.ToInt32(rd["L4R"]),
                        N5 = Convert.ToInt32(rd["N5"]),
                        D5 = Convert.ToInt32(rd["D5"]),
                        L5L = Convert.ToInt32(rd["L5L"]),
                        L5R = Convert.ToInt32(rd["L5R"]),
                        LN1 = Convert.ToInt32(rd["LN1"]),
                        LD1 = Convert.ToInt32(rd["LD1"]),
                        LN2 = Convert.ToInt32(rd["LN2"]),
                        LD2 = Convert.ToInt32(rd["LD2"]),
                        LL2 = Convert.ToInt32(rd["LL2"]),
                        LN3 = Convert.ToInt32(rd["LN3"]),
                        LD3 = Convert.ToInt32(rd["LD3"]),
                        LL3 = Convert.ToInt32(rd["LL3"]),
                        LN4 = Convert.ToInt32(rd["LN4"]),
                        LD4 = Convert.ToInt32(rd["LD4"]),
                        LL4 = Convert.ToInt32(rd["LL4"]),
                        LN5 = Convert.ToInt32(rd["LN5"]),
                        LD5 = Convert.ToInt32(rd["LD5"]),
                        LL5 = Convert.ToInt32(rd["LL5"]),
                        RN2 = Convert.ToInt32(rd["RN2"]),
                        RD2 = Convert.ToInt32(rd["RD2"]),
                        RL2 = Convert.ToInt32(rd["RL2"]),
                        RN3 = Convert.ToInt32(rd["RN3"]),
                        RD3 = Convert.ToInt32(rd["RD3"]),
                        RL3 = Convert.ToInt32(rd["RL3"]),
                        RN4 = Convert.ToInt32(rd["RN4"]),
                        RD4 = Convert.ToInt32(rd["RD4"]),
                        RL4 = Convert.ToInt32(rd["RL4"]),
                        RN5 = Convert.ToInt32(rd["RN5"]),
                        RD5 = Convert.ToInt32(rd["RD5"]),
                        RL5 = Convert.ToInt32(rd["RL5"]),
                        N11 = Convert.ToInt32(rd["N11"]),
                        D11 = Convert.ToInt32(rd["D11"]),
                        RS2 = Convert.ToInt32(rd["RS2"]),
                        Cot2 = Convert.ToChar(rd["Cot2"]),
                        Cot3 = Convert.ToChar(rd["Cot3"]),
                        Cot4 = Convert.ToChar(rd["Cot4"]),
                        Cot5 = Convert.ToChar(rd["Cot5"]),
                        Cob2 = Convert.ToChar(rd["Cob2"]),
                        Cob3 = Convert.ToChar(rd["Cob3"]),
                        Cob4 = Convert.ToChar(rd["Cob4"]),
                        Cob5 = Convert.ToChar(rd["Cob5"]),
                        Ds = Convert.ToInt32(rd["Ds"]),
                        Nso = Convert.ToInt32(rd["Nso"]),
                        Nsi = Convert.ToInt32(rd["Nsi"]),
                        Nslo = Convert.ToInt32(rd["Nslo"]),
                        Nsli = Convert.ToInt32(rd["Nsli"]),
                        Nsro = Convert.ToInt32(rd["Nsro"]),
                        Nsri = Convert.ToInt32(rd["Nsri"]),
                        Stln = Convert.ToInt32(rd["Stln"]),
                        Stls = Convert.ToInt32(rd["Stls"]),
                        Sts = Convert.ToInt32(rd["Sts"]),
                        Strn = Convert.ToInt32(rd["Strn"]),
                        Strs = Convert.ToInt32(rd["Strs"]),
                        MEMLIST = rd["MEMLIST"].ToString(),
                        SX0 = rd["SX0"] != DBNull.Value ? float.Parse(rd["SX0"].ToString()) : 0,
                        BX0 = rd["BX0"] != DBNull.Value ? float.Parse(rd["BX0"].ToString()) : 0,
                        AX0 = rd["AX0"] != DBNull.Value ? float.Parse(rd["AX0"].ToString()) : 0,
                        CaseX0 = rd["CaseX0"] != DBNull.Value ? rd["CaseX0"].ToString() : "",
                        M3X0 = rd["M3X0"] != DBNull.Value ? float.Parse(rd["M3X0"].ToString()) : 0,
                        V2X0 = rd["V2X0"] != DBNull.Value ? float.Parse(rd["V2X0"].ToString()) : 0,
                        TX0 = rd["TX0"] != DBNull.Value ? float.Parse(rd["TX0"].ToString()) : 0,
                        PX0 = rd["PX0"] != DBNull.Value ? float.Parse(rd["PX0"].ToString()) : 0,
                        SX1 = rd["SX1"] != DBNull.Value ? float.Parse(rd["SX1"].ToString()) : 0,
                        StaX1 = rd["StaX1"] != DBNull.Value ? rd["StaX1"].ToString() : "",
                        BX1 = rd["BX1"] != DBNull.Value ? float.Parse(rd["BX1"].ToString()) : 0,
                        AX1 = rd["AX1"] != DBNull.Value ? float.Parse(rd["AX1"].ToString()) : 0,
                        CaseX1 = rd["CaseX1"] != DBNull.Value ? rd["CaseX1"].ToString() : "",
                        M3X1 = rd["M3X1"] != DBNull.Value ? float.Parse(rd["M3X1"].ToString()) : 0,
                        V2X1 = rd["V2X1"] != DBNull.Value ? float.Parse(rd["V2X1"].ToString()) : 0,
                        TX1 = rd["TX1"] != DBNull.Value ? float.Parse(rd["TX1"].ToString()) : 0,
                        PX1 = rd["PX1"] != DBNull.Value ? float.Parse(rd["PX1"].ToString()) : 0,
                        SX2 = rd["SX2"] != DBNull.Value ? float.Parse(rd["SX2"].ToString()) : 0,
                        StaX2 = rd["StaX2"] != DBNull.Value ? rd["StaX2"].ToString() : "",
                        BX2 = rd["BX2"] != DBNull.Value ? float.Parse(rd["BX2"].ToString()) : 0,
                        AX2 = rd["AX2"] != DBNull.Value ? float.Parse(rd["AX2"].ToString()) : 0,
                        CaseX2 = rd["CaseX2"] != DBNull.Value ? rd["CaseX2"].ToString() : "",
                        M3X2 = rd["M3X2"] != DBNull.Value ? float.Parse(rd["M3X2"].ToString()) : 0,
                        V2X2 = rd["V2X2"] != DBNull.Value ? float.Parse(rd["V2X2"].ToString()) : 0,
                        TX2 = rd["TX2"] != DBNull.Value ? float.Parse(rd["TX2"].ToString()) : 0,
                        PX2 = rd["PX2"] != DBNull.Value ? float.Parse(rd["PX2"].ToString()) : 0,
                        SXM = rd["SXM"] != DBNull.Value ? float.Parse(rd["SXM"].ToString()) : 0,
                        BXM = rd["BXM"] != DBNull.Value ? float.Parse(rd["BXM"].ToString()) : 0,
                        AXM = rd["AXM"] != DBNull.Value ? float.Parse(rd["AXM"].ToString()) : 0,
                        CaseXM = rd["CaseXM"] != DBNull.Value ? rd["CaseXM"].ToString() : "",
                        M3XM = rd["M3XM"] != DBNull.Value ? float.Parse(rd["M3XM"].ToString()) : 0,
                        V2XM = rd["V2XM"] != DBNull.Value ? float.Parse(rd["V2XM"].ToString()) : 0,
                        TXM = rd["TXM"] != DBNull.Value ? float.Parse(rd["TXM"].ToString()) : 0,
                        PXM = rd["PXM"] != DBNull.Value ? float.Parse(rd["PXM"].ToString()) : 0,
                        SX8 = rd["SX8"] != DBNull.Value ? float.Parse(rd["SX8"].ToString()) : 0,
                        StaX8 = rd["StaX8"] != DBNull.Value ? rd["StaX8"].ToString() : "",
                        BX8 = rd["BX8"] != DBNull.Value ? float.Parse(rd["BX8"].ToString()) : 0,
                        AX8 = rd["AX8"] != DBNull.Value ? float.Parse(rd["AX8"].ToString()) : 0,
                        CaseX8 = rd["CaseX8"] != DBNull.Value ? rd["CaseX8"].ToString() : "",
                        M3X8 = rd["M3X8"] != DBNull.Value ? float.Parse(rd["M3X8"].ToString()) : 0,
                        V2X8 = rd["V2X8"] != DBNull.Value ? float.Parse(rd["V2X8"].ToString()) : 0,
                        TX8 = rd["TX8"] != DBNull.Value ? float.Parse(rd["TX8"].ToString()) : 0,
                        PX8 = rd["PX8"] != DBNull.Value ? float.Parse(rd["PX8"].ToString()) : 0,
                        SX9 = rd["SX9"] != DBNull.Value ? float.Parse(rd["SX9"].ToString()) : 0,
                        StaX9 = rd["StaX9"] != DBNull.Value ? rd["StaX9"].ToString() : "",
                        BX9 = rd["BX9"] != DBNull.Value ? float.Parse(rd["BX9"].ToString()) : 0,
                        AX9 = rd["AX9"] != DBNull.Value ? float.Parse(rd["AX9"].ToString()) : 0,
                        CaseX9 = rd["CaseX9"] != DBNull.Value ? rd["CaseX9"].ToString() : "",
                        M3X9 = rd["M3X9"] != DBNull.Value ? float.Parse(rd["M3X9"].ToString()) : 0,
                        V2X9 = rd["V2X9"] != DBNull.Value ? float.Parse(rd["V2X9"].ToString()) : 0,
                        TX9 = rd["TX9"] != DBNull.Value ? float.Parse(rd["TX9"].ToString()) : 0,
                        PX9 = rd["PX9"] != DBNull.Value ? float.Parse(rd["PX9"].ToString()) : 0,
                        SX10 = rd["SX10"] != DBNull.Value ? float.Parse(rd["SX10"].ToString()) : 0,
                        BX10 = rd["BX10"] != DBNull.Value ? float.Parse(rd["BX10"].ToString()) : 0,
                        AX10 = rd["AX10"] != DBNull.Value ? float.Parse(rd["AX10"].ToString()) : 0,
                        CaseX10 = rd["CaseX10"] != DBNull.Value ? rd["CaseX10"].ToString() : "",
                        M3X10 = rd["M3X10"] != DBNull.Value ? float.Parse(rd["M3X10"].ToString()) : 0,
                        V2X10 = rd["V2X10"] != DBNull.Value ? float.Parse(rd["V2X10"].ToString()) : 0,
                        TX10 = rd["TX10"] != DBNull.Value ? float.Parse(rd["TX10"].ToString()) : 0,
                        PX10 = rd["PX10"] != DBNull.Value ? float.Parse(rd["PX10"].ToString()) : 0,
                        XV0 = rd["XV0"] != DBNull.Value ? float.Parse(rd["XV0"].ToString()) : 0,
                        XT0 = rd["XT0"] != DBNull.Value ? float.Parse(rd["XT0"].ToString()) : 0,
                        XVc0 = rd["XVc0"] != DBNull.Value ? float.Parse(rd["XVc0"].ToString()) : 0,
                        CaseXV0 = rd["CaseXV0"] != DBNull.Value ? rd["CaseXV0"].ToString() : "",
                        M3XV0 = rd["M3XV0"] != DBNull.Value ? float.Parse(rd["M3XV0"].ToString()) : 0,
                        V2XV0 = rd["V2XV0"] != DBNull.Value ? float.Parse(rd["V2XV0"].ToString()) : 0,
                        TXV0 = rd["TXV0"] != DBNull.Value ? float.Parse(rd["TXV0"].ToString()) : 0,
                        PXV0 = rd["PXV0"] != DBNull.Value ? float.Parse(rd["PXV0"].ToString()) : 0,
                        XV1 = rd["XV1"] != DBNull.Value ? float.Parse(rd["XV1"].ToString()) : 0,
                        XT1 = rd["XT1"] != DBNull.Value ? float.Parse(rd["XT1"].ToString()) : 0,
                        XVc1 = rd["XVc1"] != DBNull.Value ? float.Parse(rd["XVc1  "].ToString()) : 0,
                        CaseXV1 = rd["CaseXV1"] != DBNull.Value ? rd["CaseXV1"].ToString() : "",
                        M3XV1 = rd["M3XV1"] != DBNull.Value ? float.Parse(rd["M3XV1"].ToString()) : 0,
                        V2XV1 = rd["V2XV1"] != DBNull.Value ? float.Parse(rd["V2XV1"].ToString()) : 0,
                        TXV1 = rd["TXV1"] != DBNull.Value ? float.Parse(rd["TXV1"].ToString()) : 0,
                        PXV1 = rd["PXV1"] != DBNull.Value ? float.Parse(rd["PXV1"].ToString()) : 0,
                        XV9 = rd["XV9"] != DBNull.Value ? float.Parse(rd["XV9"].ToString()) : 0,
                        XT9 = rd["XT9"] != DBNull.Value ? float.Parse(rd["XT9"].ToString()) : 0,
                        XVc9 = rd["XVc9"] != DBNull.Value ? float.Parse(rd["XVc9"].ToString()) : 0,
                        CaseXV9 = rd["CaseXV9"] != DBNull.Value ? rd["CaseXV9"].ToString() : "",
                        M3XV9 = rd["M3XV9"] != DBNull.Value ? float.Parse(rd["M3XV9"].ToString()) : 0,
                        V2XV9 = rd["V2XV9"] != DBNull.Value ? float.Parse(rd["V2XV9"].ToString()) : 0,
                        TXV9 = rd["TXV9"] != DBNull.Value ? float.Parse(rd["TXV9"].ToString()) : 0,
                        PXV9 = rd["PXV9"] != DBNull.Value ? float.Parse(rd["PXV9"].ToString()) : 0,
                        XV10 = rd["XV10"] != DBNull.Value ? float.Parse(rd["XV10"].ToString()) : 0,
                        XT10 = rd["XT10"] != DBNull.Value ? float.Parse(rd["XT10"].ToString()) : 0,
                        XVc10 = rd["XVc10"] != DBNull.Value ? float.Parse(rd["XVc10"].ToString()) : 0,
                        CaseXV10 = rd["CaseXV10"] != DBNull.Value ? rd["CaseXV10"].ToString() : "",
                        M3XV10 = rd["M3XV10"] != DBNull.Value ? float.Parse(rd["M3XV10"].ToString()) : 0,
                        V2XV10 = rd["V2XV10"] != DBNull.Value ? float.Parse(rd["V2XV10"].ToString()) : 0,
                        TXV10 = rd["TXV10"] != DBNull.Value ? float.Parse(rd["TXV10"].ToString()) : 0,
                        PXV10 = rd["PXV10"] != DBNull.Value ? float.Parse(rd["PXV10"].ToString()) : 0,
                        SXMax = rd["SXMax"] != DBNull.Value ? float.Parse(rd["SXMax"].ToString()) : 0,
                        BXMax = rd["BXMax"] != DBNull.Value ? float.Parse(rd["BXMax "].ToString()) : 0,
                        AXMax = rd["AXMax"] != DBNull.Value ? float.Parse(rd["AXMax"].ToString()) : 0,
                        SMaxSta = rd["SMaxSta"] != DBNull.Value ? float.Parse(rd["SMaxSta"].ToString()) : 0,
                        M3XMax = rd["M3XMax"] != DBNull.Value ? float.Parse(rd["M3XMax"].ToString()) : 0,
                        V2XMax = rd["V2XMax"] != DBNull.Value ? float.Parse(rd["V2XMax"].ToString()) : 0,
                        TXMax = rd["TXMax"] != DBNull.Value ? float.Parse(rd["TXMax"].ToString()) : 0,
                        PXMax = rd["PXMax"] != DBNull.Value ? float.Parse(rd["PXMax"].ToString()) : 0,
                        XVMax = rd["XVMax"] != DBNull.Value ? float.Parse(rd["XVMax"].ToString()) : 0,
                        XTMax = rd["XTMax"] != DBNull.Value ? float.Parse(rd["XTMax"].ToString()) : 0,
                        XVcMax = rd["XVcMax"] != DBNull.Value ? float.Parse(rd["XVcMax"].ToString()) : 0,
                        VMaxSta = rd["VMaxSta"] != DBNull.Value ? float.Parse(rd["VMaxSta"].ToString()) : 0,
                        M3XVMax = rd["M3XVMax"] != DBNull.Value ? float.Parse(rd["M3XVMax"].ToString()) : 0,
                        V2XVMax = rd["V2XVMax"] != DBNull.Value ? float.Parse(rd["V2XVMax"].ToString()) : 0,
                        TXVMax = rd["TXVMax"] != DBNull.Value ? float.Parse(rd["TXVMax"].ToString()) : 0,
                        PXVMax = rd["PXVMax"] != DBNull.Value ? float.Parse(rd["PXVMax"].ToString()) : 0,
                        BL = rd["BL"] != DBNull.Value ? Convert.ToChar(rd["BL"]) : ' ',
                        BR = rd["BR"] != DBNull.Value ? Convert.ToChar(rd["BR"]) : ' ',
                        AsMin = rd["AsMin"] != DBNull.Value ? Convert.ToChar(rd["AsMin"]) : ' ',
                    });
                }

            }
            rd.Close();
            ViewBag.rc = list1;
            ViewBag.Message = "RC";

            List<Strength> str = new List<Strength>();
            String q5 = "select * from Strength where accessFileID = " + aid;
            SqlCommand sqlc5 = new SqlCommand(q5, sqlconn);
            SqlDataReader cf = sqlc5.ExecuteReader();
            while (cf.Read())
            {
                str.Add(new Strength
                {
                    utf = decimal.Parse(cf["utf"].ToString()),
                });
                ViewBag.st = str;
            }
            sqlconn.Close();

            return View();
        }
        public ActionResult ElementForcesFrames()
        {
            int aid = Convert.ToInt32(Session["aid"]);
            List<Element_Forces___Frames> EFF = new List<Element_Forces___Frames>();

            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();

            String q3 = "select TOP 500 * from ElementForcesFrames where accessFileID = " + aid;

            SqlCommand sqlc3 = new SqlCommand(q3, sqlconn);

            SqlDataReader ef = sqlc3.ExecuteReader();
            while (ef.Read())
            {
                EFF.Add(new Element_Forces___Frames
                {
                    Frame = Convert.ToInt32(ef["Frame"]),
                    Station = Convert.ToInt32(ef["Station"]),
                    OutputCase = ef["OutputCase"].ToString(),
                    CaseType = ef["CaseType"].ToString(),
                    P = float.Parse(ef["P"].ToString()),
                    V2 = float.Parse(ef["V2"].ToString()),
                    V3 = float.Parse(ef["V3"].ToString()),
                    T = float.Parse(ef["T"].ToString()),
                    M2 = float.Parse(ef["M2"].ToString()),
                    M3 = float.Parse(ef["M3"].ToString())

                });
                ViewBag.EFF = EFF;
            }
            sqlconn.Close();
            return View();
        }
        public ActionResult FrameSectionAssignments()
        {
            int aid = Convert.ToInt32(Session["aid"]);
            List<Frame_Section_Assignments> FSA = new List<Frame_Section_Assignments>();
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();

            String q4 = "select * from FrameSectionAssignments where accessFileID = " + aid;
            SqlCommand sqlc4 = new SqlCommand(q4, sqlconn);
            SqlDataReader fsa = sqlc4.ExecuteReader();
            while (fsa.Read())
            {
                FSA.Add(new Frame_Section_Assignments
                {
                    Frame = Convert.ToInt32(fsa["Frame"]),
                    SectionType = fsa["SectionType"].ToString(),
                    AutoSelect = fsa["AutoSelect"].ToString(),
                    AnalSect = fsa["AnalSect"].ToString(),
                    DesignSect = fsa["DesignSect"].ToString(),
                    MatProp = fsa["MatProp"].ToString()

                });
                ViewBag.FSA = FSA;
            }
            sqlconn.Close();
            return View();
        }
        public ActionResult ConnectivityFrame()
        {
            int aid = Convert.ToInt32(Session["aid"]);
            List<Connectivity___Frame> CF = new List<Connectivity___Frame>();
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            String q5 = "select * from ConnectivityFrame where accessFileID = " + aid;
            SqlCommand sqlc5 = new SqlCommand(q5, sqlconn);
            SqlDataReader cf = sqlc5.ExecuteReader();
            while (cf.Read())
            {
                CF.Add(new Connectivity___Frame
                {
                    Frame = Convert.ToInt32(cf["Frame"]),
                    JointI = float.Parse(cf["JointI"].ToString()),
                    JointJ = float.Parse(cf["JointJ"].ToString()),
                    IsCurved = cf["IsCurved"].ToString(),
                    Length = float.Parse(cf["Length"].ToString()),
                    CentroidX = float.Parse(cf["CentroidX"].ToString()),
                    CentroidY = float.Parse(cf["CentroidY"].ToString()),
                    CentroidZ = float.Parse(cf["CentroidZ"].ToString()),
                    GUID = cf["GUID"].ToString()
                });
                ViewBag.CF = CF;
            }
            sqlconn.Close();
            return View();
        }
        public ActionResult Stress()
        {
            int aid = Convert.ToInt32(Session["aid"]);
            List<Stress> newStress = new List<Stress>();

            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();

            String stressQuery = "select * from Stress where accessFileID = " + aid + " Order By [Case] , [Station]";

            SqlCommand sqlcStress = new SqlCommand(stressQuery, sqlconn);

            SqlDataReader cf = sqlcStress.ExecuteReader();
            while (cf.Read())
            {
                newStress.Add(new Stress
                {
                    Id = Convert.ToInt32(cf["id"].ToString()),
                    MEM = cf["MEM"] != DBNull.Value ? Convert.ToInt32(cf["MEM"].ToString()) : 0,
                    Station = cf["Station"] != DBNull.Value ? float.Parse(cf["Station"].ToString()) : 0,
                    Secc = cf["Secc"] != DBNull.Value ? cf["Secc"].ToString() : "",
                    Case = cf["Case"] != DBNull.Value ? cf["Case"].ToString() : "",
                    M3 = cf["M3"] != DBNull.Value ? float.Parse(cf["M3"].ToString()) : 0,
                    V2 = cf["V2"] != DBNull.Value ? float.Parse(cf["V2"].ToString()) : 0,
                    T = cf["T"] != DBNull.Value ? float.Parse(cf["T"].ToString()) : 0,
                    P = cf["P"] != DBNull.Value ? float.Parse(cf["P"].ToString()) : 0,
                    N1c = cf["N1c"] != DBNull.Value ? Convert.ToInt32(cf["N1c"].ToString()) : 0,
                    D1c = cf["D1c"] != DBNull.Value ? Convert.ToInt32(cf["D1c"].ToString()) : 0,
                    Ld1c = cf["Ld1c"] != DBNull.Value ? float.Parse(cf["Ld1c"].ToString()) : 0,
                    N2c = cf["N2c"] != DBNull.Value ? Convert.ToInt32(cf["N2c"].ToString()) : 0,
                    D2c = cf["D2c"] != DBNull.Value ? Convert.ToInt32(cf["D2c"].ToString()) : 0,
                    Ld2c = cf["Ld2c"] != DBNull.Value ? float.Parse(cf["Ld2c"].ToString()) : 0,
                    N3c = cf["N3c"] != DBNull.Value ? Convert.ToInt32(cf["N3c"].ToString()) : 0,
                    D3c = cf["D3c"] != DBNull.Value ? Convert.ToInt32(cf["D3c"].ToString()) : 0,
                    Ld3c = cf["Ld3c"] != DBNull.Value ? float.Parse(cf["Ld3c"].ToString()) : 0,
                    N4c = cf["N4c"] != DBNull.Value ? Convert.ToInt32(cf["N4c"].ToString()) : 0,
                    D4c = cf["D4c"] != DBNull.Value ? Convert.ToInt32(cf["D4c"].ToString()) : 0,
                    Ld4c = cf["Ld4c"] != DBNull.Value ? float.Parse(cf["Ld4c"].ToString()) : 0,
                    N5c = cf["N5c"] != DBNull.Value ? Convert.ToInt32(cf["N5c"].ToString()) : 0,
                    D5c = cf["D5c"] != DBNull.Value ? Convert.ToInt32(cf["D5c"].ToString()) : 0,
                    Ld5c = cf["Ld5c"] != DBNull.Value ? float.Parse(cf["Ld5c"].ToString()) : 0,
                    N1t = cf["N1t"] != DBNull.Value ? Convert.ToInt32(cf["N1t"].ToString()) : 0,
                    D1t = cf["D1t"] != DBNull.Value ? Convert.ToInt32(cf["D1t"].ToString()) : 0,
                    Ld1t = cf["Ld1t"] != DBNull.Value ? float.Parse(cf["Ld1t"].ToString()) : 0,
                    N2t = cf["N2t"] != DBNull.Value ? Convert.ToInt32(cf["N2t"].ToString()) : 0,
                    D2t = cf["D2t"] != DBNull.Value ? Convert.ToInt32(cf["D2t"].ToString()) : 0,
                    Ld2t = cf["Ld2t"] != DBNull.Value ? float.Parse(cf["Ld2t"].ToString()) : 0,
                    N3t = cf["N3t"] != DBNull.Value ? Convert.ToInt32(cf["N3t"].ToString()) : 0,
                    D3t = cf["D3t"] != DBNull.Value ? Convert.ToInt32(cf["D3t"].ToString()) : 0,
                    Ld3t = cf["Ld3t"] != DBNull.Value ? float.Parse(cf["Ld3t"].ToString()) : 0,
                    N4t = cf["N4t"] != DBNull.Value ? Convert.ToInt32(cf["N4t"].ToString()) : 0,
                    D4t = cf["D4t"] != DBNull.Value ? Convert.ToInt32(cf["D4t"].ToString()) : 0,
                    Ld4t = cf["Ld4t"] != DBNull.Value ? float.Parse(cf["Ld4t"].ToString()) : 0,
                    N5t = cf["N5t"] != DBNull.Value ? Convert.ToInt32(cf["N5t"].ToString()) : 0,
                    D5t = cf["D5t"] != DBNull.Value ? Convert.ToInt32(cf["D5t"].ToString()) : 0,
                    Ld5t = cf["Ld5t"] != DBNull.Value ? float.Parse(cf["Ld5t"].ToString()) : 0,
                    N11 = cf["N11"] != DBNull.Value ? Convert.ToInt32(cf["N11"].ToString()) : 0,
                    D11 = cf["D11"] != DBNull.Value ? Convert.ToInt32(cf["D11"].ToString()) : 0,
                    Ld11 = cf["Ld11"] != DBNull.Value ? float.Parse(cf["Ld11"].ToString()) : 0,
                    Ds = cf["Ds"] != DBNull.Value ? Convert.ToInt32(cf["Ds"].ToString()) : 0,
                    Nsout = cf["Nsout"] != DBNull.Value ? Convert.ToInt32(cf["Nsout"].ToString()) : 0,
                    Nsin = cf["Nsin"] != DBNull.Value ? Convert.ToInt32(cf["Nsin"].ToString()) : 0,
                    S = cf["S"] != DBNull.Value ? Convert.ToInt32(cf["S"].ToString()) : 0,
                    Asmin = cf["Asmin"] != DBNull.Value ? Convert.ToChar(cf["Asmin"].ToString()) : ' ',
                    Strength = cf["Strength"] != DBNull.Value ? float.Parse(cf["Strength"].ToString()) : 0,
                    Bending = cf["Bending"] != DBNull.Value ? float.Parse(cf["Bending"].ToString()) : 0,
                    Axial = cf["Axial"] != DBNull.Value ? float.Parse(cf["Axial"].ToString()) : 0,
                    Torsion = cf["Torsion"] != DBNull.Value ? float.Parse(cf["Torsion"].ToString()) : 0,
                    Shear = cf["Shear"] != DBNull.Value ? float.Parse(cf["Shear"].ToString()) : 0,
                    Vconc = cf["Vconc"] != DBNull.Value ? cf["Vconc"].ToString() : "0",
                    Mm = cf["Mm"] != DBNull.Value ? Convert.ToChar(cf["Mm"].ToString()) : ' ',
                    BL = cf["BL"] != DBNull.Value ? Convert.ToChar(cf["BL"].ToString()) : ' ',
                    BR = cf["BR"] != DBNull.Value ? Convert.ToChar(cf["BR"].ToString()) : ' ',
                    Al = cf["Al"] != DBNull.Value ? float.Parse(cf["Al"].ToString()) : 0,
                    Avs = cf["Avs"] != DBNull.Value ? cf["Avs"].ToString() : "0",
                    Ats = cf["Ats"] != DBNull.Value ? float.Parse(cf["Ats"].ToString()) : 0,
                    AvtSoutprovide = cf["AvtSoutprovide"] != DBNull.Value ? float.Parse(cf["AvtSoutprovide"].ToString()) : 0,
                    AvSinprovide = cf["AvSinprovide"] != DBNull.Value ? float.Parse(cf["AvSinprovide"].ToString()) : 0,
                    AvSallprovide = cf["AvSallprovide"] != DBNull.Value ? float.Parse(cf["AvSallprovide"].ToString()) : 0,
                    d = cf["d"] != DBNull.Value ? float.Parse(cf["d"].ToString()) : 0,
                    FINALEACHEDGEBOT1 = cf["FINALEACHEDGEBOT1"] != DBNull.Value ? float.Parse(cf["FINALEACHEDGEBOT1"].ToString()) : 0,
                    FINALEACHEDGEBOT2 = cf["FINALEACHEDGEBOT2"] != DBNull.Value ? float.Parse(cf["FINALEACHEDGEBOT2"].ToString()) : 0,
                    FINALEACHEDGEBOT3 = cf["FINALEACHEDGEBOT3"] != DBNull.Value ? float.Parse(cf["FINALEACHEDGEBOT3"].ToString()) : 0,
                    FINALEACHEDGEBOT4 = cf["FINALEACHEDGEBOT4"] != DBNull.Value ? float.Parse(cf["FINALEACHEDGEBOT4"].ToString()) : 0,
                    FINALEACHEDGEBOT5 = cf["FINALEACHEDGEBOT5"] != DBNull.Value ? float.Parse(cf["FINALEACHEDGEBOT5"].ToString()) : 0,
                    FINALEACHEDGETOP1 = cf["FINALEACHEDGETOP1"] != DBNull.Value ? float.Parse(cf["FINALEACHEDGETOP1"].ToString()) : 0,
                    FINALEACHEDGETOP2 = cf["FINALEACHEDGETOP2"] != DBNull.Value ? float.Parse(cf["FINALEACHEDGETOP2"].ToString()) : 0,
                    FINALEACHEDGETOP3 = cf["FINALEACHEDGETOP3"] != DBNull.Value ? float.Parse(cf["FINALEACHEDGETOP3"].ToString()) : 0,
                    FINALEACHEDGETOP4 = cf["FINALEACHEDGETOP4"] != DBNull.Value ? float.Parse(cf["FINALEACHEDGETOP4"].ToString()) : 0,
                    FINALEACHEDGETOP5 = cf["FINALEACHEDGETOP5"] != DBNull.Value ? float.Parse(cf["FINALEACHEDGETOP5"].ToString()) : 0
                });
                ViewBag.Stress = newStress;
            }
            sqlconn.Close();
            return View();
        }
        public ActionResult ShowformDesign()
        {
            ViewBag.CLList = new List<char> { 'F', 'T' };
            int aid = Convert.ToInt32(Session["aid"]);
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            List<RC_Beam> listfirst = new List<RC_Beam>();
            String query6 = "select * from RCBeam where accessFileID = " + aid + " AND Id = (select min(Id) from RCBeam where accessFileID = " + aid + ")";
            SqlCommand sqlcomm6 = new SqlCommand(query6, sqlconn);
            SqlDataReader lf = sqlcomm6.ExecuteReader();
            RC_Beam m = new RC_Beam();
            while (lf.Read())
            {
                listfirst.Add(new RC_Beam
                {
                    ID = Convert.ToInt32(lf["Id"]),
                    Mem = Convert.ToInt32(lf["Mem"]),
                    Mark = lf["Mark"].ToString(),
                    B = lf["B"].ToString(),
                    T = lf["T"].ToString(),
                    L = float.Parse(lf["L"].ToString()),
                    LT = float.Parse(lf["LT"].ToString()),
                    Name = lf["Name"].ToString(),
                    Ct = Convert.ToInt32(lf["Ct"]),
                    Cb = Convert.ToInt32(lf["Cb"]),
                    CLT = Convert.ToChar(lf["CLT"]),
                    CRT = Convert.ToChar(lf["CRT"]),
                    CL = Convert.ToChar(lf["CL"]),
                    CR = Convert.ToChar(lf["CR"]),
                    CLH = Convert.ToChar(lf["CLH"]),
                    CRH = Convert.ToChar(lf["CRH"]),
                    AXL = Convert.ToChar(lf["AXL"]),
                    FL = Convert.ToChar(lf["FL"]),
                    FR = Convert.ToChar(lf["FR"]),
                    TL = Convert.ToInt32(lf["TL"]),
                    TR = Convert.ToInt32(lf["TR"]),
                    N1 = Convert.ToInt32(lf["N1"]),
                    D1 = Convert.ToInt32(lf["D1"]),
                    N2 = Convert.ToInt32(lf["N2"]),
                    D2 = Convert.ToInt32(lf["D2"]),
                    L2L = Convert.ToInt32(lf["L2L"]),
                    L2R = Convert.ToInt32(lf["L2R"]),
                    N3 = Convert.ToInt32(lf["N3"]),
                    D3 = Convert.ToInt32(lf["D3"]),
                    L3L = Convert.ToInt32(lf["L3L"]),
                    L3R = Convert.ToInt32(lf["L3R"]),
                    N4 = Convert.ToInt32(lf["N4"]),
                    D4 = Convert.ToInt32(lf["D4"]),
                    L4L = Convert.ToInt32(lf["L4L"]),
                    L4R = Convert.ToInt32(lf["L4R"]),
                    N5 = Convert.ToInt32(lf["N5"]),
                    D5 = Convert.ToInt32(lf["D5"]),
                    L5L = Convert.ToInt32(lf["L5L"]),
                    L5R = Convert.ToInt32(lf["L5R"]),
                    LN1 = Convert.ToInt32(lf["LN1"]),
                    LD1 = Convert.ToInt32(lf["LD1"]),
                    LN2 = Convert.ToInt32(lf["LN2"]),
                    LD2 = Convert.ToInt32(lf["LD2"]),
                    LL2 = Convert.ToInt32(lf["LL2"]),
                    LN3 = Convert.ToInt32(lf["LN3"]),
                    LD3 = Convert.ToInt32(lf["LD3"]),
                    LL3 = Convert.ToInt32(lf["LL3"]),
                    LN4 = Convert.ToInt32(lf["LN4"]),
                    LD4 = Convert.ToInt32(lf["LD4"]),
                    LL4 = Convert.ToInt32(lf["LL4"]),
                    LN5 = Convert.ToInt32(lf["LN5"]),
                    LD5 = Convert.ToInt32(lf["LD5"]),
                    LL5 = Convert.ToInt32(lf["LL5"]),
                    RN2 = Convert.ToInt32(lf["RN2"]),
                    RD2 = Convert.ToInt32(lf["RD2"]),
                    RL2 = Convert.ToInt32(lf["RL2"]),
                    RN3 = Convert.ToInt32(lf["RN3"]),
                    RD3 = Convert.ToInt32(lf["RD3"]),
                    RL3 = Convert.ToInt32(lf["RL3"]),
                    RN4 = Convert.ToInt32(lf["RN4"]),
                    RD4 = Convert.ToInt32(lf["RD4"]),
                    RL4 = Convert.ToInt32(lf["RL4"]),
                    RN5 = Convert.ToInt32(lf["RN5"]),
                    RD5 = Convert.ToInt32(lf["RD5"]),
                    RL5 = Convert.ToInt32(lf["RL5"]),
                    N11 = Convert.ToInt32(lf["N11"]),
                    D11 = Convert.ToInt32(lf["D11"]),
                    RS2 = Convert.ToInt32(lf["RS2"]),
                    Cot2 = Convert.ToChar(lf["Cot2"]),
                    Cot3 = Convert.ToChar(lf["Cot3"]),
                    Cot4 = Convert.ToChar(lf["Cot4"]),
                    Cot5 = Convert.ToChar(lf["Cot5"]),
                    Cob2 = Convert.ToChar(lf["Cob2"]),
                    Cob3 = Convert.ToChar(lf["Cob3"]),
                    Cob4 = Convert.ToChar(lf["Cob4"]),
                    Cob5 = Convert.ToChar(lf["Cob5"]),
                    Ds = Convert.ToInt32(lf["Ds"]),
                    Nso = Convert.ToInt32(lf["Nso"]),
                    Nsi = Convert.ToInt32(lf["Nsi"]),
                    Nslo = Convert.ToInt32(lf["Nslo"]),
                    Nsli = Convert.ToInt32(lf["Nsli"]),
                    Nsro = Convert.ToInt32(lf["Nsro"]),
                    Nsri = Convert.ToInt32(lf["Nsri"]),
                    Stln = Convert.ToInt32(lf["Stln"]),
                    Stls = Convert.ToInt32(lf["Stls"]),
                    Sts = Convert.ToInt32(lf["Sts"]),
                    Strn = Convert.ToInt32(lf["Strn"]),
                    Strs = Convert.ToInt32(lf["Strs"]),
                    MEMLIST = lf["MEMLIST"].ToString(),
                    SX0 = lf["SX0"] != DBNull.Value ? float.Parse(lf["SX0"].ToString()) : 0,
                    BX0 = lf["BX0"] != DBNull.Value ? float.Parse(lf["BX0"].ToString()) : 0,
                    AX0 = lf["AX0"] != DBNull.Value ? float.Parse(lf["AX0"].ToString()) : 0,
                    CaseX0 = lf["CaseX0"] != DBNull.Value ? lf["CaseX0"].ToString() : "",
                    M3X0 = lf["M3X0"] != DBNull.Value ? float.Parse(lf["M3X0"].ToString()) : 0,
                    V2X0 = lf["V2X0"] != DBNull.Value ? float.Parse(lf["V2X0"].ToString()) : 0,
                    TX0 = lf["TX0"] != DBNull.Value ? float.Parse(lf["TX0"].ToString()) : 0,
                    PX0 = lf["PX0"] != DBNull.Value ? float.Parse(lf["PX0"].ToString()) : 0,
                    SX1 = lf["SX1"] != DBNull.Value ? float.Parse(lf["SX1"].ToString()) : 0,
                    StaX1 = lf["StaX1"] != DBNull.Value ? lf["StaX1"].ToString() : "",
                    BX1 = lf["BX1"] != DBNull.Value ? float.Parse(lf["BX1"].ToString()) : 0,
                    AX1 = lf["AX1"] != DBNull.Value ? float.Parse(lf["AX1"].ToString()) : 0,
                    CaseX1 = lf["CaseX1"] != DBNull.Value ? lf["CaseX1"].ToString() : "",
                    M3X1 = lf["M3X1"] != DBNull.Value ? float.Parse(lf["M3X1"].ToString()) : 0,
                    V2X1 = lf["V2X1"] != DBNull.Value ? float.Parse(lf["V2X1"].ToString()) : 0,
                    TX1 = lf["TX1"] != DBNull.Value ? float.Parse(lf["TX1"].ToString()) : 0,
                    PX1 = lf["PX1"] != DBNull.Value ? float.Parse(lf["PX1"].ToString()) : 0,
                    SX2 = lf["SX2"] != DBNull.Value ? float.Parse(lf["SX2"].ToString()) : 0,
                    StaX2 = lf["StaX2"] != DBNull.Value ? lf["StaX2"].ToString() : "",
                    BX2 = lf["BX2"] != DBNull.Value ? float.Parse(lf["BX2"].ToString()) : 0,
                    AX2 = lf["AX2"] != DBNull.Value ? float.Parse(lf["AX2"].ToString()) : 0,
                    CaseX2 = lf["CaseX2"] != DBNull.Value ? lf["CaseX2"].ToString() : "",
                    M3X2 = lf["M3X2"] != DBNull.Value ? float.Parse(lf["M3X2"].ToString()) : 0,
                    V2X2 = lf["V2X2"] != DBNull.Value ? float.Parse(lf["V2X2"].ToString()) : 0,
                    TX2 = lf["TX2"] != DBNull.Value ? float.Parse(lf["TX2"].ToString()) : 0,
                    PX2 = lf["PX2"] != DBNull.Value ? float.Parse(lf["PX2"].ToString()) : 0,
                    SXM = lf["SXM"] != DBNull.Value ? float.Parse(lf["SXM"].ToString()) : 0,
                    BXM = lf["BXM"] != DBNull.Value ? float.Parse(lf["BXM"].ToString()) : 0,
                    AXM = lf["AXM"] != DBNull.Value ? float.Parse(lf["AXM"].ToString()) : 0,
                    CaseXM = lf["CaseXM"] != DBNull.Value ? lf["CaseXM"].ToString() : "",
                    M3XM = lf["M3XM"] != DBNull.Value ? float.Parse(lf["M3XM"].ToString()) : 0,
                    V2XM = lf["V2XM"] != DBNull.Value ? float.Parse(lf["V2XM"].ToString()) : 0,
                    TXM = lf["TXM"] != DBNull.Value ? float.Parse(lf["TXM"].ToString()) : 0,
                    PXM = lf["PXM"] != DBNull.Value ? float.Parse(lf["PXM"].ToString()) : 0,
                    SX8 = lf["SX8"] != DBNull.Value ? float.Parse(lf["SX8"].ToString()) : 0,
                    StaX8 = lf["StaX8"] != DBNull.Value ? lf["StaX8"].ToString() : "",
                    BX8 = lf["BX8"] != DBNull.Value ? float.Parse(lf["BX8"].ToString()) : 0,
                    AX8 = lf["AX8"] != DBNull.Value ? float.Parse(lf["AX8"].ToString()) : 0,
                    CaseX8 = lf["CaseX8"] != DBNull.Value ? lf["CaseX8"].ToString() : "",
                    M3X8 = lf["M3X8"] != DBNull.Value ? float.Parse(lf["M3X8"].ToString()) : 0,
                    V2X8 = lf["V2X8"] != DBNull.Value ? float.Parse(lf["V2X8"].ToString()) : 0,
                    TX8 = lf["TX8"] != DBNull.Value ? float.Parse(lf["TX8"].ToString()) : 0,
                    PX8 = lf["PX8"] != DBNull.Value ? float.Parse(lf["PX8"].ToString()) : 0,
                    SX9 = lf["SX9"] != DBNull.Value ? float.Parse(lf["SX9"].ToString()) : 0,
                    StaX9 = lf["StaX9"] != DBNull.Value ? lf["StaX9"].ToString() : "",
                    BX9 = lf["BX9"] != DBNull.Value ? float.Parse(lf["BX9"].ToString()) : 0,
                    AX9 = lf["AX9"] != DBNull.Value ? float.Parse(lf["AX9"].ToString()) : 0,
                    CaseX9 = lf["CaseX9"] != DBNull.Value ? lf["CaseX9"].ToString() : "",
                    M3X9 = lf["M3X9"] != DBNull.Value ? float.Parse(lf["M3X9"].ToString()) : 0,
                    V2X9 = lf["V2X9"] != DBNull.Value ? float.Parse(lf["V2X9"].ToString()) : 0,
                    TX9 = lf["TX9"] != DBNull.Value ? float.Parse(lf["TX9"].ToString()) : 0,
                    PX9 = lf["PX9"] != DBNull.Value ? float.Parse(lf["PX9"].ToString()) : 0,
                    SX10 = lf["SX10"] != DBNull.Value ? float.Parse(lf["SX10"].ToString()) : 0,
                    BX10 = lf["BX10"] != DBNull.Value ? float.Parse(lf["BX10"].ToString()) : 0,
                    AX10 = lf["AX10"] != DBNull.Value ? float.Parse(lf["AX10"].ToString()) : 0,
                    CaseX10 = lf["CaseX10"] != DBNull.Value ? lf["CaseX10"].ToString() : "",
                    M3X10 = lf["M3X10"] != DBNull.Value ? float.Parse(lf["M3X10"].ToString()) : 0,
                    V2X10 = lf["V2X10"] != DBNull.Value ? float.Parse(lf["V2X10"].ToString()) : 0,
                    TX10 = lf["TX10"] != DBNull.Value ? float.Parse(lf["TX10"].ToString()) : 0,
                    PX10 = lf["PX10"] != DBNull.Value ? float.Parse(lf["PX10"].ToString()) : 0,
                    XV0 = lf["XV0"] != DBNull.Value ? float.Parse(lf["XV0"].ToString()) : 0,
                    XT0 = lf["XT0"] != DBNull.Value ? float.Parse(lf["XT0"].ToString()) : 0,
                    XVc0 = lf["XVc0"] != DBNull.Value ? float.Parse(lf["XVc0"].ToString()) : 0,
                    CaseXV0 = lf["CaseXV0"] != DBNull.Value ? lf["CaseXV0"].ToString() : "",
                    M3XV0 = lf["M3XV0"] != DBNull.Value ? float.Parse(lf["M3XV0"].ToString()) : 0,
                    V2XV0 = lf["V2XV0"] != DBNull.Value ? float.Parse(lf["V2XV0"].ToString()) : 0,
                    TXV0 = lf["TXV0"] != DBNull.Value ? float.Parse(lf["TXV0"].ToString()) : 0,
                    PXV0 = lf["PXV0"] != DBNull.Value ? float.Parse(lf["PXV0"].ToString()) : 0,
                    XV1 = lf["XV1"] != DBNull.Value ? float.Parse(lf["XV1"].ToString()) : 0,
                    XT1 = lf["XT1"] != DBNull.Value ? float.Parse(lf["XT1"].ToString()) : 0,
                    XVc1 = lf["XVc1"] != DBNull.Value ? float.Parse(lf["XVc1  "].ToString()) : 0,
                    CaseXV1 = lf["CaseXV1"] != DBNull.Value ? lf["CaseXV1"].ToString() : "",
                    M3XV1 = lf["M3XV1"] != DBNull.Value ? float.Parse(lf["M3XV1"].ToString()) : 0,
                    V2XV1 = lf["V2XV1"] != DBNull.Value ? float.Parse(lf["V2XV1"].ToString()) : 0,
                    TXV1 = lf["TXV1"] != DBNull.Value ? float.Parse(lf["TXV1"].ToString()) : 0,
                    PXV1 = lf["PXV1"] != DBNull.Value ? float.Parse(lf["PXV1"].ToString()) : 0,
                    XV9 = lf["XV9"] != DBNull.Value ? float.Parse(lf["XV9"].ToString()) : 0,
                    XT9 = lf["XT9"] != DBNull.Value ? float.Parse(lf["XT9"].ToString()) : 0,
                    XVc9 = lf["XVc9"] != DBNull.Value ? float.Parse(lf["XVc9"].ToString()) : 0,
                    CaseXV9 = lf["CaseXV9"] != DBNull.Value ? lf["CaseXV9"].ToString() : "",
                    M3XV9 = lf["M3XV9"] != DBNull.Value ? float.Parse(lf["M3XV9"].ToString()) : 0,
                    V2XV9 = lf["V2XV9"] != DBNull.Value ? float.Parse(lf["V2XV9"].ToString()) : 0,
                    TXV9 = lf["TXV9"] != DBNull.Value ? float.Parse(lf["TXV9"].ToString()) : 0,
                    PXV9 = lf["PXV9"] != DBNull.Value ? float.Parse(lf["PXV9"].ToString()) : 0,
                    XV10 = lf["XV10"] != DBNull.Value ? float.Parse(lf["XV10"].ToString()) : 0,
                    XT10 = lf["XT10"] != DBNull.Value ? float.Parse(lf["XT10"].ToString()) : 0,
                    XVc10 = lf["XVc10"] != DBNull.Value ? float.Parse(lf["XVc10"].ToString()) : 0,
                    CaseXV10 = lf["CaseXV10"] != DBNull.Value ? lf["CaseXV10"].ToString() : "",
                    M3XV10 = lf["M3XV10"] != DBNull.Value ? float.Parse(lf["M3XV10"].ToString()) : 0,
                    V2XV10 = lf["V2XV10"] != DBNull.Value ? float.Parse(lf["V2XV10"].ToString()) : 0,
                    TXV10 = lf["TXV10"] != DBNull.Value ? float.Parse(lf["TXV10"].ToString()) : 0,
                    PXV10 = lf["PXV10"] != DBNull.Value ? float.Parse(lf["PXV10"].ToString()) : 0,
                    SXMax = lf["SXMax"] != DBNull.Value ? float.Parse(lf["SXMax"].ToString()) : 0,
                    BXMax = lf["BXMax"] != DBNull.Value ? float.Parse(lf["BXMax "].ToString()) : 0,
                    AXMax = lf["AXMax"] != DBNull.Value ? float.Parse(lf["AXMax"].ToString()) : 0,
                    SMaxSta = lf["SMaxSta"] != DBNull.Value ? float.Parse(lf["SMaxSta"].ToString()) : 0,
                    M3XMax = lf["M3XMax"] != DBNull.Value ? float.Parse(lf["M3XMax"].ToString()) : 0,
                    V2XMax = lf["V2XMax"] != DBNull.Value ? float.Parse(lf["V2XMax"].ToString()) : 0,
                    TXMax = lf["TXMax"] != DBNull.Value ? float.Parse(lf["TXMax"].ToString()) : 0,
                    PXMax = lf["PXMax"] != DBNull.Value ? float.Parse(lf["PXMax"].ToString()) : 0,
                    XVMax = lf["XVMax"] != DBNull.Value ? float.Parse(lf["XVMax"].ToString()) : 0,
                    XTMax = lf["XTMax"] != DBNull.Value ? float.Parse(lf["XTMax"].ToString()) : 0,
                    XVcMax = lf["XVcMax"] != DBNull.Value ? float.Parse(lf["XVcMax"].ToString()) : 0,
                    VMaxSta = lf["VMaxSta"] != DBNull.Value ? float.Parse(lf["VMaxSta"].ToString()) : 0,
                    M3XVMax = lf["M3XVMax"] != DBNull.Value ? float.Parse(lf["M3XVMax"].ToString()) : 0,
                    V2XVMax = lf["V2XVMax"] != DBNull.Value ? float.Parse(lf["V2XVMax"].ToString()) : 0,
                    TXVMax = lf["TXVMax"] != DBNull.Value ? float.Parse(lf["TXVMax"].ToString()) : 0,
                    PXVMax = lf["PXVMax"] != DBNull.Value ? float.Parse(lf["PXVMax"].ToString()) : 0,
                    BL = lf["BL"] != DBNull.Value ? Convert.ToChar(lf["BL"]) : ' ',
                    BR = lf["BR"] != DBNull.Value ? Convert.ToChar(lf["BR"]) : ' ',
                    AsMin = lf["AsMin"] != DBNull.Value ? Convert.ToChar(lf["AsMin"]) : ' ',
                });
            }
            sqlconn.Close();
            ViewBag.lf = listfirst;
            if(listfirst.Count != 0)
            {
                Session["rid"] = listfirst[0].ID;
            }

            decimal utf=0;
            sqlconn.Open();
            String qst = "select * from Strength where accessFileID = " + aid;
            SqlCommand sqlcst = new SqlCommand(qst, sqlconn);
            SqlDataReader cst = sqlcst.ExecuteReader();
            while (cst.Read())
            {
                utf = decimal.Parse(cst["utf"].ToString());
            }
            sqlconn.Close();
            Session["utf"] = utf.ToString();
            if (listfirst.Count != 0)
            {
                return View(listfirst[0]);
            }
            RC_Beam list = new RC_Beam();
            return View(list);
        }

        [HttpPost]
        public ActionResult ShowformDesign(String submit, RC_Beam rc)
        {
            int rid = Convert.ToInt32(Session["rid"]);
            int aid = Convert.ToInt32(Session["aid"]);
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            switch (submit)
            {
                case "first":
                    RC_Beam listf = new RC_Beam();
                    List<RC_Beam> listFirst = new List<RC_Beam>();
                    String query6 = "select * from RCBeam where accessFileID = " + aid + " AND Id = (select min(Id) from RCBeam where accessFileID = " + aid + ")";
                    SqlCommand sqlcomm6 = new SqlCommand(query6, sqlconn);
                    SqlDataReader lf = sqlcomm6.ExecuteReader();
                    while (lf.Read())
                    {
                        listFirst.Add(new RC_Beam
                        {
                            ID = Convert.ToInt32(lf["Id"]),
                            Mem = Convert.ToInt32(lf["Mem"]),
                            Mark = lf["Mark"].ToString(),
                            B = lf["B"].ToString(),
                            T = lf["T"].ToString(),
                            L = float.Parse(lf["L"].ToString()),
                            LT = float.Parse(lf["LT"].ToString()),
                            Name = lf["Name"].ToString(),
                            Ct = Convert.ToInt32(lf["Ct"]),
                            Cb = Convert.ToInt32(lf["Cb"]),
                            CLT = Convert.ToChar(lf["CLT"]),
                            CRT = Convert.ToChar(lf["CRT"]),
                            CL = Convert.ToChar(lf["CL"]),
                            CR = Convert.ToChar(lf["CR"]),
                            CLH = Convert.ToChar(lf["CLH"]),
                            CRH = Convert.ToChar(lf["CRH"]),
                            AXL = Convert.ToChar(lf["AXL"]),
                            FL = Convert.ToChar(lf["FL"]),
                            FR = Convert.ToChar(lf["FR"]),
                            TL = Convert.ToInt32(lf["TL"]),
                            TR = Convert.ToInt32(lf["TR"]),
                            N1 = Convert.ToInt32(lf["N1"]),
                            D1 = Convert.ToInt32(lf["D1"]),
                            N2 = Convert.ToInt32(lf["N2"]),
                            D2 = Convert.ToInt32(lf["D2"]),
                            L2L = Convert.ToInt32(lf["L2L"]),
                            L2R = Convert.ToInt32(lf["L2R"]),
                            N3 = Convert.ToInt32(lf["N3"]),
                            D3 = Convert.ToInt32(lf["D3"]),
                            L3L = Convert.ToInt32(lf["L3L"]),
                            L3R = Convert.ToInt32(lf["L3R"]),
                            N4 = Convert.ToInt32(lf["N4"]),
                            D4 = Convert.ToInt32(lf["D4"]),
                            L4L = Convert.ToInt32(lf["L4L"]),
                            L4R = Convert.ToInt32(lf["L4R"]),
                            N5 = Convert.ToInt32(lf["N5"]),
                            D5 = Convert.ToInt32(lf["D5"]),
                            L5L = Convert.ToInt32(lf["L5L"]),
                            L5R = Convert.ToInt32(lf["L5R"]),
                            LN1 = Convert.ToInt32(lf["LN1"]),
                            LD1 = Convert.ToInt32(lf["LD1"]),
                            LN2 = Convert.ToInt32(lf["LN2"]),
                            LD2 = Convert.ToInt32(lf["LD2"]),
                            LL2 = Convert.ToInt32(lf["LL2"]),
                            LN3 = Convert.ToInt32(lf["LN3"]),
                            LD3 = Convert.ToInt32(lf["LD3"]),
                            LL3 = Convert.ToInt32(lf["LL3"]),
                            LN4 = Convert.ToInt32(lf["LN4"]),
                            LD4 = Convert.ToInt32(lf["LD4"]),
                            LL4 = Convert.ToInt32(lf["LL4"]),
                            LN5 = Convert.ToInt32(lf["LN5"]),
                            LD5 = Convert.ToInt32(lf["LD5"]),
                            LL5 = Convert.ToInt32(lf["LL5"]),
                            RN2 = Convert.ToInt32(lf["RN2"]),
                            RD2 = Convert.ToInt32(lf["RD2"]),
                            RL2 = Convert.ToInt32(lf["RL2"]),
                            RN3 = Convert.ToInt32(lf["RN3"]),
                            RD3 = Convert.ToInt32(lf["RD3"]),
                            RL3 = Convert.ToInt32(lf["RL3"]),
                            RN4 = Convert.ToInt32(lf["RN4"]),
                            RD4 = Convert.ToInt32(lf["RD4"]),
                            RL4 = Convert.ToInt32(lf["RL4"]),
                            RN5 = Convert.ToInt32(lf["RN5"]),
                            RD5 = Convert.ToInt32(lf["RD5"]),
                            RL5 = Convert.ToInt32(lf["RL5"]),
                            N11 = Convert.ToInt32(lf["N11"]),
                            D11 = Convert.ToInt32(lf["D11"]),
                            RS2 = Convert.ToInt32(lf["RS2"]),
                            Cot2 = Convert.ToChar(lf["Cot2"]),
                            Cot3 = Convert.ToChar(lf["Cot3"]),
                            Cot4 = Convert.ToChar(lf["Cot4"]),
                            Cot5 = Convert.ToChar(lf["Cot5"]),
                            Cob2 = Convert.ToChar(lf["Cob2"]),
                            Cob3 = Convert.ToChar(lf["Cob3"]),
                            Cob4 = Convert.ToChar(lf["Cob4"]),
                            Cob5 = Convert.ToChar(lf["Cob5"]),
                            Ds = Convert.ToInt32(lf["Ds"]),
                            Nso = Convert.ToInt32(lf["Nso"]),
                            Nsi = Convert.ToInt32(lf["Nsi"]),
                            Nslo = Convert.ToInt32(lf["Nslo"]),
                            Nsli = Convert.ToInt32(lf["Nsli"]),
                            Nsro = Convert.ToInt32(lf["Nsro"]),
                            Nsri = Convert.ToInt32(lf["Nsri"]),
                            Stln = Convert.ToInt32(lf["Stln"]),
                            Stls = Convert.ToInt32(lf["Stls"]),
                            Sts = Convert.ToInt32(lf["Sts"]),
                            Strn = Convert.ToInt32(lf["Strn"]),
                            Strs = Convert.ToInt32(lf["Strs"]),
                            MEMLIST = lf["MEMLIST"].ToString(),
                            SX0 = lf["SX0"] != DBNull.Value ? float.Parse(lf["SX0"].ToString()) : 0,
                            BX0 = lf["BX0"] != DBNull.Value ? float.Parse(lf["BX0"].ToString()) : 0,
                            AX0 = lf["AX0"] != DBNull.Value ? float.Parse(lf["AX0"].ToString()) : 0,
                            CaseX0 = lf["CaseX0"] != DBNull.Value ? lf["CaseX0"].ToString() : "",
                            M3X0 = lf["M3X0"] != DBNull.Value ? float.Parse(lf["M3X0"].ToString()) : 0,
                            V2X0 = lf["V2X0"] != DBNull.Value ? float.Parse(lf["V2X0"].ToString()) : 0,
                            TX0 = lf["TX0"] != DBNull.Value ? float.Parse(lf["TX0"].ToString()) : 0,
                            PX0 = lf["PX0"] != DBNull.Value ? float.Parse(lf["PX0"].ToString()) : 0,
                            SX1 = lf["SX1"] != DBNull.Value ? float.Parse(lf["SX1"].ToString()) : 0,
                            StaX1 = lf["StaX1"] != DBNull.Value ? lf["StaX1"].ToString() : "",
                            BX1 = lf["BX1"] != DBNull.Value ? float.Parse(lf["BX1"].ToString()) : 0,
                            AX1 = lf["AX1"] != DBNull.Value ? float.Parse(lf["AX1"].ToString()) : 0,
                            CaseX1 = lf["CaseX1"] != DBNull.Value ? lf["CaseX1"].ToString() : "",
                            M3X1 = lf["M3X1"] != DBNull.Value ? float.Parse(lf["M3X1"].ToString()) : 0,
                            V2X1 = lf["V2X1"] != DBNull.Value ? float.Parse(lf["V2X1"].ToString()) : 0,
                            TX1 = lf["TX1"] != DBNull.Value ? float.Parse(lf["TX1"].ToString()) : 0,
                            PX1 = lf["PX1"] != DBNull.Value ? float.Parse(lf["PX1"].ToString()) : 0,
                            SX2 = lf["SX2"] != DBNull.Value ? float.Parse(lf["SX2"].ToString()) : 0,
                            StaX2 = lf["StaX2"] != DBNull.Value ? lf["StaX2"].ToString() : "",
                            BX2 = lf["BX2"] != DBNull.Value ? float.Parse(lf["BX2"].ToString()) : 0,
                            AX2 = lf["AX2"] != DBNull.Value ? float.Parse(lf["AX2"].ToString()) : 0,
                            CaseX2 = lf["CaseX2"] != DBNull.Value ? lf["CaseX2"].ToString() : "",
                            M3X2 = lf["M3X2"] != DBNull.Value ? float.Parse(lf["M3X2"].ToString()) : 0,
                            V2X2 = lf["V2X2"] != DBNull.Value ? float.Parse(lf["V2X2"].ToString()) : 0,
                            TX2 = lf["TX2"] != DBNull.Value ? float.Parse(lf["TX2"].ToString()) : 0,
                            PX2 = lf["PX2"] != DBNull.Value ? float.Parse(lf["PX2"].ToString()) : 0,
                            SXM = lf["SXM"] != DBNull.Value ? float.Parse(lf["SXM"].ToString()) : 0,
                            BXM = lf["BXM"] != DBNull.Value ? float.Parse(lf["BXM"].ToString()) : 0,
                            AXM = lf["AXM"] != DBNull.Value ? float.Parse(lf["AXM"].ToString()) : 0,
                            CaseXM = lf["CaseXM"] != DBNull.Value ? lf["CaseXM"].ToString() : "",
                            M3XM = lf["M3XM"] != DBNull.Value ? float.Parse(lf["M3XM"].ToString()) : 0,
                            V2XM = lf["V2XM"] != DBNull.Value ? float.Parse(lf["V2XM"].ToString()) : 0,
                            TXM = lf["TXM"] != DBNull.Value ? float.Parse(lf["TXM"].ToString()) : 0,
                            PXM = lf["PXM"] != DBNull.Value ? float.Parse(lf["PXM"].ToString()) : 0,
                            SX8 = lf["SX8"] != DBNull.Value ? float.Parse(lf["SX8"].ToString()) : 0,
                            StaX8 = lf["StaX8"] != DBNull.Value ? lf["StaX8"].ToString() : "",
                            BX8 = lf["BX8"] != DBNull.Value ? float.Parse(lf["BX8"].ToString()) : 0,
                            AX8 = lf["AX8"] != DBNull.Value ? float.Parse(lf["AX8"].ToString()) : 0,
                            CaseX8 = lf["CaseX8"] != DBNull.Value ? lf["CaseX8"].ToString() : "",
                            M3X8 = lf["M3X8"] != DBNull.Value ? float.Parse(lf["M3X8"].ToString()) : 0,
                            V2X8 = lf["V2X8"] != DBNull.Value ? float.Parse(lf["V2X8"].ToString()) : 0,
                            TX8 = lf["TX8"] != DBNull.Value ? float.Parse(lf["TX8"].ToString()) : 0,
                            PX8 = lf["PX8"] != DBNull.Value ? float.Parse(lf["PX8"].ToString()) : 0,
                            SX9 = lf["SX9"] != DBNull.Value ? float.Parse(lf["SX9"].ToString()) : 0,
                            StaX9 = lf["StaX9"] != DBNull.Value ? lf["StaX9"].ToString() : "",
                            BX9 = lf["BX9"] != DBNull.Value ? float.Parse(lf["BX9"].ToString()) : 0,
                            AX9 = lf["AX9"] != DBNull.Value ? float.Parse(lf["AX9"].ToString()) : 0,
                            CaseX9 = lf["CaseX9"] != DBNull.Value ? lf["CaseX9"].ToString() : "",
                            M3X9 = lf["M3X9"] != DBNull.Value ? float.Parse(lf["M3X9"].ToString()) : 0,
                            V2X9 = lf["V2X9"] != DBNull.Value ? float.Parse(lf["V2X9"].ToString()) : 0,
                            TX9 = lf["TX9"] != DBNull.Value ? float.Parse(lf["TX9"].ToString()) : 0,
                            PX9 = lf["PX9"] != DBNull.Value ? float.Parse(lf["PX9"].ToString()) : 0,
                            SX10 = lf["SX10"] != DBNull.Value ? float.Parse(lf["SX10"].ToString()) : 0,
                            BX10 = lf["BX10"] != DBNull.Value ? float.Parse(lf["BX10"].ToString()) : 0,
                            AX10 = lf["AX10"] != DBNull.Value ? float.Parse(lf["AX10"].ToString()) : 0,
                            CaseX10 = lf["CaseX10"] != DBNull.Value ? lf["CaseX10"].ToString() : "",
                            M3X10 = lf["M3X10"] != DBNull.Value ? float.Parse(lf["M3X10"].ToString()) : 0,
                            V2X10 = lf["V2X10"] != DBNull.Value ? float.Parse(lf["V2X10"].ToString()) : 0,
                            TX10 = lf["TX10"] != DBNull.Value ? float.Parse(lf["TX10"].ToString()) : 0,
                            PX10 = lf["PX10"] != DBNull.Value ? float.Parse(lf["PX10"].ToString()) : 0,
                            XV0 = lf["XV0"] != DBNull.Value ? float.Parse(lf["XV0"].ToString()) : 0,
                            XT0 = lf["XT0"] != DBNull.Value ? float.Parse(lf["XT0"].ToString()) : 0,
                            XVc0 = lf["XVc0"] != DBNull.Value ? float.Parse(lf["XVc0"].ToString()) : 0,
                            CaseXV0 = lf["CaseXV0"] != DBNull.Value ? lf["CaseXV0"].ToString() : "",
                            M3XV0 = lf["M3XV0"] != DBNull.Value ? float.Parse(lf["M3XV0"].ToString()) : 0,
                            V2XV0 = lf["V2XV0"] != DBNull.Value ? float.Parse(lf["V2XV0"].ToString()) : 0,
                            TXV0 = lf["TXV0"] != DBNull.Value ? float.Parse(lf["TXV0"].ToString()) : 0,
                            PXV0 = lf["PXV0"] != DBNull.Value ? float.Parse(lf["PXV0"].ToString()) : 0,
                            XV1 = lf["XV1"] != DBNull.Value ? float.Parse(lf["XV1"].ToString()) : 0,
                            XT1 = lf["XT1"] != DBNull.Value ? float.Parse(lf["XT1"].ToString()) : 0,
                            XVc1 = lf["XVc1"] != DBNull.Value ? float.Parse(lf["XVc1  "].ToString()) : 0,
                            CaseXV1 = lf["CaseXV1"] != DBNull.Value ? lf["CaseXV1"].ToString() : "",
                            M3XV1 = lf["M3XV1"] != DBNull.Value ? float.Parse(lf["M3XV1"].ToString()) : 0,
                            V2XV1 = lf["V2XV1"] != DBNull.Value ? float.Parse(lf["V2XV1"].ToString()) : 0,
                            TXV1 = lf["TXV1"] != DBNull.Value ? float.Parse(lf["TXV1"].ToString()) : 0,
                            PXV1 = lf["PXV1"] != DBNull.Value ? float.Parse(lf["PXV1"].ToString()) : 0,
                            XV9 = lf["XV9"] != DBNull.Value ? float.Parse(lf["XV9"].ToString()) : 0,
                            XT9 = lf["XT9"] != DBNull.Value ? float.Parse(lf["XT9"].ToString()) : 0,
                            XVc9 = lf["XVc9"] != DBNull.Value ? float.Parse(lf["XVc9"].ToString()) : 0,
                            CaseXV9 = lf["CaseXV9"] != DBNull.Value ? lf["CaseXV9"].ToString() : "",
                            M3XV9 = lf["M3XV9"] != DBNull.Value ? float.Parse(lf["M3XV9"].ToString()) : 0,
                            V2XV9 = lf["V2XV9"] != DBNull.Value ? float.Parse(lf["V2XV9"].ToString()) : 0,
                            TXV9 = lf["TXV9"] != DBNull.Value ? float.Parse(lf["TXV9"].ToString()) : 0,
                            PXV9 = lf["PXV9"] != DBNull.Value ? float.Parse(lf["PXV9"].ToString()) : 0,
                            XV10 = lf["XV10"] != DBNull.Value ? float.Parse(lf["XV10"].ToString()) : 0,
                            XT10 = lf["XT10"] != DBNull.Value ? float.Parse(lf["XT10"].ToString()) : 0,
                            XVc10 = lf["XVc10"] != DBNull.Value ? float.Parse(lf["XVc10"].ToString()) : 0,
                            CaseXV10 = lf["CaseXV10"] != DBNull.Value ? lf["CaseXV10"].ToString() : "",
                            M3XV10 = lf["M3XV10"] != DBNull.Value ? float.Parse(lf["M3XV10"].ToString()) : 0,
                            V2XV10 = lf["V2XV10"] != DBNull.Value ? float.Parse(lf["V2XV10"].ToString()) : 0,
                            TXV10 = lf["TXV10"] != DBNull.Value ? float.Parse(lf["TXV10"].ToString()) : 0,
                            PXV10 = lf["PXV10"] != DBNull.Value ? float.Parse(lf["PXV10"].ToString()) : 0,
                            SXMax = lf["SXMax"] != DBNull.Value ? float.Parse(lf["SXMax"].ToString()) : 0,
                            BXMax = lf["BXMax"] != DBNull.Value ? float.Parse(lf["BXMax "].ToString()) : 0,
                            AXMax = lf["AXMax"] != DBNull.Value ? float.Parse(lf["AXMax"].ToString()) : 0,
                            SMaxSta = lf["SMaxSta"] != DBNull.Value ? float.Parse(lf["SMaxSta"].ToString()) : 0,
                            M3XMax = lf["M3XMax"] != DBNull.Value ? float.Parse(lf["M3XMax"].ToString()) : 0,
                            V2XMax = lf["V2XMax"] != DBNull.Value ? float.Parse(lf["V2XMax"].ToString()) : 0,
                            TXMax = lf["TXMax"] != DBNull.Value ? float.Parse(lf["TXMax"].ToString()) : 0,
                            PXMax = lf["PXMax"] != DBNull.Value ? float.Parse(lf["PXMax"].ToString()) : 0,
                            XVMax = lf["XVMax"] != DBNull.Value ? float.Parse(lf["XVMax"].ToString()) : 0,
                            XTMax = lf["XTMax"] != DBNull.Value ? float.Parse(lf["XTMax"].ToString()) : 0,
                            XVcMax = lf["XVcMax"] != DBNull.Value ? float.Parse(lf["XVcMax"].ToString()) : 0,
                            VMaxSta = lf["VMaxSta"] != DBNull.Value ? float.Parse(lf["VMaxSta"].ToString()) : 0,
                            M3XVMax = lf["M3XVMax"] != DBNull.Value ? float.Parse(lf["M3XVMax"].ToString()) : 0,
                            V2XVMax = lf["V2XVMax"] != DBNull.Value ? float.Parse(lf["V2XVMax"].ToString()) : 0,
                            TXVMax = lf["TXVMax"] != DBNull.Value ? float.Parse(lf["TXVMax"].ToString()) : 0,
                            PXVMax = lf["PXVMax"] != DBNull.Value ? float.Parse(lf["PXVMax"].ToString()) : 0,
                            BL = lf["BL"] != DBNull.Value ? Convert.ToChar(lf["BL"]) : ' ',
                            BR = lf["BR"] != DBNull.Value ? Convert.ToChar(lf["BR"]) : ' ',
                            AsMin = lf["AsMin"] != DBNull.Value ? Convert.ToChar(lf["AsMin"]) : ' ',
                        });
                    }
                    ViewBag.lf = listFirst;
                    ViewBag.Message = "First";
                    if (listFirst.Count != 0)
                    {
                        Session["rid"] = listFirst[0].ID;
                    }
                    return View(listFirst[0]);
                case "last":
                    RC_Beam listl = new RC_Beam();
                    List<RC_Beam> listLast = new List<RC_Beam>();
                    String query7 = "select * from RCBeam where accessFileID = " + aid + " AND Id = (select max(Id) from RCBeam where accessFileID = " + aid + ")";
                    SqlCommand sqlcomm7 = new SqlCommand(query7, sqlconn);
                    SqlDataReader ll = sqlcomm7.ExecuteReader();
                    while (ll.Read())
                    {

                        listLast.Add(new RC_Beam
                        {
                            ID = Convert.ToInt32(ll["Id"]),
                            Mem = Convert.ToInt32(ll["Mem"]),
                            Mark = ll["Mark"].ToString(),
                            B = ll["B"].ToString(),
                            T = ll["T"].ToString(),
                            L = float.Parse(ll["L"].ToString()),
                            LT = float.Parse(ll["LT"].ToString()),
                            Name = ll["Name"].ToString(),
                            Ct = Convert.ToInt32(ll["Ct"]),
                            Cb = Convert.ToInt32(ll["Cb"]),
                            CLT = Convert.ToChar(ll["CLT"]),
                            CRT = Convert.ToChar(ll["CRT"]),
                            CL = Convert.ToChar(ll["CL"]),
                            CR = Convert.ToChar(ll["CR"]),
                            CLH = Convert.ToChar(ll["CLH"]),
                            CRH = Convert.ToChar(ll["CRH"]),
                            AXL = Convert.ToChar(ll["AXL"]),
                            FL = Convert.ToChar(ll["FL"]),
                            FR = Convert.ToChar(ll["FR"]),
                            TL = Convert.ToInt32(ll["TL"]),
                            TR = Convert.ToInt32(ll["TR"]),
                            N1 = Convert.ToInt32(ll["N1"]),
                            D1 = Convert.ToInt32(ll["D1"]),
                            N2 = Convert.ToInt32(ll["N2"]),
                            D2 = Convert.ToInt32(ll["D2"]),
                            L2L = Convert.ToInt32(ll["L2L"]),
                            L2R = Convert.ToInt32(ll["L2R"]),
                            N3 = Convert.ToInt32(ll["N3"]),
                            D3 = Convert.ToInt32(ll["D3"]),
                            L3L = Convert.ToInt32(ll["L3L"]),
                            L3R = Convert.ToInt32(ll["L3R"]),
                            N4 = Convert.ToInt32(ll["N4"]),
                            D4 = Convert.ToInt32(ll["D4"]),
                            L4L = Convert.ToInt32(ll["L4L"]),
                            L4R = Convert.ToInt32(ll["L4R"]),
                            N5 = Convert.ToInt32(ll["N5"]),
                            D5 = Convert.ToInt32(ll["D5"]),
                            L5L = Convert.ToInt32(ll["L5L"]),
                            L5R = Convert.ToInt32(ll["L5R"]),
                            LN1 = Convert.ToInt32(ll["LN1"]),
                            LD1 = Convert.ToInt32(ll["LD1"]),
                            LN2 = Convert.ToInt32(ll["LN2"]),
                            LD2 = Convert.ToInt32(ll["LD2"]),
                            LL2 = Convert.ToInt32(ll["LL2"]),
                            LN3 = Convert.ToInt32(ll["LN3"]),
                            LD3 = Convert.ToInt32(ll["LD3"]),
                            LL3 = Convert.ToInt32(ll["LL3"]),
                            LN4 = Convert.ToInt32(ll["LN4"]),
                            LD4 = Convert.ToInt32(ll["LD4"]),
                            LL4 = Convert.ToInt32(ll["LL4"]),
                            LN5 = Convert.ToInt32(ll["LN5"]),
                            LD5 = Convert.ToInt32(ll["LD5"]),
                            LL5 = Convert.ToInt32(ll["LL5"]),
                            RN2 = Convert.ToInt32(ll["RN2"]),
                            RD2 = Convert.ToInt32(ll["RD2"]),
                            RL2 = Convert.ToInt32(ll["RL2"]),
                            RN3 = Convert.ToInt32(ll["RN3"]),
                            RD3 = Convert.ToInt32(ll["RD3"]),
                            RL3 = Convert.ToInt32(ll["RL3"]),
                            RN4 = Convert.ToInt32(ll["RN4"]),
                            RD4 = Convert.ToInt32(ll["RD4"]),
                            RL4 = Convert.ToInt32(ll["RL4"]),
                            RN5 = Convert.ToInt32(ll["RN5"]),
                            RD5 = Convert.ToInt32(ll["RD5"]),
                            RL5 = Convert.ToInt32(ll["RL5"]),
                            N11 = Convert.ToInt32(ll["N11"]),
                            D11 = Convert.ToInt32(ll["D11"]),
                            RS2 = Convert.ToInt32(ll["RS2"]),
                            Cot2 = Convert.ToChar(ll["Cot2"]),
                            Cot3 = Convert.ToChar(ll["Cot3"]),
                            Cot4 = Convert.ToChar(ll["Cot4"]),
                            Cot5 = Convert.ToChar(ll["Cot5"]),
                            Cob2 = Convert.ToChar(ll["Cob2"]),
                            Cob3 = Convert.ToChar(ll["Cob3"]),
                            Cob4 = Convert.ToChar(ll["Cob4"]),
                            Cob5 = Convert.ToChar(ll["Cob5"]),
                            Ds = Convert.ToInt32(ll["Ds"]),
                            Nso = Convert.ToInt32(ll["Nso"]),
                            Nsi = Convert.ToInt32(ll["Nsi"]),
                            Nslo = Convert.ToInt32(ll["Nslo"]),
                            Nsli = Convert.ToInt32(ll["Nsli"]),
                            Nsro = Convert.ToInt32(ll["Nsro"]),
                            Nsri = Convert.ToInt32(ll["Nsri"]),
                            Stln = Convert.ToInt32(ll["Stln"]),
                            Stls = Convert.ToInt32(ll["Stls"]),
                            Sts = Convert.ToInt32(ll["Sts"]),
                            Strn = Convert.ToInt32(ll["Strn"]),
                            Strs = Convert.ToInt32(ll["Strs"]),
                            MEMLIST = ll["MEMLIST"].ToString(),
                            SX0 = ll["SX0"] != DBNull.Value ? float.Parse(ll["SX0"].ToString()) : 0,
                            BX0 = ll["BX0"] != DBNull.Value ? float.Parse(ll["BX0"].ToString()) : 0,
                            AX0 = ll["AX0"] != DBNull.Value ? float.Parse(ll["AX0"].ToString()) : 0,
                            CaseX0 = ll["CaseX0"] != DBNull.Value ? ll["CaseX0"].ToString() : "",
                            M3X0 = ll["M3X0"] != DBNull.Value ? float.Parse(ll["M3X0"].ToString()) : 0,
                            V2X0 = ll["V2X0"] != DBNull.Value ? float.Parse(ll["V2X0"].ToString()) : 0,
                            TX0 = ll["TX0"] != DBNull.Value ? float.Parse(ll["TX0"].ToString()) : 0,
                            PX0 = ll["PX0"] != DBNull.Value ? float.Parse(ll["PX0"].ToString()) : 0,
                            SX1 = ll["SX1"] != DBNull.Value ? float.Parse(ll["SX1"].ToString()) : 0,
                            StaX1 = ll["StaX1"] != DBNull.Value ? ll["StaX1"].ToString() : "",
                            BX1 = ll["BX1"] != DBNull.Value ? float.Parse(ll["BX1"].ToString()) : 0,
                            AX1 = ll["AX1"] != DBNull.Value ? float.Parse(ll["AX1"].ToString()) : 0,
                            CaseX1 = ll["CaseX1"] != DBNull.Value ? ll["CaseX1"].ToString() : "",
                            M3X1 = ll["M3X1"] != DBNull.Value ? float.Parse(ll["M3X1"].ToString()) : 0,
                            V2X1 = ll["V2X1"] != DBNull.Value ? float.Parse(ll["V2X1"].ToString()) : 0,
                            TX1 = ll["TX1"] != DBNull.Value ? float.Parse(ll["TX1"].ToString()) : 0,
                            PX1 = ll["PX1"] != DBNull.Value ? float.Parse(ll["PX1"].ToString()) : 0,
                            SX2 = ll["SX2"] != DBNull.Value ? float.Parse(ll["SX2"].ToString()) : 0,
                            StaX2 = ll["StaX2"] != DBNull.Value ? ll["StaX2"].ToString() : "",
                            BX2 = ll["BX2"] != DBNull.Value ? float.Parse(ll["BX2"].ToString()) : 0,
                            AX2 = ll["AX2"] != DBNull.Value ? float.Parse(ll["AX2"].ToString()) : 0,
                            CaseX2 = ll["CaseX2"] != DBNull.Value ? ll["CaseX2"].ToString() : "",
                            M3X2 = ll["M3X2"] != DBNull.Value ? float.Parse(ll["M3X2"].ToString()) : 0,
                            V2X2 = ll["V2X2"] != DBNull.Value ? float.Parse(ll["V2X2"].ToString()) : 0,
                            TX2 = ll["TX2"] != DBNull.Value ? float.Parse(ll["TX2"].ToString()) : 0,
                            PX2 = ll["PX2"] != DBNull.Value ? float.Parse(ll["PX2"].ToString()) : 0,
                            SXM = ll["SXM"] != DBNull.Value ? float.Parse(ll["SXM"].ToString()) : 0,
                            BXM = ll["BXM"] != DBNull.Value ? float.Parse(ll["BXM"].ToString()) : 0,
                            AXM = ll["AXM"] != DBNull.Value ? float.Parse(ll["AXM"].ToString()) : 0,
                            CaseXM = ll["CaseXM"] != DBNull.Value ? ll["CaseXM"].ToString() : "",
                            M3XM = ll["M3XM"] != DBNull.Value ? float.Parse(ll["M3XM"].ToString()) : 0,
                            V2XM = ll["V2XM"] != DBNull.Value ? float.Parse(ll["V2XM"].ToString()) : 0,
                            TXM = ll["TXM"] != DBNull.Value ? float.Parse(ll["TXM"].ToString()) : 0,
                            PXM = ll["PXM"] != DBNull.Value ? float.Parse(ll["PXM"].ToString()) : 0,
                            SX8 = ll["SX8"] != DBNull.Value ? float.Parse(ll["SX8"].ToString()) : 0,
                            StaX8 = ll["StaX8"] != DBNull.Value ? ll["StaX8"].ToString() : "",
                            BX8 = ll["BX8"] != DBNull.Value ? float.Parse(ll["BX8"].ToString()) : 0,
                            AX8 = ll["AX8"] != DBNull.Value ? float.Parse(ll["AX8"].ToString()) : 0,
                            CaseX8 = ll["CaseX8"] != DBNull.Value ? ll["CaseX8"].ToString() : "",
                            M3X8 = ll["M3X8"] != DBNull.Value ? float.Parse(ll["M3X8"].ToString()) : 0,
                            V2X8 = ll["V2X8"] != DBNull.Value ? float.Parse(ll["V2X8"].ToString()) : 0,
                            TX8 = ll["TX8"] != DBNull.Value ? float.Parse(ll["TX8"].ToString()) : 0,
                            PX8 = ll["PX8"] != DBNull.Value ? float.Parse(ll["PX8"].ToString()) : 0,
                            SX9 = ll["SX9"] != DBNull.Value ? float.Parse(ll["SX9"].ToString()) : 0,
                            StaX9 = ll["StaX9"] != DBNull.Value ? ll["StaX9"].ToString() : "",
                            BX9 = ll["BX9"] != DBNull.Value ? float.Parse(ll["BX9"].ToString()) : 0,
                            AX9 = ll["AX9"] != DBNull.Value ? float.Parse(ll["AX9"].ToString()) : 0,
                            CaseX9 = ll["CaseX9"] != DBNull.Value ? ll["CaseX9"].ToString() : "",
                            M3X9 = ll["M3X9"] != DBNull.Value ? float.Parse(ll["M3X9"].ToString()) : 0,
                            V2X9 = ll["V2X9"] != DBNull.Value ? float.Parse(ll["V2X9"].ToString()) : 0,
                            TX9 = ll["TX9"] != DBNull.Value ? float.Parse(ll["TX9"].ToString()) : 0,
                            PX9 = ll["PX9"] != DBNull.Value ? float.Parse(ll["PX9"].ToString()) : 0,
                            SX10 = ll["SX10"] != DBNull.Value ? float.Parse(ll["SX10"].ToString()) : 0,
                            BX10 = ll["BX10"] != DBNull.Value ? float.Parse(ll["BX10"].ToString()) : 0,
                            AX10 = ll["AX10"] != DBNull.Value ? float.Parse(ll["AX10"].ToString()) : 0,
                            CaseX10 = ll["CaseX10"] != DBNull.Value ? ll["CaseX10"].ToString() : "",
                            M3X10 = ll["M3X10"] != DBNull.Value ? float.Parse(ll["M3X10"].ToString()) : 0,
                            V2X10 = ll["V2X10"] != DBNull.Value ? float.Parse(ll["V2X10"].ToString()) : 0,
                            TX10 = ll["TX10"] != DBNull.Value ? float.Parse(ll["TX10"].ToString()) : 0,
                            PX10 = ll["PX10"] != DBNull.Value ? float.Parse(ll["PX10"].ToString()) : 0,
                            XV0 = ll["XV0"] != DBNull.Value ? float.Parse(ll["XV0"].ToString()) : 0,
                            XT0 = ll["XT0"] != DBNull.Value ? float.Parse(ll["XT0"].ToString()) : 0,
                            XVc0 = ll["XVc0"] != DBNull.Value ? float.Parse(ll["XVc0"].ToString()) : 0,
                            CaseXV0 = ll["CaseXV0"] != DBNull.Value ? ll["CaseXV0"].ToString() : "",
                            M3XV0 = ll["M3XV0"] != DBNull.Value ? float.Parse(ll["M3XV0"].ToString()) : 0,
                            V2XV0 = ll["V2XV0"] != DBNull.Value ? float.Parse(ll["V2XV0"].ToString()) : 0,
                            TXV0 = ll["TXV0"] != DBNull.Value ? float.Parse(ll["TXV0"].ToString()) : 0,
                            PXV0 = ll["PXV0"] != DBNull.Value ? float.Parse(ll["PXV0"].ToString()) : 0,
                            XV1 = ll["XV1"] != DBNull.Value ? float.Parse(ll["XV1"].ToString()) : 0,
                            XT1 = ll["XT1"] != DBNull.Value ? float.Parse(ll["XT1"].ToString()) : 0,
                            XVc1 = ll["XVc1"] != DBNull.Value ? float.Parse(ll["XVc1  "].ToString()) : 0,
                            CaseXV1 = ll["CaseXV1"] != DBNull.Value ? ll["CaseXV1"].ToString() : "",
                            M3XV1 = ll["M3XV1"] != DBNull.Value ? float.Parse(ll["M3XV1"].ToString()) : 0,
                            V2XV1 = ll["V2XV1"] != DBNull.Value ? float.Parse(ll["V2XV1"].ToString()) : 0,
                            TXV1 = ll["TXV1"] != DBNull.Value ? float.Parse(ll["TXV1"].ToString()) : 0,
                            PXV1 = ll["PXV1"] != DBNull.Value ? float.Parse(ll["PXV1"].ToString()) : 0,
                            XV9 = ll["XV9"] != DBNull.Value ? float.Parse(ll["XV9"].ToString()) : 0,
                            XT9 = ll["XT9"] != DBNull.Value ? float.Parse(ll["XT9"].ToString()) : 0,
                            XVc9 = ll["XVc9"] != DBNull.Value ? float.Parse(ll["XVc9"].ToString()) : 0,
                            CaseXV9 = ll["CaseXV9"] != DBNull.Value ? ll["CaseXV9"].ToString() : "",
                            M3XV9 = ll["M3XV9"] != DBNull.Value ? float.Parse(ll["M3XV9"].ToString()) : 0,
                            V2XV9 = ll["V2XV9"] != DBNull.Value ? float.Parse(ll["V2XV9"].ToString()) : 0,
                            TXV9 = ll["TXV9"] != DBNull.Value ? float.Parse(ll["TXV9"].ToString()) : 0,
                            PXV9 = ll["PXV9"] != DBNull.Value ? float.Parse(ll["PXV9"].ToString()) : 0,
                            XV10 = ll["XV10"] != DBNull.Value ? float.Parse(ll["XV10"].ToString()) : 0,
                            XT10 = ll["XT10"] != DBNull.Value ? float.Parse(ll["XT10"].ToString()) : 0,
                            XVc10 = ll["XVc10"] != DBNull.Value ? float.Parse(ll["XVc10"].ToString()) : 0,
                            CaseXV10 = ll["CaseXV10"] != DBNull.Value ? ll["CaseXV10"].ToString() : "",
                            M3XV10 = ll["M3XV10"] != DBNull.Value ? float.Parse(ll["M3XV10"].ToString()) : 0,
                            V2XV10 = ll["V2XV10"] != DBNull.Value ? float.Parse(ll["V2XV10"].ToString()) : 0,
                            TXV10 = ll["TXV10"] != DBNull.Value ? float.Parse(ll["TXV10"].ToString()) : 0,
                            PXV10 = ll["PXV10"] != DBNull.Value ? float.Parse(ll["PXV10"].ToString()) : 0,
                            SXMax = ll["SXMax"] != DBNull.Value ? float.Parse(ll["SXMax"].ToString()) : 0,
                            BXMax = ll["BXMax"] != DBNull.Value ? float.Parse(ll["BXMax "].ToString()) : 0,
                            AXMax = ll["AXMax"] != DBNull.Value ? float.Parse(ll["AXMax"].ToString()) : 0,
                            SMaxSta = ll["SMaxSta"] != DBNull.Value ? float.Parse(ll["SMaxSta"].ToString()) : 0,
                            M3XMax = ll["M3XMax"] != DBNull.Value ? float.Parse(ll["M3XMax"].ToString()) : 0,
                            V2XMax = ll["V2XMax"] != DBNull.Value ? float.Parse(ll["V2XMax"].ToString()) : 0,
                            TXMax = ll["TXMax"] != DBNull.Value ? float.Parse(ll["TXMax"].ToString()) : 0,
                            PXMax = ll["PXMax"] != DBNull.Value ? float.Parse(ll["PXMax"].ToString()) : 0,
                            XVMax = ll["XVMax"] != DBNull.Value ? float.Parse(ll["XVMax"].ToString()) : 0,
                            XTMax = ll["XTMax"] != DBNull.Value ? float.Parse(ll["XTMax"].ToString()) : 0,
                            XVcMax = ll["XVcMax"] != DBNull.Value ? float.Parse(ll["XVcMax"].ToString()) : 0,
                            VMaxSta = ll["VMaxSta"] != DBNull.Value ? float.Parse(ll["VMaxSta"].ToString()) : 0,
                            M3XVMax = ll["M3XVMax"] != DBNull.Value ? float.Parse(ll["M3XVMax"].ToString()) : 0,
                            V2XVMax = ll["V2XVMax"] != DBNull.Value ? float.Parse(ll["V2XVMax"].ToString()) : 0,
                            TXVMax = ll["TXVMax"] != DBNull.Value ? float.Parse(ll["TXVMax"].ToString()) : 0,
                            PXVMax = ll["PXVMax"] != DBNull.Value ? float.Parse(ll["PXVMax"].ToString()) : 0,
                            BL = ll["BL"] != DBNull.Value ? Convert.ToChar(ll["BL"]) : ' ',
                            BR = ll["BR"] != DBNull.Value ? Convert.ToChar(ll["BR"]) : ' ',
                            AsMin = ll["AsMin"] != DBNull.Value ? Convert.ToChar(ll["AsMin"]) : ' ',
                        });
                    }
                    ViewBag.lf = listLast;
                    ViewBag.Message = "Last";
                    if (listLast.Count != 0)
                    {
                        Session["rid"] = listLast[0].ID;
                    }
                    return View(listLast[0]);
                    break;
                case "next":
                    int rNext = rid + 1;
                    RC_Beam listn = new RC_Beam();
                    List<RC_Beam> listNext = new List<RC_Beam>();
                    String query8 = "select * from RCBeam where ID = CASE when " + rNext + " > (select MAX(RCBeam.Id) from RCBeam where accessFileID =  " + aid + " ) THEN (select MAX(RCBeam.Id) from RCBeam where accessFileID =  " + aid + ") Else " + rNext + " END ";
                    SqlCommand sqlcomm8 = new SqlCommand(query8, sqlconn);
                    SqlDataReader ln = sqlcomm8.ExecuteReader();
                    while (ln.Read())
                    {

                        listNext.Add(new RC_Beam
                        {
                            ID = Convert.ToInt32(ln["Id"]),
                            Mem = Convert.ToInt32(ln["Mem"]),
                            Mark = ln["Mark"].ToString(),
                            B = ln["B"].ToString(),
                            T = ln["T"].ToString(),
                            L = float.Parse(ln["L"].ToString()),
                            LT = float.Parse(ln["LT"].ToString()),
                            Name = ln["Name"].ToString(),
                            Ct = Convert.ToInt32(ln["Ct"]),
                            Cb = Convert.ToInt32(ln["Cb"]),
                            CLT = Convert.ToChar(ln["CLT"]),
                            CRT = Convert.ToChar(ln["CRT"]),
                            CL = Convert.ToChar(ln["CL"]),
                            CR = Convert.ToChar(ln["CR"]),
                            CLH = Convert.ToChar(ln["CLH"]),
                            CRH = Convert.ToChar(ln["CRH"]),
                            AXL = Convert.ToChar(ln["AXL"]),
                            FL = Convert.ToChar(ln["FL"]),
                            FR = Convert.ToChar(ln["FR"]),
                            TL = Convert.ToInt32(ln["TL"]),
                            TR = Convert.ToInt32(ln["TR"]),
                            N1 = Convert.ToInt32(ln["N1"]),
                            D1 = Convert.ToInt32(ln["D1"]),
                            N2 = Convert.ToInt32(ln["N2"]),
                            D2 = Convert.ToInt32(ln["D2"]),
                            L2L = Convert.ToInt32(ln["L2L"]),
                            L2R = Convert.ToInt32(ln["L2R"]),
                            N3 = Convert.ToInt32(ln["N3"]),
                            D3 = Convert.ToInt32(ln["D3"]),
                            L3L = Convert.ToInt32(ln["L3L"]),
                            L3R = Convert.ToInt32(ln["L3R"]),
                            N4 = Convert.ToInt32(ln["N4"]),
                            D4 = Convert.ToInt32(ln["D4"]),
                            L4L = Convert.ToInt32(ln["L4L"]),
                            L4R = Convert.ToInt32(ln["L4R"]),
                            N5 = Convert.ToInt32(ln["N5"]),
                            D5 = Convert.ToInt32(ln["D5"]),
                            L5L = Convert.ToInt32(ln["L5L"]),
                            L5R = Convert.ToInt32(ln["L5R"]),
                            LN1 = Convert.ToInt32(ln["LN1"]),
                            LD1 = Convert.ToInt32(ln["LD1"]),
                            LN2 = Convert.ToInt32(ln["LN2"]),
                            LD2 = Convert.ToInt32(ln["LD2"]),
                            LL2 = Convert.ToInt32(ln["LL2"]),
                            LN3 = Convert.ToInt32(ln["LN3"]),
                            LD3 = Convert.ToInt32(ln["LD3"]),
                            LL3 = Convert.ToInt32(ln["LL3"]),
                            LN4 = Convert.ToInt32(ln["LN4"]),
                            LD4 = Convert.ToInt32(ln["LD4"]),
                            LL4 = Convert.ToInt32(ln["LL4"]),
                            LN5 = Convert.ToInt32(ln["LN5"]),
                            LD5 = Convert.ToInt32(ln["LD5"]),
                            LL5 = Convert.ToInt32(ln["LL5"]),
                            RN2 = Convert.ToInt32(ln["RN2"]),
                            RD2 = Convert.ToInt32(ln["RD2"]),
                            RL2 = Convert.ToInt32(ln["RL2"]),
                            RN3 = Convert.ToInt32(ln["RN3"]),
                            RD3 = Convert.ToInt32(ln["RD3"]),
                            RL3 = Convert.ToInt32(ln["RL3"]),
                            RN4 = Convert.ToInt32(ln["RN4"]),
                            RD4 = Convert.ToInt32(ln["RD4"]),
                            RL4 = Convert.ToInt32(ln["RL4"]),
                            RN5 = Convert.ToInt32(ln["RN5"]),
                            RD5 = Convert.ToInt32(ln["RD5"]),
                            RL5 = Convert.ToInt32(ln["RL5"]),
                            N11 = Convert.ToInt32(ln["N11"]),
                            D11 = Convert.ToInt32(ln["D11"]),
                            RS2 = Convert.ToInt32(ln["RS2"]),
                            Cot2 = Convert.ToChar(ln["Cot2"]),
                            Cot3 = Convert.ToChar(ln["Cot3"]),
                            Cot4 = Convert.ToChar(ln["Cot4"]),
                            Cot5 = Convert.ToChar(ln["Cot5"]),
                            Cob2 = Convert.ToChar(ln["Cob2"]),
                            Cob3 = Convert.ToChar(ln["Cob3"]),
                            Cob4 = Convert.ToChar(ln["Cob4"]),
                            Cob5 = Convert.ToChar(ln["Cob5"]),
                            Ds = Convert.ToInt32(ln["Ds"]),
                            Nso = Convert.ToInt32(ln["Nso"]),
                            Nsi = Convert.ToInt32(ln["Nsi"]),
                            Nslo = Convert.ToInt32(ln["Nslo"]),
                            Nsli = Convert.ToInt32(ln["Nsli"]),
                            Nsro = Convert.ToInt32(ln["Nsro"]),
                            Nsri = Convert.ToInt32(ln["Nsri"]),
                            Stln = Convert.ToInt32(ln["Stln"]),
                            Stls = Convert.ToInt32(ln["Stls"]),
                            Sts = Convert.ToInt32(ln["Sts"]),
                            Strn = Convert.ToInt32(ln["Strn"]),
                            Strs = Convert.ToInt32(ln["Strs"]),
                            MEMLIST = ln["MEMLIST"].ToString(),
                            SX0 = ln["SX0"] != DBNull.Value ? float.Parse(ln["SX0"].ToString()) : 0,
                            BX0 = ln["BX0"] != DBNull.Value ? float.Parse(ln["BX0"].ToString()) : 0,
                            AX0 = ln["AX0"] != DBNull.Value ? float.Parse(ln["AX0"].ToString()) : 0,
                            CaseX0 = ln["CaseX0"] != DBNull.Value ? ln["CaseX0"].ToString() : "",
                            M3X0 = ln["M3X0"] != DBNull.Value ? float.Parse(ln["M3X0"].ToString()) : 0,
                            V2X0 = ln["V2X0"] != DBNull.Value ? float.Parse(ln["V2X0"].ToString()) : 0,
                            TX0 = ln["TX0"] != DBNull.Value ? float.Parse(ln["TX0"].ToString()) : 0,
                            PX0 = ln["PX0"] != DBNull.Value ? float.Parse(ln["PX0"].ToString()) : 0,
                            SX1 = ln["SX1"] != DBNull.Value ? float.Parse(ln["SX1"].ToString()) : 0,
                            StaX1 = ln["StaX1"] != DBNull.Value ? ln["StaX1"].ToString() : "",
                            BX1 = ln["BX1"] != DBNull.Value ? float.Parse(ln["BX1"].ToString()) : 0,
                            AX1 = ln["AX1"] != DBNull.Value ? float.Parse(ln["AX1"].ToString()) : 0,
                            CaseX1 = ln["CaseX1"] != DBNull.Value ? ln["CaseX1"].ToString() : "",
                            M3X1 = ln["M3X1"] != DBNull.Value ? float.Parse(ln["M3X1"].ToString()) : 0,
                            V2X1 = ln["V2X1"] != DBNull.Value ? float.Parse(ln["V2X1"].ToString()) : 0,
                            TX1 = ln["TX1"] != DBNull.Value ? float.Parse(ln["TX1"].ToString()) : 0,
                            PX1 = ln["PX1"] != DBNull.Value ? float.Parse(ln["PX1"].ToString()) : 0,
                            SX2 = ln["SX2"] != DBNull.Value ? float.Parse(ln["SX2"].ToString()) : 0,
                            StaX2 = ln["StaX2"] != DBNull.Value ? ln["StaX2"].ToString() : "",
                            BX2 = ln["BX2"] != DBNull.Value ? float.Parse(ln["BX2"].ToString()) : 0,
                            AX2 = ln["AX2"] != DBNull.Value ? float.Parse(ln["AX2"].ToString()) : 0,
                            CaseX2 = ln["CaseX2"] != DBNull.Value ? ln["CaseX2"].ToString() : "",
                            M3X2 = ln["M3X2"] != DBNull.Value ? float.Parse(ln["M3X2"].ToString()) : 0,
                            V2X2 = ln["V2X2"] != DBNull.Value ? float.Parse(ln["V2X2"].ToString()) : 0,
                            TX2 = ln["TX2"] != DBNull.Value ? float.Parse(ln["TX2"].ToString()) : 0,
                            PX2 = ln["PX2"] != DBNull.Value ? float.Parse(ln["PX2"].ToString()) : 0,
                            SXM = ln["SXM"] != DBNull.Value ? float.Parse(ln["SXM"].ToString()) : 0,
                            BXM = ln["BXM"] != DBNull.Value ? float.Parse(ln["BXM"].ToString()) : 0,
                            AXM = ln["AXM"] != DBNull.Value ? float.Parse(ln["AXM"].ToString()) : 0,
                            CaseXM = ln["CaseXM"] != DBNull.Value ? ln["CaseXM"].ToString() : "",
                            M3XM = ln["M3XM"] != DBNull.Value ? float.Parse(ln["M3XM"].ToString()) : 0,
                            V2XM = ln["V2XM"] != DBNull.Value ? float.Parse(ln["V2XM"].ToString()) : 0,
                            TXM = ln["TXM"] != DBNull.Value ? float.Parse(ln["TXM"].ToString()) : 0,
                            PXM = ln["PXM"] != DBNull.Value ? float.Parse(ln["PXM"].ToString()) : 0,
                            SX8 = ln["SX8"] != DBNull.Value ? float.Parse(ln["SX8"].ToString()) : 0,
                            StaX8 = ln["StaX8"] != DBNull.Value ? ln["StaX8"].ToString() : "",
                            BX8 = ln["BX8"] != DBNull.Value ? float.Parse(ln["BX8"].ToString()) : 0,
                            AX8 = ln["AX8"] != DBNull.Value ? float.Parse(ln["AX8"].ToString()) : 0,
                            CaseX8 = ln["CaseX8"] != DBNull.Value ? ln["CaseX8"].ToString() : "",
                            M3X8 = ln["M3X8"] != DBNull.Value ? float.Parse(ln["M3X8"].ToString()) : 0,
                            V2X8 = ln["V2X8"] != DBNull.Value ? float.Parse(ln["V2X8"].ToString()) : 0,
                            TX8 = ln["TX8"] != DBNull.Value ? float.Parse(ln["TX8"].ToString()) : 0,
                            PX8 = ln["PX8"] != DBNull.Value ? float.Parse(ln["PX8"].ToString()) : 0,
                            SX9 = ln["SX9"] != DBNull.Value ? float.Parse(ln["SX9"].ToString()) : 0,
                            StaX9 = ln["StaX9"] != DBNull.Value ? ln["StaX9"].ToString() : "",
                            BX9 = ln["BX9"] != DBNull.Value ? float.Parse(ln["BX9"].ToString()) : 0,
                            AX9 = ln["AX9"] != DBNull.Value ? float.Parse(ln["AX9"].ToString()) : 0,
                            CaseX9 = ln["CaseX9"] != DBNull.Value ? ln["CaseX9"].ToString() : "",
                            M3X9 = ln["M3X9"] != DBNull.Value ? float.Parse(ln["M3X9"].ToString()) : 0,
                            V2X9 = ln["V2X9"] != DBNull.Value ? float.Parse(ln["V2X9"].ToString()) : 0,
                            TX9 = ln["TX9"] != DBNull.Value ? float.Parse(ln["TX9"].ToString()) : 0,
                            PX9 = ln["PX9"] != DBNull.Value ? float.Parse(ln["PX9"].ToString()) : 0,
                            SX10 = ln["SX10"] != DBNull.Value ? float.Parse(ln["SX10"].ToString()) : 0,
                            BX10 = ln["BX10"] != DBNull.Value ? float.Parse(ln["BX10"].ToString()) : 0,
                            AX10 = ln["AX10"] != DBNull.Value ? float.Parse(ln["AX10"].ToString()) : 0,
                            CaseX10 = ln["CaseX10"] != DBNull.Value ? ln["CaseX10"].ToString() : "",
                            M3X10 = ln["M3X10"] != DBNull.Value ? float.Parse(ln["M3X10"].ToString()) : 0,
                            V2X10 = ln["V2X10"] != DBNull.Value ? float.Parse(ln["V2X10"].ToString()) : 0,
                            TX10 = ln["TX10"] != DBNull.Value ? float.Parse(ln["TX10"].ToString()) : 0,
                            PX10 = ln["PX10"] != DBNull.Value ? float.Parse(ln["PX10"].ToString()) : 0,
                            XV0 = ln["XV0"] != DBNull.Value ? float.Parse(ln["XV0"].ToString()) : 0,
                            XT0 = ln["XT0"] != DBNull.Value ? float.Parse(ln["XT0"].ToString()) : 0,
                            XVc0 = ln["XVc0"] != DBNull.Value ? float.Parse(ln["XVc0"].ToString()) : 0,
                            CaseXV0 = ln["CaseXV0"] != DBNull.Value ? ln["CaseXV0"].ToString() : "",
                            M3XV0 = ln["M3XV0"] != DBNull.Value ? float.Parse(ln["M3XV0"].ToString()) : 0,
                            V2XV0 = ln["V2XV0"] != DBNull.Value ? float.Parse(ln["V2XV0"].ToString()) : 0,
                            TXV0 = ln["TXV0"] != DBNull.Value ? float.Parse(ln["TXV0"].ToString()) : 0,
                            PXV0 = ln["PXV0"] != DBNull.Value ? float.Parse(ln["PXV0"].ToString()) : 0,
                            XV1 = ln["XV1"] != DBNull.Value ? float.Parse(ln["XV1"].ToString()) : 0,
                            XT1 = ln["XT1"] != DBNull.Value ? float.Parse(ln["XT1"].ToString()) : 0,
                            XVc1 = ln["XVc1"] != DBNull.Value ? float.Parse(ln["XVc1  "].ToString()) : 0,
                            CaseXV1 = ln["CaseXV1"] != DBNull.Value ? ln["CaseXV1"].ToString() : "",
                            M3XV1 = ln["M3XV1"] != DBNull.Value ? float.Parse(ln["M3XV1"].ToString()) : 0,
                            V2XV1 = ln["V2XV1"] != DBNull.Value ? float.Parse(ln["V2XV1"].ToString()) : 0,
                            TXV1 = ln["TXV1"] != DBNull.Value ? float.Parse(ln["TXV1"].ToString()) : 0,
                            PXV1 = ln["PXV1"] != DBNull.Value ? float.Parse(ln["PXV1"].ToString()) : 0,
                            XV9 = ln["XV9"] != DBNull.Value ? float.Parse(ln["XV9"].ToString()) : 0,
                            XT9 = ln["XT9"] != DBNull.Value ? float.Parse(ln["XT9"].ToString()) : 0,
                            XVc9 = ln["XVc9"] != DBNull.Value ? float.Parse(ln["XVc9"].ToString()) : 0,
                            CaseXV9 = ln["CaseXV9"] != DBNull.Value ? ln["CaseXV9"].ToString() : "",
                            M3XV9 = ln["M3XV9"] != DBNull.Value ? float.Parse(ln["M3XV9"].ToString()) : 0,
                            V2XV9 = ln["V2XV9"] != DBNull.Value ? float.Parse(ln["V2XV9"].ToString()) : 0,
                            TXV9 = ln["TXV9"] != DBNull.Value ? float.Parse(ln["TXV9"].ToString()) : 0,
                            PXV9 = ln["PXV9"] != DBNull.Value ? float.Parse(ln["PXV9"].ToString()) : 0,
                            XV10 = ln["XV10"] != DBNull.Value ? float.Parse(ln["XV10"].ToString()) : 0,
                            XT10 = ln["XT10"] != DBNull.Value ? float.Parse(ln["XT10"].ToString()) : 0,
                            XVc10 = ln["XVc10"] != DBNull.Value ? float.Parse(ln["XVc10"].ToString()) : 0,
                            CaseXV10 = ln["CaseXV10"] != DBNull.Value ? ln["CaseXV10"].ToString() : "",
                            M3XV10 = ln["M3XV10"] != DBNull.Value ? float.Parse(ln["M3XV10"].ToString()) : 0,
                            V2XV10 = ln["V2XV10"] != DBNull.Value ? float.Parse(ln["V2XV10"].ToString()) : 0,
                            TXV10 = ln["TXV10"] != DBNull.Value ? float.Parse(ln["TXV10"].ToString()) : 0,
                            PXV10 = ln["PXV10"] != DBNull.Value ? float.Parse(ln["PXV10"].ToString()) : 0,
                            SXMax = ln["SXMax"] != DBNull.Value ? float.Parse(ln["SXMax"].ToString()) : 0,
                            BXMax = ln["BXMax"] != DBNull.Value ? float.Parse(ln["BXMax "].ToString()) : 0,
                            AXMax = ln["AXMax"] != DBNull.Value ? float.Parse(ln["AXMax"].ToString()) : 0,
                            SMaxSta = ln["SMaxSta"] != DBNull.Value ? float.Parse(ln["SMaxSta"].ToString()) : 0,
                            M3XMax = ln["M3XMax"] != DBNull.Value ? float.Parse(ln["M3XMax"].ToString()) : 0,
                            V2XMax = ln["V2XMax"] != DBNull.Value ? float.Parse(ln["V2XMax"].ToString()) : 0,
                            TXMax = ln["TXMax"] != DBNull.Value ? float.Parse(ln["TXMax"].ToString()) : 0,
                            PXMax = ln["PXMax"] != DBNull.Value ? float.Parse(ln["PXMax"].ToString()) : 0,
                            XVMax = ln["XVMax"] != DBNull.Value ? float.Parse(ln["XVMax"].ToString()) : 0,
                            XTMax = ln["XTMax"] != DBNull.Value ? float.Parse(ln["XTMax"].ToString()) : 0,
                            XVcMax = ln["XVcMax"] != DBNull.Value ? float.Parse(ln["XVcMax"].ToString()) : 0,
                            VMaxSta = ln["VMaxSta"] != DBNull.Value ? float.Parse(ln["VMaxSta"].ToString()) : 0,
                            M3XVMax = ln["M3XVMax"] != DBNull.Value ? float.Parse(ln["M3XVMax"].ToString()) : 0,
                            V2XVMax = ln["V2XVMax"] != DBNull.Value ? float.Parse(ln["V2XVMax"].ToString()) : 0,
                            TXVMax = ln["TXVMax"] != DBNull.Value ? float.Parse(ln["TXVMax"].ToString()) : 0,
                            PXVMax = ln["PXVMax"] != DBNull.Value ? float.Parse(ln["PXVMax"].ToString()) : 0,
                            BL = ln["BL"] != DBNull.Value ? Convert.ToChar(ln["BL"]) : ' ',
                            BR = ln["BR"] != DBNull.Value ? Convert.ToChar(ln["BR"]) : ' ',
                            AsMin = ln["AsMin"] != DBNull.Value ? Convert.ToChar(ln["AsMin"]) : ' ',
                        });

                    }
                    ViewBag.lf = listNext;
                    ViewBag.Message = "Next";
                    if (listNext.Count != 0)
                    {
                        Session["rid"] = listNext[0].ID;
                    }
                    return View(listNext[0]);
                    break;
                case "previous":
                    int rPrevious = rid - 1;
                    RC_Beam listp = new RC_Beam();
                    List<RC_Beam> listPrevious = new List<RC_Beam>();
                    String query9 = "select * from RCBeam where ID = CASE when " + rPrevious + " < (select MIN(RCBeam.Id) from RCBeam where accessFileID =  " + aid + " ) THEN (select MIN(RCBeam.Id) from RCBeam where accessFileID =  " + aid + ") Else " + rPrevious + " END ";
                    SqlCommand sqlcomm9 = new SqlCommand(query9, sqlconn);
                    SqlDataReader lp = sqlcomm9.ExecuteReader();
                    while (lp.Read())
                    {

                        listPrevious.Add(new RC_Beam
                        {
                            ID = Convert.ToInt32(lp["Id"]),
                            Mem = Convert.ToInt32(lp["Mem"]),
                            Mark = lp["Mark"].ToString(),
                            B = lp["B"].ToString(),
                            T = lp["T"].ToString(),
                            L = float.Parse(lp["L"].ToString()),
                            LT = float.Parse(lp["LT"].ToString()),
                            Name = lp["Name"].ToString(),
                            Ct = Convert.ToInt32(lp["Ct"]),
                            Cb = Convert.ToInt32(lp["Cb"]),
                            CLT = Convert.ToChar(lp["CLT"]),
                            CRT = Convert.ToChar(lp["CRT"]),
                            CL = Convert.ToChar(lp["CL"]),
                            CR = Convert.ToChar(lp["CR"]),
                            CLH = Convert.ToChar(lp["CLH"]),
                            CRH = Convert.ToChar(lp["CRH"]),
                            AXL = Convert.ToChar(lp["AXL"]),
                            FL = Convert.ToChar(lp["FL"]),
                            FR = Convert.ToChar(lp["FR"]),
                            TL = Convert.ToInt32(lp["TL"]),
                            TR = Convert.ToInt32(lp["TR"]),
                            N1 = Convert.ToInt32(lp["N1"]),
                            D1 = Convert.ToInt32(lp["D1"]),
                            N2 = Convert.ToInt32(lp["N2"]),
                            D2 = Convert.ToInt32(lp["D2"]),
                            L2L = Convert.ToInt32(lp["L2L"]),
                            L2R = Convert.ToInt32(lp["L2R"]),
                            N3 = Convert.ToInt32(lp["N3"]),
                            D3 = Convert.ToInt32(lp["D3"]),
                            L3L = Convert.ToInt32(lp["L3L"]),
                            L3R = Convert.ToInt32(lp["L3R"]),
                            N4 = Convert.ToInt32(lp["N4"]),
                            D4 = Convert.ToInt32(lp["D4"]),
                            L4L = Convert.ToInt32(lp["L4L"]),
                            L4R = Convert.ToInt32(lp["L4R"]),
                            N5 = Convert.ToInt32(lp["N5"]),
                            D5 = Convert.ToInt32(lp["D5"]),
                            L5L = Convert.ToInt32(lp["L5L"]),
                            L5R = Convert.ToInt32(lp["L5R"]),
                            LN1 = Convert.ToInt32(lp["LN1"]),
                            LD1 = Convert.ToInt32(lp["LD1"]),
                            LN2 = Convert.ToInt32(lp["LN2"]),
                            LD2 = Convert.ToInt32(lp["LD2"]),
                            LL2 = Convert.ToInt32(lp["LL2"]),
                            LN3 = Convert.ToInt32(lp["LN3"]),
                            LD3 = Convert.ToInt32(lp["LD3"]),
                            LL3 = Convert.ToInt32(lp["LL3"]),
                            LN4 = Convert.ToInt32(lp["LN4"]),
                            LD4 = Convert.ToInt32(lp["LD4"]),
                            LL4 = Convert.ToInt32(lp["LL4"]),
                            LN5 = Convert.ToInt32(lp["LN5"]),
                            LD5 = Convert.ToInt32(lp["LD5"]),
                            LL5 = Convert.ToInt32(lp["LL5"]),
                            RN2 = Convert.ToInt32(lp["RN2"]),
                            RD2 = Convert.ToInt32(lp["RD2"]),
                            RL2 = Convert.ToInt32(lp["RL2"]),
                            RN3 = Convert.ToInt32(lp["RN3"]),
                            RD3 = Convert.ToInt32(lp["RD3"]),
                            RL3 = Convert.ToInt32(lp["RL3"]),
                            RN4 = Convert.ToInt32(lp["RN4"]),
                            RD4 = Convert.ToInt32(lp["RD4"]),
                            RL4 = Convert.ToInt32(lp["RL4"]),
                            RN5 = Convert.ToInt32(lp["RN5"]),
                            RD5 = Convert.ToInt32(lp["RD5"]),
                            RL5 = Convert.ToInt32(lp["RL5"]),
                            N11 = Convert.ToInt32(lp["N11"]),
                            D11 = Convert.ToInt32(lp["D11"]),
                            RS2 = Convert.ToInt32(lp["RS2"]),
                            Cot2 = Convert.ToChar(lp["Cot2"]),
                            Cot3 = Convert.ToChar(lp["Cot3"]),
                            Cot4 = Convert.ToChar(lp["Cot4"]),
                            Cot5 = Convert.ToChar(lp["Cot5"]),
                            Cob2 = Convert.ToChar(lp["Cob2"]),
                            Cob3 = Convert.ToChar(lp["Cob3"]),
                            Cob4 = Convert.ToChar(lp["Cob4"]),
                            Cob5 = Convert.ToChar(lp["Cob5"]),
                            Ds = Convert.ToInt32(lp["Ds"]),
                            Nso = Convert.ToInt32(lp["Nso"]),
                            Nsi = Convert.ToInt32(lp["Nsi"]),
                            Nslo = Convert.ToInt32(lp["Nslo"]),
                            Nsli = Convert.ToInt32(lp["Nsli"]),
                            Nsro = Convert.ToInt32(lp["Nsro"]),
                            Nsri = Convert.ToInt32(lp["Nsri"]),
                            Stln = Convert.ToInt32(lp["Stln"]),
                            Stls = Convert.ToInt32(lp["Stls"]),
                            Sts = Convert.ToInt32(lp["Sts"]),
                            Strn = Convert.ToInt32(lp["Strn"]),
                            Strs = Convert.ToInt32(lp["Strs"]),
                            MEMLIST = lp["MEMLIST"].ToString(),
                            SX0 = lp["SX0"] != DBNull.Value ? float.Parse(lp["SX0"].ToString()) : 0,
                            BX0 = lp["BX0"] != DBNull.Value ? float.Parse(lp["BX0"].ToString()) : 0,
                            AX0 = lp["AX0"] != DBNull.Value ? float.Parse(lp["AX0"].ToString()) : 0,
                            CaseX0 = lp["CaseX0"] != DBNull.Value ? lp["CaseX0"].ToString() : "",
                            M3X0 = lp["M3X0"] != DBNull.Value ? float.Parse(lp["M3X0"].ToString()) : 0,
                            V2X0 = lp["V2X0"] != DBNull.Value ? float.Parse(lp["V2X0"].ToString()) : 0,
                            TX0 = lp["TX0"] != DBNull.Value ? float.Parse(lp["TX0"].ToString()) : 0,
                            PX0 = lp["PX0"] != DBNull.Value ? float.Parse(lp["PX0"].ToString()) : 0,
                            SX1 = lp["SX1"] != DBNull.Value ? float.Parse(lp["SX1"].ToString()) : 0,
                            StaX1 = lp["StaX1"] != DBNull.Value ? lp["StaX1"].ToString() : "",
                            BX1 = lp["BX1"] != DBNull.Value ? float.Parse(lp["BX1"].ToString()) : 0,
                            AX1 = lp["AX1"] != DBNull.Value ? float.Parse(lp["AX1"].ToString()) : 0,
                            CaseX1 = lp["CaseX1"] != DBNull.Value ? lp["CaseX1"].ToString() : "",
                            M3X1 = lp["M3X1"] != DBNull.Value ? float.Parse(lp["M3X1"].ToString()) : 0,
                            V2X1 = lp["V2X1"] != DBNull.Value ? float.Parse(lp["V2X1"].ToString()) : 0,
                            TX1 = lp["TX1"] != DBNull.Value ? float.Parse(lp["TX1"].ToString()) : 0,
                            PX1 = lp["PX1"] != DBNull.Value ? float.Parse(lp["PX1"].ToString()) : 0,
                            SX2 = lp["SX2"] != DBNull.Value ? float.Parse(lp["SX2"].ToString()) : 0,
                            StaX2 = lp["StaX2"] != DBNull.Value ? lp["StaX2"].ToString() : "",
                            BX2 = lp["BX2"] != DBNull.Value ? float.Parse(lp["BX2"].ToString()) : 0,
                            AX2 = lp["AX2"] != DBNull.Value ? float.Parse(lp["AX2"].ToString()) : 0,
                            CaseX2 = lp["CaseX2"] != DBNull.Value ? lp["CaseX2"].ToString() : "",
                            M3X2 = lp["M3X2"] != DBNull.Value ? float.Parse(lp["M3X2"].ToString()) : 0,
                            V2X2 = lp["V2X2"] != DBNull.Value ? float.Parse(lp["V2X2"].ToString()) : 0,
                            TX2 = lp["TX2"] != DBNull.Value ? float.Parse(lp["TX2"].ToString()) : 0,
                            PX2 = lp["PX2"] != DBNull.Value ? float.Parse(lp["PX2"].ToString()) : 0,
                            SXM = lp["SXM"] != DBNull.Value ? float.Parse(lp["SXM"].ToString()) : 0,
                            BXM = lp["BXM"] != DBNull.Value ? float.Parse(lp["BXM"].ToString()) : 0,
                            AXM = lp["AXM"] != DBNull.Value ? float.Parse(lp["AXM"].ToString()) : 0,
                            CaseXM = lp["CaseXM"] != DBNull.Value ? lp["CaseXM"].ToString() : "",
                            M3XM = lp["M3XM"] != DBNull.Value ? float.Parse(lp["M3XM"].ToString()) : 0,
                            V2XM = lp["V2XM"] != DBNull.Value ? float.Parse(lp["V2XM"].ToString()) : 0,
                            TXM = lp["TXM"] != DBNull.Value ? float.Parse(lp["TXM"].ToString()) : 0,
                            PXM = lp["PXM"] != DBNull.Value ? float.Parse(lp["PXM"].ToString()) : 0,
                            SX8 = lp["SX8"] != DBNull.Value ? float.Parse(lp["SX8"].ToString()) : 0,
                            StaX8 = lp["StaX8"] != DBNull.Value ? lp["StaX8"].ToString() : "",
                            BX8 = lp["BX8"] != DBNull.Value ? float.Parse(lp["BX8"].ToString()) : 0,
                            AX8 = lp["AX8"] != DBNull.Value ? float.Parse(lp["AX8"].ToString()) : 0,
                            CaseX8 = lp["CaseX8"] != DBNull.Value ? lp["CaseX8"].ToString() : "",
                            M3X8 = lp["M3X8"] != DBNull.Value ? float.Parse(lp["M3X8"].ToString()) : 0,
                            V2X8 = lp["V2X8"] != DBNull.Value ? float.Parse(lp["V2X8"].ToString()) : 0,
                            TX8 = lp["TX8"] != DBNull.Value ? float.Parse(lp["TX8"].ToString()) : 0,
                            PX8 = lp["PX8"] != DBNull.Value ? float.Parse(lp["PX8"].ToString()) : 0,
                            SX9 = lp["SX9"] != DBNull.Value ? float.Parse(lp["SX9"].ToString()) : 0,
                            StaX9 = lp["StaX9"] != DBNull.Value ? lp["StaX9"].ToString() : "",
                            BX9 = lp["BX9"] != DBNull.Value ? float.Parse(lp["BX9"].ToString()) : 0,
                            AX9 = lp["AX9"] != DBNull.Value ? float.Parse(lp["AX9"].ToString()) : 0,
                            CaseX9 = lp["CaseX9"] != DBNull.Value ? lp["CaseX9"].ToString() : "",
                            M3X9 = lp["M3X9"] != DBNull.Value ? float.Parse(lp["M3X9"].ToString()) : 0,
                            V2X9 = lp["V2X9"] != DBNull.Value ? float.Parse(lp["V2X9"].ToString()) : 0,
                            TX9 = lp["TX9"] != DBNull.Value ? float.Parse(lp["TX9"].ToString()) : 0,
                            PX9 = lp["PX9"] != DBNull.Value ? float.Parse(lp["PX9"].ToString()) : 0,
                            SX10 = lp["SX10"] != DBNull.Value ? float.Parse(lp["SX10"].ToString()) : 0,
                            BX10 = lp["BX10"] != DBNull.Value ? float.Parse(lp["BX10"].ToString()) : 0,
                            AX10 = lp["AX10"] != DBNull.Value ? float.Parse(lp["AX10"].ToString()) : 0,
                            CaseX10 = lp["CaseX10"] != DBNull.Value ? lp["CaseX10"].ToString() : "",
                            M3X10 = lp["M3X10"] != DBNull.Value ? float.Parse(lp["M3X10"].ToString()) : 0,
                            V2X10 = lp["V2X10"] != DBNull.Value ? float.Parse(lp["V2X10"].ToString()) : 0,
                            TX10 = lp["TX10"] != DBNull.Value ? float.Parse(lp["TX10"].ToString()) : 0,
                            PX10 = lp["PX10"] != DBNull.Value ? float.Parse(lp["PX10"].ToString()) : 0,
                            XV0 = lp["XV0"] != DBNull.Value ? float.Parse(lp["XV0"].ToString()) : 0,
                            XT0 = lp["XT0"] != DBNull.Value ? float.Parse(lp["XT0"].ToString()) : 0,
                            XVc0 = lp["XVc0"] != DBNull.Value ? float.Parse(lp["XVc0"].ToString()) : 0,
                            CaseXV0 = lp["CaseXV0"] != DBNull.Value ? lp["CaseXV0"].ToString() : "",
                            M3XV0 = lp["M3XV0"] != DBNull.Value ? float.Parse(lp["M3XV0"].ToString()) : 0,
                            V2XV0 = lp["V2XV0"] != DBNull.Value ? float.Parse(lp["V2XV0"].ToString()) : 0,
                            TXV0 = lp["TXV0"] != DBNull.Value ? float.Parse(lp["TXV0"].ToString()) : 0,
                            PXV0 = lp["PXV0"] != DBNull.Value ? float.Parse(lp["PXV0"].ToString()) : 0,
                            XV1 = lp["XV1"] != DBNull.Value ? float.Parse(lp["XV1"].ToString()) : 0,
                            XT1 = lp["XT1"] != DBNull.Value ? float.Parse(lp["XT1"].ToString()) : 0,
                            XVc1 = lp["XVc1"] != DBNull.Value ? float.Parse(lp["XVc1  "].ToString()) : 0,
                            CaseXV1 = lp["CaseXV1"] != DBNull.Value ? lp["CaseXV1"].ToString() : "",
                            M3XV1 = lp["M3XV1"] != DBNull.Value ? float.Parse(lp["M3XV1"].ToString()) : 0,
                            V2XV1 = lp["V2XV1"] != DBNull.Value ? float.Parse(lp["V2XV1"].ToString()) : 0,
                            TXV1 = lp["TXV1"] != DBNull.Value ? float.Parse(lp["TXV1"].ToString()) : 0,
                            PXV1 = lp["PXV1"] != DBNull.Value ? float.Parse(lp["PXV1"].ToString()) : 0,
                            XV9 = lp["XV9"] != DBNull.Value ? float.Parse(lp["XV9"].ToString()) : 0,
                            XT9 = lp["XT9"] != DBNull.Value ? float.Parse(lp["XT9"].ToString()) : 0,
                            XVc9 = lp["XVc9"] != DBNull.Value ? float.Parse(lp["XVc9"].ToString()) : 0,
                            CaseXV9 = lp["CaseXV9"] != DBNull.Value ? lp["CaseXV9"].ToString() : "",
                            M3XV9 = lp["M3XV9"] != DBNull.Value ? float.Parse(lp["M3XV9"].ToString()) : 0,
                            V2XV9 = lp["V2XV9"] != DBNull.Value ? float.Parse(lp["V2XV9"].ToString()) : 0,
                            TXV9 = lp["TXV9"] != DBNull.Value ? float.Parse(lp["TXV9"].ToString()) : 0,
                            PXV9 = lp["PXV9"] != DBNull.Value ? float.Parse(lp["PXV9"].ToString()) : 0,
                            XV10 = lp["XV10"] != DBNull.Value ? float.Parse(lp["XV10"].ToString()) : 0,
                            XT10 = lp["XT10"] != DBNull.Value ? float.Parse(lp["XT10"].ToString()) : 0,
                            XVc10 = lp["XVc10"] != DBNull.Value ? float.Parse(lp["XVc10"].ToString()) : 0,
                            CaseXV10 = lp["CaseXV10"] != DBNull.Value ? lp["CaseXV10"].ToString() : "",
                            M3XV10 = lp["M3XV10"] != DBNull.Value ? float.Parse(lp["M3XV10"].ToString()) : 0,
                            V2XV10 = lp["V2XV10"] != DBNull.Value ? float.Parse(lp["V2XV10"].ToString()) : 0,
                            TXV10 = lp["TXV10"] != DBNull.Value ? float.Parse(lp["TXV10"].ToString()) : 0,
                            PXV10 = lp["PXV10"] != DBNull.Value ? float.Parse(lp["PXV10"].ToString()) : 0,
                            SXMax = lp["SXMax"] != DBNull.Value ? float.Parse(lp["SXMax"].ToString()) : 0,
                            BXMax = lp["BXMax"] != DBNull.Value ? float.Parse(lp["BXMax "].ToString()) : 0,
                            AXMax = lp["AXMax"] != DBNull.Value ? float.Parse(lp["AXMax"].ToString()) : 0,
                            SMaxSta = lp["SMaxSta"] != DBNull.Value ? float.Parse(lp["SMaxSta"].ToString()) : 0,
                            M3XMax = lp["M3XMax"] != DBNull.Value ? float.Parse(lp["M3XMax"].ToString()) : 0,
                            V2XMax = lp["V2XMax"] != DBNull.Value ? float.Parse(lp["V2XMax"].ToString()) : 0,
                            TXMax = lp["TXMax"] != DBNull.Value ? float.Parse(lp["TXMax"].ToString()) : 0,
                            PXMax = lp["PXMax"] != DBNull.Value ? float.Parse(lp["PXMax"].ToString()) : 0,
                            XVMax = lp["XVMax"] != DBNull.Value ? float.Parse(lp["XVMax"].ToString()) : 0,
                            XTMax = lp["XTMax"] != DBNull.Value ? float.Parse(lp["XTMax"].ToString()) : 0,
                            XVcMax = lp["XVcMax"] != DBNull.Value ? float.Parse(lp["XVcMax"].ToString()) : 0,
                            VMaxSta = lp["VMaxSta"] != DBNull.Value ? float.Parse(lp["VMaxSta"].ToString()) : 0,
                            M3XVMax = lp["M3XVMax"] != DBNull.Value ? float.Parse(lp["M3XVMax"].ToString()) : 0,
                            V2XVMax = lp["V2XVMax"] != DBNull.Value ? float.Parse(lp["V2XVMax"].ToString()) : 0,
                            TXVMax = lp["TXVMax"] != DBNull.Value ? float.Parse(lp["TXVMax"].ToString()) : 0,
                            PXVMax = lp["PXVMax"] != DBNull.Value ? float.Parse(lp["PXVMax"].ToString()) : 0,
                            BL = lp["BL"] != DBNull.Value ? Convert.ToChar(lp["BL"]) : ' ',
                            BR = lp["BR"] != DBNull.Value ? Convert.ToChar(lp["BR"]) : ' ',
                            AsMin = lp["AsMin"] != DBNull.Value ? Convert.ToChar(lp["AsMin"]) : ' ',
                        });

                    }
                    ViewBag.lf = listPrevious;
                    ViewBag.Message = "Previous";
                    if (listPrevious.Count != 0)
                    {
                        Session["rid"] = listPrevious[0].ID;
                    }
                    return View(listPrevious[0]);
                    break;
                case "Save":

                    if (rc.IsCheck)
                    {
                        rc.FL = 'T';
                    }
                    else if (rc.IsCheck2)
                    {
                        rc.FR = 'T';
                    }
                    else if (rc.IsCheck3)
                    {
                        rc.Cot2 = 'T';
                    }
                    else if (rc.IsCheck4)
                    {
                        rc.Cot3 = 'T';
                    }
                    else if (rc.IsCheck5)
                    {
                        rc.Cot4 = 'T';
                    }
                    else if (rc.IsCheck6)
                    {
                        rc.Cot5 = 'T';
                    }
                    else if (rc.IsCheck7)
                    {
                        rc.Cob2 = 'T';
                    }
                    else if (rc.IsCheck8)
                    {
                        rc.Cob3 = 'T';
                    }
                    else if (rc.IsCheck9)
                    {
                        rc.Cob4 = 'T';
                    }
                    else if (rc.IsCheck10)
                    {
                        rc.Cob5 = 'T';
                    }

                    String query10 = "UPDATE RCBeam SET Ct = @Ct , Cb = @Cb , CLT = @CLT , CRT = @CRT , CL = @CL , CR = @CR , CLH = @CLH , CRH = @CRH , AXL = @AXL , FL = @FL , FR = @FR , TL = @TL , TR = @TR , N1 = @N1 , D1 = @D1 , N2 = @N2 , D2 = @D2 , L2L = @L2L , L2R = @L2R , N3 = @N3 , D3 = @D3 , L3L = @L3L , L3R = @L3R , N4 = @N4 , D4 = @D4 , L4L = @L4L , L4R = @L4R , N5 = @N5 , D5 = @D5 , L5L = @L5L , L5R = @L5R , LN1 = @LN1 , LD1 = @LD1 , LN2 = @LN2 , LD2 = @LD2 , LL2 = @LL2 , LN3 = @LN3 , LD3 = @LD3 , LL3 = @LL3 , LN4 = @LN4 , LD4 = @LD4 , LL4 = @LL4 , LN5 = @LN5 , LD5 = @LD5 , LL5 = @LL5 , RN2 = @RN2 , RD2 = @RD2 , RL2 = @RL2 , RN3 = @RN3 , RD3 = @RD3 , RL3 = @RL3 , RN4 = @RN4 , RD4 = @RD4 , RL4 = @RL4 , RN5 = @RN5 , RD5 = @RD5 , RL5 = @RL5 , N11 = @N11 , D11 = @D11 , RS2 = @RS2 , Cot2 = @Cot2 , Cot3 = @Cot3 , Cot4 = @Cot4 , Cot5 = @Cot5 , Cob2 = @Cob2 , Cob3 = @Cob3 , Cob4 = @Cob4 , Cob5 = @Cob5 , Ds = @Ds , Nso = @Nso , Nsi = @Nsi , Nslo = @Nslo , Nsli = @Nsli , Nsro = @Nsro , Nsri = @Nsri , Stln = @Stln , Stls = @Stls , Sts = @Sts , Strn = @Strn , Strs = @Strs , MEMLIST = @MEMLIST where accessFileID = " + aid + " AND Id = " + rid;
                    //String query10 = "UPDATE RCBeam SET Ct = @Ct , Cb = @Cb , CLT = @CLT , CRT = @CRT , CL = @CL , CR = @CR , CLH = @CLH , CRH = @CRH , AXL = @AXL , FL = @FL , FR = @FR , TL = @TL , TR = @TR , N1 = @N1 , D1 = @D1 , N2 = @N2 , D2 = @D2 , L2L = @L2L , L2R = @L2R , N3 = @N3 , D3 = @D3 , L3L = @L3L , L3R = @L3R , N4 = @N4 , D4 = @D4 , L4L = @L4L , L4R = @L4R , N5 = @N5 , D5 = @D5 , L5L = @L5L , L5R = @L5R , LN1 = @LN1 , LD1 = @LD1 , LN2 = @LN2 , LD2 = @LD2 , LL2 = @LL2 , LN3 = @LN3 , LD3 = @LD3 , LL3 = @LL3 , LN4 = @LN4 , LD4 = @LD4 , LL4 = @LL4 , LN5 = @LN5 , LD5 = @LD5 , LL5 = @LL5 , RN2 = @RN2 , RD2 = @RD2 , RL2 = @RL2 , RN3 = @RN3 , RD3 = @RD3 , RL3 = @RL3 , RN4 = @RN4 , RD4 = @RD4 , RL4 = @RL4 , RN5 = @RN5 , RD5 = @RD5 , RL5 = @RL5 , N11 = @N11 , D11 = @D11 , RS2 = @RS2 , Ds = @Ds , Nso = @Nso , Nsi = @Nsi , Nslo = @Nslo , Nsli = @Nsli , Nsro = @Nsro , Nsri = @Nsri , Stln = @Stln , Stls = @Stls , Sts = @Sts , Strn = @Strn , Strs = @Strs , MEMLIST = @MEMLIST where accessFileID = " + aid + " AND Id = " + rid;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.Parameters.AddWithValue("@Ct", rc.Ct);
                    sqlcomm10.Parameters.AddWithValue("@Cb", rc.Cb);
                    sqlcomm10.Parameters.AddWithValue("@CLT", rc.CLT);
                    sqlcomm10.Parameters.AddWithValue("@CRT", rc.CRT);
                    sqlcomm10.Parameters.AddWithValue("@CL", rc.CL);
                    sqlcomm10.Parameters.AddWithValue("@CR", rc.CR);
                    sqlcomm10.Parameters.AddWithValue("@CLH", rc.CLH);
                    sqlcomm10.Parameters.AddWithValue("@CRH", rc.CRH);
                    sqlcomm10.Parameters.AddWithValue("@AXL", rc.AXL);
                    sqlcomm10.Parameters.AddWithValue("@FL", rc.FL);
                    sqlcomm10.Parameters.AddWithValue("@FR", rc.FR);
                    sqlcomm10.Parameters.AddWithValue("@TL", rc.TL);
                    sqlcomm10.Parameters.AddWithValue("@TR", rc.TR);
                    sqlcomm10.Parameters.AddWithValue("@N1", rc.N1);
                    sqlcomm10.Parameters.AddWithValue("@D1", rc.D1);
                    sqlcomm10.Parameters.AddWithValue("@N2", rc.N2);
                    sqlcomm10.Parameters.AddWithValue("@D2", rc.D2);
                    sqlcomm10.Parameters.AddWithValue("@L2L", rc.L2L);
                    sqlcomm10.Parameters.AddWithValue("@L2R", rc.L2R);
                    sqlcomm10.Parameters.AddWithValue("@N3", rc.N3);
                    sqlcomm10.Parameters.AddWithValue("@D3", rc.D3);
                    sqlcomm10.Parameters.AddWithValue("@L3L", rc.L3L);
                    sqlcomm10.Parameters.AddWithValue("@L3R", rc.L3R);
                    sqlcomm10.Parameters.AddWithValue("@N4", rc.N4);
                    sqlcomm10.Parameters.AddWithValue("@D4", rc.D4);
                    sqlcomm10.Parameters.AddWithValue("@L4L", rc.L4L);
                    sqlcomm10.Parameters.AddWithValue("@L4R", rc.L4R);
                    sqlcomm10.Parameters.AddWithValue("@N5", rc.N5);
                    sqlcomm10.Parameters.AddWithValue("@D5", rc.D5);
                    sqlcomm10.Parameters.AddWithValue("@L5L", rc.L5L);
                    sqlcomm10.Parameters.AddWithValue("@L5R", rc.L5R);
                    sqlcomm10.Parameters.AddWithValue("@LN1", rc.LN1);
                    sqlcomm10.Parameters.AddWithValue("@LD1", rc.LD1);
                    sqlcomm10.Parameters.AddWithValue("@LN2", rc.LN2);
                    sqlcomm10.Parameters.AddWithValue("@LD2", rc.LD2);
                    sqlcomm10.Parameters.AddWithValue("@LL2", rc.LL2);
                    sqlcomm10.Parameters.AddWithValue("@LN3", rc.LN3);
                    sqlcomm10.Parameters.AddWithValue("@LD3", rc.LD3);
                    sqlcomm10.Parameters.AddWithValue("@LL3", rc.LL3);
                    sqlcomm10.Parameters.AddWithValue("@LN4", rc.LN4);
                    sqlcomm10.Parameters.AddWithValue("@LD4", rc.LD4);
                    sqlcomm10.Parameters.AddWithValue("@LL4", rc.LL4);
                    sqlcomm10.Parameters.AddWithValue("@LN5", rc.LN5);
                    sqlcomm10.Parameters.AddWithValue("@LD5", rc.LD5);
                    sqlcomm10.Parameters.AddWithValue("@LL5", rc.LL5);
                    sqlcomm10.Parameters.AddWithValue("@RN2", rc.RN2);
                    sqlcomm10.Parameters.AddWithValue("@RD2", rc.RD2);
                    sqlcomm10.Parameters.AddWithValue("@RL2", rc.RL2);
                    sqlcomm10.Parameters.AddWithValue("@RN3", rc.RN3);
                    sqlcomm10.Parameters.AddWithValue("@RD3", rc.RD3);
                    sqlcomm10.Parameters.AddWithValue("@RL3", rc.RL3);
                    sqlcomm10.Parameters.AddWithValue("@RN4", rc.RN4);
                    sqlcomm10.Parameters.AddWithValue("@RD4", rc.RD4);
                    sqlcomm10.Parameters.AddWithValue("@RL4", rc.RL4);
                    sqlcomm10.Parameters.AddWithValue("@RN5", rc.RN5);
                    sqlcomm10.Parameters.AddWithValue("@RD5", rc.RD5);
                    sqlcomm10.Parameters.AddWithValue("@RL5", rc.RL5);
                    sqlcomm10.Parameters.AddWithValue("@N11", rc.N11);
                    sqlcomm10.Parameters.AddWithValue("@D11", rc.D11);
                    sqlcomm10.Parameters.AddWithValue("@RS2", rc.RS2);
                    sqlcomm10.Parameters.AddWithValue("@Cot2", rc.Cot2);
                    sqlcomm10.Parameters.AddWithValue("@Cot3", rc.Cot3);
                    sqlcomm10.Parameters.AddWithValue("@Cot4", rc.Cot4);
                    sqlcomm10.Parameters.AddWithValue("@Cot5", rc.Cot5);
                    sqlcomm10.Parameters.AddWithValue("@Cob2", rc.Cob2);
                    sqlcomm10.Parameters.AddWithValue("@Cob3", rc.Cob3);
                    sqlcomm10.Parameters.AddWithValue("@Cob4", rc.Cob4);
                    sqlcomm10.Parameters.AddWithValue("@Cob5", rc.Cob5);
                    sqlcomm10.Parameters.AddWithValue("@Ds", rc.Ds);
                    sqlcomm10.Parameters.AddWithValue("@Nso", rc.Nso);
                    sqlcomm10.Parameters.AddWithValue("@Nsi", rc.Nsi);
                    sqlcomm10.Parameters.AddWithValue("@Nslo", rc.Nslo);
                    sqlcomm10.Parameters.AddWithValue("@Nsli", rc.Nsli);
                    sqlcomm10.Parameters.AddWithValue("@Nsro", rc.Nsro);
                    sqlcomm10.Parameters.AddWithValue("@Nsri", rc.Nsri);
                    sqlcomm10.Parameters.AddWithValue("@Stln", rc.Stln);
                    sqlcomm10.Parameters.AddWithValue("@Stls", rc.Stls);
                    sqlcomm10.Parameters.AddWithValue("@Sts", rc.Sts);
                    sqlcomm10.Parameters.AddWithValue("@Strn", rc.Strn);
                    sqlcomm10.Parameters.AddWithValue("@Strs", rc.Strs);
                    sqlcomm10.Parameters.AddWithValue("@MEMLIST", rc.MEMLIST);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    ViewBag.Message = "save";
                    return RedirectToAction("ShowformDesign", "Home");
                    break;
                case "Design":
                    List<RC_Beam> listdesign = new List<RC_Beam>();
                    String query11 = "select * from RCBeam where accessFileID = " + aid + " AND Id = " + rid;
                    SqlCommand sqlcomm11 = new SqlCommand(query11, sqlconn);
                    SqlDataReader ld = sqlcomm11.ExecuteReader();
                    RC_Beam m = new RC_Beam();
                    while (ld.Read())
                    {
                        m.Mem = Convert.ToInt32(ld["Mem"]);
                        m.ID = Convert.ToInt32(ld["Id"]);
                        m.B = ld["B"].ToString();
                        m.T = ld["T"].ToString();
                        m.Ct = Convert.ToInt32(ld["Ct"]);
                        m.Cb = Convert.ToInt32(ld["Cb"]);
                        m.CLT = Convert.ToChar(ld["CLT"]);
                        m.CRT = Convert.ToChar(ld["CRT"]);
                        m.CL = Convert.ToChar(ld["CL"]);
                        m.CR = Convert.ToChar(ld["CR"]);
                        m.CLH = Convert.ToChar(ld["CLH"]);
                        m.CRH = Convert.ToChar(ld["CRH"]);
                        m.AXL = Convert.ToChar(ld["AXL"]);
                        m.FL = Convert.ToChar(ld["FL"]);
                        m.FR = Convert.ToChar(ld["FR"]);
                        m.TL = Convert.ToInt32(ld["TL"]);
                        m.TR = Convert.ToInt32(ld["TR"]);
                        m.N1 = Convert.ToInt32(ld["N1"]);
                        m.D1 = Convert.ToInt32(ld["D1"]);
                        m.N2 = Convert.ToInt32(ld["N2"]);
                        m.D2 = Convert.ToInt32(ld["D2"]);
                        m.L2L = Convert.ToInt32(ld["L2L"]);
                        m.L2R = Convert.ToInt32(ld["L2R"]);
                        m.N3 = Convert.ToInt32(ld["N3"]);
                        m.D3 = Convert.ToInt32(ld["D3"]);
                        m.L3L = Convert.ToInt32(ld["L3L"]);
                        m.L3R = Convert.ToInt32(ld["L3R"]);
                        m.N4 = Convert.ToInt32(ld["N4"]);
                        m.D4 = Convert.ToInt32(ld["D4"]);
                        m.L4L = Convert.ToInt32(ld["L4L"]);
                        m.L4R = Convert.ToInt32(ld["L4R"]);
                        m.N5 = Convert.ToInt32(ld["N5"]);
                        m.D5 = Convert.ToInt32(ld["D5"]);
                        m.L5L = Convert.ToInt32(ld["L5L"]);
                        m.L5R = Convert.ToInt32(ld["L5R"]);
                        m.LN1 = Convert.ToInt32(ld["LN1"]);
                        m.LD1 = Convert.ToInt32(ld["LD1"]);
                        m.LN2 = Convert.ToInt32(ld["LN2"]);
                        m.LD2 = Convert.ToInt32(ld["LD2"]);
                        m.LL2 = Convert.ToInt32(ld["LL2"]);
                        m.LN3 = Convert.ToInt32(ld["LN3"]);
                        m.LD3 = Convert.ToInt32(ld["LD3"]);
                        m.LL3 = Convert.ToInt32(ld["LL3"]);
                        m.LN4 = Convert.ToInt32(ld["LN4"]);
                        m.LD4 = Convert.ToInt32(ld["LD4"]);
                        m.LL4 = Convert.ToInt32(ld["LL4"]);
                        m.LN5 = Convert.ToInt32(ld["LN5"]);
                        m.LD5 = Convert.ToInt32(ld["LD5"]);
                        m.LL5 = Convert.ToInt32(ld["LL5"]);
                        m.RN2 = Convert.ToInt32(ld["RN2"]);
                        m.RD2 = Convert.ToInt32(ld["RD2"]);
                        m.RL2 = Convert.ToInt32(ld["RL2"]);
                        m.RN3 = Convert.ToInt32(ld["RN3"]);
                        m.RD3 = Convert.ToInt32(ld["RD3"]);
                        m.RL3 = Convert.ToInt32(ld["RL3"]);
                        m.RN4 = Convert.ToInt32(ld["RN4"]);
                        m.RD4 = Convert.ToInt32(ld["RD4"]);
                        m.RL4 = Convert.ToInt32(ld["RL4"]);
                        m.RN5 = Convert.ToInt32(ld["RN5"]);
                        m.RD5 = Convert.ToInt32(ld["RD5"]);
                        m.RL5 = Convert.ToInt32(ld["RL5"]);
                        m.N11 = Convert.ToInt32(ld["N11"]);
                        m.D11 = Convert.ToInt32(ld["D11"]);
                        m.RS2 = Convert.ToInt32(ld["RS2"]);
                        m.Cot2 = Convert.ToChar(ld["Cot2"]);
                        m.Cot3 = Convert.ToChar(ld["Cot3"]);
                        m.Cot4 = Convert.ToChar(ld["Cot4"]);
                        m.Cot5 = Convert.ToChar(ld["Cot5"]);
                        m.Cob2 = Convert.ToChar(ld["Cob2"]);
                        m.Cob3 = Convert.ToChar(ld["Cob3"]);
                        m.Cob4 = Convert.ToChar(ld["Cob4"]);
                        m.Cob5 = Convert.ToChar(ld["Cob5"]);
                        m.Ds = Convert.ToInt32(ld["Ds"]);
                        m.Nso = Convert.ToInt32(ld["Nso"]);
                        m.Nsi = Convert.ToInt32(ld["Nsi"]);
                        m.Nslo = Convert.ToInt32(ld["Nslo"]);
                        m.Nsli = Convert.ToInt32(ld["Nsli"]);
                        m.Nsro = Convert.ToInt32(ld["Nsro"]);
                        m.Nsri = Convert.ToInt32(ld["Nsri"]);
                        m.Stln = Convert.ToInt32(ld["Stln"]);
                        m.Stls = Convert.ToInt32(ld["Stls"]);
                        m.Sts = Convert.ToInt32(ld["Sts"]);
                        m.Strn = Convert.ToInt32(ld["Strn"]);
                        m.Strs = Convert.ToInt32(ld["Strs"]);
                        m.MEMLIST = ld["MEMLIST"].ToString();
                        listdesign.Add(new RC_Beam
                        {
                            Mem = Convert.ToInt32(ld["Mem"]),
                            ID = Convert.ToInt32(ld["Id"]),
                            B = ld["B"].ToString(),
                            T = ld["T"].ToString(),
                            Ct = Convert.ToInt32(ld["Ct"]),
                            Cb = Convert.ToInt32(ld["Cb"]),
                            CLT = Convert.ToChar(ld["CLT"]),
                            CRT = Convert.ToChar(ld["CRT"]),
                            CL = Convert.ToChar(ld["CL"]),
                            CR = Convert.ToChar(ld["CR"]),
                            CLH = Convert.ToChar(ld["CLH"]),
                            CRH = Convert.ToChar(ld["CRH"]),
                            AXL = Convert.ToChar(ld["AXL"]),
                            FL = Convert.ToChar(ld["FL"]),
                            FR = Convert.ToChar(ld["FR"]),
                            TL = Convert.ToInt32(ld["TL"]),
                            TR = Convert.ToInt32(ld["TR"]),
                            N1 = Convert.ToInt32(ld["N1"]),
                            D1 = Convert.ToInt32(ld["D1"]),
                            N2 = Convert.ToInt32(ld["N2"]),
                            D2 = Convert.ToInt32(ld["D2"]),
                            L2L = Convert.ToInt32(ld["L2L"]),
                            L2R = Convert.ToInt32(ld["L2R"]),
                            N3 = Convert.ToInt32(ld["N3"]),
                            D3 = Convert.ToInt32(ld["D3"]),
                            L3L = Convert.ToInt32(ld["L3L"]),
                            L3R = Convert.ToInt32(ld["L3R"]),
                            N4 = Convert.ToInt32(ld["N4"]),
                            D4 = Convert.ToInt32(ld["D4"]),
                            L4L = Convert.ToInt32(ld["L4L"]),
                            L4R = Convert.ToInt32(ld["L4R"]),
                            N5 = Convert.ToInt32(ld["N5"]),
                            D5 = Convert.ToInt32(ld["D5"]),
                            L5L = Convert.ToInt32(ld["L5L"]),
                            L5R = Convert.ToInt32(ld["L5R"]),
                            LN1 = Convert.ToInt32(ld["LN1"]),
                            LD1 = Convert.ToInt32(ld["LD1"]),
                            LN2 = Convert.ToInt32(ld["LN2"]),
                            LD2 = Convert.ToInt32(ld["LD2"]),
                            LL2 = Convert.ToInt32(ld["LL2"]),
                            LN3 = Convert.ToInt32(ld["LN3"]),
                            LD3 = Convert.ToInt32(ld["LD3"]),
                            LL3 = Convert.ToInt32(ld["LL3"]),
                            LN4 = Convert.ToInt32(ld["LN4"]),
                            LD4 = Convert.ToInt32(ld["LD4"]),
                            LL4 = Convert.ToInt32(ld["LL4"]),
                            LN5 = Convert.ToInt32(ld["LN5"]),
                            LD5 = Convert.ToInt32(ld["LD5"]),
                            LL5 = Convert.ToInt32(ld["LL5"]),
                            RN2 = Convert.ToInt32(ld["RN2"]),
                            RD2 = Convert.ToInt32(ld["RD2"]),
                            RL2 = Convert.ToInt32(ld["RL2"]),
                            RN3 = Convert.ToInt32(ld["RN3"]),
                            RD3 = Convert.ToInt32(ld["RD3"]),
                            RL3 = Convert.ToInt32(ld["RL3"]),
                            RN4 = Convert.ToInt32(ld["RN4"]),
                            RD4 = Convert.ToInt32(ld["RD4"]),
                            RL4 = Convert.ToInt32(ld["RL4"]),
                            RN5 = Convert.ToInt32(ld["RN5"]),
                            RD5 = Convert.ToInt32(ld["RD5"]),
                            RL5 = Convert.ToInt32(ld["RL5"]),
                            N11 = Convert.ToInt32(ld["N11"]),
                            D11 = Convert.ToInt32(ld["D11"]),
                            RS2 = Convert.ToInt32(ld["RS2"]),
                            Cot2 = Convert.ToChar(ld["Cot2"]),
                            Cot3 = Convert.ToChar(ld["Cot3"]),
                            Cot4 = Convert.ToChar(ld["Cot4"]),
                            Cot5 = Convert.ToChar(ld["Cot5"]),
                            Cob2 = Convert.ToChar(ld["Cob2"]),
                            Cob3 = Convert.ToChar(ld["Cob3"]),
                            Cob4 = Convert.ToChar(ld["Cob4"]),
                            Cob5 = Convert.ToChar(ld["Cob5"]),
                            Ds = Convert.ToInt32(ld["Ds"]),
                            Nso = Convert.ToInt32(ld["Nso"]),
                            Nsi = Convert.ToInt32(ld["Nsi"]),
                            Nslo = Convert.ToInt32(ld["Nslo"]),
                            Nsli = Convert.ToInt32(ld["Nsli"]),
                            Nsro = Convert.ToInt32(ld["Nsro"]),
                            Nsri = Convert.ToInt32(ld["Nsri"]),
                            Stln = Convert.ToInt32(ld["Stln"]),
                            Stls = Convert.ToInt32(ld["Stls"]),
                            Sts = Convert.ToInt32(ld["Sts"]),
                            Strn = Convert.ToInt32(ld["Strn"]),
                            Strs = Convert.ToInt32(ld["Strs"]),
                            MEMLIST = ld["MEMLIST"].ToString()
                        });
                    }
                    ViewBag.lf = listdesign;
                    return View(listdesign[0]);
                    break;
            }
            return View(rc);
        }
        public ActionResult newPJ()
        {
            return View();
        }

        [HttpPost]
        public ActionResult newPJ(Project nP)
        {
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            string sqlquery1 = "insert into Project(projectName,Uid) values(@pName , @Uid)";
            SqlCommand sqlcomm1 = new SqlCommand(sqlquery1, sqlconn);
            sqlconn.Open();
            sqlcomm1.Parameters.AddWithValue("@pName", nP.projectName);
            sqlcomm1.Parameters.AddWithValue("@Uid", Session["uid"]);
            sqlcomm1.ExecuteNonQuery();
            return RedirectToAction("MyProject", "Home");
        }

        public ActionResult Strength()
        {
            int aid = Convert.ToInt32(Session["aid"]);
            List<Strength> str = new List<Strength>();
            List<Strength> str2 = new List<Strength>();
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            String q5 = "select * from Strength where accessFileID = " + aid;
            SqlCommand sqlc5 = new SqlCommand(q5, sqlconn);
            SqlDataReader cf = sqlc5.ExecuteReader();
            while (cf.Read())
            {
                str.Add(new Strength
                {
                    fcp = float.Parse(cf["fcp"].ToString()),
                    fy = float.Parse(cf["fy"].ToString()),
                    fvyr = float.Parse(cf["fvyr"].ToString()),
                    fvyd = float.Parse(cf["fvyd"].ToString()),
                    ec = decimal.Parse(cf["ec"].ToString()),
                    es = float.Parse(cf["es"].ToString()),
                    utf = decimal.Parse(cf["utf"].ToString()),
                });
                ViewBag.st = str;
            }
            sqlconn.Close();
            for (int i = 0; i <= str.Count; i++)
            {
                if (str.Count > 0)
                {
                    return View();
                }
                else
                {
                    string sqlquery1 = "insert into Strength(accessFileID,fcp,fy,fvyr,fvyd,es,ec,utf) values(@aid,@fcp,@fy,@fvyr,@fvyd,@es,@ec,@utf)";
                    SqlCommand sqlcomm1 = new SqlCommand(sqlquery1, sqlconn);
                    sqlconn.Open();
                    sqlcomm1.Parameters.AddWithValue("@aid", aid);
                    sqlcomm1.Parameters.AddWithValue("@fcp", 240);
                    sqlcomm1.Parameters.AddWithValue("@fy", 4000);
                    sqlcomm1.Parameters.AddWithValue("@fvyr", 2400);
                    sqlcomm1.Parameters.AddWithValue("@fvyd", 4000);
                    sqlcomm1.Parameters.AddWithValue("@es", 2039000);
                    sqlcomm1.Parameters.AddWithValue("@ec", Math.Sqrt(240) * 15120);
                    sqlcomm1.Parameters.AddWithValue("@utf", 1.00);
                    sqlcomm1.ExecuteNonQuery();
                    sqlconn.Close();
                    sqlconn.Open();
                    String q6 = "select * from Strength where accessFileID = " + aid;
                    SqlCommand sqlc6 = new SqlCommand(q6, sqlconn);
                    SqlDataReader c6 = sqlc6.ExecuteReader();
                    while (c6.Read())
                    {
                        str2.Add(new Strength
                        {
                            fcp = float.Parse(c6["fcp"].ToString()),
                            fy = float.Parse(c6["fy"].ToString()),
                            fvyr = float.Parse(c6["fvyr"].ToString()),
                            fvyd = float.Parse(c6["fvyd"].ToString()),
                            ec = decimal.Parse(c6["ec"].ToString()),
                            es = float.Parse(c6["es"].ToString()),
                            utf = decimal.Parse(c6["utf"].ToString()),
                        });
                        ViewBag.st = str2;
                    }
                    sqlconn.Close();
                    return View();
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult SaveStrength(Strength st)
        {
            int aid = Convert.ToInt32(Session["aid"]);
            st.ec = decimal.Parse((Math.Sqrt(st.fcp) * 15120).ToString());
            List<Strength> str = new List<Strength>();
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            String queryUpdate = "UPDATE Strength SET fcp = @fcp , fy = @fy , fvyr = @fvyr , es = @es , ec = @ec , utf = @utf where accessFileID =" + aid;
            SqlCommand sqlcommUp = new SqlCommand(queryUpdate, sqlconn);
            sqlcommUp.Parameters.AddWithValue("@fcp", st.fcp);
            sqlcommUp.Parameters.AddWithValue("@fy", st.fy);
            sqlcommUp.Parameters.AddWithValue("@fvyr", st.fvyr);
            sqlcommUp.Parameters.AddWithValue("@fvyd", st.fvyd);
            sqlcommUp.Parameters.AddWithValue("@es", st.es);
            sqlcommUp.Parameters.AddWithValue("@ec", st.ec);
            sqlcommUp.Parameters.AddWithValue("@utf", st.utf);
            sqlcommUp.ExecuteNonQuery();
            sqlconn.Close();
            return Json(new { st = st });
        }

        [HttpPost]
        public ActionResult Design(RC_Beam formContainer)
        {
            this.Error = 0;
            int aid = Convert.ToInt32(Session["aid"]);
            List<Connectivity___Frame> MemGT = new List<Connectivity___Frame>();
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            String q5 = "select * from ConnectivityFrame where accessFileID = " + aid;
            SqlCommand sqlc5 = new SqlCommand(q5, sqlconn);
            SqlDataReader cf = sqlc5.ExecuteReader();
            while (cf.Read())
            {
                MemGT.Add(new Connectivity___Frame
                {
                    Frame = Convert.ToInt32(cf["Frame"]),
                    JointI = float.Parse(cf["JointI"].ToString()),
                    JointJ = float.Parse(cf["JointJ"].ToString()),
                    Length = float.Parse(cf["Length"].ToString())
                });
                TempData["MemGT"] = MemGT;
            }
            sqlconn.Close();
            string MemL = formContainer.MEMLIST;
            sqlconn.Open();
            int rid = Convert.ToInt32(Session["rid"]);
            int check6 = 0;
            Single[,] memlist;
            memlist = FindMemList(MemL);
            if (memlist.GetUpperBound(0) != 0)
            {
                check6 = CheckMemList(memlist);
            }

            Single Lt;
            Lt = FindLt(memlist);
            String query10 = "UPDATE RCBeam SET Lt = @Lt where accessFileID = " + aid + " AND Id = " + rid;
            SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
            sqlcomm10.Parameters.AddWithValue("@Lt", Lt);
            sqlcomm10.ExecuteNonQuery();
            sqlconn.Close();

            Single[,] Steel;
            Steel = FindSteel(formContainer);
            int[] ShearSteel;
            ShearSteel = FindShearSteel(formContainer);
            int dd;
            dd = Finddd(Steel, formContainer);
            Single[] Lenextra;
            Lenextra = CheckLenExtra(Lt, dd, formContainer);
            if (Lenextra[0] == -1)
            {
                return Json(new { Lenexx = Lenextra[0] });
            }
            else
            {
                Putdata(formContainer);
                FindSecFor();
                int[] Secc;
                Secc = addrow(dd, Lenextra, Lt);
                Strength str = new Strength();
                sqlconn.Open();
                String qst = "select * from Strength where accessFileID = " + aid;
                SqlCommand sqlcst = new SqlCommand(qst, sqlconn);
                SqlDataReader cst = sqlcst.ExecuteReader();
                while (cst.Read())
                {
                    str.fcp = float.Parse(cst["fcp"].ToString());
                    str.fy = float.Parse(cst["fy"].ToString());
                    str.fvyr = float.Parse(cst["fvyr"].ToString());
                    str.fvyd = float.Parse(cst["fvyd"].ToString());
                    str.ec = decimal.Parse(cst["ec"].ToString());
                    str.es = float.Parse(cst["es"].ToString());
                    str.utf = decimal.Parse(cst["utf"].ToString());
                }
                sqlconn.Close();
                Single[,] DevLen;
                DevLen = FindDevelopmentLength(str.fy, str.fcp, Steel);
                Lt = Lt * 1000;
                DoStress(DevLen, Steel, ShearSteel, Lenextra, Lt, Secc, str.fcp, str.fy, str.fvyr, str.fvyd, str.es, formContainer);
                Single[,] Results;
                Results = FindMaxStress(formContainer);
                fillSheet(Results, rid, formContainer.checkDesignAll, str.utf);

                return Json(new { checkk = check6, Lenexx = Lenextra[0], results = Results, chkDeAll = formContainer.checkDesignAll, utf = str.utf });
            }

            //return Json(new { checkk = check6, Lenexx = Lenextra[0] });
        }

        public void Putdata(RC_Beam formContainer)
        {
            int rid = Convert.ToInt32(Session["rid"]);
            int aid = Convert.ToInt32(Session["aid"]);
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();

            if (formContainer.IsCheck)
            {
                formContainer.FL = 'T';
                formContainer.CL = 'F';
                formContainer.CLT = 'F';
            }
            else
            {
                formContainer.FL = ' ';
            }
            if (formContainer.IsCheck2)
            {
                formContainer.FR = 'T';
                formContainer.CR = 'F';
                formContainer.CRT = 'F';
            }
            else
            {
                formContainer.FR = ' ';
            }
            if (formContainer.IsCheck3)
            {
                formContainer.Cot2 = 'T';
            }
            else
            {
                formContainer.Cot2 = ' ';
            }
            if (formContainer.IsCheck4)
            {
                formContainer.Cot3 = 'T';
            }
            else
            {
                formContainer.Cot3 = ' ';
            }
            if (formContainer.IsCheck5)
            {
                formContainer.Cot4 = 'T';
            }
            else
            {
                formContainer.Cot4 = ' ';
            }
            if (formContainer.IsCheck6)
            {
                formContainer.Cot5 = 'T';
            }
            else
            {
                formContainer.Cot5 = ' ';
            }
            if (formContainer.IsCheck7)
            {
                formContainer.Cob2 = 'T';
            }
            else
            {
                formContainer.Cob2 = ' ';
            }
            if (formContainer.IsCheck8)
            {
                formContainer.Cob3 = 'T';
            }
            else
            {
                formContainer.Cob3 = ' ';
            }
            if (formContainer.IsCheck9)
            {
                formContainer.Cob4 = 'T';
            }
            else
            {
                formContainer.Cob4 = ' ';
            }
            if (formContainer.IsCheck10)
            {
                formContainer.Cob5 = 'T';
            }
            else
            {
                formContainer.Cob5 = ' ';
            }

            String query10 = "UPDATE RCBeam SET Ct = @Ct , Cb = @Cb , CLT = @CLT , CRT = @CRT , CL = @CL , CR = @CR , CLH = @CLH , CRH = @CRH , AXL = @AXL , FL = @FL , FR = @FR , TL = @TL , TR = @TR , N1 = @N1 , D1 = @D1 , N2 = @N2 , D2 = @D2 , L2L = @L2L , L2R = @L2R , N3 = @N3 , D3 = @D3 , L3L = @L3L , L3R = @L3R , N4 = @N4 , D4 = @D4 , L4L = @L4L , L4R = @L4R , N5 = @N5 , D5 = @D5 , L5L = @L5L , L5R = @L5R , LN1 = @LN1 , LD1 = @LD1 , LN2 = @LN2 , LD2 = @LD2 , LL2 = @LL2 , LN3 = @LN3 , LD3 = @LD3 , LL3 = @LL3 , LN4 = @LN4 , LD4 = @LD4 , LL4 = @LL4 , LN5 = @LN5 , LD5 = @LD5 , LL5 = @LL5 , RN2 = @RN2 , RD2 = @RD2 , RL2 = @RL2 , RN3 = @RN3 , RD3 = @RD3 , RL3 = @RL3 , RN4 = @RN4 , RD4 = @RD4 , RL4 = @RL4 , RN5 = @RN5 , RD5 = @RD5 , RL5 = @RL5 , N11 = @N11 , D11 = @D11 , RS2 = @RS2 , Cot2 = @Cot2 , Cot3 = @Cot3 , Cot4 = @Cot4 , Cot5 = @Cot5 , Cob2 = @Cob2 , Cob3 = @Cob3 , Cob4 = @Cob4 , Cob5 = @Cob5 , Ds = @Ds , Nso = @Nso , Nsi = @Nsi , Nslo = @Nslo , Nsli = @Nsli , Nsro = @Nsro , Nsri = @Nsri , Stln = @Stln , Stls = @Stls , Sts = @Sts , Strn = @Strn , Strs = @Strs , MEMLIST = @MEMLIST where accessFileID = " + aid + " AND Id = " + rid;
            //String query10 = "UPDATE RCBeam SET Ct = @Ct , Cb = @Cb , CLT = @CLT , CRT = @CRT , CL = @CL , CR = @CR , CLH = @CLH , CRH = @CRH , AXL = @AXL , FL = @FL , FR = @FR , TL = @TL , TR = @TR , N1 = @N1 , D1 = @D1 , N2 = @N2 , D2 = @D2 , L2L = @L2L , L2R = @L2R , N3 = @N3 , D3 = @D3 , L3L = @L3L , L3R = @L3R , N4 = @N4 , D4 = @D4 , L4L = @L4L , L4R = @L4R , N5 = @N5 , D5 = @D5 , L5L = @L5L , L5R = @L5R , LN1 = @LN1 , LD1 = @LD1 , LN2 = @LN2 , LD2 = @LD2 , LL2 = @LL2 , LN3 = @LN3 , LD3 = @LD3 , LL3 = @LL3 , LN4 = @LN4 , LD4 = @LD4 , LL4 = @LL4 , LN5 = @LN5 , LD5 = @LD5 , LL5 = @LL5 , RN2 = @RN2 , RD2 = @RD2 , RL2 = @RL2 , RN3 = @RN3 , RD3 = @RD3 , RL3 = @RL3 , RN4 = @RN4 , RD4 = @RD4 , RL4 = @RL4 , RN5 = @RN5 , RD5 = @RD5 , RL5 = @RL5 , N11 = @N11 , D11 = @D11 , RS2 = @RS2 , Ds = @Ds , Nso = @Nso , Nsi = @Nsi , Nslo = @Nslo , Nsli = @Nsli , Nsro = @Nsro , Nsri = @Nsri , Stln = @Stln , Stls = @Stls , Sts = @Sts , Strn = @Strn , Strs = @Strs , MEMLIST = @MEMLIST where accessFileID = " + aid + " AND Id = " + rid;
            SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
            sqlcomm10.Parameters.AddWithValue("@Ct", formContainer.Ct);
            sqlcomm10.Parameters.AddWithValue("@Cb", formContainer.Cb);
            sqlcomm10.Parameters.AddWithValue("@CLT", formContainer.CLT);
            sqlcomm10.Parameters.AddWithValue("@CRT", formContainer.CRT);
            sqlcomm10.Parameters.AddWithValue("@CL", formContainer.CL);
            sqlcomm10.Parameters.AddWithValue("@CR", formContainer.CR);
            sqlcomm10.Parameters.AddWithValue("@CLH", formContainer.CLH);
            sqlcomm10.Parameters.AddWithValue("@CRH", formContainer.CRH);
            sqlcomm10.Parameters.AddWithValue("@AXL", formContainer.AXL);
            sqlcomm10.Parameters.AddWithValue("@FL", formContainer.FL);
            sqlcomm10.Parameters.AddWithValue("@FR", formContainer.FR);
            sqlcomm10.Parameters.AddWithValue("@TL", formContainer.TL);
            sqlcomm10.Parameters.AddWithValue("@TR", formContainer.TR);
            sqlcomm10.Parameters.AddWithValue("@N1", formContainer.N1);
            sqlcomm10.Parameters.AddWithValue("@D1", formContainer.D1);
            sqlcomm10.Parameters.AddWithValue("@N2", formContainer.N2);
            sqlcomm10.Parameters.AddWithValue("@D2", formContainer.D2);
            sqlcomm10.Parameters.AddWithValue("@L2L", formContainer.L2L);
            sqlcomm10.Parameters.AddWithValue("@L2R", formContainer.L2R);
            sqlcomm10.Parameters.AddWithValue("@N3", formContainer.N3);
            sqlcomm10.Parameters.AddWithValue("@D3", formContainer.D3);
            sqlcomm10.Parameters.AddWithValue("@L3L", formContainer.L3L);
            sqlcomm10.Parameters.AddWithValue("@L3R", formContainer.L3R);
            sqlcomm10.Parameters.AddWithValue("@N4", formContainer.N4);
            sqlcomm10.Parameters.AddWithValue("@D4", formContainer.D4);
            sqlcomm10.Parameters.AddWithValue("@L4L", formContainer.L4L);
            sqlcomm10.Parameters.AddWithValue("@L4R", formContainer.L4R);
            sqlcomm10.Parameters.AddWithValue("@N5", formContainer.N5);
            sqlcomm10.Parameters.AddWithValue("@D5", formContainer.D5);
            sqlcomm10.Parameters.AddWithValue("@L5L", formContainer.L5L);
            sqlcomm10.Parameters.AddWithValue("@L5R", formContainer.L5R);
            sqlcomm10.Parameters.AddWithValue("@LN1", formContainer.LN1);
            sqlcomm10.Parameters.AddWithValue("@LD1", formContainer.LD1);
            sqlcomm10.Parameters.AddWithValue("@LN2", formContainer.LN2);
            sqlcomm10.Parameters.AddWithValue("@LD2", formContainer.LD2);
            sqlcomm10.Parameters.AddWithValue("@LL2", formContainer.LL2);
            sqlcomm10.Parameters.AddWithValue("@LN3", formContainer.LN3);
            sqlcomm10.Parameters.AddWithValue("@LD3", formContainer.LD3);
            sqlcomm10.Parameters.AddWithValue("@LL3", formContainer.LL3);
            sqlcomm10.Parameters.AddWithValue("@LN4", formContainer.LN4);
            sqlcomm10.Parameters.AddWithValue("@LD4", formContainer.LD4);
            sqlcomm10.Parameters.AddWithValue("@LL4", formContainer.LL4);
            sqlcomm10.Parameters.AddWithValue("@LN5", formContainer.LN5);
            sqlcomm10.Parameters.AddWithValue("@LD5", formContainer.LD5);
            sqlcomm10.Parameters.AddWithValue("@LL5", formContainer.LL5);
            sqlcomm10.Parameters.AddWithValue("@RN2", formContainer.RN2);
            sqlcomm10.Parameters.AddWithValue("@RD2", formContainer.RD2);
            sqlcomm10.Parameters.AddWithValue("@RL2", formContainer.RL2);
            sqlcomm10.Parameters.AddWithValue("@RN3", formContainer.RN3);
            sqlcomm10.Parameters.AddWithValue("@RD3", formContainer.RD3);
            sqlcomm10.Parameters.AddWithValue("@RL3", formContainer.RL3);
            sqlcomm10.Parameters.AddWithValue("@RN4", formContainer.RN4);
            sqlcomm10.Parameters.AddWithValue("@RD4", formContainer.RD4);
            sqlcomm10.Parameters.AddWithValue("@RL4", formContainer.RL4);
            sqlcomm10.Parameters.AddWithValue("@RN5", formContainer.RN5);
            sqlcomm10.Parameters.AddWithValue("@RD5", formContainer.RD5);
            sqlcomm10.Parameters.AddWithValue("@RL5", formContainer.RL5);
            sqlcomm10.Parameters.AddWithValue("@N11", formContainer.N11);
            sqlcomm10.Parameters.AddWithValue("@D11", formContainer.D11);
            sqlcomm10.Parameters.AddWithValue("@RS2", formContainer.RS2);
            sqlcomm10.Parameters.AddWithValue("@Cot2", formContainer.Cot2);
            sqlcomm10.Parameters.AddWithValue("@Cot3", formContainer.Cot3);
            sqlcomm10.Parameters.AddWithValue("@Cot4", formContainer.Cot4);
            sqlcomm10.Parameters.AddWithValue("@Cot5", formContainer.Cot5);
            sqlcomm10.Parameters.AddWithValue("@Cob2", formContainer.Cob2);
            sqlcomm10.Parameters.AddWithValue("@Cob3", formContainer.Cob3);
            sqlcomm10.Parameters.AddWithValue("@Cob4", formContainer.Cob4);
            sqlcomm10.Parameters.AddWithValue("@Cob5", formContainer.Cob5);
            sqlcomm10.Parameters.AddWithValue("@Ds", formContainer.Ds);
            sqlcomm10.Parameters.AddWithValue("@Nso", formContainer.Nso);
            sqlcomm10.Parameters.AddWithValue("@Nsi", formContainer.Nsi);
            sqlcomm10.Parameters.AddWithValue("@Nslo", formContainer.Nslo);
            sqlcomm10.Parameters.AddWithValue("@Nsli", formContainer.Nsli);
            sqlcomm10.Parameters.AddWithValue("@Nsro", formContainer.Nsro);
            sqlcomm10.Parameters.AddWithValue("@Nsri", formContainer.Nsri);
            sqlcomm10.Parameters.AddWithValue("@Stln", formContainer.Stln);
            sqlcomm10.Parameters.AddWithValue("@Stls", formContainer.Stls);
            sqlcomm10.Parameters.AddWithValue("@Sts", formContainer.Sts);
            sqlcomm10.Parameters.AddWithValue("@Strn", formContainer.Strn);
            sqlcomm10.Parameters.AddWithValue("@Strs", formContainer.Strs);
            sqlcomm10.Parameters.AddWithValue("@MEMLIST", formContainer.MEMLIST);
            sqlcomm10.ExecuteNonQuery();
            sqlconn.Close();
            ViewBag.Message = "save";
        }

        public void DoStress(Single[,] DevLen, Single[,] Steel, int[] ShearSteel, Single[] Lenextra, Single Lt, int[] Sec, Single fcp, Single fy, Single fvyr, Single fvyd, Single es, RC_Beam formContainer)
        {

            DesignSheet Ds = new DesignSheet();
            List<DesignSheet> DsList = new List<DesignSheet>();
            int aid = Convert.ToInt32(Session["aid"]);
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            String qf = "select * from Stress where accessFileID = " + aid + " Order By [Case] , [Station]";
            SqlCommand sqlcf = new SqlCommand(qf, sqlconn);
            SqlDataReader cf = sqlcf.ExecuteReader();
            List<Stress> Str = new List<Stress>();
            while (cf.Read())
            {
                Str.Add(new Stress
                {
                    Id = Convert.ToInt32(cf["id"].ToString()),
                    MEM = cf["N1c"] != DBNull.Value ? Convert.ToInt32(cf["MEM"].ToString()) : 0,
                    Station = cf["Station"] != DBNull.Value ? float.Parse(cf["Station"].ToString()) : 0,
                    Secc = cf["Secc"] != DBNull.Value ? cf["Secc"].ToString() : "",
                    Case = cf["Case"] != DBNull.Value ? cf["Case"].ToString() : "",
                    M3 = cf["M3"] != DBNull.Value ? float.Parse(cf["M3"].ToString()) : 0,
                    V2 = cf["V2"] != DBNull.Value ? float.Parse(cf["V2"].ToString()) : 0,
                    T = cf["T"] != DBNull.Value ? float.Parse(cf["T"].ToString()) : 0,
                    P = cf["P"] != DBNull.Value ? float.Parse(cf["P"].ToString()) : 0,
                    N1c = cf["N1c"] != DBNull.Value ? Convert.ToInt32(cf["N1c"].ToString()) : 0,
                    D1c = cf["D1c"] != DBNull.Value ? Convert.ToInt32(cf["D1c"].ToString()) : 0,
                    Ld1c = cf["Ld1c"] != DBNull.Value ? float.Parse(cf["Ld1c"].ToString()) : 0,
                    N2c = cf["N2c"] != DBNull.Value ? Convert.ToInt32(cf["N2c"].ToString()) : 0,
                    D2c = cf["D2c"] != DBNull.Value ? Convert.ToInt32(cf["D2c"].ToString()) : 0,
                    Ld2c = cf["Ld2c"] != DBNull.Value ? float.Parse(cf["Ld2c"].ToString()) : 0,
                    N3c = cf["N3c"] != DBNull.Value ? Convert.ToInt32(cf["N3c"].ToString()) : 0,
                    D3c = cf["D3c"] != DBNull.Value ? Convert.ToInt32(cf["D3c"].ToString()) : 0,
                    Ld3c = cf["Ld3c"] != DBNull.Value ? float.Parse(cf["Ld3c"].ToString()) : 0,
                    N4c = cf["N4c"] != DBNull.Value ? Convert.ToInt32(cf["N4c"].ToString()) : 0,
                    D4c = cf["D4c"] != DBNull.Value ? Convert.ToInt32(cf["D4c"].ToString()) : 0,
                    Ld4c = cf["Ld4c"] != DBNull.Value ? float.Parse(cf["Ld4c"].ToString()) : 0,
                    N5c = cf["N5c"] != DBNull.Value ? Convert.ToInt32(cf["N5c"].ToString()) : 0,
                    D5c = cf["D5c"] != DBNull.Value ? Convert.ToInt32(cf["D5c"].ToString()) : 0,
                    Ld5c = cf["Ld5c"] != DBNull.Value ? float.Parse(cf["Ld5c"].ToString()) : 0,
                    N1t = cf["N1t"] != DBNull.Value ? Convert.ToInt32(cf["N1t"].ToString()) : 0,
                    D1t = cf["D1t"] != DBNull.Value ? Convert.ToInt32(cf["D1t"].ToString()) : 0,
                    Ld1t = cf["Ld1t"] != DBNull.Value ? float.Parse(cf["Ld1t"].ToString()) : 0,
                    N2t = cf["N2t"] != DBNull.Value ? Convert.ToInt32(cf["N2t"].ToString()) : 0,
                    D2t = cf["D2t"] != DBNull.Value ? Convert.ToInt32(cf["D2t"].ToString()) : 0,
                    Ld2t = cf["Ld2t"] != DBNull.Value ? float.Parse(cf["Ld2t"].ToString()) : 0,
                    N3t = cf["N3t"] != DBNull.Value ? Convert.ToInt32(cf["N3t"].ToString()) : 0,
                    D3t = cf["D3t"] != DBNull.Value ? Convert.ToInt32(cf["D3t"].ToString()) : 0,
                    Ld3t = cf["Ld3t"] != DBNull.Value ? float.Parse(cf["Ld3t"].ToString()) : 0,
                    N4t = cf["N4t"] != DBNull.Value ? Convert.ToInt32(cf["N4t"].ToString()) : 0,
                    D4t = cf["D4t"] != DBNull.Value ? Convert.ToInt32(cf["D4t"].ToString()) : 0,
                    Ld4t = cf["Ld4t"] != DBNull.Value ? float.Parse(cf["Ld4t"].ToString()) : 0,
                    N5t = cf["N5t"] != DBNull.Value ? Convert.ToInt32(cf["N5t"].ToString()) : 0,
                    D5t = cf["D5t"] != DBNull.Value ? Convert.ToInt32(cf["D5t"].ToString()) : 0,
                    Ld5t = cf["Ld5t"] != DBNull.Value ? float.Parse(cf["Ld5t"].ToString()) : 0,
                    N11 = cf["N11"] != DBNull.Value ? Convert.ToInt32(cf["N11"].ToString()) : 0,
                    D11 = cf["D11"] != DBNull.Value ? Convert.ToInt32(cf["D11"].ToString()) : 0,
                    Ld11 = cf["Ld11"] != DBNull.Value ? float.Parse(cf["Ld11"].ToString()) : 0,
                    Ds = cf["Ds"] != DBNull.Value ? Convert.ToInt32(cf["Ds"].ToString()) : 0,
                    Nsout = cf["Nsout"] != DBNull.Value ? Convert.ToInt32(cf["Nsout"].ToString()) : 0,
                    Nsin = cf["Nsin"] != DBNull.Value ? Convert.ToInt32(cf["Nsin"].ToString()) : 0,
                    S = cf["S"] != DBNull.Value ? Convert.ToInt32(cf["S"].ToString()) : 0,
                    Asmin = cf["Asmin"] != DBNull.Value ? Convert.ToChar(cf["Asmin"].ToString()) : ' ',
                    Strength = cf["Strength"] != DBNull.Value ? float.Parse(cf["Strength"].ToString()) : 0,
                    Bending = cf["Bending"] != DBNull.Value ? float.Parse(cf["Bending"].ToString()) : 0,
                    Axial = cf["Axial"] != DBNull.Value ? float.Parse(cf["Axial"].ToString()) : 0,
                    Torsion = cf["Torsion"] != DBNull.Value ? float.Parse(cf["Torsion"].ToString()) : 0,
                    Shear = cf["Shear"] != DBNull.Value ? float.Parse(cf["Shear"].ToString()) : 0,
                    Vconc = cf["Vconc"] != DBNull.Value ? cf["Vconc"].ToString() : " ",
                    Mm = cf["Mm"] != DBNull.Value ? Convert.ToChar(cf["Mm"].ToString()) : ' ',
                    BL = cf["BL"] != DBNull.Value ? Convert.ToChar(cf["BL"].ToString()) : ' ',
                    BR = cf["BR"] != DBNull.Value ? Convert.ToChar(cf["BR"].ToString()) : ' '
                });
            }
            cf.Close();
            sqlconn.Close();

            int B = Convert.ToInt32(formContainer.B);
            int T = Convert.ToInt32(formContainer.T);
            int Cb = Convert.ToInt32(formContainer.Cb);
            int Ct = Convert.ToInt32(formContainer.Ct);

            Boolean Asmin = false;


            Boolean DesignAll;
            if (formContainer.checkDesignAll == true)
            {
                DesignAll = true;
            }
            else
            {
                DesignAll = false;
            }

            Single[,] UsableSteel = new Single[13, 3];
            int i = 0, count = 0;
            while (i <= Str.Count - 1)
            {
                if (Str[i].Secc != "" || DesignAll == true)
                {
                    UsableSteel = FindUsableSteel(Lenextra, Lt, Steel, ShearSteel, DevLen, Sec, T, formContainer, Str[i]);

                    Ds.b = B;
                    Ds.H = T;
                    if (Str[i].Secc == "X0" || Str[i].Secc == "X10")
                    {
                        if (Str[i].M3 <= 0)
                        {
                            Ds.cbot = float.Parse((Cb / 10 + 2.5).ToString());
                            Ds.ctop = Ct / 10;
                        }
                        else
                        {
                            Ds.cbot = Cb / 10;
                            Ds.ctop = float.Parse((Ct / 10 + 2.5).ToString());
                        }
                    }
                    else
                    {
                        Ds.cbot = Cb / 10;
                        Ds.ctop = Ct / 10;
                    }
                    Ds.Ln1_ = UsableSteel[0, 0];
                    Ds.Ld1_ = UsableSteel[0, 1];
                    Ds.REDUCETOPdev1 = UsableSteel[0, 2];
                    Ds.Ln2_ = UsableSteel[1, 0];
                    Ds.Ld2_ = UsableSteel[1, 1];
                    Ds.REDUCETOPdev2 = UsableSteel[1, 2];
                    Ds.Ln3_ = UsableSteel[2, 0];
                    Ds.Ld3_ = UsableSteel[2, 1];
                    Ds.REDUCETOPdev3 = UsableSteel[2, 2];
                    Ds.Ln4_ = UsableSteel[3, 0];
                    Ds.Ld4_ = UsableSteel[3, 1];
                    Ds.REDUCETOPdev4 = UsableSteel[3, 2];
                    Ds.Ln5_ = UsableSteel[4, 0];
                    Ds.Ld5_ = UsableSteel[4, 1];
                    Ds.REDUCETOPdev5 = UsableSteel[4, 2];
                    Ds.N1_ = UsableSteel[5, 0];
                    Ds.D1_ = UsableSteel[5, 1];
                    Ds.REDUCEBOTdev1 = UsableSteel[5, 2];
                    Ds.N2_ = UsableSteel[6, 0];
                    Ds.D2_ = UsableSteel[6, 1];
                    Ds.REDUCEBOTdev2 = UsableSteel[6, 2];
                    Ds.N3_ = UsableSteel[7, 0];
                    Ds.D3_ = UsableSteel[7, 1];
                    Ds.REDUCEBOTdev3 = UsableSteel[7, 2];
                    Ds.N4_ = UsableSteel[8, 0];
                    Ds.D4_ = UsableSteel[8, 1];
                    Ds.REDUCEBOTdev4 = UsableSteel[8, 2];
                    Ds.N5_ = UsableSteel[9, 0];
                    Ds.D5_ = UsableSteel[9, 1];
                    Ds.REDUCEBOTdev5 = UsableSteel[9, 2];
                    Ds.N11_ = UsableSteel[10, 0];
                    Ds.D11_ = UsableSteel[10, 1];
                    Ds.REDUCEN11 = UsableSteel[10, 2];
                    Ds.NSout = UsableSteel[11, 1];
                    Ds.NSin = UsableSteel[12, 1];
                    Ds.Diaoutstirr = UsableSteel[11, 0];
                    Ds.Spacingout = UsableSteel[11, 2];
                    Ds.Mu = Math.Abs(Str[i].M3);
                    Ds.Vu = Math.Abs(Str[i].V2);
                    Ds.Tu = Math.Abs(Str[i].T);
                    if (formContainer.AXL == 'T')
                    {
                        Ds.Pu = (-1) * Str[i].P;
                    }
                    else
                    {
                        Ds.Pu = 0;
                    }
                    Ds.fcprime = fcp;
                    Ds.Fy = fy;
                    if (UsableSteel[11, 0] <= 9)
                    {
                        Ds.Fys = fvyr;
                    }
                    else
                    {
                        Ds.Fys = fvyd;
                    }
                    Ds.AvtSmin = float.Parse((Math.Max(3.5 * Ds.b / Ds.Fys, 0.2 * Math.Sqrt(Ds.fcprime) * Ds.b / Ds.Fys)).ToString());
                    Ds.Es = es;
                    Ds.Cleardistlayers = Steel[0, 1] / 10;
                    if (Asmin == false)
                    {
                        Ds.CONDITIONREINF = 'F';
                    }
                    else
                    {
                        Ds.CONDITIONREINF = 'T';
                    }

                    Ds.NetBotArea1 = float.Parse((Ds.N1_ * Math.PI * Math.Pow((Ds.D1_ / 10), 2) / 4 * Ds.REDUCEBOTdev1).ToString());
                    Ds.NetBotArea2 = float.Parse((Ds.N2_ * Math.PI * Math.Pow((Ds.D2_ / 10), 2) / 4 * Ds.REDUCEBOTdev2).ToString());
                    Ds.NetBotArea3 = float.Parse((Ds.N3_ * Math.PI * Math.Pow((Ds.D3_ / 10), 2) / 4 * Ds.REDUCEBOTdev3).ToString());
                    Ds.NetBotArea4 = float.Parse((Ds.N4_ * Math.PI * Math.Pow((Ds.D4_ / 10), 2) / 4 * Ds.REDUCEBOTdev4).ToString());
                    Ds.NetBotArea5 = float.Parse((Ds.N5_ * Math.PI * Math.Pow((Ds.D5_ / 10), 2) / 4 * Ds.REDUCEBOTdev5).ToString());
                    Ds.NetTopArea1 = float.Parse((Ds.Ln1_ * Math.PI * Math.Pow((Ds.Ld1_ / 10), 2) / 4 * Ds.REDUCETOPdev1).ToString());
                    Ds.NetTopArea2 = float.Parse((Ds.Ln2_ * Math.PI * Math.Pow((Ds.Ld2_ / 10), 2) / 4 * Ds.REDUCETOPdev2).ToString());
                    Ds.NetTopArea3 = float.Parse((Ds.Ln3_ * Math.PI * Math.Pow((Ds.Ld3_ / 10), 2) / 4 * Ds.REDUCETOPdev3).ToString());
                    Ds.NetTopArea4 = float.Parse((Ds.Ln4_ * Math.PI * Math.Pow((Ds.Ld4_ / 10), 2) / 4 * Ds.REDUCETOPdev4).ToString());
                    Ds.NetTopArea5 = float.Parse((Ds.Ln5_ * Math.PI * Math.Pow((Ds.Ld5_ / 10), 2) / 4 * Ds.REDUCETOPdev5).ToString());
                    Ds.NETAREAN11 = float.Parse((Ds.N11_ * Math.PI * Math.Pow((Ds.D11_ / 10), 2) / 4 * Ds.REDUCEN11).ToString());

                    Ds.EachNetBotArea1 = Ds.N1_ > 0 ? Ds.NetBotArea1 / Ds.N1_ : 0;
                    Ds.EachNetBotArea2 = Ds.N2_ > 0 ? Ds.NetBotArea2 / Ds.N2_ : 0;
                    Ds.EachNetBotArea3 = Ds.N3_ > 0 ? Ds.NetBotArea3 / Ds.N3_ : 0;
                    Ds.EachNetBotArea4 = Ds.Ln4_ > 0 ? Ds.NetBotArea4 / Ds.N4_ : 0;
                    Ds.EachNetBotArea5 = Ds.Ln5_ > 0 ? (Ds.NetBotArea5 != 0 && Ds.N5_ != 0 ? Ds.NetBotArea5 / Ds.N5_ : 0) : 0;
                    Ds.EachNetTopArea1 = Ds.N1_ > 0 ? (Ds.NetTopArea1 != 0 && Ds.Ln1_ != 0 ? Ds.NetTopArea1 / Ds.Ln1_ : 0) : 0;
                    Ds.EachNetTopArea2 = Ds.N2_ > 0 ? (Ds.NetTopArea2 != 0 && Ds.Ln2_ != 0 ? Ds.NetTopArea2 / Ds.Ln2_ : 0) : 0;
                    Ds.EachNetTopArea3 = Ds.N3_ > 0 ? (Ds.NetTopArea3 != 0 && Ds.Ln3_ != 0 ? Ds.NetTopArea3 / Ds.Ln3_ : 0) : 0;
                    Ds.EachNetTopArea4 = Ds.Ln4_ > 0 ? (Ds.NetTopArea4 != 0 && Ds.Ln4_ != 0 ? Ds.NetTopArea4 / Ds.Ln4_ : 0) : 0;
                    Ds.EachNetTopArea5 = Ds.Ln5_ > 0 ? (Ds.NetTopArea5 != 0 && Ds.Ln5_ != 0 ? Ds.NetTopArea5 / Ds.Ln5_ : 0) : 0;
                    Ds.EACHNETAREAN11 = Ds.N11_ > 0 ? Ds.NETAREAN11 / Ds.N11_ : 0;
                    Boolean R34C5 = ((Ds.Mu != 0 && Ds.NetBotArea1 == 0 && Ds.NetBotArea2 == 0 && Ds.NetBotArea3 == 0 && Ds.NetBotArea4 == 0 && Ds.NetBotArea5 == 0)) ? true : false;



                    Ds.DISTBOT1stTO2nd = Ds.N3_ > 0 ? Ds.Cleardistlayers : 0;
                    Ds.DISTBOT2ndTO3rd = Ds.N4_ > 0 ? Ds.Cleardistlayers : 0;
                    Ds.DISTBOT3rdTO4th = Ds.N5_ > 0 ? Ds.Cleardistlayers : 0;
                    Ds.DISTTOP1stTO2nd = Ds.Ln3_ > 0 ? Ds.Cleardistlayers : 0;
                    Ds.DISTTOP2ndTO3rd = Ds.Ln4_ > 0 ? Ds.Cleardistlayers : 0;
                    Ds.DISTTOP3rdTO4th = Ds.Ln5_ > 0 ? Ds.Cleardistlayers : 0;

                    if (formContainer.CONSIDERAXIAL == true)
                    {
                        Ds.CONSIDERAXIAL = 'T';
                    }
                    else
                    {
                        Ds.CONSIDERAXIAL = 'F';
                    }

                    if (formContainer.DETAILEDCALCULATION == true)
                    {
                        Ds.DETAILEDCALCULATION = 'T';
                    }
                    else
                    {
                        Ds.DETAILEDCALCULATION = 'F';
                    }


                    if (Ds.N1_ > 0)
                    {
                        Ds.d_1 = Ds.H - Ds.cbot - Ds.Diaoutstirr / 10 - Math.Max((Ds.D1_ / 20), (Ds.D2_ / 20));
                        //Ds.A1d1 = float.Parse(((Ds.N1_ * Math.PI * Math.Pow((Ds.D1_ / 10), 2) / 4 + Ds.N2_ * Math.PI * Math.Pow((Ds.D2_ / 10), 2) / 4) * Ds.d_1).ToString());
                        Ds.A1d1 = float.Parse((Ds.N1_ * Math.PI * Math.Pow((Ds.D1_ / 10), 2) / 4 * (Ds.H - Ds.cbot - (Ds.Diaoutstirr / 10) - (Ds.D1_ / 10) / 2)).ToString());
                    }
                    else
                    {
                        Ds.d_1 = 0;
                        Ds.A1d1 = 0;
                    }

                    if (Ds.N3_ > 0)
                    {
                        Ds.d_2 = Ds.d_1 - Math.Max((Ds.D1_ / 20), (Ds.D2_ / 20)) - Ds.DISTBOT1stTO2nd - Ds.D3_ / 20;
                        Ds.A2d2 = float.Parse((Ds.N3_ * Math.PI * (Math.Pow((Ds.D3_ / 10), 2) / 4) * (Ds.H - Ds.cbot - (Ds.Diaoutstirr / 10) - (Ds.D1_ / 10) - Ds.DISTBOT1stTO2nd - (Ds.D3_ / 10) / 2)).ToString());
                    }
                    else
                    {
                        Ds.d_2 = 0;
                        Ds.A2d2 = 0;
                    }

                    if (Ds.N4_ > 0)
                    {
                        Ds.d_3 = Ds.d_2 - Ds.D3_ / 20 - Ds.DISTBOT2ndTO3rd - Ds.D4_ / 20;
                        Ds.A3d3 = float.Parse((Ds.N4_ * Math.PI * (Math.Pow((Ds.D4_ / 10), 2) / 4) * (Ds.H - Ds.cbot - (Ds.Diaoutstirr / 10) - (Ds.D1_ / 10) - Ds.DISTBOT1stTO2nd - (Ds.D3_ / 10) - Ds.DISTBOT2ndTO3rd - (Ds.D4_ / 10) / 2)).ToString());
                    }
                    else
                    {
                        Ds.d_3 = 0;
                        Ds.A3d3 = 0;
                    }

                    if (Ds.N5_ > 0)
                    {
                        Ds.d_4 = Ds.d_3 - Ds.D4_ / 20 - Ds.DISTBOT3rdTO4th - Ds.D5_ / 20;
                        Ds.A4d4 = float.Parse((Ds.N5_ * Math.PI * (Math.Pow((Ds.D5_ / 10), 2) / 4) * (Ds.H - Ds.cbot - (Ds.Diaoutstirr / 10) - (Ds.D1_ / 10) - Ds.DISTBOT1stTO2nd - (Ds.D3_ / 10) - Ds.DISTBOT2ndTO3rd - (Ds.D4_ / 10) - Ds.DISTBOT3rdTO4th - (Ds.D5_ / 10) / 2)).ToString());
                    }
                    else
                    {
                        Ds.d_4 = 0;
                        Ds.A4d4 = 0;
                    }
                    Ds.d = float.Parse(((Ds.A1d1 + Ds.A2d2 + Ds.A3d3 + Ds.A4d4) / (Ds.N1_ * Math.PI * Math.Pow((Ds.D1_ / 10), 2) / 4 + Ds.N3_ * Math.PI * Math.Pow((Ds.D3_ / 10), 2) / 4 + Ds.N4_ * Math.PI * Math.Pow((Ds.D4_ / 10), 2) / 4 + Ds.N5_ * Math.PI * Math.Pow((Ds.D5_ / 10), 2) / 4)).ToString());
                    //Ds.d = float.Parse(((Ds.A1d1 + Ds.A2d2 + Ds.A3d3 + Ds.A4d4) / (Ds.N1_ * Math.PI * Math.Pow((Ds.D1_ / 10), 2) / 4 + Ds.N2_ * Math.PI * Math.Pow((Ds.D2_ / 10), 2) / 4 + Ds.N3_ * Math.PI * Math.Pow((Ds.D3_ / 10), 2) / 4 + Ds.N4_ * Math.PI * Math.Pow((Ds.D4_ / 10), 2) / 4 + Ds.N5_ * Math.PI * Math.Pow((Ds.D5_ / 10), 2) / 4)).ToString());
                    //Ds.d = float.Parse(((Ds.A1d1 + Ds.A2d2 + Ds.A3d3 + Ds.A4d4) / (Ds.N1_ * Math.PI * Math.Pow((Ds.D1_ / 10), 2) / 4 + Ds.N3_ * Math.PI * Math.Pow((Ds.D3_ / 10), 2) / 4 + Ds.N4_ * Math.PI * Math.Pow((Ds.D4_ / 10), 2) / 4 + Ds.N5_ * Math.PI * Math.Pow((Ds.D5_ / 10), 2) / 4)).ToString());
                    if (Ds.Ln1_ > 0)
                    {
                        Ds.dprime1 = Ds.ctop + Ds.Diaoutstirr / 10 + Math.Max((Ds.Ld1_ / 20), (Ds.Ld2_ / 20));
                        Ds.Aprime1dprime1 = float.Parse(((Ds.Ln1_ * Math.PI * Math.Pow((Ds.Ld1_ / 10), 2) / 4 + Ds.Ln2_ * Math.PI * Math.Pow((Ds.Ld2_ / 10), 2) / 4) * Ds.dprime1).ToString());
                    }
                    else
                    {
                        Ds.dprime1 = 0;
                        Ds.Aprime1dprime1 = 0;
                    }

                    if (Ds.Ln3_ > 0)
                    {
                        Ds.dprime2 = Ds.dprime1 + Math.Max((Ds.Ld1_ / 20), (Ds.Ld2_ / 20)) + Ds.DISTTOP1stTO2nd + Ds.Ld3_ / 20;
                        Ds.Aprime2dprime2 = float.Parse((Ds.Ln3_ * Math.PI * Math.Pow((Ds.Ld3_ / 10), 2) / 4 * Ds.dprime2).ToString());
                    }
                    else
                    {
                        Ds.dprime2 = 0;
                        Ds.Aprime2dprime2 = 0;
                    }

                    if (Ds.Ln4_ > 0)
                    {
                        Ds.dprime3 = Ds.dprime2 + Ds.Ld3_ / 20 + Ds.DISTTOP2ndTO3rd + Ds.Ld4_ / 20;
                        Ds.Aprime3dprime3 = float.Parse((Ds.Ln4_ * Math.PI * Math.Pow((Ds.Ld4_ / 10), 2) / 4 * Ds.dprime3).ToString());
                    }
                    else
                    {
                        Ds.dprime3 = 0;
                        Ds.Aprime3dprime3 = 0;
                    }

                    if (Ds.Ln5_ > 0)
                    {
                        Ds.dprime4 = Ds.dprime3 + Ds.Ld4_ / 20 + Ds.DISTTOP3rdTO4th + Ds.Ld5_ / 20;
                        Ds.Aprime4dprime4 = float.Parse((Ds.Ln5_ * Math.PI * Math.Pow((Ds.Ld5_ / 10), 2) / 4 * Ds.dprime4).ToString());
                    }
                    else
                    {
                        Ds.dprime4 = 0;
                        Ds.Aprime4dprime4 = 0;
                    }
                    Ds.dprime = (Ds.Aprime1dprime1 + Ds.Aprime2dprime2 + Ds.Aprime3dprime3 + Ds.Aprime4dprime4) / float.Parse((Ds.Ln1_ * Math.PI * Math.Pow((Ds.Ld1_ / 10), 2) / 4 + Ds.Ln2_ * Math.PI * Math.Pow((Ds.Ld2_ / 10), 2) / 4 + Ds.Ln3_ * Math.PI * Math.Pow((Ds.Ld3_ / 10), 2) / 4 + Ds.Ln4_ * Math.PI * Math.Pow((Ds.Ld5_ / 10), 2) / 4 + Ds.Ln5_ * Math.PI * Math.Pow((Ds.Ld5_ / 10), 2) / 4).ToString());

                    //Ds.DETAILEDcetaVcNOAXIAL = float.Parse((Math.Min(Convert.ToDouble(0.75 * (0.5 * Math.Sqrt(Ds.fcprime) + 176 * (((Ds.N1_ * Math.PI * Math.Pow((Ds.D1_ / 10), 2) / 4) + (Ds.N2_ * Math.PI * Math.Pow((Ds.D2_ / 10), 2) / 4) + (Ds.N3_ * Math.PI * Math.Pow((Ds.D3_ / 10), 2) / 4) + (Ds.N4_ * Math.PI * Math.Pow((Ds.D4_ / 10), 2) / 4) + (Ds.N5_ * Math.PI * Math.Pow((Ds.D5_ / 10), 2) / 4)) / (Ds.b * Ds.d)) * Math.Min(1, (Ds.Vu * Ds.d / (Ds.Mu * 100)))) * Ds.b * Ds.d), Convert.ToDouble(0.75 * 0.93 * Math.Sqrt(Ds.fcprime) * Ds.b * Ds.d))).ToString());
                    //Ds.DETAILEDcetaVcwithCOMP = Ds.Pu >= 0 ? float.Parse((Math.Min(0.75 * 0.93 * Math.Sqrt(Ds.fcprime) * Ds.b * Ds.d * Math.Sqrt(1 + Ds.Pu / (35 * Ds.Ag)), (0.75 * (0.5 * Math.Sqrt(Ds.fcprime) + 176 * (((Ds.N1_ * Math.PI * Math.Pow((Ds.D1_ / 10), 2) / 4) + (Ds.N2_ * Math.PI * Math.Pow((Ds.D2_ / 10), 2) / 4) + (Ds.N3_ * Math.PI * Math.Pow((Ds.D3_ / 10), 2) / 4) + (Ds.N4_ * Math.PI * Math.Pow((Ds.D4_ / 10), 2) / 4) + (Ds.N5_ * Math.PI * Math.Pow((Ds.D5_ / 10), 2) / 4)) / (Ds.b * Ds.d)) * (Ds.Vu * Ds.d / (Ds.Mu * 100 - Ds.Pu * (4 * Ds.H - Ds.d) / 8))) * Ds.b * Ds.d))).ToString()) : 0;
                    //Ds.DETAILEDcetaVcwithTENS = Ds.Pu < 0 ? float.Parse((Math.Max((0.75 * 0.53 * Math.Sqrt(Ds.fcprime) * (1 + Ds.Pu / (35 * Ds.Ag)) * Ds.b * Ds.d), 0)).ToString()) : 0;
                    //Ds.APPROXcetaVcNOAXIAL = float.Parse((0.75 * 0.53 * Math.Sqrt(Ds.fcprime) * Ds.b * Ds.d).ToString());
                    //Ds.APPROXcetaVcwithCOMP = Ds.Pu >= 0 ? float.Parse((0.75 * 0.53 * Math.Sqrt(Ds.fcprime) * (1 + Ds.Pu / (140 * Ds.Ag)) * Ds.b * Ds.d).ToString()) : 0;
                    //Ds.APPROXcetaVcwithTENS = 0;

                    Ds.DETAILEDcetaVcNOAXIAL = float.Parse((0.75 * Math.Min((0.5 * Math.Sqrt(Ds.fcprime) + 176 * (Ds.N1_ * Math.PI * Math.Pow((Ds.D1_ / 10), 2) / 4 + Ds.N2_ * Math.PI * Math.Pow((Ds.D2_ / 10), 2) / 4 + Ds.N3_ * Math.PI * Math.Pow((Ds.D3_ / 10), 2) / 4 + Ds.N4_ * Math.PI * Math.Pow((Ds.D4_ / 10), 2) / 4 + Ds.N5_ * Math.PI * Math.Pow((Ds.D5_ / 10), 2) / 4) / (Ds.b * Ds.d) * Math.Min(Ds.Vu * Ds.d / (Ds.Mu * 100), 1)) * Ds.b * Ds.d, 0.93 * Math.Sqrt(Ds.fcprime) * Ds.b * Ds.d)).ToString());

                    if (Ds.Pu >= 0)
                    {
                        Ds.DETAILEDcetaVcwithCOMP = float.Parse((0.75 * Math.Min((0.5 * Math.Sqrt(Ds.fcprime) + 176 * (Ds.N1_ * Math.PI * Math.Pow((Ds.D1_ / 10), 2) / 4 + Ds.N2_ * Math.PI * Math.Pow((Ds.D2_ / 10), 2) / 4 + Ds.N3_ * Math.PI * Math.Pow((Ds.D3_ / 10), 2) / 4 + Ds.N4_ * Math.PI * Math.Pow((Ds.D4_ / 10), 2) / 4 + Ds.N5_ * Math.PI * Math.Pow((Ds.D5_ / 10), 2) / 4) / (Ds.b * Ds.d) * Ds.Vu * Ds.d / (Ds.Mu * 100 - Ds.Pu * (4 * Ds.d) / 8)) * Ds.b * Ds.d, 0.93 * Math.Sqrt(Ds.fcprime) * Ds.b * Ds.d * Math.Sqrt(1 + Ds.Pu / (35 * Ds.b * Ds.H)))).ToString());
                    }
                    else
                    {
                        Ds.DETAILEDcetaVcwithCOMP = 0;
                    }

                    if (Ds.Pu < 0)
                    {
                        Ds.DETAILEDcetaVcwithTENS = float.Parse((Math.Max(0.75 * 0.53 * (1 + Ds.Pu / (35 * Ds.b * Ds.H)) * Math.Sqrt(Ds.fcprime) * Ds.b * Ds.d, 0)).ToString());
                    }
                    else
                    {
                        Ds.DETAILEDcetaVcwithTENS = 0;
                    }
                    Ds.APPROXcetaVcNOAXIAL = float.Parse((0.75 * 0.53 * Math.Sqrt(Ds.fcprime) * Ds.b * Ds.d).ToString());

                    if (Ds.Pu >= 0)
                    {
                        Ds.APPROXcetaVcwithCOMP = float.Parse((0.75 * 0.53 * (1 + Ds.Pu / (140 * Ds.b * Ds.H)) * Math.Sqrt(Ds.fcprime) * Ds.b * Ds.d).ToString());
                    }
                    else
                    {
                        Ds.APPROXcetaVcwithCOMP = 0;
                    }
                    Ds.APPROXcetaVcwithTENS = 0;

                    if (Ds.DETAILEDCALCULATION == 'T' && Ds.CONSIDERAXIAL == 'F')
                    {
                        Ds.cetaVc = Ds.DETAILEDcetaVcNOAXIAL;
                    }
                    else
                    {
                        if (Ds.DETAILEDCALCULATION == 'T' && Ds.CONSIDERAXIAL == 'T')
                        {
                            Ds.cetaVc = float.Parse((Math.Max(Ds.DETAILEDcetaVcwithCOMP, Ds.DETAILEDcetaVcwithTENS)).ToString());
                        }
                        else
                        {
                            if (Ds.DETAILEDCALCULATION == 'F' && Ds.CONSIDERAXIAL == 'F')
                            {
                                Ds.cetaVc = Ds.APPROXcetaVcNOAXIAL;
                            }
                            else
                            {
                                Ds.cetaVc = float.Parse((Math.Max(Ds.APPROXcetaVcwithCOMP, Ds.APPROXcetaVcwithTENS)).ToString());
                            }

                        }

                    }




                    //if(Ds.DETAILEDCALCULATION == 'T')
                    //{
                    //    if(Ds.CONSIDERAXIAL == 'F')
                    //    {
                    //        Ds.cetaVc = Ds.DETAILEDcetaVcNOAXIAL;
                    //    }
                    //    else
                    //    {
                    //        Ds.cetaVc = float.Parse((Math.Max(Ds.DETAILEDcetaVcwithCOMP, Ds.DETAILEDcetaVcwithTENS)).ToString());
                    //    }
                    //}
                    //else if(Ds.DETAILEDCALCULATION == 'F')
                    //{
                    //    Ds.cetaVc = Ds.APPROXcetaVcNOAXIAL;
                    //}
                    //else
                    //{
                    //    Ds.cetaVc = float.Parse((Math.Max(Ds.APPROXcetaVcwithCOMP, Ds.APPROXcetaVcwithTENS)).ToString());
                    //}
                    Ds.Acp = Ds.b * Ds.H;
                    Ds.Pcp = 2 * Ds.b + 2 * Ds.H;
                    Ds.cetaTcr = float.Parse(((0.75 * 0.27 * Math.Sqrt(Ds.fcprime) * Math.Pow(Ds.Acp, 2) / Ds.Pcp) / 100).ToString());
                    Boolean R35C5 = ((Ds.Vu >= (Ds.cetaVc / 2) || Ds.Tu >= Ds.cetaTcr) && Ds.NSout == 0) ? true : false;
                    Boolean R36C5 = ((((Ds.H - Ds.cbot - Ds.ctop - (2 * Ds.Diaoutstirr / 10) - Math.Max(Ds.Ld1_ / 10, Ds.Ld2_ / 10) - (Ds.Ld3_ + Ds.Ld4_ + Ds.Ld5_) / 10 - Math.Max(Ds.D1_ / 10, Ds.D2_ / 10) - (Ds.D3_ + Ds.D4_ + Ds.D5_) / 10 - Ds.DISTTOP1stTO2nd - Ds.DISTTOP2ndTO3rd - Ds.DISTTOP3rdTO4th - Ds.DISTBOT1stTO2nd - Ds.DISTBOT2ndTO3rd - Ds.DISTBOT3rdTO4th) - ((Ds.N11_ / 2) * Ds.D11_ / 10) / (Ds.N11_ / 2 + 1)) > 30) && (Ds.Tu > Ds.cetaTcr)) == true ? true : false;
                    Ds.Ph = 2 * (Ds.H - Ds.cbot - Ds.ctop) + 2 * (Ds.b - Ds.cbot - Ds.ctop) - 2 * Ds.Diaoutstirr / 10;
                    Ds.Aoh = (Ds.b - Ds.cbot - Ds.ctop - Ds.Diaoutstirr / 10) * (Ds.H - Ds.cbot - Ds.ctop - Ds.Diaoutstirr / 10);
                    Ds.A0 = float.Parse("0.85") * Ds.Aoh;
                    Ds.AtSmin = Ds.Tu < Ds.cetaTcr ? 0 : float.Parse((1.75 * Ds.b / Ds.Fys).ToString());
                    Ds.Ats = Ds.Tu < Ds.cetaTcr ? 0 : float.Parse((Math.Max((Ds.Tu * 100 / (0.75 * 2 * Ds.A0 * Ds.Fys)), Convert.ToDouble(Ds.AtSmin))).ToString());
                    Ds.Almin = Ds.Tu < Ds.cetaTcr ? 0 : ((1.33 * Math.Sqrt(Convert.ToDouble(Ds.fcprime)) * Convert.ToDouble(Ds.Acp)) / Convert.ToDouble(Ds.Fy) - Convert.ToDouble(Ds.Ats * Ds.Ph * Ds.Fys * Ds.Fy) > 0 ? 0 : float.Parse(((1.33 * Math.Sqrt(Ds.fcprime) * Ds.Acp) / Ds.Fy - (Ds.Ats * Ds.Ph * Ds.Fys / Ds.Fy)).ToString()));
                    Ds.Al = Ds.Tu < Ds.cetaTcr ? 0 : float.Parse((Math.Max(Convert.ToDouble(Ds.Tu * 100 * Ds.Ph / (0.75 * 2 * Ds.A0 * Ds.Fys) * (Ds.Fys / Ds.Fy)), Convert.ToDouble(Ds.Almin))).ToString());

                    Ds.REDUCEDBOTTOR1 = Ds.N1_ > 0 ? Convert.ToDouble(Ds.EachNetBotArea1 / (Ds.NetTopArea1 + Ds.NetTopArea2 + 2 * Ds.EachNetTopArea3 + 2 * Ds.EachNetTopArea4 + 2 * Ds.EachNetTopArea5 + Ds.NetBotArea1 + Ds.NetBotArea2 + 2 * Ds.EachNetBotArea3 + 2 * Ds.EachNetBotArea4 + 2 * Ds.EachNetBotArea5 + Ds.NETAREAN11) * Ds.Al) == Double.NaN ? 0 : (Ds.EachNetBotArea1 / (Ds.NetTopArea1 + Ds.NetTopArea2 + 2 * Ds.EachNetTopArea3 + 2 * Ds.EachNetTopArea4 + 2 * Ds.EachNetTopArea5 + Ds.NetBotArea1 + Ds.NetBotArea2 + 2 * Ds.EachNetBotArea3 + 2 * Ds.EachNetBotArea4 + 2 * Ds.EachNetBotArea5 + Ds.NETAREAN11) * Ds.Al) : 0;
                    Ds.REDUCEDBOTTOR2 = Ds.N2_ > 0 ? Ds.EachNetBotArea2 / (Ds.NetTopArea1 + Ds.NetTopArea2 + 2 * Ds.EachNetTopArea3 + 2 * Ds.EachNetTopArea4 + 2 * Ds.EachNetTopArea5 + Ds.NetBotArea1 + Ds.NetBotArea2 + 2 * Ds.EachNetBotArea3 + 2 * Ds.EachNetBotArea4 + 2 * Ds.EachNetBotArea5 + Ds.NETAREAN11) * Ds.Al : 0;
                    Ds.REDUCEDBOTTOR3 = Ds.N3_ > 0 ? Ds.EachNetBotArea3 / (Ds.NetTopArea1 + Ds.NetTopArea2 + 2 * Ds.EachNetTopArea3 + 2 * Ds.EachNetTopArea4 + 2 * Ds.EachNetTopArea5 + Ds.NetBotArea1 + Ds.NetBotArea2 + 2 * Ds.EachNetBotArea3 + 2 * Ds.EachNetBotArea4 + 2 * Ds.EachNetBotArea5 + Ds.NETAREAN11) * Ds.Al : 0;
                    Ds.REDUCEDBOTTOR4 = Ds.N4_ > 0 ? Ds.EachNetBotArea4 / (Ds.NetTopArea1 + Ds.NetTopArea2 + 2 * Ds.EachNetTopArea3 + 2 * Ds.EachNetTopArea4 + 2 * Ds.EachNetTopArea5 + Ds.NetBotArea1 + Ds.NetBotArea2 + 2 * Ds.EachNetBotArea3 + 2 * Ds.EachNetBotArea4 + 2 * Ds.EachNetBotArea5 + Ds.NETAREAN11) * Ds.Al : 0;
                    Ds.REDUCEDBOTTOR5 = Ds.N5_ > 0 ? Ds.EachNetBotArea5 / (Ds.NetTopArea1 + Ds.NetTopArea2 + 2 * Ds.EachNetTopArea3 + 2 * Ds.EachNetTopArea4 + 2 * Ds.EachNetTopArea5 + Ds.NetBotArea1 + Ds.NetBotArea2 + 2 * Ds.EachNetBotArea3 + 2 * Ds.EachNetBotArea4 + 2 * Ds.EachNetBotArea5 + Ds.NETAREAN11) * Ds.Al : 0;
                    Ds.REDUCEDTOPTOR1 = Ds.Ln1_ > 0 ? Ds.EachNetTopArea1 / (Ds.NetTopArea1 + Ds.NetTopArea2 + 2 * Ds.EachNetTopArea3 + 2 * Ds.EachNetTopArea4 + 2 * Ds.EachNetTopArea5 + Ds.NetBotArea1 + Ds.NetBotArea2 + 2 * Ds.EachNetTopArea3 + 2 * Ds.EachNetTopArea4 + 2 * Ds.EachNetTopArea5 + Ds.NETAREAN11) * Ds.Al : 0;
                    Ds.REDUCEDTOPTOR2 = Ds.Ln2_ > 0 ? Ds.EachNetTopArea2 / (Ds.NetTopArea1 + Ds.NetTopArea2 + 2 * Ds.EachNetTopArea3 + 2 * Ds.EachNetTopArea4 + 2 * Ds.EachNetTopArea5 + Ds.NetBotArea1 + Ds.NetBotArea2 + 2 * Ds.EachNetTopArea3 + 2 * Ds.EachNetTopArea4 + 2 * Ds.EachNetTopArea5 + Ds.NETAREAN11) * Ds.Al : 0;
                    Ds.REDUCEDTOPTOR3 = Ds.Ln3_ > 0 ? Ds.EachNetTopArea3 / (Ds.NetTopArea1 + Ds.NetTopArea2 + 2 * Ds.EachNetTopArea3 + 2 * Ds.EachNetTopArea4 + 2 * Ds.EachNetTopArea5 + Ds.NetBotArea1 + Ds.NetBotArea2 + 2 * Ds.EachNetTopArea3 + 2 * Ds.EachNetTopArea4 + 2 * Ds.EachNetTopArea5 + Ds.NETAREAN11) * Ds.Al : 0;
                    Ds.REDUCEDTOPTOR4 = Ds.Ln4_ > 0 ? Ds.EachNetTopArea4 / (Ds.NetTopArea1 + Ds.NetTopArea2 + 2 * Ds.EachNetTopArea3 + 2 * Ds.EachNetTopArea4 + 2 * Ds.EachNetTopArea5 + Ds.NetBotArea1 + Ds.NetBotArea2 + 2 * Ds.EachNetTopArea3 + 2 * Ds.EachNetTopArea4 + 2 * Ds.EachNetTopArea5 + Ds.NETAREAN11) * Ds.Al : 0;
                    Ds.REDUCEDTOPTOR5 = Ds.Ln5_ > 0 ? Ds.EachNetTopArea5 / (Ds.NetTopArea1 + Ds.NetTopArea2 + 2 * Ds.EachNetTopArea3 + 2 * Ds.EachNetTopArea4 + 2 * Ds.EachNetTopArea5 + Ds.NetBotArea1 + Ds.NetBotArea2 + 2 * Ds.EachNetTopArea3 + 2 * Ds.EachNetTopArea4 + 2 * Ds.EachNetTopArea5 + Ds.NETAREAN11) * Ds.Al : 0;
                    Ds.Reducedreinfareaduetorsion11 = Ds.NETAREAN11 * Ds.Al / (Ds.EachNetTopArea1 + Ds.EachNetTopArea2 + 2 * Ds.NetTopArea3 + 2 * Ds.NetTopArea4 + 2 * Ds.NetTopArea5 + Ds.EachNetBotArea1 + Ds.EachNetBotArea2 + 2 * Ds.NetBotArea3 + Ds.NetBotArea4 + 2 * Ds.EachNetBotArea5 + Ds.EACHNETAREAN11);

                    Ds.FINALEACHEDGEBOT1 = Ds.CONDITIONREINF == 'T' ? float.Parse((0.75 * (Ds.EachNetBotArea1 - Ds.REDUCEDBOTTOR1)).ToString()) : Ds.EachNetBotArea1 - Ds.REDUCEDBOTTOR1;
                    Ds.FINALEACHEDGEBOT2 = Ds.CONDITIONREINF == 'T' ? float.Parse((0.75 * (Ds.EachNetBotArea2 - Ds.REDUCEDBOTTOR2)).ToString()) : Ds.EachNetBotArea2 - Ds.REDUCEDBOTTOR2;
                    Ds.FINALEACHEDGEBOT3 = Ds.CONDITIONREINF == 'T' ? float.Parse((0.75 * (Ds.EachNetBotArea3 - Ds.REDUCEDBOTTOR3)).ToString()) : Ds.EachNetBotArea3 - Ds.REDUCEDBOTTOR3;
                    Ds.FINALEACHEDGEBOT4 = Ds.CONDITIONREINF == 'T' ? float.Parse((0.75 * (Ds.EachNetBotArea4 - Ds.REDUCEDBOTTOR4)).ToString()) : Ds.EachNetBotArea4 - Ds.REDUCEDBOTTOR4;
                    Ds.FINALEACHEDGEBOT5 = Ds.CONDITIONREINF == 'T' ? float.Parse((0.75 * (Ds.EachNetBotArea5 - Ds.REDUCEDBOTTOR5)).ToString()) : Ds.EachNetBotArea5 - Ds.REDUCEDBOTTOR5;
                    Ds.FINALEACHEDGETOP1 = Ds.CONDITIONREINF == 'T' ? float.Parse((0.75 * (Ds.EachNetTopArea1 - Ds.REDUCEDTOPTOR1)).ToString()) : Ds.EachNetTopArea1 - Ds.REDUCEDTOPTOR1;
                    Ds.FINALEACHEDGETOP2 = Ds.CONDITIONREINF == 'T' ? float.Parse((0.75 * (Ds.EachNetTopArea2 - Ds.REDUCEDTOPTOR2)).ToString()) : Ds.EachNetTopArea2 - Ds.REDUCEDTOPTOR2;
                    Ds.FINALEACHEDGETOP3 = Ds.CONDITIONREINF == 'T' ? float.Parse((0.75 * (Ds.EachNetTopArea3 - Ds.REDUCEDTOPTOR3)).ToString()) : Ds.EachNetTopArea3 - Ds.REDUCEDTOPTOR3;
                    Ds.FINALEACHEDGETOP4 = Ds.CONDITIONREINF == 'T' ? float.Parse((0.75 * (Ds.EachNetTopArea4 - Ds.REDUCEDTOPTOR4)).ToString()) : Ds.EachNetTopArea4 - Ds.REDUCEDTOPTOR4;
                    Ds.FINALEACHEDGETOP5 = Ds.CONDITIONREINF == 'T' ? float.Parse((0.75 * (Ds.EachNetTopArea5 - Ds.REDUCEDTOPTOR5)).ToString()) : Ds.EachNetTopArea5 - Ds.REDUCEDTOPTOR5;

                    Ds.Asmin = float.Parse((Math.Max(0.8 * Math.Sqrt(Ds.fcprime) / Ds.Fy * Ds.b * Ds.d, 14 * Ds.b * Ds.d / Ds.Fy)).ToString());

                    Ds.SAs = Ds.N1_ * Ds.FINALEACHEDGEBOT1 + Ds.N2_ * Ds.FINALEACHEDGEBOT2 + (Ds.N3_ - 2) * Ds.EachNetBotArea3 + 2 * Ds.FINALEACHEDGEBOT3 + (Ds.N4_ - 2) * Ds.EachNetBotArea4 + 2 * Ds.FINALEACHEDGEBOT4 + (Ds.N5_ - 2) * Ds.EachNetBotArea5 + 2 * Ds.FINALEACHEDGEBOT5;
                    char R21C16;
                    if (Ds.CONDITIONREINF == 'T')
                    {
                        R21C16 = 'F';
                    }
                    else if (Ds.SAs > Ds.Asmin)
                    {
                        R21C16 = 'F';
                    }
                    else
                    {
                        R21C16 = 'T';
                    }
                    Ds.Totaltopreinfarea1 = Ds.FINALEACHEDGETOP1 * Ds.Ln1_;
                    Ds.Totaltopreinfarea2 = Ds.FINALEACHEDGETOP2 * Ds.Ln2_;
                    Ds.Totaltopreinfarea3 = Ds.FINALEACHEDGETOP3 * Ds.Ln3_;
                    Ds.Totaltopreinfarea4 = Ds.FINALEACHEDGETOP3 * Ds.Ln4_;
                    Ds.Totaltopreinfarea5 = Ds.FINALEACHEDGETOP3 * Ds.Ln5_;
                    Ds.Totalbotreinfarea1 = Ds.FINALEACHEDGEBOT1 * Ds.N1_;
                    Ds.Totalbotreinfarea2 = Ds.FINALEACHEDGEBOT2 * Ds.N2_;
                    Ds.Totalbotreinfarea3 = Ds.FINALEACHEDGEBOT3 * Ds.N3_;
                    Ds.Totalbotreinfarea4 = Ds.FINALEACHEDGEBOT4 * Ds.N4_;
                    Ds.Totalbotreinfarea5 = Ds.FINALEACHEDGEBOT5 * Ds.N5_;

                    Boolean R37C5 = (Ds.Ln1_ * Ds.EachNetTopArea1 + Ds.Ln2_ * Ds.EachNetTopArea2 + 2 * (Ds.EachNetTopArea3 + Ds.EachNetTopArea4 + Ds.EachNetTopArea5) + Ds.N1_ * Ds.EachNetBotArea1 + Ds.N2_ * Ds.EachNetBotArea2 + 2 * (Ds.EachNetBotArea3 + Ds.EachNetBotArea4 + Ds.EachNetBotArea5) + 2 * Ds.EACHNETAREAN11 > Ds.Al) == true ? false : true;
                    Ds.cetaVs = Ds.Vu <= Ds.cetaVc ? 0 : Ds.Vu - Ds.cetaVc;
                    Ds.cetaVsmax = Ds.Vu <= Ds.cetaVc ? 0 : float.Parse((0.75 * 2.2 * Math.Sqrt(Ds.fcprime) * Ds.b * Ds.d).ToString());
                    Ds.AvSmin = Ds.Tu > Ds.cetaTcr ? 0 : (Ds.Vu <= (Ds.cetaVc / 2) ? 0 : float.Parse((Math.Max((0.2 * Math.Sqrt(Ds.fcprime) * Ds.b / Ds.Fys), Convert.ToDouble(3.5 * Ds.b / Ds.Fys))).ToString()));
                    //if (Ds.Vu <= Ds.cetaVc / 2)
                    //{
                    //    Ds.AvS = "0";
                    //}
                    //else
                    //{
                    //    if (Ds.Vu <= Ds.cetaVc)
                    //    {
                    //        Ds.AvS = Math.Max(3.5 * Ds.b / Ds.Fys, 0.2 * Math.Sqrt(Ds.fcprime) * Ds.b / Ds.Fys).ToString();
                    //    }
                    //    else
                    //    {
                    //        if ((Ds.Vu > Ds.cetaVc) && (Ds.Vu <= (Ds.cetaVc + Ds.cetaVsmax)))
                    //        {
                    //            if ((Ds.cetaVs / (0.75 * Ds.Fys * Ds.d)) < (Math.Max((3.5 * Ds.b / Ds.Fys), (0.2 * Math.Sqrt(Ds.fcprime) * Ds.b / Ds.Fys))))
                    //            {
                    //                if (Ds.Tu > Ds.cetaTcr)
                    //                {
                    //                    Ds.AvS = (Ds.cetaVs / (0.75 * Ds.Fys * Ds.d)).ToString();
                    //                }
                    //                else
                    //                {
                    //                    Ds.AvS = Ds.AvSmin.ToString();
                    //                }
                    //            }
                    //            else
                    //            {
                    //                Ds.AvS = (Ds.cetaVs / (0.75 * Ds.Fys * Ds.d)).ToString();
                    //            }
                    //        }
                    //        else
                    //        {
                    //            Ds.AvS = "REQUIRE BIGGER SECTION";
                    //        }
                    //    }
                    //}

                    if (Ds.Vu <= Ds.cetaVc / 2)
                    {
                        Ds.AvS = "0";
                    }
                    else if (Ds.Vu <= Ds.cetaVc)
                    {
                        Ds.AvS = Math.Max(3.5 * Ds.b / Ds.Fys, 0.2 * Math.Sqrt(Ds.fcprime) * Ds.b / Ds.Fys).ToString();
                    }
                    else if ((Ds.Vu > Ds.cetaVc) && (Ds.Vu <= (Ds.cetaVc + Ds.cetaVsmax)))
                    {
                        if ((Ds.cetaVs / (0.75 * Ds.Fys * Ds.d) < Math.Max(3.5 * Ds.b / Ds.Fys, 0.2 * Math.Sqrt(Ds.fcprime) * Ds.b / Ds.Fys)))
                        {
                            if (Ds.Tu > Ds.cetaTcr)
                            {
                                Ds.AvS = (Ds.cetaVs / (0.75 * Ds.Fys * Ds.d)).ToString();
                            }
                            else
                            {
                                Ds.AvS = Ds.AvSmin.ToString();
                            }

                        }
                        else
                        {
                            Ds.AvS = (Ds.cetaVs / (0.75 * Ds.Fys * Ds.d)).ToString();
                        }
                    }
                    else
                    {
                        Ds.AvS = "REQUIRE BIGGER SECTION";
                    }






                    //if (Ds.Vu <= Ds.cetaVc / 2)
                    //{
                    //    Ds.AvS = "0";
                    //}
                    //else if (Ds.Vu <= Ds.cetaVc)
                    //{
                    //    Ds.AvS = Ds.AvSmin.ToString();
                    //}
                    //else if (Ds.Vu > Ds.cetaVc && Ds.Vu <= (Ds.cetaVc + Ds.cetaVsmax))
                    //{
                    //    if (Ds.cetaVs / (0.75 * Ds.Fys * Ds.d) < Ds.AvSmin)
                    //    {
                    //        if (Ds.Tu > Ds.cetaTcr)
                    //        {
                    //            Ds.AvS = (Ds.cetaVs / (0.75 * Ds.Fys * Ds.d)).ToString();
                    //        }
                    //        else
                    //        {
                    //            Ds.AvS = Ds.AvSmin.ToString();
                    //        }
                    //    }
                    //    else
                    //    {
                    //        Ds.AvS = (Ds.cetaVs / (0.75 * Ds.Fys * Ds.d)).ToString();
                    //    }
                    //}
                    //else
                    //{
                    //    Ds.AvS = "REQUIRE BIGGER SECTION";
                    //}


                    if (Math.Sqrt((Ds.Vu / Math.Pow((Ds.b * Ds.d), 2) + Math.Pow((Ds.Tu * 100 * Ds.Ph / (1.7 * Math.Pow(Ds.Aoh, 2))), 2))) <= 0.75 * (Ds.cetaVc / (0.75 * Ds.b * Ds.d) + 2.12 * Math.Sqrt(Ds.fcprime)))
                    {
                        Ds.CHECKSECTIONDUETOSHEARTORSION = "OK";
                    }
                    else
                    {
                        Ds.CHECKSECTIONDUETOSHEARTORSION = "REQUIRE BIGGER SECTION";
                    }

                    Boolean R38C5;
                    if (Ds.AvS == "REQUIRE BIGGER SECTION" || Ds.CHECKSECTIONDUETOSHEARTORSION == "REQUIRE BIGGER SECTION")
                    {
                        R38C5 = true;
                    }
                    else
                    {
                        R38C5 = false;
                    }

                    if (Ds.CHECKSECTIONDUETOSHEARTORSION == "REQUIRE BIGGER SECTION")
                    {
                        Ds.Error6 = true;
                    }
                    else
                    {
                        Ds.Error6 = false;
                    }

                    if (Ds.NSin == 0)
                    {
                        Ds.Diainstirr = Ds.Diaoutstirr;
                        Ds.Spacingin = Ds.Spacingout;
                    }
                    else
                    {
                        Ds.Diainstirr = 0;
                        Ds.Spacingin = 0;
                    }

                    Ds.MaxSpacingV = Ds.Vu < Ds.cetaVc / 2 ? "NOT REQUIRED SHEAR STIRR." : (Ds.cetaVs < 1.1 * Math.Sqrt(Ds.fcprime) * Ds.b * Ds.d ? Math.Min(Ds.d * 10 / 2, 600) : Math.Min(Ds.d * 10 / 4, 300)).ToString();
                    Ds.MaxSpacingT = Ds.Tu < Ds.cetaTcr ? "NOT REQUIRED TORSION STIRR." : Math.Min(Ds.Ph * 10 / 8, 300).ToString();
                    if (Ds.MaxSpacingV == "NOT REQUIRED SHEAR STIRR." || Ds.MaxSpacingT == "NOT REQUIRED TORSION STIRR.")
                    {
                        Ds.AllowMaxSpacingoutStir = Ds.H * 10;
                    }
                    else
                    {
                        Ds.AllowMaxSpacingoutStir = Math.Min(float.Parse(Ds.MaxSpacingV), float.Parse(Ds.MaxSpacingT));
                    }

                    if (Ds.Spacingout <= Ds.AllowMaxSpacingoutStir)
                    {
                        Ds.CheckSpacingoutStir = "OK";
                    }
                    else
                    {
                        Ds.CheckSpacingoutStir = "NOT OK";
                    }

                    if (Ds.NSin == 0)
                    {
                        Ds.AllowMaxSpacinginnStirForShear = "-";
                    }
                    else if (Ds.Vu <= Ds.cetaVc / 2)
                    {
                        Ds.AllowMaxSpacinginnStirForShear = "NOT REQUIRE SHEAR STIRRUP";
                    }
                    else if (Ds.cetaVs < 1.1 * Math.Sqrt(Ds.fcprime) * Ds.b * Ds.d)
                    {
                        Ds.AllowMaxSpacinginnStirForShear = Math.Min((Ds.d * 10 / 2), 600).ToString();
                    }
                    else
                    {
                        Ds.AllowMaxSpacinginnStirForShear = Math.Min((Ds.d * 10 / 4), 300).ToString();
                    }
                    if (Ds.AllowMaxSpacinginnStirForShear == "NOT REQUIRE SHEAR STIRRUP")
                    {
                        Ds.AllowMaxSpacinginnStir = (Ds.H * 10).ToString();
                    }
                    else
                    {
                        Ds.AllowMaxSpacinginnStir = Ds.AllowMaxSpacinginnStirForShear;
                    }
                    if (Ds.NSin == 0)
                    {
                        Ds.CheckSpacinginnStir = "-";
                    }
                    else if (Ds.Spacingout <= float.Parse(Ds.AllowMaxSpacinginnStir))
                    {
                        Ds.CheckSpacinginnStir = "OK";
                    }
                    else
                    {
                        Ds.CheckSpacinginnStir = "NOT OK";
                    }
                    Ds.Ag = Ds.b * Ds.H;
                    Ds.Ast = Ds.Ln1_ * Ds.FINALEACHEDGETOP1 + Ds.Ln2_ * Ds.FINALEACHEDGETOP2 + (Ds.Ln3_ - 2) * Ds.EachNetTopArea3 + 2 * Ds.FINALEACHEDGETOP3 + (Ds.Ln4_ - 2) * Ds.EachNetTopArea4 + 2 * Ds.FINALEACHEDGETOP4 + (Ds.Ln5_ - 2) * Ds.EachNetTopArea5 + 2 * Ds.FINALEACHEDGETOP5 + Ds.N1_ * Ds.FINALEACHEDGEBOT1 + Ds.N2_ * Ds.FINALEACHEDGEBOT2 + (Ds.N3_ - 2) * Ds.EachNetBotArea3 + 2 * Ds.FINALEACHEDGEBOT3 + (Ds.N4_ - 2) * Ds.EachNetBotArea4 + 2 * Ds.FINALEACHEDGEBOT4 + (Ds.N5_ - 2) * Ds.EachNetBotArea5 + 2 * Ds.FINALEACHEDGEBOT5;
                    Ds.P0 = float.Parse("0.85") * Ds.fcprime * (Ds.Ag - Ds.Ast) + Ds.Fy * Ds.Ast;
                    Ds.CetaP0 = float.Parse(String.Format("{0:F3}", 0.65)) * Ds.P0;
                    Ds.CetaPnMax = 0.8 * Ds.CetaP0;
                    Ds.CetaPntMax = -0.9 * (Ds.Fy * Ds.Ast);
                    Ds.EsR61C8 = Ds.Fy / Ds.Es;
                    Ds.beta1 = Ds.fcprime <= 280 ? float.Parse("0.85") : float.Parse((0.85 - 0.05 * (Ds.fcprime - 280) / 70 >= 0.65 ? 0.85 - 0.05 * (Ds.fcprime - 280) / 70 : 0.65).ToString());



                    List<FindMomentRatio> findMomentRatio = new List<FindMomentRatio>
                    {
                        new FindMomentRatio {
                            Z = 1,
                            Es1=0,
                            Ccm=0,
                            Fs1plus2ksc = 0,
                            Es3=0,
                            Fs3ksc = 0,
                            Es4 = 0,
                            Fs4ksc=0,
                            Es5=0,
                            Fs5ksc = 0,
                            EsPrime1=0,
                            FsPrime1plus2ksc = 0,
                            EsPrime3 = 0,
                            FsPrime3ksc = 0,
                            EsPrime4 = 0,
                            FsPrime4ksc = 0,
                            EsPrime5 = 0,
                            FsPrime5ksc = 0,
                            acm=0,
                            Ckg=0,
                            fs1plus2kg = 0,
                            Fs3kg=0,
                            Fs4kg=0,
                            Fs5kg=0,
                            SFsikg=0,
                            FsPrime1plus2kg=0,
                            FsPrime3kg=0,
                            FsPrime4kg=0,
                            FsPrime5kg=0,
                            SFsPrimekg=0,
                            Cha=0,
                            SFsiHDi=0,
                            SFsPrimei=0,
                            Pnkg=0,
                            Mnkgm=0,
                            Ceta=0,
                            CetaPnMaxKg=0,
                            CetaPnkg=Ds.CetaPnMax,
                            CetaMnkgm =0,
                            M=0,
                            DIFFm=999999,
                            CONDITION1=" ",
                            GREATERm=" ",
                            LOWERm=" ",
                            CetaPnkgOFLEFTPOINT=0,
                            CetaMnkgOFLEFTPOINT=0,
                            CetaPnkgOFRIGHTPOINT=0,
                            CetaMnkgOFRIGHTPOINT=0,
                        },
                        new FindMomentRatio { Z = 0.75 },
                        new FindMomentRatio { Z = 0.5 },
                        new FindMomentRatio { Z = 0.25 },
                        new FindMomentRatio { Z = 0.125 },
                        new FindMomentRatio { Z = 0 },
                        new FindMomentRatio { Z = -0.25 },
                        new FindMomentRatio { Z = -0.5 },
                        new FindMomentRatio { Z = -0.75 },
                        new FindMomentRatio { Z = -1 },
                        new FindMomentRatio { Z = -1.5 },
                        new FindMomentRatio { Z = -2 },
                        new FindMomentRatio { Z = -2.5 },
                        new FindMomentRatio { Z = -3 },
                        new FindMomentRatio { Z = -4 },
                        new FindMomentRatio { Z = -6 },
                        new FindMomentRatio { Z = 17,
                            Es1=0,
                            Ccm=0,
                            Fs1plus2ksc = 0,
                            Es3=0,
                            Fs3ksc = 0,
                            Es4 = 0,
                            Fs4ksc=0,
                            Es5=0,
                            Fs5ksc = 0,
                            EsPrime1=0,
                            FsPrime1plus2ksc = 0,
                            EsPrime3 = 0,
                            FsPrime3ksc = 0,
                            EsPrime4 = 0,
                            FsPrime4ksc = 0,
                            EsPrime5 = 0,
                            FsPrime5ksc = 0,
                            acm=0,
                            Ckg=0,
                            fs1plus2kg = 0,
                            Fs3kg=0,
                            Fs4kg=0,
                            Fs5kg=0,
                            SFsikg=0,
                            FsPrime1plus2kg=0,
                            FsPrime3kg=0,
                            FsPrime4kg=0,
                            FsPrime5kg=0,
                            SFsPrimekg=0,
                            Cha=0,
                            SFsiHDi=0,
                            SFsPrimei=0,
                            Pnkg=Ds.CetaPntMax/0.9,
                            Mnkgm=0,
                            Ceta=0,
                            CetaPnMaxKg=0,
                            CetaPnkg=Ds.CetaPntMax,
                            CetaMnkgm =0,
                            M=0,
                            DIFFm=-999999,
                            CONDITION1=" ",
                            GREATERm=" ",
                            LOWERm=" ",
                            CetaPnkgOFLEFTPOINT=0,
                            CetaMnkgOFLEFTPOINT=0,
                            CetaPnkgOFRIGHTPOINT=0,
                            CetaMnkgOFRIGHTPOINT=0, }
                    };
                    for (int c = 0; c <= findMomentRatio.Count - 1; c++)
                    {
                        findMomentRatio[c].Es1 = Ds.EsR61C8 * findMomentRatio[c].Z;
                        findMomentRatio[c].Ccm = 0.003 * Ds.d_1 / (0.003 - findMomentRatio[c].Es1);
                        //double checkedMin = Convert.ToDouble((findMomentRatio[c].Z * findMomentRatio[c].Es1).ToString());
                        double Fs1plus2ksc = (findMomentRatio[c].Z > 0 ? Convert.ToDouble(Math.Min(Convert.ToDouble(findMomentRatio[c].Z * Ds.EsR61C8 * Ds.Es), Convert.ToDouble(Ds.Fy))) : Convert.ToDouble(Math.Max(Convert.ToDouble(findMomentRatio[c].Z * Ds.EsR61C8 * Ds.Es), Convert.ToDouble(-Ds.Fy))));
                        if (Fs1plus2ksc >= 1000)
                        {
                            findMomentRatio[c].Fs1plus2ksc = Convert.ToInt32(Fs1plus2ksc / 1000) * 1000;
                        }
                        else
                        {
                            findMomentRatio[c].Fs1plus2ksc = Convert.ToInt32(Fs1plus2ksc / 100) * 100;
                        }

                        if (Ds.N3_ > 0)
                        {
                            if (findMomentRatio[c].Es1 >= 0)
                            {
                                findMomentRatio[c].Es3 = (0.003 - findMomentRatio[c].Es1) * (Ds.d_1 - Ds.d_2) / Ds.d_1 + findMomentRatio[c].Es1;
                            }
                            else
                            {
                                if (Ds.d_2 < findMomentRatio[c].Ccm)
                                {
                                    findMomentRatio[c].Es3 = (findMomentRatio[c].Ccm - Ds.d_2) / findMomentRatio[c].Ccm * 0.003;
                                }
                                else
                                {
                                    findMomentRatio[c].Es3 = findMomentRatio[c].Es1 * (Ds.d_2 - findMomentRatio[c].Ccm) / (Ds.d_1 - findMomentRatio[c].Ccm);
                                }
                            }
                        }
                        else
                        {
                            findMomentRatio[c].Es3 = 0;
                        }
                        findMomentRatio[c].Fs3ksc = findMomentRatio[c].Es3 > 0 ? Math.Min(Convert.ToDouble((findMomentRatio[c].Es3 * Ds.Es).ToString()), Ds.Fy) : Math.Max(Convert.ToDouble((findMomentRatio[c].Es3 * Ds.Es).ToString()), -Ds.Fy);
                        if (Ds.N4_ > 0)
                        {
                            if (findMomentRatio[c].Es1 >= 0)
                            {
                                findMomentRatio[c].Es4 = (0.003 - findMomentRatio[c].Es1) * (Ds.d_1 - Ds.d_3) / Ds.d_1 + findMomentRatio[c].Es1;
                            }
                            else
                            {
                                if (Ds.d_3 < findMomentRatio[c + 1].Ccm)
                                {
                                    findMomentRatio[c].Es4 = 0.003 * (findMomentRatio[c + 1].Ccm - Ds.d_3) / findMomentRatio[c + 1].Ccm;
                                }
                                else
                                {
                                    findMomentRatio[c].Es4 = findMomentRatio[c].Es1 * (Ds.d_3 - findMomentRatio[c + 1].Ccm) / (Ds.d_1 - findMomentRatio[c + 1].Ccm);
                                }
                            }
                        }
                        else
                        {
                            findMomentRatio[c].Es4 = 0;
                        }
                        findMomentRatio[c].Fs4ksc = findMomentRatio[c].Es4 > 0 ? Math.Min(Convert.ToDouble((findMomentRatio[c].Es4 * Ds.Es).ToString()), Ds.Fy) : Math.Max(Convert.ToDouble((findMomentRatio[c].Es4 * Ds.Es).ToString()), -Ds.Fy);
                        if (Ds.N5_ > 0)
                        {
                            if (findMomentRatio[c].Es1 >= 0)
                            {
                                findMomentRatio[c].Es5 = (0.003 - findMomentRatio[c].Es1) * (Ds.d_1 - Ds.d_4) / Ds.d_1 + findMomentRatio[c].Es1;
                            }
                            else
                            {
                                if (Ds.d_4 < findMomentRatio[c + 1].Ccm)
                                {
                                    findMomentRatio[c].Es5 = 0.003 * (findMomentRatio[c + 1].Ccm - Ds.d_4) / findMomentRatio[c + 1].Ccm;
                                }
                                else
                                {
                                    findMomentRatio[c].Es5 = findMomentRatio[c].Es1 * (Ds.d_4 - findMomentRatio[c + 1].Ccm) / (Ds.d_1 - findMomentRatio[c + 1].Ccm);
                                }
                            }
                        }
                        else
                        {
                            findMomentRatio[c].Es5 = 0;
                        }
                        findMomentRatio[c].Fs5ksc = findMomentRatio[c].Es5 > 0 ? Math.Min(Convert.ToDouble((findMomentRatio[c].Es5 * Ds.Es).ToString()), Ds.Fy) : Math.Max(Convert.ToDouble((findMomentRatio[c].Es5 * Ds.Es).ToString()), -Ds.Fy);
                        if (Ds.Ln1_ > 0)
                        {
                            if (findMomentRatio[c].Es1 >= 0)
                            {
                                findMomentRatio[c].EsPrime1 = (0.003 - findMomentRatio[c].Es1) * (Ds.d_1 - Ds.dprime1) / Ds.d_1 + findMomentRatio[c].Es1;
                            }
                            else
                            {
                                if (Ds.dprime1 > findMomentRatio[c].Ccm)
                                {
                                    findMomentRatio[c].EsPrime1 = findMomentRatio[c].Es1 * (Ds.dprime1 - findMomentRatio[c].Ccm) / (Ds.d_1 - findMomentRatio[c].Ccm);
                                }
                                else
                                {
                                    findMomentRatio[c].EsPrime1 = 0.003 * (findMomentRatio[c].Ccm - Ds.dprime1) / findMomentRatio[c].Ccm;
                                }
                            }
                        }
                        else
                        {
                            findMomentRatio[c].EsPrime1 = 0;
                        }
                        findMomentRatio[c].FsPrime1plus2ksc = findMomentRatio[c].EsPrime1 > 0 ? Convert.ToDouble(Math.Min(Convert.ToDouble(findMomentRatio[c].EsPrime1 * Ds.Es), Convert.ToDouble(Ds.Fy))) : Convert.ToDouble(Math.Max(Convert.ToDouble(findMomentRatio[c].EsPrime1 * Ds.Es), Convert.ToDouble(-Ds.Fy)));
                        if (Ds.Ln3_ > 0)
                        {
                            if (findMomentRatio[c].Es1 >= 0)
                            {
                                findMomentRatio[c].EsPrime3 = (0.003 - findMomentRatio[c].Es1) * (Ds.d_1 - Ds.dprime2) / Ds.d_1 + findMomentRatio[c].Es1;
                            }
                            else
                            {
                                if (Ds.dprime2 > findMomentRatio[c].Ccm)
                                {
                                    findMomentRatio[c].EsPrime3 = findMomentRatio[c].Es1 * (Ds.dprime2 - findMomentRatio[c].Ccm) / (Ds.d_1 - findMomentRatio[c].Ccm);
                                }
                                else
                                {
                                    findMomentRatio[c].EsPrime3 = 0.003 * (findMomentRatio[c].Ccm - Ds.dprime2) / findMomentRatio[c].Ccm;
                                }
                            }
                        }
                        else
                        {
                            findMomentRatio[c].EsPrime3 = 0;
                        }
                        findMomentRatio[c].FsPrime3ksc = findMomentRatio[c].EsPrime3 > 0 ? Math.Min(Convert.ToDouble((findMomentRatio[c].EsPrime3 * Ds.Es).ToString()), Ds.Fy) : Math.Max(Convert.ToDouble((findMomentRatio[c].EsPrime3 * Ds.Es).ToString()), -Ds.Fy);
                        if (Ds.Ln4_ > 0)
                        {
                            if (findMomentRatio[c].Es1 >= 0)
                            {
                                findMomentRatio[c].EsPrime4 = (0.003 - findMomentRatio[c].Es1) * (Ds.d_1 - Ds.dprime3) / Ds.d_1 + findMomentRatio[c].Es1;
                            }
                            else
                            {
                                if (Ds.dprime3 > findMomentRatio[c].Ccm)
                                {
                                    findMomentRatio[c].EsPrime4 = findMomentRatio[c].Es1 * (Ds.dprime3 - findMomentRatio[c].Ccm) / (Ds.d_1 - findMomentRatio[c].Ccm);
                                }
                                else
                                {
                                    findMomentRatio[c].EsPrime4 = 0.003 * (findMomentRatio[c].Ccm - Ds.dprime3) / findMomentRatio[c].Ccm;
                                }
                            }
                        }
                        else
                        {
                            findMomentRatio[c].EsPrime4 = 0;
                        }
                        findMomentRatio[c].FsPrime4ksc = findMomentRatio[c].EsPrime4 > 0 ? Math.Min(Convert.ToDouble((findMomentRatio[c].EsPrime4 * Ds.Es).ToString()), Ds.Fy) : Math.Max(Convert.ToDouble((findMomentRatio[c].EsPrime4 * Ds.Es).ToString()), -Ds.Fy);
                        if (Ds.Ln5_ > 0)
                        {
                            if (findMomentRatio[c].Es1 >= 0)
                            {
                                findMomentRatio[c].EsPrime5 = (0.003 - findMomentRatio[c].Es1) * (Ds.d_1 - Ds.dprime4) / Ds.d_1 + findMomentRatio[c].Es1;
                            }
                            else
                            {
                                if (Ds.dprime4 > findMomentRatio[c].Ccm)
                                {
                                    findMomentRatio[c].EsPrime5 = findMomentRatio[c].Es1 * (Ds.dprime4 - findMomentRatio[c].Ccm) / (Ds.d_1 - findMomentRatio[c].Ccm);
                                }
                                else
                                {
                                    findMomentRatio[c].EsPrime5 = 0.003 * (findMomentRatio[c].Ccm - Ds.dprime4) / findMomentRatio[c].Ccm;
                                }
                            }
                        }
                        else
                        {
                            findMomentRatio[c].EsPrime5 = 0;
                        }
                        findMomentRatio[c].FsPrime5ksc = findMomentRatio[c].EsPrime5 > 0 ? Math.Min(Convert.ToDouble((findMomentRatio[c].EsPrime5 * Ds.Es).ToString()), Ds.Fy) : Math.Max(Convert.ToDouble((findMomentRatio[c].EsPrime5 * Ds.Es).ToString()), -Ds.Fy);
                        findMomentRatio[c].acm = Convert.ToDouble(Math.Min(Convert.ToDouble(findMomentRatio[c].Ccm * Ds.beta1), Ds.H));
                        findMomentRatio[c].Ckg = 0.85 * Ds.fcprime * findMomentRatio[c].acm * Ds.b;
                        findMomentRatio[c].fs1plus2kg = Ds.d_1 < findMomentRatio[c].acm ? (findMomentRatio[c].Fs1plus2ksc - 0.85 * Ds.fcprime) * (Ds.N1_ * Ds.FINALEACHEDGEBOT1 + Ds.N2_ * Ds.FINALEACHEDGEBOT2) : findMomentRatio[c].Fs1plus2ksc * (Ds.N1_ * Ds.FINALEACHEDGEBOT1 + Ds.N2_ * Ds.FINALEACHEDGEBOT2);
                        findMomentRatio[c].Fs3kg = Ds.d_2 < findMomentRatio[c].acm ? (findMomentRatio[c].Fs3ksc - 0.85 * Ds.fcprime) * (Math.Max((Ds.N3_ - 2) * Ds.EachNetBotArea3 + 2 * Ds.FINALEACHEDGEBOT3, 0)) : findMomentRatio[c].Fs3ksc * (Math.Max((Ds.N3_ - 2) * Ds.EachNetBotArea3 + 2 * Ds.FINALEACHEDGEBOT3, 0));
                        findMomentRatio[c].Fs4kg = Ds.d_3 < findMomentRatio[c].acm ? (findMomentRatio[c].Fs4ksc - 0.85 * Ds.fcprime) * (Math.Max((Ds.N4_ - 2) * Ds.EachNetBotArea4 + 2 * Ds.FINALEACHEDGEBOT4, 0)) : findMomentRatio[c].Fs4ksc * (Math.Max((Ds.N4_ - 2) * Ds.EachNetBotArea4 + 2 * Ds.FINALEACHEDGEBOT4, 0));
                        findMomentRatio[c].Fs5kg = Ds.d_4 < findMomentRatio[c].acm ? (findMomentRatio[c].Fs5ksc - 0.85 * Ds.fcprime) * (Math.Max((Ds.N5_ - 2) * Ds.EachNetBotArea5 + 2 * Ds.FINALEACHEDGEBOT5, 0)) : findMomentRatio[c].Fs5ksc * (Math.Max((Ds.N5_ - 2) * Ds.EachNetBotArea5 + 2 * Ds.FINALEACHEDGEBOT5, 0));
                        findMomentRatio[c].SFsikg = findMomentRatio[c].fs1plus2kg + findMomentRatio[c].Fs3kg + findMomentRatio[c].Fs4kg + findMomentRatio[c].Fs5kg;
                        findMomentRatio[c].FsPrime1plus2kg = Ds.dprime1 < findMomentRatio[c].acm ? (findMomentRatio[c].FsPrime1plus2ksc - 0.85 * Ds.fcprime) * (Ds.Ln1_ * Ds.FINALEACHEDGEBOT1 + Ds.Ln2_ * Ds.FINALEACHEDGEBOT2) : (findMomentRatio[c].FsPrime1plus2ksc) * (Ds.Ln1_ * Ds.FINALEACHEDGEBOT1 + Ds.Ln2_ * Ds.FINALEACHEDGEBOT2);
                        findMomentRatio[c].FsPrime3kg = Ds.dprime2 < findMomentRatio[c].acm ? (findMomentRatio[c].FsPrime3ksc - 0.85 * Ds.fcprime) * Math.Max(((Ds.Ln3_ - 2) * Ds.EachNetBotArea3 + 2 * Ds.FINALEACHEDGETOP3), 0) : (findMomentRatio[c].FsPrime3ksc) * Math.Max(((Ds.Ln3_ - 2) * Ds.EachNetTopArea3 + 2 * Ds.FINALEACHEDGETOP3), 0);
                        findMomentRatio[c].FsPrime4kg = Ds.dprime3 < findMomentRatio[c].acm ? (findMomentRatio[c].FsPrime4ksc - 0.85 * Ds.fcprime) * Math.Max(((Ds.Ln4_ - 2) * Ds.EachNetBotArea4 + 2 * Ds.FINALEACHEDGETOP4), 0) : (findMomentRatio[c].FsPrime4ksc) * Math.Max(((Ds.Ln4_ - 2) * Ds.EachNetTopArea4 + 2 * Ds.FINALEACHEDGETOP4), 0);
                        findMomentRatio[c].FsPrime5kg = Ds.dprime4 < findMomentRatio[c].acm ? (findMomentRatio[c].FsPrime5ksc - 0.85 * Ds.fcprime) * Math.Max(((Ds.Ln5_ - 2) * Ds.EachNetBotArea5 + 2 * Ds.FINALEACHEDGETOP5), 0) : (findMomentRatio[c].FsPrime5ksc) * Math.Max(((Ds.Ln5_ - 2) * Ds.EachNetTopArea5 + 2 * Ds.FINALEACHEDGETOP5), 0);
                        findMomentRatio[c].SFsPrimekg = findMomentRatio[c].FsPrime1plus2kg + findMomentRatio[c].FsPrime3kg + findMomentRatio[c].FsPrime4kg + findMomentRatio[c].FsPrime5kg;
                        findMomentRatio[c].Cha = (findMomentRatio[c].Ckg * (Ds.H / 2 - findMomentRatio[c].acm / 2)) / 100;
                        findMomentRatio[c].SFsiHDi = (findMomentRatio[c].fs1plus2kg * (Ds.H / 2 - Ds.d_1) + findMomentRatio[c].Fs3kg * (Ds.H / 2 - Ds.d_2) + findMomentRatio[c].Fs4kg * (Ds.H / 2 - Ds.d_3) + findMomentRatio[c].Fs5kg * (Ds.H / 2 - Ds.d_4)) / 100;
                        findMomentRatio[c].SFsPrimei = (findMomentRatio[c].FsPrime1plus2kg * (Ds.H / 2 - Ds.dprime1) + findMomentRatio[c].FsPrime3kg * (Ds.H / 2 - Ds.dprime2) + findMomentRatio[c].FsPrime4kg * (Ds.H / 2 - Ds.dprime3) + findMomentRatio[c].FsPrime5kg * (Ds.H / 2 - Ds.dprime4)) / 100;
                        findMomentRatio[c].Pnkg = findMomentRatio[c].Ckg + findMomentRatio[c].SFsikg + findMomentRatio[c].SFsPrimekg;
                        findMomentRatio[c].Mnkgm = Math.Max(Convert.ToDouble(findMomentRatio[c].Cha + findMomentRatio[c].SFsiHDi + findMomentRatio[c].SFsPrimei), 0);
                        if (findMomentRatio[c].Es1 >= 0)
                        {
                            findMomentRatio[c].Ceta = 0.65;
                        }
                        else
                        {
                            if (findMomentRatio[c].Es1 <= -0.005)
                            {
                                findMomentRatio[c].Ceta = 0.9;
                            }
                            else
                            {
                                findMomentRatio[c].Ceta = 0.65 + Math.Max((Math.Abs(Convert.ToDouble(findMomentRatio[c].Es1)) - Ds.EsR61C8), 0) * (0.25 / (0.005 - Ds.EsR61C8));
                            }
                        }
                        findMomentRatio[c].CetaPnMaxKg = 0.8 * Ds.CetaP0;
                        findMomentRatio[c].CetaPnkg = Math.Min(Convert.ToDouble(findMomentRatio[c].Ceta * findMomentRatio[c].Pnkg), Convert.ToDouble(findMomentRatio[c].CetaPnMaxKg));
                        findMomentRatio[c].CetaMnkgm = findMomentRatio[c].Mnkgm * findMomentRatio[c].Ceta;
                        findMomentRatio[c].M = findMomentRatio[c].CetaMnkgm == 0 ? 0 : findMomentRatio[c].CetaPnkg / findMomentRatio[c].CetaMnkgm;
                        findMomentRatio[c].DIFFm = findMomentRatio[c].M == 0 ? 999999 : findMomentRatio[c].M - Ds.Pu / Math.Max(Ds.Mu, 1);
                        findMomentRatio[c].CONDITION1 = findMomentRatio[c].DIFFm > 0 ? "GREATER" : "LOWER";
                        findMomentRatio[c].GREATERm = findMomentRatio[c].CONDITION1 == "GREATER" ? (findMomentRatio[c].DIFFm).ToString() : "-";
                        findMomentRatio[c].LOWERm = findMomentRatio[c].CONDITION1 == "LOWER" ? findMomentRatio[c].DIFFm.ToString() : "-";

                    }
                    findMomentRatio[16].DIFFm = -999999;
                    for (int c = 1; c <= findMomentRatio.Count - 1; c++)
                    {
                        List<double> gmax = new List<double>();
                        List<double> lmin = new List<double>();
                        for (int d = 1; d < findMomentRatio.Count - 1; d++)
                        {
                            if (findMomentRatio[d].GREATERm != "-")
                            {
                                gmax.Add(Convert.ToDouble(findMomentRatio[d].GREATERm));
                            }
                            if (findMomentRatio[d].LOWERm != "-")
                            {
                                lmin.Add(Convert.ToDouble(findMomentRatio[d].LOWERm));
                            }
                        }

                        if (findMomentRatio[c].GREATERm != "-")
                        {
                            findMomentRatio[c].CetaPnkgOFLEFTPOINT = Convert.ToDouble(findMomentRatio[c].GREATERm) == gmax.Min() ? findMomentRatio[c].CetaPnkg : 0;
                            findMomentRatio[c].CetaMnkgOFLEFTPOINT = Convert.ToDouble(findMomentRatio[c].GREATERm) == gmax.Min() ? findMomentRatio[c].CetaMnkgm : 0;
                        }
                        if (findMomentRatio[c].LOWERm != "-")
                        {
                            findMomentRatio[c].CetaPnkgOFRIGHTPOINT = Convert.ToDouble(findMomentRatio[c].LOWERm) == lmin.Max() ? findMomentRatio[c].CetaPnkg : 0;
                            findMomentRatio[c].CetaMnkgOFRIGHTPOINT = Convert.ToDouble(findMomentRatio[c].LOWERm) == lmin.Max() ? findMomentRatio[c].CetaMnkgm : 0;
                        }
                    }
                    float cetaPntMax = float.Parse((-0.9).ToString()) * (Ds.Fy * Ds.Ast);
                    for (int c = 1; c <= findMomentRatio.Count - 1; c++)
                    {
                        findMomentRatio[0].DIFFm = 999999;
                        findMomentRatio[16].DIFFm = -999999;
                        findMomentRatio[c].CONDITION2 = findMomentRatio[c].DIFFm > 0 ? "GREATER" : "LOWER";
                        findMomentRatio[0].CONDITION2 = "GREATER";
                        findMomentRatio[16].CONDITION2 = "LOWER";
                        findMomentRatio[0].CONDITION1 = "GREATER";
                        findMomentRatio[16].CONDITION1 = "LOWER";
                        findMomentRatio[c].POF1stNEARESTPOINT = findMomentRatio[c].CONDITION1 == "GREATER" && findMomentRatio[c + 1].CONDITION1 == "LOWER" ? findMomentRatio[c].CetaPnkg.ToString() : "-";
                        findMomentRatio[c].MOF1stNEARESTPOINT = findMomentRatio[c].CONDITION1 == "GREATER" && findMomentRatio[c + 1].CONDITION1 == "LOWER" ? findMomentRatio[c].CetaMnkgm.ToString() : "-";
                        findMomentRatio[c].POF2ndNEARESTPOINT = findMomentRatio[c - 1].CONDITION1 == "GREATER" && findMomentRatio[c].CONDITION1 == "LOWER" ? findMomentRatio[c].CetaPnkg.ToString() : "-";
                        findMomentRatio[c].MOF2ndNEARESTPOINT = findMomentRatio[c - 1].CONDITION1 == "GREATER" && findMomentRatio[c].CONDITION1 == "LOWER" ? findMomentRatio[c].CetaMnkgm.ToString() : "-";
                    }
                    findMomentRatio[0].CetaPnkg = 0.8 * Ds.CetaP0;
                    findMomentRatio[0].CetaMnkgm = 0;
                    findMomentRatio[0].CONDITION2 = findMomentRatio[0].DIFFm > 0 ? "GREATER" : "LOWER";
                    findMomentRatio[0].POF1stNEARESTPOINT = findMomentRatio[0].CONDITION2 == "GREATER" && findMomentRatio[1].CONDITION2 == "LOWER" ? findMomentRatio[0].CetaPnkg.ToString() : "-";
                    findMomentRatio[0].MOF1stNEARESTPOINT = findMomentRatio[0].CONDITION2 == "GREATER" && findMomentRatio[1].CONDITION2 == "LOWER" ? findMomentRatio[0].CetaMnkgm.ToString() : "-";
                    findMomentRatio[0].POF2ndNEARESTPOINT = "-";
                    findMomentRatio[0].MOF2ndNEARESTPOINT = "-";

                    findMomentRatio[16].Pnkg = cetaPntMax / 0.9;
                    findMomentRatio[16].Mnkgm = 0;
                    findMomentRatio[16].CetaPnkg = cetaPntMax;
                    findMomentRatio[16].CetaMnkgm = 0;
                    findMomentRatio[16].DIFFm = -999999;
                    findMomentRatio[16].CONDITION2 = findMomentRatio[16].DIFFm > 0 ? "GREATER" : "LOWER";
                    findMomentRatio[16].POF1stNEARESTPOINT = "-";
                    findMomentRatio[16].MOF1stNEARESTPOINT = "-";
                    findMomentRatio[16].POF2ndNEARESTPOINT = findMomentRatio[15].CONDITION2 == "GREATER" && findMomentRatio[16].CONDITION2 == "LOWER" ? findMomentRatio[16].CetaPnkg.ToString() : "-";
                    findMomentRatio[16].MOF2ndNEARESTPOINT = findMomentRatio[15].CONDITION2 == "GREATER" && findMomentRatio[16].CONDITION2 == "LOWER" ? findMomentRatio[16].CetaMnkgm.ToString() : "-";
                    List<float> MaxPOF1stNEARESTPOINT = new List<float>(), MaxMOF1stNEARESTPOINT = new List<float>(), MaxPOF2ndNEARESTPOINT = new List<float>(), MaxMOF2ndNEARESTPOINT = new List<float>();
                    for (int f = 0; f <= findMomentRatio.Count - 1; f++)
                    {
                        if (findMomentRatio[f].POF1stNEARESTPOINT != "-")
                        {
                            MaxPOF1stNEARESTPOINT.Add(float.Parse(findMomentRatio[f].POF1stNEARESTPOINT));
                        }
                        if (findMomentRatio[f].MOF1stNEARESTPOINT != "-")
                        {
                            MaxMOF1stNEARESTPOINT.Add(float.Parse(findMomentRatio[f].MOF1stNEARESTPOINT));
                        }
                        if (findMomentRatio[f].POF2ndNEARESTPOINT != "-")
                        {
                            MaxPOF2ndNEARESTPOINT.Add(float.Parse(findMomentRatio[f].POF2ndNEARESTPOINT));
                        }
                        if (findMomentRatio[f].MOF2ndNEARESTPOINT != "-")
                        {
                            MaxMOF2ndNEARESTPOINT.Add(float.Parse(findMomentRatio[f].MOF2ndNEARESTPOINT));
                        }
                    }
                    Ds.CetaMOF1stNEARESTPOINT = MaxMOF1stNEARESTPOINT.Max();
                    Ds.CetaPOF1stNEARESTPOINT = MaxPOF1stNEARESTPOINT.Max();
                    Ds.CetaMOF2ndNEARESTPOINT = MaxMOF2ndNEARESTPOINT.Max();
                    if (MaxPOF2ndNEARESTPOINT.Max() == 0)
                    {
                        Ds.CetaPOF2ndNEARESTPOINT = findMomentRatio[findMomentRatio.Count - 1].Pnkg;
                    }
                    else
                    {
                        Ds.CetaPOF2ndNEARESTPOINT = MaxPOF2ndNEARESTPOINT.Max();
                    }

                    Ds.M2 = (Ds.CetaPOF2ndNEARESTPOINT - Ds.CetaPOF1stNEARESTPOINT) / (Ds.CetaMOF2ndNEARESTPOINT - Ds.CetaMOF1stNEARESTPOINT);
                    if (((Ds.CetaPOF2ndNEARESTPOINT - Ds.CetaPOF1stNEARESTPOINT) / (Ds.CetaMOF2ndNEARESTPOINT - Ds.CetaMOF1stNEARESTPOINT)) == 0)
                    {
                        Ds.M = 0.001;
                    }
                    else
                    {
                        Ds.M = ((Ds.CetaPOF2ndNEARESTPOINT - Ds.CetaPOF1stNEARESTPOINT) / (Ds.CetaMOF2ndNEARESTPOINT - Ds.CetaMOF1stNEARESTPOINT));
                    }
                    Ds.C = Ds.CetaPOF1stNEARESTPOINT - Ds.M * Ds.CetaMOF1stNEARESTPOINT;
                    Ds.MPobj = (Ds.Pu - Ds.C) / Ds.M;
                    Ds.AvtSoutprovide = float.Parse((2 * Ds.NSout * Math.PI * Math.Pow((Ds.Diaoutstirr / 10), 2) / 4 / (Ds.Spacingout / 10)).ToString());
                    Ds.AvSinprovide = float.Parse((Ds.NSin == 0 ? 0 : (2 * Ds.NSin * Math.PI * Math.Pow((Ds.Diainstirr / 10), 2) / 4 / (Ds.Spacingin / 10))).ToString());
                    Ds.AvtSallprovide = Ds.AvtSoutprovide + Ds.AvSinprovide;



                    if (R34C5 == true)
                    {
                        this.Error = 1;
                    }
                    if (R35C5 == true)
                    {
                        this.Error = 2;
                    }
                    if (R36C5 == true)
                    {
                        this.Error = 3;
                    }
                    if (R37C5 == true)
                    {
                        this.Error = 4;
                    }
                    if (R38C5 == true)
                    {
                        this.Error = 5;
                    }
                    if (Ds.Error6 == true)
                    {
                        this.Error = 5;
                    }



                    for (int k = 0; k <= 9; k++)
                    {
                        switch (k)
                        {
                            case 0:
                                Str[i].N1c = Convert.ToInt32(UsableSteel[k, 0]);
                                Str[i].D1c = Convert.ToInt32(UsableSteel[k, 1]);
                                Str[i].Ld1c = UsableSteel[k, 2];
                                break;
                            case 1:
                                Str[i].N2c = Convert.ToInt32(UsableSteel[k, 0]);
                                Str[i].D2c = Convert.ToInt32(UsableSteel[k, 1]);
                                Str[i].Ld2c = UsableSteel[k, 2];
                                break;
                            case 2:
                                Str[i].N3c = Convert.ToInt32(UsableSteel[k, 0]);
                                Str[i].D3c = Convert.ToInt32(UsableSteel[k, 1]);
                                Str[i].Ld3c = UsableSteel[k, 2];
                                break;
                            case 3:
                                Str[i].N4c = Convert.ToInt32(UsableSteel[k, 0]);
                                Str[i].D4c = Convert.ToInt32(UsableSteel[k, 1]);
                                Str[i].Ld4c = UsableSteel[k, 2];
                                break;
                            case 4:
                                Str[i].N5c = Convert.ToInt32(UsableSteel[k, 0]);
                                Str[i].D5c = Convert.ToInt32(UsableSteel[k, 1]);
                                Str[i].Ld5c = UsableSteel[k, 2];
                                break;
                            case 5:
                                Str[i].N1t = Convert.ToInt32(UsableSteel[k, 0]);
                                Str[i].D1t = Convert.ToInt32(UsableSteel[k, 1]);
                                Str[i].Ld1t = UsableSteel[k, 2];
                                break;
                            case 6:
                                Str[i].N2t = Convert.ToInt32(UsableSteel[k, 0]);
                                Str[i].D2t = Convert.ToInt32(UsableSteel[k, 1]);
                                Str[i].Ld2t = UsableSteel[k, 2];
                                break;
                            case 7:
                                Str[i].N3t = Convert.ToInt32(UsableSteel[k, 0]);
                                Str[i].D3t = Convert.ToInt32(UsableSteel[k, 1]);
                                Str[i].Ld3t = UsableSteel[k, 2];
                                break;
                            case 8:
                                Str[i].N4t = Convert.ToInt32(UsableSteel[k, 0]);
                                Str[i].D4t = Convert.ToInt32(UsableSteel[k, 1]);
                                Str[i].Ld4t = UsableSteel[k, 2];
                                break;
                            case 9:
                                Str[i].N5t = Convert.ToInt32(UsableSteel[k, 0]);
                                Str[i].D5t = Convert.ToInt32(UsableSteel[k, 1]);
                                Str[i].Ld5t = UsableSteel[k, 2];
                                break;
                        }
                    }


                    Str[i].N11 = Convert.ToInt32(UsableSteel[10, 0]);
                    Str[i].D11 = Convert.ToInt32(UsableSteel[10, 1]);
                    Str[i].Ld11 = UsableSteel[10, 2];
                    Str[i].Ds = Convert.ToInt32(UsableSteel[11, 0]);
                    Str[i].Nsout = Convert.ToInt32(UsableSteel[11, 1]);
                    Str[i].Nsin = Convert.ToInt32(UsableSteel[12, 1]);
                    Str[i].S = Convert.ToInt32(UsableSteel[11, 2]);


                    if (Ds.Mu != 0)
                    {
                        Str[i].Strength = float.Parse((Math.Sqrt(Math.Pow(Ds.Mu, 2) + Math.Pow(Ds.Pu, 2)) / Math.Sqrt(Math.Pow(Convert.ToDouble((Ds.CetaPOF2ndNEARESTPOINT - (Ds.CetaPOF2ndNEARESTPOINT - Ds.CetaPOF1stNEARESTPOINT) / (Ds.CetaMOF2ndNEARESTPOINT - Ds.CetaMOF1stNEARESTPOINT) * Ds.CetaMOF2ndNEARESTPOINT) / (Ds.Pu / Ds.Mu - Ds.M2)), 2) + Math.Pow(Convert.ToDouble((Ds.CetaPOF2ndNEARESTPOINT - (Ds.CetaPOF2ndNEARESTPOINT - Ds.CetaPOF1stNEARESTPOINT) / (Ds.CetaMOF2ndNEARESTPOINT - Ds.CetaMOF1stNEARESTPOINT) * Ds.CetaMOF2ndNEARESTPOINT) / (Ds.Pu / Ds.Mu - Ds.M2) * Ds.Pu / Ds.Mu), 2))).ToString());
                    }
                    else
                    {
                        Str[i].Strength = 0;
                    }
                    Str[i].Bending = float.Parse((Convert.ToDouble(Ds.Mu) / Ds.MPobj).ToString());
                    Str[i].Axial = Ds.Pu < 0 ? float.Parse((Ds.Pu / Ds.CetaPntMax).ToString()) : float.Parse((Ds.Pu / Ds.CetaPnMax).ToString());
                    if (Ds.Vu < Ds.cetaVc / 2)
                    {
                        if (Ds.Tu < Ds.cetaTcr)
                        {
                            Str[i].Torsion = 0;
                        }
                        else if (Ds.AvtSoutprovide >= 2 * Ds.AtSmin)
                        {
                            if (Ds.Spacingout <= float.Parse((Ds.MaxSpacingT != "NOT REQUIRED TORSION STIRR." ? Ds.MaxSpacingT : "0").ToString()))
                            {
                                Str[i].Torsion = 2 * Ds.Ats / Ds.AvtSoutprovide;
                            }
                            else
                            {
                                Str[i].Torsion = 8;
                            }
                        }
                        else
                        {
                            Str[i].Torsion = 9;
                        }
                    }
                    else if (Ds.Tu > Ds.cetaTcr)
                    {
                        if (Ds.AvtSoutprovide < 2 * Ds.AtSmin)
                        {
                            Str[i].Torsion = 9;
                        }
                        else if (Ds.Spacingout <= float.Parse((Ds.MaxSpacingT != "NOT REQUIRED TORSION STIRR." ? Ds.MaxSpacingT : "0").ToString()))
                        {
                            Str[i].Torsion = 2 * Ds.Ats / Ds.AvtSoutprovide;
                        }
                        else
                        {
                            Str[i].Torsion = 8;
                        }
                    }
                    else if (Ds.Spacingout <= float.Parse((Ds.MaxSpacingT != "NOT REQUIRED TORSION STIRR." ? Ds.MaxSpacingT : "0").ToString()) || Ds.MaxSpacingT == "NOT REQUIRED TORSION STIRR.")
                    {
                        Str[i].Torsion = 0;
                    }
                    else
                    {
                        Str[i].Torsion = 8;
                    }


                    if (Ds.Vu < Ds.cetaVc / 2)
                    {
                        if (Ds.Tu < Ds.cetaTcr)
                        {
                            Str[i].Shear = 0;
                        }
                        else if (Ds.Spacingout <= float.Parse((Ds.MaxSpacingV != "NOT REQUIRED SHEAR STIRR." ? Ds.MaxSpacingV : "0").ToString()) || Ds.MaxSpacingV == "NOT REQUIRED SHEAR STIRR.")
                        {
                            if (2 * Ds.Ats > Ds.AvtSoutprovide)
                            {
                                Str[i].Shear = 7;
                            }
                            else
                            {
                                Str[i].Shear = 0;
                            }
                        }
                        else
                        {
                            Str[i].Shear = 8;
                        }
                    }
                    else if (Ds.AvtSallprovide >= Ds.AvtSmin)
                    {
                        if (Ds.Spacingout <= float.Parse((Ds.MaxSpacingV != "NOT REQUIRED SHEAR STIRR." ? Ds.MaxSpacingV : "0").ToString()) || Ds.MaxSpacingV == "NOT REQUIRED SHEAR STIRR.")
                        {
                            if (2 * Ds.Ats > Ds.AvtSoutprovide)
                            {
                                Str[i].Shear = 7;
                            }
                            else if (Ds.Tu < Ds.cetaTcr)
                            {
                                Str[i].Shear = float.Parse((Ds.AvS).ToString()) / Ds.AvtSallprovide;
                            }
                            else
                            {
                                Str[i].Shear = float.Parse((Ds.AvS).ToString()) / (Ds.AvtSallprovide - 2 * Ds.Ats);
                            }
                        }
                        else
                        {
                            Str[i].Shear = 8;
                        }
                    }
                    else
                    {
                        Str[i].Shear = 9;
                    }

                    Ds.VcRatio = Str[i].Torsion == 0 && Str[i].Shear == 0 ? Math.Max((Ds.Vu / (Ds.cetaVc / 2)), (Ds.Tu / Ds.cetaTcr)) : 2;


                    if (Ds.VcRatio != 2)
                    {
                        Str[i].Vconc = Ds.VcRatio.ToString();
                    }
                    else
                    {
                        Str[i].Vconc = "0";
                    }


                    if (Str[i].Secc == "X0" && Str[i].M3 >= 0)
                    {
                        Str[i].BL = 'T';
                    }
                    else if (Str[i].Secc == "X10" && Str[i].M3 >= 0)
                    {
                        Str[i].BR = 'T';
                    }
                    else
                    {
                        Str[i].BL = ' ';
                        Str[i].BR = ' ';
                    }

                    if (Str[i].Secc == "X10")
                    {
                        Asmin = false;
                    }
                    Str[i].Al = Ds.Al;
                    Str[i].Avs = Ds.AvS;
                    Str[i].Ats = Ds.Ats;
                    Str[i].AvtSoutprovide = Ds.AvtSoutprovide;
                    Str[i].AvSinprovide = Ds.AvSinprovide;
                    Str[i].AvSallprovide = Ds.AvtSallprovide;
                    Str[i].d = Ds.d;
                    Str[i].FINALEACHEDGEBOT1 = Ds.FINALEACHEDGEBOT1;
                    Str[i].FINALEACHEDGEBOT2 = Ds.FINALEACHEDGEBOT2;
                    Str[i].FINALEACHEDGEBOT3 = Ds.FINALEACHEDGEBOT3;
                    Str[i].FINALEACHEDGEBOT4 = Ds.FINALEACHEDGEBOT4;
                    Str[i].FINALEACHEDGEBOT5 = Ds.FINALEACHEDGEBOT5;
                    Str[i].FINALEACHEDGETOP1 = Ds.FINALEACHEDGETOP1;
                    Str[i].FINALEACHEDGETOP2 = Ds.FINALEACHEDGETOP2;
                    Str[i].FINALEACHEDGETOP3 = Ds.FINALEACHEDGETOP3;
                    Str[i].FINALEACHEDGETOP4 = Ds.FINALEACHEDGETOP4;
                    Str[i].FINALEACHEDGETOP5 = Ds.FINALEACHEDGETOP5;


                    sqlconn.Open();
                    //String query20 = "UPDATE Stress SET N1c = @N1c where accessFileID = " + aid + " AND id = " + Str[i].Id;
                    String query20 = "UPDATE Stress SET N1c = @N1c , D1c = @D1c ,Ld1c = @Ld1c , N2c = @N2c , D2c = @D2c , Ld2c = @Ld2c , N3c = @N3c , D3c = @D3c , Ld3c = @Ld3c , N4c = @N4c , D4c = @D4c , Ld4c = @Ld4c , N5c = @N5c , D5c = @D5c , Ld5c = @Ld5c , N1t = @N1t , D1t = @D1t , Ld1t = @Ld1t , N2t = @N2t , D2t = @D2t , Ld2t = @Ld2t , N3t = @N3t , D3t = @D3t , Ld3t = @Ld3t , N4t = @N4t , D4t = @D4t , Ld4t = @Ld4t , N5t = @N5t , D5t = @D5t , Ld5t = @Ld5t , N11 = @N11 , D11 = @D11 , Ld11 = @Ld11 , Ds = @Ds , Nsout = @Nsout , Nsin = @Nsin , S = @S , Strength = @Strength , Bending = @Bending , Axial = @Axial , Torsion = @Torsion , Shear = @Shear , Vconc = @Vconc , BL = @BL , BR = @BR , Asmin = @Asmin, Al = @Al,Avs = @Avs,Ats = @Ats,AvtSoutprovide = @AvtSoutprovide,AvSinprovide = @AvSinprovide,AvSallprovide = @AvSallprovide,d = @d,FINALEACHEDGEBOT1 = @FINALEACHEDGEBOT1,FINALEACHEDGEBOT2 = @FINALEACHEDGEBOT2,FINALEACHEDGEBOT3 = @FINALEACHEDGEBOT3,FINALEACHEDGEBOT4 = @FINALEACHEDGEBOT4,FINALEACHEDGEBOT5 = @FINALEACHEDGEBOT5,FINALEACHEDGETOP1 = @FINALEACHEDGETOP1,FINALEACHEDGETOP2 = @FINALEACHEDGETOP2,FINALEACHEDGETOP3 = @FINALEACHEDGETOP3,FINALEACHEDGETOP4 = @FINALEACHEDGETOP4,FINALEACHEDGETOP5 = @FINALEACHEDGETOP5   where id = " + Str[i].Id;
                    SqlCommand sqlcomm20 = new SqlCommand(query20, sqlconn);
                    sqlcomm20.Parameters.AddWithValue("@N1c", Str[i].N1c);
                    sqlcomm20.Parameters.AddWithValue("@D1c", Str[i].D1c);
                    sqlcomm20.Parameters.AddWithValue("@Ld1c", Str[i].Ld1c);
                    sqlcomm20.Parameters.AddWithValue("@N2c", Str[i].N2c);
                    sqlcomm20.Parameters.AddWithValue("@D2c", Str[i].D2c);
                    sqlcomm20.Parameters.AddWithValue("@Ld2c", Str[i].Ld2c);
                    sqlcomm20.Parameters.AddWithValue("@N3c", Str[i].N3c);
                    sqlcomm20.Parameters.AddWithValue("@D3c", Str[i].D3c);
                    sqlcomm20.Parameters.AddWithValue("@Ld3c", Str[i].Ld3c);
                    sqlcomm20.Parameters.AddWithValue("@N4c", Str[i].N4c);
                    sqlcomm20.Parameters.AddWithValue("@D4c", Str[i].D4c);
                    sqlcomm20.Parameters.AddWithValue("@Ld4c", Str[i].Ld4c);
                    sqlcomm20.Parameters.AddWithValue("@N5c", Str[i].N5c);
                    sqlcomm20.Parameters.AddWithValue("@D5c", Str[i].D5c);
                    sqlcomm20.Parameters.AddWithValue("@Ld5c", Str[i].Ld5c);
                    sqlcomm20.Parameters.AddWithValue("@N1t", Str[i].N1t);
                    sqlcomm20.Parameters.AddWithValue("@D1t", Str[i].D1t);
                    sqlcomm20.Parameters.AddWithValue("@Ld1t", Str[i].Ld1t);
                    sqlcomm20.Parameters.AddWithValue("@N2t", Str[i].N2t);
                    sqlcomm20.Parameters.AddWithValue("@D2t", Str[i].D2t);
                    sqlcomm20.Parameters.AddWithValue("@Ld2t", Str[i].Ld2t);
                    sqlcomm20.Parameters.AddWithValue("@N3t", Str[i].N3t);
                    sqlcomm20.Parameters.AddWithValue("@D3t", Str[i].D3t);
                    sqlcomm20.Parameters.AddWithValue("@Ld3t", Str[i].Ld3t);
                    sqlcomm20.Parameters.AddWithValue("@N4t", Str[i].N4t);
                    sqlcomm20.Parameters.AddWithValue("@D4t", Str[i].D4t);
                    sqlcomm20.Parameters.AddWithValue("@Ld4t", Str[i].Ld4t);
                    sqlcomm20.Parameters.AddWithValue("@N5t", Str[i].N5t);
                    sqlcomm20.Parameters.AddWithValue("@D5t", Str[i].D5t);
                    sqlcomm20.Parameters.AddWithValue("@Ld5t", Str[i].Ld5t);
                    sqlcomm20.Parameters.AddWithValue("@N11", Str[i].N11);
                    sqlcomm20.Parameters.AddWithValue("@D11", Str[i].D11);
                    sqlcomm20.Parameters.AddWithValue("@Ld11", Str[i].Ld11);
                    sqlcomm20.Parameters.AddWithValue("@Ds", Str[i].Ds);
                    sqlcomm20.Parameters.AddWithValue("@Nsout", Str[i].Nsout);
                    sqlcomm20.Parameters.AddWithValue("@Nsin", Str[i].Nsin);
                    sqlcomm20.Parameters.AddWithValue("@S", Str[i].S);
                    sqlcomm20.Parameters.AddWithValue("@Strength", Str[i].Strength);
                    sqlcomm20.Parameters.AddWithValue("@Bending", Str[i].Bending);
                    sqlcomm20.Parameters.AddWithValue("@Axial", Str[i].Axial);
                    sqlcomm20.Parameters.AddWithValue("@Torsion", Str[i].Torsion);
                    sqlcomm20.Parameters.AddWithValue("@Shear", Str[i].Shear);
                    sqlcomm20.Parameters.AddWithValue("@Vconc", Str[i].Vconc);
                    sqlcomm20.Parameters.AddWithValue("@BL", Str[i].BL);
                    sqlcomm20.Parameters.AddWithValue("@BR", Str[i].BR);
                    sqlcomm20.Parameters.AddWithValue("@Asmin", Str[i].Asmin);
                    sqlcomm20.Parameters.AddWithValue("@Al", Str[i].Al);
                    sqlcomm20.Parameters.AddWithValue("@Avs", Str[i].Avs);
                    sqlcomm20.Parameters.AddWithValue("@Ats", Str[i].Ats);
                    sqlcomm20.Parameters.AddWithValue("@AvtSoutprovide", Str[i].AvtSoutprovide);
                    sqlcomm20.Parameters.AddWithValue("@AvSinprovide", Str[i].AvSinprovide);
                    sqlcomm20.Parameters.AddWithValue("@AvSallprovide", Str[i].AvSallprovide);
                    sqlcomm20.Parameters.AddWithValue("@d", Str[i].d);
                    sqlcomm20.Parameters.AddWithValue("@FINALEACHEDGEBOT1", Str[i].FINALEACHEDGEBOT1);
                    sqlcomm20.Parameters.AddWithValue("@FINALEACHEDGEBOT2", Str[i].FINALEACHEDGEBOT2);
                    sqlcomm20.Parameters.AddWithValue("@FINALEACHEDGEBOT3", Str[i].FINALEACHEDGEBOT3);
                    sqlcomm20.Parameters.AddWithValue("@FINALEACHEDGEBOT4", Str[i].FINALEACHEDGEBOT4);
                    sqlcomm20.Parameters.AddWithValue("@FINALEACHEDGEBOT5", Str[i].FINALEACHEDGEBOT5);
                    sqlcomm20.Parameters.AddWithValue("@FINALEACHEDGETOP1", Str[i].FINALEACHEDGETOP1);
                    sqlcomm20.Parameters.AddWithValue("@FINALEACHEDGETOP2", Str[i].FINALEACHEDGETOP2);
                    sqlcomm20.Parameters.AddWithValue("@FINALEACHEDGETOP3", Str[i].FINALEACHEDGETOP3);
                    sqlcomm20.Parameters.AddWithValue("@FINALEACHEDGETOP4", Str[i].FINALEACHEDGETOP4);
                    sqlcomm20.Parameters.AddWithValue("@FINALEACHEDGETOP5", Str[i].FINALEACHEDGETOP5);
                    sqlcomm20.ExecuteNonQuery();
                    sqlconn.Close();

                    if (Str[i].Secc == "X10")
                    {
                        Asmin = false;
                    }
                    if (R21C16 == 'T')
                    {
                        Asmin = true;
                        Str[i].Asmin = 'T';
                        while (Str[i].Secc != "X0")
                        {
                            i--;
                        }
                        i--;
                    }
                }
                i++;
            }
        }
        public Single[,] FindUsableSteel(Single[] Lenextra, Single Lt, Single[,] Steel, int[] ShearSteel, Single[,] DevLen, int[] Secc, int T, RC_Beam formContainer, Stress Str)
        {
            Boolean ChkCont = false;

            Single LL2, RL2, LL3, RL3, LL4, RL4, LL5, RL5, L2L, L2R, L3L, L3R, L4L, L4R, L5L, L5R, Tl, Tr;
            int Ds, Nsin, Nsout, Nslin, Nslout, Nsrin, Nsrout, Stln, Stls, Sts, Strn, Strs;
            int XP, X1A, X1B, X1C, X1D, X2A, X2B, X2C, X2D, X8A, XM, X8B, X8C, X8D, X9A, X9B, X9C, X9D, X1V, X9V;
            int X1 = 0, X9 = 0, X2 = 0, X8 = 0;
            Single LL = 0, RL = 0;
            float X;


            LL2 = Lenextra[1];
            LL3 = Lenextra[2];
            LL4 = Lenextra[3];
            LL5 = Lenextra[4];
            RL2 = Lenextra[5];
            RL3 = Lenextra[6];
            RL4 = Lenextra[7];
            RL5 = Lenextra[8];
            L2L = Lenextra[9];
            L3L = Lenextra[10];
            L4L = Lenextra[11];
            L5L = Lenextra[12];
            L2R = Lenextra[13];
            L3R = Lenextra[14];
            L4R = Lenextra[15];
            L5R = Lenextra[16];
            Tl = Lenextra[22];
            Tr = Lenextra[23];


            Ds = ShearSteel[0];
            Nsin = ShearSteel[1];
            Nsout = ShearSteel[2];
            Nslin = ShearSteel[3];
            Nslout = ShearSteel[4];
            Nsrin = ShearSteel[5];
            Nsrout = ShearSteel[6];
            Stln = ShearSteel[7];
            Stls = ShearSteel[8];
            Sts = ShearSteel[9];
            Strn = ShearSteel[10];
            Strs = ShearSteel[11];


            X1D = Secc[0];
            X1C = Secc[1];
            X1B = Secc[2];
            X1A = Secc[3];
            X2A = Secc[4];
            X2B = Secc[5];
            X2C = Secc[6];
            X2D = Secc[7];
            X8D = Secc[8];
            X8C = Secc[9];
            X8B = Secc[10];
            X8A = Secc[11];
            X9A = Secc[12];
            X9B = Secc[13];
            X9C = Secc[14];
            X9D = Secc[15];
            X1V = Secc[16];
            X9V = Secc[17];


            Single[,] UsableSteel = new Single[13, 3];


            X = float.Parse(Str.Station.ToString()) * 1000;


            if (float.Parse(Str.M3.ToString()) < 0)
            {
                UsableSteel[5, 0] = Steel[1, 0];
                UsableSteel[5, 1] = Steel[1, 1];
                UsableSteel[5, 2] = TopCont(X, Steel[1, 1], DevLen[0, 0], DevLen[0, 3], Lt, Tl, Tr, 50, formContainer);
            }
            else
            {
                UsableSteel[0, 0] = Steel[1, 0];
                UsableSteel[0, 1] = Steel[1, 1];
                UsableSteel[0, 2] = TopCont(X, Steel[1, 1], DevLen[0, 2], DevLen[0, 2], Lt, Tl, Tr, 50, formContainer);
            }

            for (int j = 1; j <= 4; j++)
            {
                switch (j)
                {
                    case 1:
                        X1 = X1A;
                        X9 = X9A;
                        LL = LL2;
                        RL = RL2;
                        if (formContainer.IsCheck3 == true)
                        {
                            ChkCont = true;
                        }
                        else
                        {
                            ChkCont = false;
                        }
                        break;
                    case 2:
                        X1 = X1B;
                        X9 = X9B;
                        LL = LL3;
                        RL = RL3;
                        if (formContainer.IsCheck4 == true)
                        {
                            ChkCont = true;
                        }
                        else
                        {
                            ChkCont = false;
                        }
                        break;
                    case 3:
                        X1 = X1C;
                        X9 = X9C;
                        LL = LL4;
                        RL = RL4;
                        if (formContainer.IsCheck5 == true)
                        {
                            ChkCont = true;
                        }
                        else
                        {
                            ChkCont = false;
                        }
                        break;
                    case 4:
                        X1 = X1D;
                        X9 = X9D;
                        LL = LL5;
                        RL = RL5;
                        if (formContainer.IsCheck6 == true)
                        {
                            ChkCont = true;
                        }
                        else
                        {
                            ChkCont = false;
                        }
                        break;

                }

                if (ChkCont == true)
                {
                    if (float.Parse(Str.M3.ToString()) < 0)
                    {
                        UsableSteel[j + 5, 0] = Steel[2 * j, 0];
                        UsableSteel[j + 5, 1] = Steel[2 * j, 1];
                        UsableSteel[j + 5, 2] = TopCont(X, Steel[2 * j, 1], DevLen[2 * j - 1, 0], DevLen[2 * j - 1, 3], Lt, Tl, Tr, 50 * j, formContainer);
                    }
                    else
                    {
                        UsableSteel[j, 0] = Steel[2 * j, 0];
                        UsableSteel[j, 1] = Steel[2 * j, 1];
                        UsableSteel[j, 2] = TopCont(X, Steel[2 * j, 1], DevLen[2 * j - 1, 2], DevLen[2 * j - 1, 2], Lt, Tl, Tr, 50 * j, formContainer);
                    }
                }
                else if (X < X1 && Steel[2 * j, 0] != 0)
                {
                    if (float.Parse(Str.M3.ToString()) < 0)
                    {
                        UsableSteel[j + 5, 0] = Steel[2 * j, 0];
                        UsableSteel[j + 5, 1] = Steel[2 * j, 1];
                        UsableSteel[j + 5, 2] = TopLeft(X, Steel[2 * j, 1], DevLen[2 * j - 1, 0], DevLen[2 * j - 1, 3], Tl, LL, 50 * j, formContainer);
                    }
                    else
                    {
                        UsableSteel[j, 0] = Steel[2 * j, 0];
                        UsableSteel[j, 1] = Steel[2 * j, 1];
                        UsableSteel[j, 2] = TopLeft(X, Steel[2 * j, 1], DevLen[2 * j - 1, 0], DevLen[2 * j - 1, 3], Tl, LL, 50 * j, formContainer);
                    }
                }
                else if (X > X9 && Steel[2 * j + 1, 0] != 0)
                {
                    if (float.Parse(Str.M3.ToString()) < 0)
                    {
                        UsableSteel[j + 5, 0] = Steel[2 * j + 1, 0];
                        UsableSteel[j + 5, 1] = Steel[2 * j + 1, 1];
                        UsableSteel[j + 5, 2] = TopRight(X, Steel[2 * j + 1, 1], DevLen[2 * j, 0], DevLen[2 * j, 3], Lt, Tr, RL, 50 * j, formContainer);
                    }
                    else
                    {
                        UsableSteel[j, 0] = Steel[2 * j + 1, 0];
                        UsableSteel[j, 1] = Steel[2 * j + 1, 1];
                        UsableSteel[j, 2] = TopRight(X, Steel[2 * j + 1, 1], DevLen[2 * j, 0], DevLen[2 * j, 3], Lt, Tr, RL, 50 * j, formContainer);
                    }
                }
            }


            if (float.Parse(Str.M3.ToString()) >= 0)
            {
                UsableSteel[5, 0] = Steel[10, 0];
                UsableSteel[5, 1] = Steel[10, 1];
                UsableSteel[5, 2] = BottomCont(X, Steel[10, 1], DevLen[9, 1], DevLen[9, 3], Lt, Tl, Tr, 50, formContainer);
            }
            else
            {
                UsableSteel[0, 0] = Steel[10, 0];
                UsableSteel[0, 1] = Steel[10, 1];
                UsableSteel[0, 2] = BottomCont(X, Steel[10, 1], DevLen[9, 2], DevLen[9, 2], Lt, Tl, Tr, 50, formContainer);
            }

            for (int j = 6; j <= 9; j++)
            {
                switch (j)
                {
                    case 6:
                        X2 = X2A;
                        X8 = X8A;
                        LL = L2L;
                        RL = L2R;
                        if (formContainer.IsCheck7 == true)
                        {
                            ChkCont = true;
                        }
                        else
                        {
                            ChkCont = false;
                        }
                        break;
                    case 7:
                        X2 = X2B;
                        X8 = X8B;
                        LL = L3L;
                        RL = L3R;
                        if (formContainer.IsCheck8 == true)
                        {
                            ChkCont = true;
                        }
                        else
                        {
                            ChkCont = false;
                        }
                        break;
                    case 8:
                        X2 = X2C;
                        X8 = X8C;
                        LL = L4L;
                        RL = L4R;
                        if (formContainer.IsCheck9 == true)
                        {
                            ChkCont = true;
                        }
                        else
                        {
                            ChkCont = false;
                        }
                        break;
                    case 9:
                        X2 = X2D;
                        X8 = X8D;
                        LL = L5L;
                        RL = L5R;
                        if (formContainer.IsCheck10 == true)
                        {
                            ChkCont = true;
                        }
                        else
                        {
                            ChkCont = false;
                        }
                        break;
                }
                if (ChkCont == true)
                {
                    if (float.Parse(Str.M3.ToString()) >= 0)
                    {
                        UsableSteel[j, 0] = Steel[j + 5, 0];
                        UsableSteel[j, 1] = Steel[j + 5, 1];
                        UsableSteel[j, 2] = BottomCont(X, Steel[j + 5, 1], DevLen[j + 4, 1], DevLen[j + 4, 3], Lt, Tl, Tr, 50 * (j - 5), formContainer);
                    }
                    else
                    {
                        UsableSteel[j - 5, 0] = Steel[j + 5, 0];
                        UsableSteel[j - 5, 1] = Steel[j + 5, 1];
                        UsableSteel[j - 5, 2] = BottomCont(X, Steel[j + 5, 1], DevLen[j + 4, 2], DevLen[j + 4, 2], Lt, Tl, Tr, 50 * (j - 5), formContainer);
                    }
                }
                else if ((X == 0 && X2 == 0 && Steel[j + 5, 0] != 0) || (X == Lt && X8 == Lt && Steel[j + 5, 0] != 0) || ((X > X2) && (X < X8) && Steel[j + 5, 0] != 0))
                {
                    if (float.Parse(Str.M3.ToString()) >= 0)
                    {
                        UsableSteel[j, 0] = Steel[j + 5, 0];
                        UsableSteel[j, 1] = Steel[j + 5, 1];
                        UsableSteel[j, 2] = BottomExtra(X, X2, X8, Steel[j + 5, 1], DevLen[j + 4, 1], DevLen[j + 4, 3], Lt, Tl, Tr, LL, RL, 50 * (j - 5), formContainer);
                    }
                    else
                    {
                        UsableSteel[j - 5, 0] = Steel[j + 5, 0];
                        UsableSteel[j - 5, 1] = Steel[j + 5, 1];
                        UsableSteel[j - 5, 2] = BottomExtra(X, X2, X8, Steel[j + 5, 1], DevLen[j + 4, 2], DevLen[j + 4, 2], Lt, Tl, Tr, LL, RL, 50 * (j - 5), formContainer);
                    }
                }
            }

            if (Steel[15, 0] != 0)
            {
                UsableSteel[10, 0] = Steel[15, 0];
                UsableSteel[10, 1] = Steel[15, 1];
                if (T / (Steel[15, 0] / 2 + 1) <= 30)
                {
                    UsableSteel[10, 2] = TorsionExtra(X, Steel[15, 1], DevLen[14, 0], Lt, Tl, Tr, formContainer);
                }
                else
                {
                    UsableSteel[10, 2] = TorsionExtra(X, Steel[15, 1], DevLen[14, 1], Lt, Tl, Tr, formContainer);
                }
            }

            if (X1V == 0 && X9V == 0)
            {
                UsableSteel[11, 0] = Ds;
                UsableSteel[11, 1] = Nsout;
                UsableSteel[11, 2] = Sts;
                UsableSteel[12, 0] = Ds;
                UsableSteel[12, 1] = Nsin;
                UsableSteel[12, 2] = Sts;
            }
            else if (X1V == 0 && X9V != 0)
            {
                if (X > X9V)
                {
                    UsableSteel[11, 0] = Ds;
                    UsableSteel[11, 1] = Nsout;
                    UsableSteel[11, 2] = Strs;
                    UsableSteel[12, 0] = Ds;
                    UsableSteel[12, 1] = Nsin;
                    UsableSteel[12, 2] = Strs;
                }
                else
                {
                    UsableSteel[11, 0] = Ds;
                    UsableSteel[11, 1] = Nsout;
                    UsableSteel[11, 2] = Sts;
                    UsableSteel[12, 0] = Ds;
                    UsableSteel[12, 1] = Nsin;
                    UsableSteel[12, 2] = Sts;
                }
            }
            else if (X1V != 0 && X9V == 0)
            {
                if (X < X1V)
                {
                    UsableSteel[11, 0] = Ds;
                    UsableSteel[11, 1] = Nsout;
                    UsableSteel[11, 2] = Stls;
                    UsableSteel[12, 0] = Ds;
                    UsableSteel[12, 1] = Nsin;
                    UsableSteel[12, 2] = Stls;
                }
                else
                {
                    UsableSteel[11, 0] = Ds;
                    UsableSteel[11, 1] = Nsout;
                    UsableSteel[11, 2] = Sts;
                    UsableSteel[12, 0] = Ds;
                    UsableSteel[12, 1] = Nsin;
                    UsableSteel[12, 2] = Sts;
                }
            }
            else if (X1V != 0 && X9V != 0)
            {
                if (X < X1V)
                {
                    UsableSteel[11, 0] = Ds;
                    UsableSteel[11, 1] = Nsout;
                    UsableSteel[11, 2] = Stls;
                    UsableSteel[12, 0] = Ds;
                    UsableSteel[12, 1] = Nsin;
                    UsableSteel[12, 2] = Stls;
                }
                else if (X >= X1V && X <= X9V)
                {
                    UsableSteel[11, 0] = Ds;
                    UsableSteel[11, 1] = Nsout;
                    UsableSteel[11, 2] = Sts;
                    UsableSteel[12, 0] = Ds;
                    UsableSteel[12, 1] = Nsin;
                    UsableSteel[12, 2] = Sts;
                }
                else if (X > X9V)
                {
                    UsableSteel[11, 0] = Ds;
                    UsableSteel[11, 1] = Nsout;
                    UsableSteel[11, 2] = Strs;
                    UsableSteel[12, 0] = Ds;
                    UsableSteel[12, 1] = Nsin;
                    UsableSteel[12, 2] = Strs;
                }
            }

            return UsableSteel;
        }
        public Single TopCont(float X, Single Db, Single DevLen, Single DevLenHook, Single Lt, Single Tl, Single Tr, int ss, RC_Beam formContainer)
        {
            if (formContainer.IsCheck)
            {
                formContainer.FL = 'T';
            }
            else
            {
                formContainer.FL = 'F';
            }
            if (formContainer.IsCheck2)
            {
                formContainer.FR = 'T';
            }
            else
            {
                formContainer.FR = 'F';
            }
            Single TopCont = 0;
            if (formContainer.CLT == 'T')
            {
                if (formContainer.CRT == 'F')
                {
                    if (formContainer.FR == 'F')
                    {
                        if (X < Lt - (Tr / 2))
                        {
                            TopCont = Math.Min((Lt + (Tr / 2) - ss - X) / DevLenHook, 1);
                        }
                        else
                        {
                            TopCont = Math.Min((Tr - ss) / DevLenHook, 1);
                        }
                    }
                    else
                    {
                        if (X >= Lt - ss)
                        {
                            TopCont = Math.Min(12 * Db / DevLen, 1);
                        }
                        else
                        {
                            TopCont = Math.Min((Lt - X - ss) / DevLenHook, 1);
                        }
                    }
                }
                else
                {
                    TopCont = 1;
                }
            }
            else
            {
                if (formContainer.CRT == 'F')
                {
                    if (formContainer.FL == 'F' && formContainer.FR == 'F')
                    {
                        if (X > Tl / 2 && X < Lt - (Tr / 2))
                        {
                            TopCont = Math.Min(Math.Min((X + Tl / 2 - ss) / DevLenHook, (Lt - X + Tr / 2 - ss) / DevLenHook), 1);
                        }
                        else if (X <= Tl / 2)
                        {
                            TopCont = Math.Min(Math.Min((Tl - ss) / DevLenHook, (Lt - X + Tr / 2 - ss) / DevLenHook), 1);
                        }
                        else
                        {
                            TopCont = Math.Min(Math.Min((Tr - ss) / DevLenHook, (X + Tl / 2 - ss) / DevLenHook), 1);
                        }
                    }
                    else if (formContainer.FL == 'F' && formContainer.FR == 'T')
                    {
                        if (X > Tl / 2 && X < Lt - ss)
                        {
                            TopCont = Math.Min(Math.Min((X + Tl / 2 - ss) / DevLenHook, (Lt - X - ss) / DevLenHook), 1);
                        }
                        else if (X >= Lt - ss)
                        {
                            TopCont = Math.Min(Math.Min((Lt + Tl / 2 - 2 * ss) / DevLenHook, 12 * Db / DevLen), 1);
                        }
                        else if (X <= Tl / 2)
                        {
                            TopCont = Math.Min(Math.Min((Lt - X - ss) / DevLenHook, (Tl - ss) / DevLenHook), 1);
                        }
                    }
                    else
                    {
                        if (X > ss && X < Lt - (Tr / 2))
                        {
                            TopCont = Math.Min(Math.Min((X - ss) / DevLenHook, (Lt - X + Tr / 2 - ss) / DevLenHook), 1);
                        }
                        else if (X <= ss)
                        {
                            TopCont = Math.Min(Math.Min(12 * Db / DevLen, (Lt + Tr / 2 - 2 * ss) / DevLenHook), 1);
                        }
                        else
                        {
                            TopCont = Math.Min(Math.Min((X - ss) / DevLenHook, (Tr - ss) / DevLenHook), 1);
                        }
                    }
                }
                else
                {
                    if (formContainer.FL == 'F')
                    {
                        if (X > Tl / 2)
                        {
                            TopCont = Math.Min((X + Tl / 2 - ss) / DevLenHook, 1);
                        }
                        else
                        {
                            TopCont = Math.Min((Tl - ss) / DevLenHook, 1);
                        }
                    }
                    else
                    {
                        if (X <= ss)
                        {
                            TopCont = Math.Min(12 * Db / DevLen, 1);
                        }
                        else
                        {
                            TopCont = Math.Min((X - ss) / DevLenHook, 1);
                        }
                    }
                }
            }
            return TopCont;
        }
        public Single TopLeft(Single X, Single Db, Single DevLen, Single DevLenHook, Single Tl, Single LL, Single ss, RC_Beam formContainer)
        {
            Single topleft;
            if (formContainer.IsCheck)
            {
                formContainer.FL = 'T';
            }
            else
            {
                formContainer.FL = 'F';
            }
            if (formContainer.IsCheck2)
            {
                formContainer.FR = 'T';
            }
            else
            {
                formContainer.FR = 'F';
            }
            if (formContainer.IsCheck3)
            {
                formContainer.Cot2 = 'T';
            }
            else
            {
                formContainer.Cot2 = 'F';
            }
            if (formContainer.IsCheck4)
            {
                formContainer.Cot3 = 'T';
            }
            else
            {
                formContainer.Cot3 = 'F';
            }
            if (formContainer.IsCheck5)
            {
                formContainer.Cot4 = 'T';
            }
            else
            {
                formContainer.Cot4 = 'F';
            }
            if (formContainer.IsCheck6)
            {
                formContainer.Cot5 = 'T';
            }
            else
            {
                formContainer.Cot5 = 'F';
            }
            if (formContainer.IsCheck7)
            {
                formContainer.Cob2 = 'T';
            }
            else
            {
                formContainer.Cob2 = 'F';
            }
            if (formContainer.IsCheck8)
            {
                formContainer.Cob3 = 'T';
            }
            else
            {
                formContainer.Cob3 = 'F';
            }
            if (formContainer.IsCheck9)
            {
                formContainer.Cob4 = 'T';
            }
            else
            {
                formContainer.Cob4 = 'F';
            }
            if (formContainer.IsCheck10)
            {
                formContainer.Cob5 = 'T';
            }
            else
            {
                formContainer.Cob5 = 'F';
            }
            if (formContainer.CLT == 'T')
            {
                topleft = Math.Min((LL - X) / DevLen, 1);
            }
            else if (formContainer.FL == 'F')
            {
                if (X <= Tl / 2)
                {
                    topleft = Math.Min(Math.Min((Tl - ss) / DevLenHook, (LL - X) / DevLen), 1);
                }
                else
                {
                    topleft = Math.Min(Math.Min((X + Tl / 2 - ss) / DevLenHook, (LL - X) / DevLen), 1);
                }
            }
            else
            {
                if (X <= ss)
                {
                    topleft = Math.Min(Math.Min(12 * Db / DevLen, (LL - ss) / DevLen), 1);
                }
                else
                {
                    topleft = Math.Min(Math.Min((X - ss) / DevLenHook, (LL - X) / DevLen), 1);
                }
            }
            return topleft;
        }
        public Single TopRight(Single X, Single Db, Single DevLen, Single DevLenHook, Single Lt, Single Tr, Single RL, int ss, RC_Beam formContainer)
        {
            Single topright;
            if (formContainer.IsCheck)
            {
                formContainer.FL = 'T';
            }
            else
            {
                formContainer.FL = 'F';
            }
            if (formContainer.IsCheck2)
            {
                formContainer.FR = 'T';
            }
            else
            {
                formContainer.FR = 'F';
            }
            if (formContainer.IsCheck3)
            {
                formContainer.Cot2 = 'T';
            }
            else
            {
                formContainer.Cot2 = 'F';
            }
            if (formContainer.IsCheck4)
            {
                formContainer.Cot3 = 'T';
            }
            else
            {
                formContainer.Cot3 = 'F';
            }
            if (formContainer.IsCheck5)
            {
                formContainer.Cot4 = 'T';
            }
            else
            {
                formContainer.Cot4 = 'F';
            }
            if (formContainer.IsCheck6)
            {
                formContainer.Cot5 = 'T';
            }
            else
            {
                formContainer.Cot5 = 'F';
            }
            if (formContainer.IsCheck7)
            {
                formContainer.Cob2 = 'T';
            }
            else
            {
                formContainer.Cob2 = 'F';
            }
            if (formContainer.IsCheck8)
            {
                formContainer.Cob3 = 'T';
            }
            else
            {
                formContainer.Cob3 = 'F';
            }
            if (formContainer.IsCheck9)
            {
                formContainer.Cob4 = 'T';
            }
            else
            {
                formContainer.Cob4 = 'F';
            }
            if (formContainer.IsCheck10)
            {
                formContainer.Cob5 = 'T';
            }
            else
            {
                formContainer.Cob5 = 'F';
            }
            if (formContainer.CRT == 'T')
            {
                topright = Math.Min((RL - (Lt - X)) / DevLen, 1);
            }
            else if (formContainer.CRT == 'F')
            {
                if (X >= (Lt - (Tr / 2)))
                {
                    topright = Math.Min(Math.Min((Tr - ss) / DevLenHook, (RL - (Lt - X)) / DevLen), 1);
                }
                else
                {
                    topright = Math.Min(Math.Min((Lt + (Tr / 2) - ss - X) / DevLenHook, (RL - (Lt - X)) / DevLen), 1);
                }
            }
            else
            {
                if (X >= Lt - ss)
                {
                    topright = Math.Min(Math.Min(12 * Db / DevLen, (RL - ss) / DevLen), 1);
                }
                else
                {
                    topright = Math.Min(Math.Min((Lt - X - ss) / DevLenHook, (RL - (Lt - X)) / DevLen), 1);
                }
            }
            return topright;
        }
        public Single BottomCont(Single X, Single Db, Single DevLen, Single DevLenHook, Single Lt, Single Tl, Single Tr, Single ss, RC_Beam formContainer)
        {
            if (formContainer.IsCheck)
            {
                formContainer.FL = 'T';
            }
            else
            {
                formContainer.FL = 'F';
            }
            if (formContainer.IsCheck2)
            {
                formContainer.FR = 'T';
            }
            else
            {
                formContainer.FR = 'F';
            }

            Single BottomCont = 0;
            if (formContainer.CLT == 'T')
            {
                if (formContainer.CRT == 'F')
                {
                    if (formContainer.FR == 'F')
                    {
                        if (formContainer.CRH == 'F')
                        {
                            if (X < Lt - (Tr / 2))
                            {
                                BottomCont = Math.Min((Lt + (Tr / 2) - 50 - X) / DevLen, 1);
                            }
                            else
                            {
                                BottomCont = Math.Min((Tr - 50) / DevLen, 1);
                            }
                        }
                        else
                        {
                            if (X < Lt - (Tr / 2))
                            {
                                BottomCont = Math.Min((Lt + (Tr / 2) - ss - X) / DevLenHook, 1);
                            }
                            else
                            {
                                BottomCont = Math.Min((Tr - ss) / DevLenHook, 1);
                            }
                        }
                    }
                    else
                    {
                        if (formContainer.CRH == 'F')
                        {
                            if (X >= Lt - 50)
                            {
                                BottomCont = 0;
                            }
                            else
                            {
                                BottomCont = Math.Min((Lt - X - 50) / DevLen, 1);
                            }
                        }
                        else
                        {
                            if (X >= Lt - ss)
                            {
                                BottomCont = Math.Min(12 * Db / DevLen, 1);
                            }
                            else
                            {
                                BottomCont = Math.Min((Lt - X - ss) / DevLenHook, 1);
                            }
                        }
                    }
                }
                else
                {
                    BottomCont = 1;
                }
            }
            else
            {
                if (formContainer.CRT == 'F')
                {
                    if (formContainer.FL == 'F' && formContainer.FR == 'F')
                    {
                        if (formContainer.CLH == 'F' && formContainer.CRH == 'F')
                        {
                            if (X > Tl / 2 && X < Lt - (Tr / 2))
                            {
                                BottomCont = Math.Min(Math.Min((X + Tl / 2 - 50) / DevLen, (Lt - X + Tr / 2 - 50) / DevLen), 1);
                            }
                            else if (X <= Tl / 2)
                            {
                                BottomCont = Math.Min(Math.Min((Tl - 50) / DevLen, (Lt - X + Tr / 2 - 50) / DevLen), 1);
                            }
                            else
                            {
                                BottomCont = Math.Min(Math.Min((Tr - 50) / DevLen, (X + Tl / 2 - 50) / DevLen), 1);
                            }
                        }
                        else if (formContainer.CLH == 'T' && formContainer.CRH == 'F')
                        {
                            if (X > Tl / 2 && X < Lt - (Tr / 2))
                            {
                                BottomCont = Math.Min(Math.Min((X + Tl / 2 - ss) / DevLenHook, (Lt - X + Tr / 2 - 50) / DevLen), 1);
                            }
                            else if (X <= Tl / 2)
                            {
                                BottomCont = Math.Min(Math.Min((Tl - ss) / DevLenHook, (Lt - X + Tr / 2 - 50) / DevLen), 1);
                            }
                            else
                            {
                                BottomCont = Math.Min(Math.Min((Tr - 50) / DevLen, (X + Tl / 2 - ss) / DevLenHook), 1);
                            }
                        }
                        else if (formContainer.CLH == 'F' && formContainer.CRH == 'T')
                        {
                            if (X > Tl / 2 && X < Lt - (Tr / 2))
                            {
                                BottomCont = Math.Min(Math.Min((X + Tl / 2 - 50) / DevLen, (Lt - X + Tr / 2 - ss) / DevLenHook), 1);
                            }
                            else if (X <= Tl / 2)
                            {
                                BottomCont = Math.Min(Math.Min((Tl - 50) / DevLen, (Lt - X + Tr / 2 - ss) / DevLenHook), 1);
                            }
                            else
                            {
                                BottomCont = Math.Min(Math.Min((Tr - ss) / DevLenHook, (X + Tl / 2 - 50) / DevLen), 1);
                            }
                        }
                        else if (formContainer.CLH == 'T' && formContainer.CRH == 'T')
                        {
                            if (X > Tl / 2 && X < Lt - (Tr / 2))
                            {
                                BottomCont = Math.Min(Math.Min((X + Tl / 2 - ss) / DevLenHook, (Lt - X + Tr / 2 - ss) / DevLenHook), 1);
                            }
                            else if (X <= Tl / 2)
                            {
                                BottomCont = Math.Min(Math.Min((Tl - ss) / DevLenHook, (Lt - X + Tr / 2 - ss) / DevLenHook), 1);
                            }
                            else
                            {
                                BottomCont = Math.Min(Math.Min((Tr - ss) / DevLenHook, (X + Tl / 2 - ss) / DevLenHook), 1);
                            }
                        }
                    }
                    else if (formContainer.FL == 'F' && formContainer.FR == 'T')
                    {
                        if (formContainer.CLH == 'F' && formContainer.CRH == 'F')
                        {
                            if (X > Tl / 2 && X < Lt - ss)
                            {
                                BottomCont = Math.Min(Math.Min((X + Tl / 2 - 50) / DevLen, (Lt - X - 50) / DevLen), 1);
                            }
                            else if (X >= Lt - 50)
                            {
                                BottomCont = 0;
                            }
                            else if (X <= Tl / 2)
                            {
                                BottomCont = Math.Min(Math.Min((Lt - X - 50) / DevLen, (Tl - 50) / DevLen), 1);
                            }
                        }
                        else if (formContainer.CLH == 'T' && formContainer.CRH == 'F')
                        {
                            if (X > Tl / 2 && X < Lt - 50)
                            {
                                BottomCont = Math.Min(Math.Min((X + Tl / 2 - ss) / DevLenHook, (Lt - X - 50) / DevLen), 1);
                            }
                            else if (X >= Lt - 50)
                            {
                                BottomCont = 0;
                            }
                            else if (X <= Tl / 2)
                            {
                                BottomCont = Math.Min(Math.Min((Lt - X - 50) / DevLen, (Tl - ss) / DevLenHook), 1);
                            }
                        }
                        else if (formContainer.CLH == 'F' && formContainer.CRH == 'T')
                        {
                            if (X > Tl / 2 && X < Lt - ss)
                            {
                                BottomCont = Math.Min(Math.Min((X + Tl / 2 - 50) / DevLen, (Lt - X - ss) / DevLenHook), 1);
                            }
                            else if (X >= Lt - ss)
                            {
                                BottomCont = Math.Min(Math.Min((Lt + Tl / 2 - 50 - ss) / DevLen, 12 * Db / DevLen), 1);
                            }
                            else if (X <= Tl / 2)
                            {
                                BottomCont = Math.Min(Math.Min((Lt - X - ss) / DevLenHook, (Tl - 50) / DevLen), 1);
                            }
                        }
                        else if (formContainer.CLH == 'T' && formContainer.CRH == 'T')
                        {
                            if (X > Tl / 2 && X < Lt - ss)
                            {
                                BottomCont = Math.Min(Math.Min((X + Tl / 2 - ss) / DevLenHook, (Lt - X - ss) / DevLenHook), 1);
                            }
                            else if (X >= Lt - ss)
                            {
                                BottomCont = Math.Min(Math.Min((Lt + Tl / 2 - 2 * ss) / DevLenHook, 12 * Db / DevLen), 1);
                            }
                            else if (X <= Tl / 2)
                            {
                                BottomCont = Math.Min(Math.Min((Lt - X - ss) / DevLenHook, (Tl - ss) / DevLenHook), 1);
                            }
                        }
                    }
                    else if (formContainer.FL == 'T' && formContainer.FR == 'F')
                    {
                        if (formContainer.CLH == 'F' && formContainer.CRH == 'F')
                        {
                            if (X > 50 && X < Lt - (Tr / 2))
                            {
                                BottomCont = Math.Min(Math.Min((X - 50) / DevLen, (Lt - X + Tr / 2 - 50) / DevLen), 1);
                            }
                            else if (X <= 50)
                            {
                                BottomCont = 0;
                            }
                            else
                            {
                                BottomCont = Math.Min(Math.Min((X - 50) / DevLen, (Tr - 50) / DevLen), 1);
                            }
                        }
                        else if (formContainer.CLH == 'T' && formContainer.CRH == 'F')
                        {
                            if (X > ss && X < Lt - (Tr / 2))
                            {
                                BottomCont = Math.Min(Math.Min((X - ss) / DevLenHook, (Lt - X + Tr / 2 - 50) / DevLen), 1);
                            }
                            else if (X <= ss)
                            {
                                BottomCont = Math.Min(Math.Min((Lt + Tr / 2 - ss - 50) / DevLenHook, 12 * Db / DevLen), 1);
                            }
                            else
                            {
                                BottomCont = Math.Min(Math.Min((X - ss) / DevLenHook, (Tr - 50) / DevLen), 1);
                            }
                        }
                        else if (formContainer.CLH == 'F' && formContainer.CRH == 'T')
                        {
                            if (X > 50 && X < Lt - (Tr / 2))
                            {
                                BottomCont = Math.Min(Math.Min((X - 50) / DevLen, (Lt - X + Tr / 2 - ss) / DevLenHook), 1);
                            }
                            else if (X <= 50)
                            {
                                BottomCont = 0;
                            }
                            else
                            {
                                BottomCont = Math.Min(Math.Min((X - 50) / DevLen, (Tr - ss) / DevLenHook), 1);
                            }
                        }
                        else if (formContainer.CLH == 'T' && formContainer.CRH == 'T')
                        {
                            if (X > ss && X < Lt - (Tr / 2))
                            {
                                BottomCont = Math.Min(Math.Min((X - ss) / DevLenHook, (Lt - X + Tr / 2 - ss) / DevLenHook), 1);
                            }
                            else if (X <= ss)
                            {
                                BottomCont = Math.Min(Math.Min((Lt + Tr / 2 - 2 * ss) / DevLenHook, 12 * Db / DevLen), 1);
                            }
                            else
                            {
                                BottomCont = Math.Min(Math.Min((X - ss) / DevLenHook, (Tr - ss) / DevLenHook), 1);
                            }
                        }
                    }
                }
                else
                {
                    if (formContainer.FL == 'F')
                    {
                        if (formContainer.CLH == 'F')
                        {
                            if (X > Tl / 2)
                            {
                                BottomCont = Math.Min((X + Tl / 2 - 50) / DevLen, 1);
                            }
                            else
                            {
                                BottomCont = Math.Min((Tl - 50) / DevLen, 1);
                            }
                        }
                        else if (formContainer.CLH == 'T')
                        {
                            if (X > Tl / 2)
                            {
                                BottomCont = Math.Min((X + Tl / 2 - ss) / DevLenHook, 1);
                            }
                            else
                            {
                                BottomCont = Math.Min((Tl - ss) / DevLenHook, 1);
                            }
                        }
                    }
                    else
                    {
                        if (formContainer.CLH == 'F')
                        {
                            if (X <= 50)
                            {
                                BottomCont = 0;
                            }
                            else
                            {
                                BottomCont = Math.Min((X - 50) / DevLen, 1);
                            }
                        }
                        else if (formContainer.CLH == 'T')
                        {
                            if (X <= ss)
                            {
                                BottomCont = Math.Min(12 * Db / DevLen, 1);
                            }
                            else
                            {
                                BottomCont = Math.Min((X - ss) / DevLenHook, 1);
                            }
                        }
                    }
                }
            }
            return BottomCont;
        }
        public Single BottomExtra(Single X, Single X2, Single X8, Single Db, Single DevLen, Single DevLenHook, Single Lt, Single Tl, Single Tr, Single LL, Single RL, Single ss, RC_Beam formContainer)

        {
            Single bottomextra = 0;
            if (formContainer.IsCheck)
            {
                formContainer.FL = 'T';
            }
            else
            {
                formContainer.FL = 'F';
            }
            if (formContainer.IsCheck2)
            {
                formContainer.FR = 'T';
            }
            else
            {
                formContainer.FR = 'F';
            }
            if (formContainer.IsCheck3)
            {
                formContainer.Cot2 = 'T';
            }
            else
            {
                formContainer.Cot2 = 'F';
            }
            if (formContainer.IsCheck4)
            {
                formContainer.Cot3 = 'T';
            }
            else
            {
                formContainer.Cot3 = 'F';
            }
            if (formContainer.IsCheck5)
            {
                formContainer.Cot4 = 'T';
            }
            else
            {
                formContainer.Cot4 = 'F';
            }
            if (formContainer.IsCheck6)
            {
                formContainer.Cot5 = 'T';
            }
            else
            {
                formContainer.Cot5 = 'F';
            }
            if (formContainer.IsCheck7)
            {
                formContainer.Cob2 = 'T';
            }
            else
            {
                formContainer.Cob2 = 'F';
            }
            if (formContainer.IsCheck8)
            {
                formContainer.Cob3 = 'T';
            }
            else
            {
                formContainer.Cob3 = 'F';
            }
            if (formContainer.IsCheck9)
            {
                formContainer.Cob4 = 'T';
            }
            else
            {
                formContainer.Cob4 = 'F';
            }
            if (formContainer.IsCheck10)
            {
                formContainer.Cob5 = 'T';
            }
            else
            {
                formContainer.Cob5 = 'F';
            }
            if (X2 == 0)
            {
                if (formContainer.CLT == 'F')
                {
                    if (formContainer.FL == 'F')
                    {
                        if (formContainer.CLH == 'F')
                        {
                            if (X <= Tl / 2)
                            {
                                bottomextra = Math.Min(Math.Min((Tl - 50) / DevLen, (Lt - RL - X) / DevLen), 1);
                            }
                            else
                            {
                                bottomextra = Math.Min(Math.Min((X + Tl / 2 - 50) / DevLen, (Lt - RL - X) / DevLen), 1);
                            }
                        }
                        else if (formContainer.CLH == 'T')
                        {
                            if (X <= Tl / 2)
                            {
                                bottomextra = Math.Min(Math.Min((Tl - ss) / DevLenHook, (Lt - RL - X) / DevLen), 1);
                            }
                            else
                            {
                                bottomextra = Math.Min(Math.Min((X + Tl / 2 - ss) / DevLenHook, (Lt - RL - X) / DevLen), 1);
                            }
                        }
                    }
                    else if (formContainer.FL == 'T')
                    {
                        if (formContainer.CLH == 'F')
                        {
                            if (X <= 50)
                            {
                                bottomextra = 0;
                            }
                            else
                            {
                                bottomextra = Math.Min(Math.Min((X - 50) / DevLen, (Lt - RL - X) / DevLen), 1);
                            }
                        }
                        else if (formContainer.CLH == 'T')
                        {
                            if (X <= ss)
                            {
                                bottomextra = Math.Min(Math.Min(12 * Db / DevLen, (Lt - RL - ss) / DevLen), 1);
                            }
                            else
                            {
                                bottomextra = Math.Min(Math.Min((X - ss) / DevLenHook, (Lt - RL - X) / DevLen), 1);
                            }
                        }
                    }
                }
                else if (formContainer.CLT == 'T')
                {
                    bottomextra = Math.Min((Lt - RL - X) / DevLen, 1);
                }
            }
            else if (X8 == Lt)
            {
                if (formContainer.CRH == 'F')
                {
                    if (formContainer.FR == 'F')
                    {
                        if (formContainer.CRH == 'F')
                        {
                            if (X >= Lt - Tr / 2)
                            {
                                bottomextra = Math.Min(Math.Min((Tr - 50) / DevLen, (X - LL) / DevLen), 1);
                            }
                            else
                            {
                                bottomextra = Math.Min(Math.Min((X + Tr / 2 - 50) / DevLen, (X - LL) / DevLen), 1);
                            }
                        }
                        else if (formContainer.CRH == 'T')
                        {
                            if (X >= Lt - Tr / 2)
                            {
                                bottomextra = Math.Min(Math.Min((Tr - ss) / DevLenHook, (X - LL) / DevLen), 1);
                            }
                            else
                            {
                                bottomextra = Math.Min(Math.Min((X + Tr / 2 - ss) / DevLenHook, (X - LL) / DevLen), 1);
                            }
                        }
                    }
                    else if (formContainer.FR == 'T')
                    {
                        if (formContainer.CRH == 'F')
                        {
                            if (X >= Lt - 50)
                            {
                                bottomextra = 0;
                            }
                            else
                            {
                                bottomextra = Math.Min(Math.Min((Lt - X - 50) / DevLen, (X - LL) / DevLen), 1);
                            }
                        }
                        else if (formContainer.CRH == 'T')
                        {
                            if (X >= Lt - ss)
                            {
                                bottomextra = Math.Min(Math.Min(12 * Db / DevLen, (Lt - LL - ss) / DevLen), 1);
                            }
                            else
                            {
                                bottomextra = Math.Min(Math.Min((X - LL) / DevLen, (Lt - X - ss) / DevLenHook), 1);
                            }
                        }
                    }
                }
                else if (formContainer.CRH == 'T')
                {
                    bottomextra = Math.Min((X - LL) / DevLen, 1);
                }
            }
            else
            {
                bottomextra = Math.Min(Math.Min((X - LL) / DevLen, (Lt - RL - X) / DevLen), 1);
            }
            return bottomextra;
        }
        public Single TorsionExtra(float X, Single Db, Single DevLen, Single Lt, Single Tl, Single Tr, RC_Beam formContainer)
        {
            Single torsionextra = 0;
            if (formContainer.IsCheck)
            {
                formContainer.FL = 'T';
            }
            else
            {
                formContainer.FL = 'F';
            }
            if (formContainer.IsCheck2)
            {
                formContainer.FR = 'T';
            }
            else
            {
                formContainer.FR = 'F';
            }
            if (formContainer.IsCheck3)
            {
                formContainer.Cot2 = 'T';
            }
            else
            {
                formContainer.Cot2 = 'F';
            }
            if (formContainer.IsCheck4)
            {
                formContainer.Cot3 = 'T';
            }
            else
            {
                formContainer.Cot3 = 'F';
            }
            if (formContainer.IsCheck5)
            {
                formContainer.Cot4 = 'T';
            }
            else
            {
                formContainer.Cot4 = 'F';
            }
            if (formContainer.IsCheck6)
            {
                formContainer.Cot5 = 'T';
            }
            else
            {
                formContainer.Cot5 = 'F';
            }
            if (formContainer.IsCheck7)
            {
                formContainer.Cob2 = 'T';
            }
            else
            {
                formContainer.Cob2 = 'F';
            }
            if (formContainer.IsCheck8)
            {
                formContainer.Cob3 = 'T';
            }
            else
            {
                formContainer.Cob3 = 'F';
            }
            if (formContainer.IsCheck9)
            {
                formContainer.Cob4 = 'T';
            }
            else
            {
                formContainer.Cob4 = 'F';
            }
            if (formContainer.IsCheck10)
            {
                formContainer.Cob5 = 'T';
            }
            else
            {
                formContainer.Cob5 = 'F';
            }
            if (formContainer.CLT == 'T')
            {
                if (formContainer.CRT == 'F')
                {
                    if (formContainer.FR == 'F')
                    {
                        if (X < Lt - (Tr / 2))
                        {
                            torsionextra = Math.Min((Lt + (Tr / 2) - 50 - X) / DevLen, 1);
                        }
                        else
                        {
                            torsionextra = Math.Min((Tr - 50) / DevLen, 1);
                        }
                    }
                    else
                    {
                        if (X >= Lt - 50)
                        {
                            torsionextra = 0;
                        }
                        else
                        {
                            torsionextra = Math.Min((Lt - X - 50) / DevLen, 1);
                        }
                    }
                }
                else
                {
                    torsionextra = 1;
                }
            }
            else
            {
                if (formContainer.CRT == 'F')
                {
                    if (formContainer.FL == 'F' && formContainer.FR == 'F')
                    {
                        if (X > Tl / 2 && X < Lt - (Tr / 2))
                        {
                            torsionextra = Math.Min(Math.Min((X + Tl / 2 - 50) / DevLen, (Lt - X + Tr / 2 - 50) / DevLen), 1);
                        }
                        else if (X <= Tl / 2)
                        {
                            torsionextra = Math.Min(Math.Min((Tl - 50) / DevLen, (Lt - X + Tr / 2 - 50) / DevLen), 1);
                        }
                        else
                        {
                            torsionextra = Math.Min(Math.Min((Tr - 50) / DevLen, (X + Tl / 2 - 50) / DevLen), 1);
                        }
                    }
                    else if (formContainer.FL == 'F' && formContainer.FR == 'T')
                    {
                        if ((X > Tl / 2) && (X < Lt))
                        {
                            torsionextra = Math.Min(Math.Min((X + Tl / 2 - 50) / DevLen, (Lt - X - 50) / DevLen), 1);
                        }
                        else if (X >= Lt - 50)
                        {
                            torsionextra = 0;
                        }
                        else if (X <= Tl / 2)
                        {
                            torsionextra = Math.Min(Math.Min((Lt - X - 50) / DevLen, (Tl - 50) / DevLen), 1);
                        }
                    }
                    else if (formContainer.FL == 'T' && formContainer.FR == 'F')
                    {
                        if ((X > 50) && (X < Lt - (Tr / 2)))
                        {
                            torsionextra = Math.Min(Math.Min((X - 50) / DevLen, (Lt - X + Tr / 2 - 50) / DevLen), 1);
                        }
                        else if (X <= 50)
                        {
                            torsionextra = 0;
                        }
                        else
                        {
                            torsionextra = Math.Min(Math.Min((X - 50) / DevLen, (Tr - 50) / DevLen), 1);
                        }
                    }
                }
                else
                {
                    if (formContainer.FL == 'F')
                    {
                        if (X > Tl / 2)
                        {
                            torsionextra = Math.Min((X + Tl / 2 - 50) / DevLen, 1);
                        }
                        else
                        {
                            torsionextra = Math.Min((Tl - 50) / DevLen, 1);
                        }
                    }
                    else
                    {
                        if (X <= 50)
                        {
                            torsionextra = 0;
                        }
                        else
                        {
                            torsionextra = Math.Min((X - 50) / DevLen, 1);
                        }
                    }
                }
            }
            return torsionextra;
        }
        public int[] addrow(int dd, Single[] Lenextra, Single Lt)
        {
            int[] addrow;
            int LL2, RL2, LL3, RL3, LL4, RL4, LL5, RL5, L2L, L2R, L3L, L3R, L4L, L4R, L5L, L5R, Tl, Tr, Stln, Stls, Sts, Strn, Strs;
            Single X, XP, X1A, X1B, X1C, X1D, X2A, X2B, X2C, X2D, X8A, XM, X8B, X8C, X8D, X9A, X9B, X9C, X9D, X1V, X9V;
            LL2 = Convert.ToInt32(Lenextra[1]);
            LL3 = Convert.ToInt32(Lenextra[2]);
            LL4 = Convert.ToInt32(Lenextra[3]);
            LL5 = Convert.ToInt32(Lenextra[4]);
            RL2 = Convert.ToInt32(Lenextra[5]);
            RL3 = Convert.ToInt32(Lenextra[6]);
            RL4 = Convert.ToInt32(Lenextra[7]);
            RL5 = Convert.ToInt32(Lenextra[8]);
            L2L = Convert.ToInt32(Lenextra[9]);
            L3L = Convert.ToInt32(Lenextra[10]);
            L4L = Convert.ToInt32(Lenextra[11]);
            L5L = Convert.ToInt32(Lenextra[12]);
            L2R = Convert.ToInt32(Lenextra[13]);
            L3R = Convert.ToInt32(Lenextra[14]);
            L4R = Convert.ToInt32(Lenextra[15]);
            L5R = Convert.ToInt32(Lenextra[16]);
            Stln = Convert.ToInt32(Lenextra[17]);
            Stls = Convert.ToInt32(Lenextra[18]);
            Sts = Convert.ToInt32(Lenextra[19]);
            Strn = Convert.ToInt32(Lenextra[20]);
            Strs = Convert.ToInt32(Lenextra[21]);
            Tl = Convert.ToInt32(Lenextra[22]);
            Tr = Convert.ToInt32(Lenextra[23]);

            X1D = Math.Max(0, LL5 - dd);
            X1C = Math.Max(X1D, LL4 - dd);
            X1B = Math.Max(X1C, LL3 - dd);
            X1A = Math.Max(X1B, LL2 - dd);
            X9D = Convert.ToInt32(Math.Round(Math.Min(Lt * 1000, Lt * 1000 - RL5 + dd), 3));
            X9C = Convert.ToInt32(Math.Round(Math.Min(X9D, Lt * 1000 - RL4 + dd), 3));
            X9B = Convert.ToInt32(Math.Round(Math.Min(X9C, Lt * 1000 - RL3 + dd), 3));
            X9A = Convert.ToInt32(Math.Round(Math.Min(X9B, Lt * 1000 - RL2 + dd), 3));

            if (Stln != 0)
            {
                X1V = (Tl / 2 + 50 + (Stln - 1) * Stls);
            }
            else
            {
                X1V = 0;
            }

            if (Strn != 0)
            {
                X9V = Lt * 1000 - (Tr / 2 + 50 + (Strn - 1) * Strs);
            }
            else
            {
                X9V = 0;
            }

            if (L2L == 0)
            {
                X2A = 0;
            }
            else
            {
                X2A = L2L + dd;
            }

            if (L3L == 0)
            {
                X2B = 0;
            }
            else
            {
                X2B = L3L + dd;
            }
            if (L4L == 0)
            {
                X2C = 0;
            }
            else
            {
                X2C = L4L + dd;
            }
            if (L5L == 0)
            {
                X2D = 0;
            }
            else
            {
                X2D = L5L + dd;
            }

            if (L5R == 0)
            {
                X8D = Convert.ToInt32(Math.Round(Lt * 1000, 0));
            }
            else
            {
                X8D = Convert.ToInt32(Math.Round(Lt * 1000 - L5R - dd, 0));
            }

            if (L4R == 0)
            {
                X8C = Convert.ToInt32(Math.Round(Lt * 1000, 0));
            }
            else
            {
                X8C = Convert.ToInt32(Math.Round(Lt * 1000 - L4R - dd, 0));
            }

            if (L3R == 0)
            {
                X8B = Convert.ToInt32(Math.Round(Lt * 1000, 0));
            }
            else
            {
                X8B = Convert.ToInt32(Math.Round(Lt * 1000 - L3R - dd, 0));
            }

            if (L2R == 0)
            {
                X8A = Convert.ToInt32(Math.Round(Lt * 1000, 0));
            }
            else
            {
                X8A = Convert.ToInt32(Math.Round(Lt * 1000 - L2R - dd, 0));
            }

            int[] Station = new int[18];
            Station[0] = Convert.ToInt32(X1D);
            Station[1] = Convert.ToInt32(X1C);
            Station[2] = Convert.ToInt32(X1B);
            Station[3] = Convert.ToInt32(X1A);
            Station[4] = Convert.ToInt32(X2A);
            Station[5] = Convert.ToInt32(X2B);
            Station[6] = Convert.ToInt32(X2C);
            Station[7] = Convert.ToInt32(X2D);
            Station[8] = Convert.ToInt32(X8D);
            Station[9] = Convert.ToInt32(X8C);
            Station[10] = Convert.ToInt32(X8B);
            Station[11] = Convert.ToInt32(X8A);
            Station[12] = Convert.ToInt32(X9A);
            Station[13] = Convert.ToInt32(X9B);
            Station[14] = Convert.ToInt32(X9C);
            Station[15] = Convert.ToInt32(X9D);
            Station[16] = Convert.ToInt32(X1V);
            Station[17] = Convert.ToInt32(X9V);

            addrow = Station;

            int aid = Convert.ToInt32(Session["aid"]);
            List<Stress> St = new List<Stress>();
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            String q5 = "select * from stress where accessFileID = " + aid + " Order By [Case], [Station]";
            SqlCommand sqlc5 = new SqlCommand(q5, sqlconn);
            SqlDataReader cf = sqlc5.ExecuteReader();
            while (cf.Read())
            {
                St.Add(new Stress
                {
                    Id = Convert.ToInt32(cf["id"]),
                    MEM = Convert.ToInt32(cf["Mem"]),
                    Station = float.Parse(cf["Station"].ToString()),
                    Secc = cf["Secc"].ToString(),
                    Case = cf["Case"].ToString(),
                    M3 = float.Parse(cf["M3"].ToString()),
                    V2 = float.Parse(cf["V2"].ToString()),
                    T = float.Parse(cf["T"].ToString()),
                    P = float.Parse(cf["P"].ToString()),
                });
            }
            sqlconn.Close();
            int i = 0;
            for (i = 0; i < St.Count; i++)
            {
                X = Convert.ToSingle(St[i].Station * 1000);
                if (i != 0)
                {
                    XP = Convert.ToSingle(St[i - 1].Station * 1000);
                }
                else
                {
                    XP = 0;
                }
                if (X == 0)
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X0' where accessFileID = " + aid + " AND id = " + St[i].Id;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X0";
                }
                else if (X == X1D)
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X1D' where accessFileID = " + aid + " AND id = " + St[i].Id;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X1D";
                }
                else if (X == X1C)
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X1C' where accessFileID = " + aid + " AND id = " + St[i].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X1C";
                }
                else if (X == X1B)
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X1B' where accessFileID = " + aid + " AND id = " + St[i].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X1B";
                }
                else if (X == X1A)
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X1A' where accessFileID = " + aid + " AND id = " + St[i].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X1A";
                }
                else if ((X > X1D) && (X != X1D) && (XP < X1D))
                {
                    addrow1(i, X1D, "X1D", St);
                }
                else if ((X > X1C) && (X != X1C) && (X1C != X1D) && (XP < X1C))
                {
                    addrow1(i, X1C, "X1C", St);
                }
                else if ((X > X1B) && (X != X1B) && (X1B != X1C) && (XP < X1B))
                {
                    addrow1(i, X1B, "X1B", St);
                }
                else if ((X > X1A) && (X != X1A) && (X1A != X1B) && (XP < X1A))
                {
                    addrow1(i, X1A, "X1A", St);
                }
                else if ((X > X9A) && (X != X9A) && (X9A != X9B) && (XP < X9A))
                {
                    addrow1(i, X9A, "X9A", St);
                }
                else if ((X > X9B) && (X != X9B) && (X9B != X9C) && (XP < X9B))
                {
                    addrow1(i, X9B, "X9B", St);
                }
                else if ((X > X9C) && (X != X9C) && (X9C != X9D) && (XP < X9C))
                {
                    addrow1(i, X9C, "X9C", St);
                }
                else if ((X > X9D) && (X != X9D) && (XP < X9D))
                {
                    addrow1(i, X9D, "X9D", St);
                }
                else if (X == Math.Round(Lt * 1000, 0))
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X10' where accessFileID = " + aid + " AND id = " + St[i].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X10";
                }
                else if (X == X9D)
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X9D' where accessFileID = " + aid + " AND id = " + St[i].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X9D";
                }
                else if (X == X9C)
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X9C' where accessFileID = " + aid + " AND id = " + St[i].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X9C";
                }
                else if (X == X9B)
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X9B' where accessFileID = " + aid + " AND id = " + St[i].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X9B";
                }
                else if (X == X9A)
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X9A' where accessFileID = " + aid + " AND id = " + St[i].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X9A";
                }
                else
                {
                }
            }

            for (i = 0; i < St.Count; i++)
            {
                X = Convert.ToSingle(St[i].Station * 1000);
                if (i != 0)
                {
                    XP = Convert.ToSingle(St[i - 1].Station) * 1000;
                }
                else
                {
                    XP = 0;
                }
                if (X == X2D && St[i].Secc.ToString() == "")
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X2D' where accessFileID = " + aid + " AND id = " + St[i].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X2D";
                }
                else if (X == X2C && St[i].Secc.ToString() == "")
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X2C' where accessFileID = " + aid + " AND id = " + St[i].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X2C";
                }
                else if (X == X2B && St[i].Secc.ToString() == "")
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X2B' where accessFileID = " + aid + " AND id = " + St[i].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X2B";
                }
                else if (X == X2A && St[i].Secc.ToString() == "")
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X2A' where accessFileID = " + aid + " AND id = " + St[i].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X2A";
                }
                else if ((X > X2A) && (X != X2A) && (X2A != X2B) && (XP < X2A))
                {
                    addrow1(i, X2A, "X2A", St);
                }
                else if ((X > X2B) && (X != X2B) && (X2B != X2C) && (XP < X2B))
                {
                    addrow1(i, X2B, "X2B", St);
                }
                else if ((X > X2C) && (X != X2C) && (X2C != X2D) && (XP < X2C))
                {
                    addrow1(i, X2C, "X2C", St);
                }
                else if ((X > X2D) && (X != X2D) && (XP < X2D))
                {
                    addrow1(i, X2D, "X2D", St);
                }
                else if ((X > X8D) && (X != X8D) && (XP < X8D))
                {
                    addrow1(i, X8D, "X8D", St);
                }
                else if ((X > X8C) && (X != X8C) && (X8D != X8C) && (XP < X8C))
                {
                    addrow1(i, X8C, "X8C", St);
                }
                else if ((X > X8B) && (X != X2B) && (X8C != X8B) && (XP < X8B))
                {
                    addrow1(i, X8B, "X8B", St);
                }
                else if ((X > X8A) && (X != X8A) && (X8B != X8A) && (XP < X8A))
                {
                    addrow1(i, X8A, "X8A", St);
                }
                else if (X == X8D && St[i].Secc.ToString() == "")
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X8D' where accessFileID = " + aid + " AND id = " + St[i].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X8D";
                }
                else if (X == X8C && St[i].Secc.ToString() == "")
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X8C' where accessFileID = " + aid + " AND id = " + St[i].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X8C";
                }
                else if (X == X8B && St[i].Secc.ToString() == "")
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X8B' where accessFileID = " + aid + " AND id = " + St[i].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X8B";
                }
                else if (X == X8A && St[i].Secc.ToString() == "")
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X8A' where accessFileID = " + aid + " AND id = " + St[i].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X8A";
                }
                else
                {
                }
            }
            int j = 0;
            int c = 1;

            while (c < St.Count)
            {
                while (c < St.Count ? St[c].Case == St[c - 1].Case : false)
                {
                    if (float.Parse(St[c].M3.ToString()) > float.Parse(St[j].M3.ToString()))
                    {
                        j = c;
                    }
                    c++;
                }
                if (St[j].Secc == null || St[j].Secc == "")
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'XM' where accessFileID = " + aid + " AND id = " + St[j].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[j].Secc = "XM";
                }
                c++;
                j = c - 1;
            }

            i = 0;

            for (i = 0; i < St.Count; i++)
            {
                X = Convert.ToSingle(St[i].Station * 1000);
                if (i != 0)
                {
                    XP = Convert.ToSingle(St[i - 1].Station) * 1000;
                }
                else
                {
                    XP = 0;
                }
                if (X == X1V && St[i].Secc.ToString() == "")
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X1V' where accessFileID = " + aid + " AND id = " + St[i].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X1V";

                }
                else if ((X == X9V) && (St[i].Secc.ToString() == ""))
                {
                    sqlconn.Open();
                    String query10 = "UPDATE Stress SET Secc = 'X9V' where accessFileID = " + aid + " AND id = " + St[i].Id; ;
                    SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
                    sqlcomm10.ExecuteNonQuery();
                    sqlconn.Close();
                    St[i].Secc = "X9V";
                }
                else if ((X > X1V) && (X != X1V) && (XP < X1V))
                {
                    addrow1(i, X1V, "X1V", St);
                }
                else if ((X > X9V) && (X != X9V) && (XP < X9V))
                {
                    addrow1(i, X9V, "X9V", St);
                }
                else
                {
                }
            }

            return addrow;
        }
        public void addrow1(int i, Single sta, string secc, List<Stress> St)
        {
            int aid = Convert.ToInt32(Session["aid"]);
            int PID = Convert.ToInt32(Session["PProject"]);
            //List<Stress> St = new List<Stress>();
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            //sqlconn.Open();
            //String q5 = "select * from stress where accessFileID = " + aid + " Order By [Case] , [Station]";
            //SqlCommand sqlc5 = new SqlCommand(q5, sqlconn);
            //SqlDataReader cf = sqlc5.ExecuteReader();
            //while (cf.Read())
            //{
            //    St.Add(new Stress
            //    {
            //        //select anything
            //        Id = Convert.ToInt32(cf["id"]),
            //        MEM = Convert.ToInt32(cf["Mem"]),
            //        Station = float.Parse(cf["Station"].ToString()),
            //        Secc = cf["Secc"].ToString(),
            //        Case = cf["Case"].ToString(),
            //        M3 = float.Parse(cf["M3"].ToString()),
            //        V2 = float.Parse(cf["V2"].ToString()),
            //        T = float.Parse(cf["T"].ToString()),
            //        P = float.Parse(cf["P"].ToString()),
            //    });
            //}
            //sqlconn.Close();
            int aq1Mem = St[i - 1].MEM;
            float aq2Station = sta / 1000;
            string aq3Secc = secc;
            string aq4Case = St[i - 1].Case.ToString();
            float aq5M3 = float.Parse(String.Format("{0:F2}", (St[i - 1].M3 + (aq2Station - St[i - 1].Station) / (St[i].Station - St[i - 1].Station) * (St[i].M3 - St[i - 1].M3))));
            float aq6V2 = float.Parse(String.Format("{0:F2}", (St[i - 1].V2 + (aq2Station - St[i - 1].Station) / (St[i].Station - St[i - 1].Station) * (St[i].V2 - St[i - 1].V2))));
            float aq7T = float.Parse(String.Format("{0:F2}", (St[i - 1].T + (aq2Station - St[i - 1].Station) / (St[i].Station - St[i - 1].Station) * (St[i].T - St[i - 1].T))));
            float aq8P = float.Parse(String.Format("{0:F2}", (St[i - 1].P + (aq2Station - St[i - 1].Station) / (St[i].Station - St[i - 1].Station) * (St[i].P - St[i - 1].P))));

            //insert value into db
            sqlconn.Open();
            String aq = "INSERT INTO Stress(projectID,accessFileID,MEM,Station,Secc,[Case],M3,V2,T,P)  VALUES(@projectID,@accessFileID,@MEM,@Station,@Secc,@Case,@M3,@V2,@T,@P)";
            SqlCommand sqlcommaq = new SqlCommand(aq, sqlconn); ;
            sqlcommaq.Parameters.AddWithValue("@projectID", PID);
            sqlcommaq.Parameters.AddWithValue("@accessFileID", aid);
            sqlcommaq.Parameters.AddWithValue("@MEM", aq1Mem);
            sqlcommaq.Parameters.AddWithValue("@Station", aq2Station);
            sqlcommaq.Parameters.AddWithValue("@Secc", aq3Secc);
            sqlcommaq.Parameters.AddWithValue("@Case", aq4Case);
            sqlcommaq.Parameters.AddWithValue("@M3", aq5M3);
            sqlcommaq.Parameters.AddWithValue("@V2", aq6V2);
            sqlcommaq.Parameters.AddWithValue("@T", aq7T);
            sqlcommaq.Parameters.AddWithValue("@P", aq8P);
            sqlcommaq.ExecuteNonQuery();
            sqlconn.Close();
            //String aq1 = "UPDATE Stress SET MEM = " + St[i - 1].MEM + " where accessFileID = " + aid + " AND id = " + St[i].Id;
            //SqlCommand asqlcomm1 = new SqlCommand(aq1, sqlconn);
            //asqlcomm1.ExecuteNonQuery();
            //String aq2 = "UPDATE Stress SET Station = " + sta / 1000 + " where accessFileID = " + aid + " AND id = " + St[i].Id;
            //SqlCommand asqlcomm2 = new SqlCommand(aq2, sqlconn);
            //asqlcomm2.ExecuteNonQuery();
            //String aq3 = "UPDATE Stress SET Secc = '" + secc + "' where accessFileID = " + aid + " AND id = " + St[i].Id;
            //SqlCommand asqlcomm3 = new SqlCommand(aq3, sqlconn);
            //asqlcomm3.ExecuteNonQuery();
            //String aq4 = "UPDATE Stress SET [Case] = '" + St[i - 1].Case.ToString() + "' where accessFileID = " + aid + " AND id = " + St[i].Id;
            //SqlCommand asqlcomm4 = new SqlCommand(aq4, sqlconn);
            //asqlcomm4.ExecuteNonQuery();
            //String aq5 = "UPDATE Stress SET M3 = " + String.Format("{0:F2}", (St[i - 1].M3 + (St[i].Station - St[i - 1].Station) / (St[i + 1].Station - St[i - 1].Station) * (St[i + 1].M3 - St[i - 1].M3))) + " where accessFileID = " + aid + " AND id = " + St[i].Id;
            //SqlCommand asqlcomm5 = new SqlCommand(aq5, sqlconn);
            //asqlcomm5.ExecuteNonQuery();
            //String aq6 = "UPDATE Stress SET V2 = " + String.Format("{0:F2}", (St[i - 1].V2 + (St[i].Station - St[i - 1].Station) / (St[i + 1].Station - St[i - 1].Station) * (St[i + 1].V2 - St[i - 1].V2))) + " where accessFileID = " + aid + " AND id = " + St[i].Id;
            //SqlCommand asqlcomm6 = new SqlCommand(aq6, sqlconn);
            //asqlcomm6.ExecuteNonQuery();
            //String aq7 = "UPDATE Stress SET T = " + String.Format("{0:F2}", (St[i - 1].T + (St[i].Station - St[i - 1].Station) / (St[i + 1].Station - St[i - 1].Station) * (St[i + 1].T - St[i - 1].T))) + " where accessFileID = " + aid + " AND id = " + St[i].Id;
            //SqlCommand asqlcomm7 = new SqlCommand(aq7, sqlconn);
            //asqlcomm7.ExecuteNonQuery();
            //String aq8 = "UPDATE Stress SET P = " + String.Format("{0:F2}", (St[i - 1].P + (St[i].Station - St[i - 1].Station) / (St[i + 1].Station - St[i - 1].Station) * (St[i + 1].P - St[i - 1].P))) + " where accessFileID = " + aid + " AND id = " + St[i].Id;
            //SqlCommand asqlcomm8 = new SqlCommand(aq8, sqlconn);
            //asqlcomm8.ExecuteNonQuery();
            sqlconn.Close();
        }

        public Single[,] FindMaxStress(RC_Beam formContainer)
        {
            int aid = Convert.ToInt32(Session["aid"]);
            List<Stress> St = new List<Stress>();
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            String q5 = "select * from Stress where accessFileID = " + aid + " Order By [Case] , [Station]";
            SqlCommand sqlc5 = new SqlCommand(q5, sqlconn);
            SqlDataReader cf = sqlc5.ExecuteReader();
            while (cf.Read())
            {
                St.Add(new Stress
                {
                    //select anything
                    Id = Convert.ToInt32(cf["id"].ToString()),
                    MEM = cf["N1c"] != DBNull.Value ? Convert.ToInt32(cf["MEM"].ToString()) : 0,
                    Station = cf["N1c"] != DBNull.Value ? float.Parse(cf["Station"].ToString()) : 0,
                    Secc = cf["Secc"] != DBNull.Value ? cf["Secc"].ToString() : "",
                    Case = cf["Case"] != DBNull.Value ? cf["Case"].ToString() : "",
                    M3 = cf["M3"] != DBNull.Value ? float.Parse(cf["M3"].ToString()) : 0,
                    V2 = cf["V2"] != DBNull.Value ? float.Parse(cf["V2"].ToString()) : 0,
                    T = cf["T"] != DBNull.Value ? float.Parse(cf["T"].ToString()) : 0,
                    P = cf["P"] != DBNull.Value ? float.Parse(cf["P"].ToString()) : 0,
                    N1c = cf["N1c"] != DBNull.Value ? Convert.ToInt32(cf["N1c"].ToString()) : 0,
                    D1c = cf["D1c"] != DBNull.Value ? Convert.ToInt32(cf["D1c"].ToString()) : 0,
                    Ld1c = cf["Ld1c"] != DBNull.Value ? float.Parse(cf["Ld1c"].ToString()) : 0,
                    N2c = cf["N2c"] != DBNull.Value ? Convert.ToInt32(cf["N2c"].ToString()) : 0,
                    D2c = cf["D2c"] != DBNull.Value ? Convert.ToInt32(cf["D2c"].ToString()) : 0,
                    Ld2c = cf["Ld2c"] != DBNull.Value ? float.Parse(cf["Ld2c"].ToString()) : 0,
                    N3c = cf["N3c"] != DBNull.Value ? Convert.ToInt32(cf["N3c"].ToString()) : 0,
                    D3c = cf["D3c"] != DBNull.Value ? Convert.ToInt32(cf["D3c"].ToString()) : 0,
                    Ld3c = cf["Ld3c"] != DBNull.Value ? float.Parse(cf["Ld3c"].ToString()) : 0,
                    N4c = cf["N4c"] != DBNull.Value ? Convert.ToInt32(cf["N4c"].ToString()) : 0,
                    D4c = cf["D4c"] != DBNull.Value ? Convert.ToInt32(cf["D4c"].ToString()) : 0,
                    Ld4c = cf["Ld4c"] != DBNull.Value ? float.Parse(cf["Ld4c"].ToString()) : 0,
                    N5c = cf["N5c"] != DBNull.Value ? Convert.ToInt32(cf["N5c"].ToString()) : 0,
                    D5c = cf["D5c"] != DBNull.Value ? Convert.ToInt32(cf["D5c"].ToString()) : 0,
                    Ld5c = cf["Ld5c"] != DBNull.Value ? float.Parse(cf["Ld5c"].ToString()) : 0,
                    N1t = cf["N1t"] != DBNull.Value ? Convert.ToInt32(cf["N1t"].ToString()) : 0,
                    D1t = cf["D1t"] != DBNull.Value ? Convert.ToInt32(cf["D1t"].ToString()) : 0,
                    Ld1t = cf["Ld1t"] != DBNull.Value ? float.Parse(cf["Ld1t"].ToString()) : 0,
                    N2t = cf["N2t"] != DBNull.Value ? Convert.ToInt32(cf["N2t"].ToString()) : 0,
                    D2t = cf["D2t"] != DBNull.Value ? Convert.ToInt32(cf["D2t"].ToString()) : 0,
                    Ld2t = cf["Ld2t"] != DBNull.Value ? float.Parse(cf["Ld2t"].ToString()) : 0,
                    N3t = cf["N3t"] != DBNull.Value ? Convert.ToInt32(cf["N3t"].ToString()) : 0,
                    D3t = cf["D3t"] != DBNull.Value ? Convert.ToInt32(cf["D3t"].ToString()) : 0,
                    Ld3t = cf["Ld3t"] != DBNull.Value ? float.Parse(cf["Ld3t"].ToString()) : 0,
                    N4t = cf["N4t"] != DBNull.Value ? Convert.ToInt32(cf["N4t"].ToString()) : 0,
                    D4t = cf["D4t"] != DBNull.Value ? Convert.ToInt32(cf["D4t"].ToString()) : 0,
                    Ld4t = cf["Ld4t"] != DBNull.Value ? float.Parse(cf["Ld4t"].ToString()) : 0,
                    N5t = cf["N5t"] != DBNull.Value ? Convert.ToInt32(cf["N5t"].ToString()) : 0,
                    D5t = cf["D5t"] != DBNull.Value ? Convert.ToInt32(cf["D5t"].ToString()) : 0,
                    Ld5t = cf["Ld5t"] != DBNull.Value ? float.Parse(cf["Ld5t"].ToString()) : 0,
                    N11 = cf["N11"] != DBNull.Value ? Convert.ToInt32(cf["N11"].ToString()) : 0,
                    D11 = cf["D11"] != DBNull.Value ? Convert.ToInt32(cf["D11"].ToString()) : 0,
                    Ld11 = cf["Ld11"] != DBNull.Value ? float.Parse(cf["Ld11"].ToString()) : 0,
                    Ds = cf["Ds"] != DBNull.Value ? Convert.ToInt32(cf["Ds"].ToString()) : 0,
                    Nsout = cf["Nsout"] != DBNull.Value ? Convert.ToInt32(cf["Nsout"].ToString()) : 0,
                    Nsin = cf["Nsin"] != DBNull.Value ? Convert.ToInt32(cf["Nsin"].ToString()) : 0,
                    S = cf["S"] != DBNull.Value ? Convert.ToInt32(cf["S"].ToString()) : 0,
                    Asmin = cf["Asmin"] != DBNull.Value ? Convert.ToChar(cf["Asmin"].ToString()) : ' ',
                    Strength = cf["Strength"] != DBNull.Value ? float.Parse(cf["Strength"].ToString()) : 0,
                    Bending = cf["Bending"] != DBNull.Value ? float.Parse(cf["Bending"].ToString()) : 0,
                    Axial = cf["Axial"] != DBNull.Value ? float.Parse(cf["Axial"].ToString()) : 0,
                    Torsion = cf["Torsion"] != DBNull.Value ? float.Parse(cf["Torsion"].ToString()) : 0,
                    Shear = cf["Shear"] != DBNull.Value ? float.Parse(cf["Shear"].ToString()) : 0,
                    Vconc = cf["Vconc"] != DBNull.Value ? cf["Vconc"].ToString() : "0",
                    Mm = cf["Mm"] != DBNull.Value ? Convert.ToChar(cf["Mm"].ToString()) : ' ',
                    BL = cf["BL"] != DBNull.Value ? Convert.ToChar(cf["BL"].ToString()) : ' ',
                    BR = cf["BR"] != DBNull.Value ? Convert.ToChar(cf["BR"].ToString()) : ' '
                });
            }
            sqlconn.Close();
            int X0, X10, XV0, XV10, i = 0;
            X0 = 0;
            X10 = 0;
            XV0 = 0;
            XV10 = 0;

            string[,] StrengthRatio = new string[26, 6];
            Single[,] ShearRatio = new Single[4, 2];


            string Station;


            Boolean DesignAll;
            if (formContainer.checkDesignAll == true)
            {
                DesignAll = true;
            }
            else
            {
                DesignAll = false;
            }


            while (i < St.Count)
            {

                Station = St[i].Secc;
                switch (Station)
                {
                    case "X0":
                        if ((St[i].Strength > float.Parse(StrengthRatio[0, 0] == null ? "0" : StrengthRatio[0, 0])) || X0 == 0)
                        {
                            StrengthRatio[0, 0] = St[i].Strength.ToString();
                            StrengthRatio[0, 1] = St[i].Bending.ToString();
                            StrengthRatio[0, 2] = St[i].Axial.ToString();
                            StrengthRatio[0, 3] = St[i].Torsion.ToString();
                            StrengthRatio[0, 4] = St[i].Shear.ToString();
                            StrengthRatio[0, 5] = i.ToString();
                            X0 = 1;
                        }
                        if (Math.Max(St[i].Torsion, St[i].Shear) > Math.Max(float.Parse(StrengthRatio[19, 3] == null ? "0" : StrengthRatio[19, 3]), float.Parse(StrengthRatio[19, 4] == null ? "0" : StrengthRatio[19, 4])) || XV0 == 0)
                        {
                            StrengthRatio[19, 0] = St[i].Strength.ToString();
                            StrengthRatio[19, 1] = St[i].Bending.ToString();
                            StrengthRatio[19, 2] = St[i].Axial.ToString();
                            StrengthRatio[19, 3] = St[i].Torsion.ToString();
                            StrengthRatio[19, 4] = St[i].Shear.ToString();
                            StrengthRatio[19, 5] = i.ToString();
                            XV0 = 1;
                        }
                        if (float.Parse(St[i].Vconc) >= float.Parse(StrengthRatio[0, 0] == null ? "0" : StrengthRatio[0, 0]))
                        {
                            ShearRatio[0, 0] = float.Parse(St[i].Vconc);
                            ShearRatio[0, 1] = i;
                        }
                        break;
                    case "X1A":
                        if (St[i].Strength > float.Parse(StrengthRatio[1, 0] == null ? "0" : StrengthRatio[1, 0]))
                        {
                            StrengthRatio[1, 0] = St[i].Strength.ToString();
                            StrengthRatio[1, 1] = St[i].Bending.ToString();
                            StrengthRatio[1, 2] = St[i].Axial.ToString();
                            StrengthRatio[1, 3] = St[i].Torsion.ToString();
                            StrengthRatio[1, 4] = St[i].Shear.ToString();
                            StrengthRatio[1, 5] = i.ToString();
                        }
                        break;
                    case "X1B":
                        if (St[i].Strength > float.Parse(StrengthRatio[2, 0] == null ? "0" : StrengthRatio[2, 0]))
                        {
                            StrengthRatio[2, 0] = St[i].Strength.ToString();
                            StrengthRatio[2, 1] = St[i].Bending.ToString();
                            StrengthRatio[2, 2] = St[i].Axial.ToString();
                            StrengthRatio[2, 3] = St[i].Torsion.ToString();
                            StrengthRatio[2, 4] = St[i].Shear.ToString();
                            StrengthRatio[2, 5] = i.ToString();
                        }
                        break;
                    case "X1C":
                        if (St[i].Strength > float.Parse(StrengthRatio[3, 0] == null ? "0" : StrengthRatio[3, 0]))
                        {
                            StrengthRatio[3, 0] = St[i].Strength.ToString();
                            StrengthRatio[3, 1] = St[i].Bending.ToString();
                            StrengthRatio[3, 2] = St[i].Axial.ToString();
                            StrengthRatio[3, 3] = St[i].Torsion.ToString();
                            StrengthRatio[3, 4] = St[i].Shear.ToString();
                            StrengthRatio[3, 5] = i.ToString();
                        }
                        break;
                    case "X1D":
                        if (St[i].Strength > float.Parse(StrengthRatio[4, 0] == null ? "0" : StrengthRatio[4, 0]))
                        {
                            StrengthRatio[4, 0] = St[i].Strength.ToString();
                            StrengthRatio[4, 1] = St[i].Bending.ToString();
                            StrengthRatio[4, 2] = St[i].Axial.ToString();
                            StrengthRatio[4, 3] = St[i].Torsion.ToString();
                            StrengthRatio[4, 4] = St[i].Shear.ToString();
                            StrengthRatio[4, 5] = i.ToString();
                        }
                        break;
                    case "X2A":
                        if (St[i].Strength > float.Parse(StrengthRatio[5, 0] == null ? "0" : StrengthRatio[5, 0]))
                        {
                            StrengthRatio[5, 0] = St[i].Strength.ToString();
                            StrengthRatio[5, 1] = St[i].Bending.ToString();
                            StrengthRatio[5, 2] = St[i].Axial.ToString();
                            StrengthRatio[5, 3] = St[i].Torsion.ToString();
                            StrengthRatio[5, 4] = St[i].Shear.ToString();
                            StrengthRatio[5, 5] = i.ToString();
                        }
                        break;
                    case "X2B":
                        if (St[i].Strength > float.Parse(StrengthRatio[6, 0] == null ? "0" : StrengthRatio[6, 0]))
                        {
                            StrengthRatio[6, 0] = St[i].Strength.ToString();
                            StrengthRatio[6, 1] = St[i].Bending.ToString();
                            StrengthRatio[6, 2] = St[i].Axial.ToString();
                            StrengthRatio[6, 3] = St[i].Torsion.ToString();
                            StrengthRatio[6, 4] = St[i].Shear.ToString();
                            StrengthRatio[6, 5] = i.ToString();
                        }
                        break;
                    case "X2C":
                        if (St[i].Strength > float.Parse(StrengthRatio[7, 0] == null ? "0" : StrengthRatio[7, 0]))
                        {
                            StrengthRatio[7, 0] = St[i].Strength.ToString();
                            StrengthRatio[7, 1] = St[i].Bending.ToString();
                            StrengthRatio[7, 2] = St[i].Axial.ToString();
                            StrengthRatio[7, 3] = St[i].Torsion.ToString();
                            StrengthRatio[7, 4] = St[i].Shear.ToString();
                            StrengthRatio[7, 5] = i.ToString();
                        }
                        break;
                    case "X2D":
                        if (St[i].Strength > float.Parse(StrengthRatio[8, 0] == null ? "0" : StrengthRatio[8, 0]))
                        {
                            StrengthRatio[8, 0] = St[i].Strength.ToString();
                            StrengthRatio[8, 1] = St[i].Bending.ToString();
                            StrengthRatio[8, 2] = St[i].Axial.ToString();
                            StrengthRatio[8, 3] = St[i].Torsion.ToString();
                            StrengthRatio[8, 4] = St[i].Shear.ToString();
                            StrengthRatio[8, 5] = i.ToString();
                        }
                        break;
                    case "XM":
                        if (St[i].Strength > float.Parse(StrengthRatio[9, 0] == null ? "0" : StrengthRatio[9, 0]))
                        {
                            StrengthRatio[9, 0] = St[i].Strength.ToString();
                            StrengthRatio[9, 1] = St[i].Bending.ToString();
                            StrengthRatio[9, 2] = St[i].Axial.ToString();
                            StrengthRatio[9, 3] = St[i].Torsion.ToString();
                            StrengthRatio[9, 4] = St[i].Shear.ToString();
                            StrengthRatio[9, 5] = i.ToString();
                        }
                        break;
                    case "X8A":
                        if (St[i].Strength > float.Parse(StrengthRatio[10, 0] == null ? "0" : StrengthRatio[10, 0]))
                        {
                            StrengthRatio[10, 0] = St[i].Strength.ToString();
                            StrengthRatio[10, 1] = St[i].Bending.ToString();
                            StrengthRatio[10, 2] = St[i].Axial.ToString();
                            StrengthRatio[10, 3] = St[i].Torsion.ToString();
                            StrengthRatio[10, 4] = St[i].Shear.ToString();
                            StrengthRatio[10, 5] = i.ToString();
                        }
                        break;
                    case "X8B":
                        if (St[i].Strength > float.Parse(StrengthRatio[11, 0] == null ? "0" : StrengthRatio[11, 0]))
                        {
                            StrengthRatio[11, 0] = St[i].Strength.ToString();
                            StrengthRatio[11, 1] = St[i].Bending.ToString();
                            StrengthRatio[11, 2] = St[i].Axial.ToString();
                            StrengthRatio[11, 3] = St[i].Torsion.ToString();
                            StrengthRatio[11, 4] = St[i].Shear.ToString();
                            StrengthRatio[11, 5] = i.ToString();
                        }
                        break;
                    case "X8C":
                        if (St[i].Strength > float.Parse(StrengthRatio[12, 0] == null ? "0" : StrengthRatio[12, 0]))
                        {
                            StrengthRatio[12, 0] = St[i].Strength.ToString();
                            StrengthRatio[12, 1] = St[i].Bending.ToString();
                            StrengthRatio[12, 2] = St[i].Axial.ToString();
                            StrengthRatio[12, 3] = St[i].Torsion.ToString();
                            StrengthRatio[12, 4] = St[i].Shear.ToString();
                            StrengthRatio[12, 5] = i.ToString();
                        }
                        break;
                    case "X8D":
                        if (St[i].Strength > float.Parse(StrengthRatio[13, 0] == null ? "0" : StrengthRatio[13, 0]))
                        {
                            StrengthRatio[13, 0] = St[i].Strength.ToString();
                            StrengthRatio[13, 1] = St[i].Bending.ToString();
                            StrengthRatio[13, 2] = St[i].Axial.ToString();
                            StrengthRatio[13, 3] = St[i].Torsion.ToString();
                            StrengthRatio[13, 4] = St[i].Shear.ToString();
                            StrengthRatio[13, 5] = i.ToString();
                        }
                        break;
                    case "X9A":
                        if (St[i].Strength > float.Parse(StrengthRatio[14, 0] == null ? "0" : StrengthRatio[14, 0]))
                        {
                            StrengthRatio[14, 0] = St[i].Strength.ToString();
                            StrengthRatio[14, 1] = St[i].Bending.ToString();
                            StrengthRatio[14, 2] = St[i].Axial.ToString();
                            StrengthRatio[14, 3] = St[i].Torsion.ToString();
                            StrengthRatio[14, 4] = St[i].Shear.ToString();
                            StrengthRatio[14, 5] = i.ToString();
                        }
                        break;
                    case "X9B":
                        if (St[i].Strength > float.Parse(StrengthRatio[15, 0] == null ? "0" : StrengthRatio[15, 0]))
                        {
                            StrengthRatio[15, 0] = St[i].Strength.ToString();
                            StrengthRatio[15, 1] = St[i].Bending.ToString();
                            StrengthRatio[15, 2] = St[i].Axial.ToString();
                            StrengthRatio[15, 3] = St[i].Torsion.ToString();
                            StrengthRatio[15, 4] = St[i].Shear.ToString();
                            StrengthRatio[15, 5] = i.ToString();
                        }
                        break;
                    case "X9C":
                        if (St[i].Strength > float.Parse(StrengthRatio[16, 0] == null ? "0" : StrengthRatio[16, 0]))
                        {
                            StrengthRatio[16, 0] = St[i].Strength.ToString();
                            StrengthRatio[16, 1] = St[i].Bending.ToString();
                            StrengthRatio[16, 2] = St[i].Axial.ToString();
                            StrengthRatio[16, 3] = St[i].Torsion.ToString();
                            StrengthRatio[16, 4] = St[i].Shear.ToString();
                            StrengthRatio[16, 5] = i.ToString();
                        }
                        break;
                    case "X9D":
                        if (St[i].Strength > float.Parse(StrengthRatio[17, 0] == null ? "0" : StrengthRatio[17, 0]))
                        {
                            StrengthRatio[17, 0] = St[i].Strength.ToString();
                            StrengthRatio[17, 1] = St[i].Bending.ToString();
                            StrengthRatio[17, 2] = St[i].Axial.ToString();
                            StrengthRatio[17, 3] = St[i].Torsion.ToString();
                            StrengthRatio[17, 4] = St[i].Shear.ToString();
                            StrengthRatio[17, 5] = i.ToString();
                        }
                        break;
                    case "X10":
                        if (St[i].Strength > float.Parse(StrengthRatio[18, 0] == null ? "0" : StrengthRatio[18, 0]) || X10 == 0)
                        {
                            StrengthRatio[18, 0] = St[i].Strength.ToString();
                            StrengthRatio[18, 1] = St[i].Bending.ToString();
                            StrengthRatio[18, 2] = St[i].Axial.ToString();
                            StrengthRatio[18, 3] = St[i].Torsion.ToString();
                            StrengthRatio[18, 4] = St[i].Shear.ToString();
                            StrengthRatio[18, 5] = i.ToString();
                            X10 = 1;
                        }
                        if (Math.Max(St[i].Torsion, St[i].Shear) > Math.Max(float.Parse(StrengthRatio[22, 3] == null ? "0" : StrengthRatio[22, 3]), float.Parse(StrengthRatio[22, 4] == null ? "0" : StrengthRatio[22, 4])) || XV10 == 0)
                        {
                            StrengthRatio[22, 0] = St[i].Strength.ToString();
                            StrengthRatio[22, 1] = St[i].Bending.ToString();
                            StrengthRatio[22, 2] = St[i].Axial.ToString();
                            StrengthRatio[22, 3] = St[i].Torsion.ToString();
                            StrengthRatio[22, 4] = St[i].Shear.ToString();
                            StrengthRatio[22, 5] = i.ToString();
                            XV10 = 1;
                        }
                        if (float.Parse(St[i].Vconc) >= ShearRatio[3, 0])
                        {
                            ShearRatio[3, 0] = float.Parse(St[i].Vconc);
                            ShearRatio[3, 1] = i;
                        }
                        break;
                    case "X1V":
                        if (Math.Max(St[i].Torsion, St[i].Shear) > Math.Max(float.Parse(StrengthRatio[20, 3] == null ? "0" : StrengthRatio[20, 3]), float.Parse(StrengthRatio[20, 4] == null ? "0" : StrengthRatio[20, 4])))
                        {
                            StrengthRatio[20, 0] = St[i].Strength.ToString();
                            StrengthRatio[20, 1] = St[i].Bending.ToString();
                            StrengthRatio[20, 2] = St[i].Axial.ToString();
                            StrengthRatio[20, 3] = St[i].Torsion.ToString();
                            StrengthRatio[20, 4] = St[i].Shear.ToString();
                            StrengthRatio[20, 5] = i.ToString();
                        }
                        if (float.Parse(St[i].Vconc) >= ShearRatio[1, 0])
                        {
                            ShearRatio[1, 0] = float.Parse(St[i].Vconc);
                            ShearRatio[1, 1] = i;
                        }
                        break;
                    case "X9V":
                        if (Math.Max(St[i].Torsion, St[i].Shear) > Math.Max(float.Parse(StrengthRatio[21, 3] == null ? "0" : StrengthRatio[21, 3]), float.Parse(StrengthRatio[21, 4] == null ? "0" : StrengthRatio[21, 4])))
                        {
                            StrengthRatio[21, 0] = St[i].Strength.ToString();
                            StrengthRatio[21, 1] = St[i].Bending.ToString();
                            StrengthRatio[21, 2] = St[i].Axial.ToString();
                            StrengthRatio[21, 3] = St[i].Torsion.ToString();
                            StrengthRatio[21, 4] = St[i].Shear.ToString();
                            StrengthRatio[21, 5] = i.ToString();
                        }
                        if (float.Parse(St[i].Vconc) >= ShearRatio[2, 0])
                        {
                            ShearRatio[2, 0] = float.Parse(St[i].Vconc);
                            ShearRatio[2, 1] = i;
                        }
                        break;
                }

                if (DesignAll == true)
                {
                    if (St[i].Strength > float.Parse(StrengthRatio[23, 0] == null ? "0" : StrengthRatio[23, 0]))
                    {
                        StrengthRatio[23, 0] = St[i].Strength.ToString();
                        StrengthRatio[23, 1] = St[i].Bending.ToString();
                        StrengthRatio[23, 2] = St[i].Axial.ToString();
                        StrengthRatio[23, 3] = St[i].Torsion.ToString();
                        StrengthRatio[23, 4] = St[i].Shear.ToString();
                        StrengthRatio[23, 5] = i.ToString();
                    }
                    if (Math.Max(St[i].Torsion, St[i].Shear) > Math.Max(float.Parse(StrengthRatio[24, 3] == null ? "0" : StrengthRatio[24, 3]), float.Parse(StrengthRatio[24, 4] == null ? "0" : StrengthRatio[24, 4])))
                    {
                        StrengthRatio[24, 0] = St[i].Strength.ToString();
                        StrengthRatio[24, 1] = St[i].Bending.ToString();
                        StrengthRatio[24, 2] = St[i].Axial.ToString();
                        StrengthRatio[24, 3] = St[i].Torsion.ToString();
                        StrengthRatio[24, 4] = St[i].Shear.ToString();
                        StrengthRatio[24, 5] = i.ToString();
                    }
                }

                if (St[i].BL == 'T' && StrengthRatio[25, 0] == null)
                {
                    StrengthRatio[25, 0] = "1";
                }
                if (St[i].BR == 'T' && StrengthRatio[25, 1] == null)
                {
                    StrengthRatio[25, 1] = "1";
                }
                if (St[i].Asmin == 'T')
                {
                    StrengthRatio[25, 2] = "1";
                }

                i = i + 1;


            }



            if (StrengthRatio[19, 5] == null && ShearRatio[0, 1] != 0)
            {
                StrengthRatio[19, 0] = St[Convert.ToInt32(ShearRatio[0, 1])].Strength.ToString();
                StrengthRatio[19, 1] = St[Convert.ToInt32(ShearRatio[0, 1])].Bending.ToString();
                StrengthRatio[19, 2] = St[Convert.ToInt32(ShearRatio[0, 1])].Axial.ToString();
                StrengthRatio[19, 3] = St[Convert.ToInt32(ShearRatio[0, 1])].Torsion.ToString();
                StrengthRatio[19, 4] = St[Convert.ToInt32(ShearRatio[0, 1])].Shear.ToString();
                StrengthRatio[19, 5] = ShearRatio[0, 1].ToString();
            }


            if (StrengthRatio[20, 5] == null && ShearRatio[1, 1] != 0)
            {
                StrengthRatio[20, 0] = St[Convert.ToInt32(ShearRatio[1, 1])].Strength.ToString();
                StrengthRatio[20, 1] = St[Convert.ToInt32(ShearRatio[1, 1])].Bending.ToString();
                StrengthRatio[20, 2] = St[Convert.ToInt32(ShearRatio[1, 1])].Axial.ToString();
                StrengthRatio[20, 3] = St[Convert.ToInt32(ShearRatio[1, 1])].Torsion.ToString();
                StrengthRatio[20, 4] = St[Convert.ToInt32(ShearRatio[1, 1])].Shear.ToString();
                StrengthRatio[20, 5] = ShearRatio[1, 1].ToString();
            }


            if (StrengthRatio[21, 5] == null && ShearRatio[2, 1] != 0)
            {
                StrengthRatio[21, 0] = St[Convert.ToInt32(ShearRatio[2, 1])].Strength.ToString();
                StrengthRatio[21, 1] = St[Convert.ToInt32(ShearRatio[2, 1])].Bending.ToString();
                StrengthRatio[21, 2] = St[Convert.ToInt32(ShearRatio[2, 1])].Axial.ToString();
                StrengthRatio[21, 3] = St[Convert.ToInt32(ShearRatio[2, 1])].Torsion.ToString();
                StrengthRatio[21, 4] = St[Convert.ToInt32(ShearRatio[2, 1])].Shear.ToString();
                StrengthRatio[21, 5] = ShearRatio[0, 1].ToString();
            }


            if (StrengthRatio[22, 5] == null && ShearRatio[3, 1] != 0)
            {
                StrengthRatio[22, 0] = St[Convert.ToInt32(ShearRatio[3, 1])].Strength.ToString();
                StrengthRatio[22, 1] = St[Convert.ToInt32(ShearRatio[3, 1])].Bending.ToString();
                StrengthRatio[22, 2] = St[Convert.ToInt32(ShearRatio[3, 1])].Axial.ToString();
                StrengthRatio[22, 3] = St[Convert.ToInt32(ShearRatio[3, 1])].Torsion.ToString();
                StrengthRatio[22, 4] = St[Convert.ToInt32(ShearRatio[3, 1])].Shear.ToString();
                StrengthRatio[22, 5] = ShearRatio[0, 1].ToString();
            }


            if (DesignAll == true && StrengthRatio[24, 3] == null && StrengthRatio[24, 4] == null)
            {
                int VcShearStation;
                VcShearStation = 3;
                i = 1;
                while (i < St.Count)
                {
                    if (float.Parse(St[i].Vconc) > float.Parse(St[VcShearStation].Vconc))
                    {
                        VcShearStation = i;
                    }
                    i = i + 1;
                }

                StrengthRatio[24, 0] = St[VcShearStation].Strength.ToString();
                StrengthRatio[24, 1] = St[VcShearStation].Bending.ToString();
                StrengthRatio[24, 2] = St[VcShearStation].Axial.ToString();
                StrengthRatio[24, 3] = St[VcShearStation].Torsion.ToString();
                StrengthRatio[24, 4] = St[VcShearStation].Shear.ToString();
                StrengthRatio[24, 5] = VcShearStation.ToString();

            }

            for (i = 0; i <= 18; i++)
            {
                if (StrengthRatio[i, 0] != null)
                {
                    St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].Mm = 'T';
                    sqlconn.Open();
                    String query20 = "UPDATE Stress SET Mm = @Mm   where id = " + St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].Id;
                    SqlCommand sqlcomm20 = new SqlCommand(query20, sqlconn);
                    sqlcomm20.Parameters.AddWithValue("@Mm", St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].Mm);
                    sqlcomm20.ExecuteNonQuery();
                    sqlconn.Close();
                }
            }


            for (i = 19; i <= 22; i++)
            {
                if (StrengthRatio[i, 3] != null || StrengthRatio[i, 4] != null)
                {
                    St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].Mm = 'T';
                    sqlconn.Open();
                    String query20 = "UPDATE Stress SET Mm = @Mm   where id = " + St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].Id;
                    SqlCommand sqlcomm20 = new SqlCommand(query20, sqlconn);
                    sqlcomm20.Parameters.AddWithValue("@Mm", St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].Mm);
                    sqlcomm20.ExecuteNonQuery();
                    sqlconn.Close();
                }
            }


            Single[,] Results = new Single[14, 11];

            Results[0, 0] = float.Parse(StrengthRatio[0, 0] == null ? "0" : StrengthRatio[0, 0]);
            Results[0, 1] = float.Parse(StrengthRatio[0, 1] == null ? "0" : StrengthRatio[0, 1]);
            Results[0, 2] = float.Parse(StrengthRatio[0, 2] == null ? "0" : StrengthRatio[0, 2]);
            Results[0, 3] = float.Parse(StrengthRatio[0, 3] == null ? "0" : StrengthRatio[0, 3]);
            Results[0, 4] = float.Parse(StrengthRatio[0, 4] == null ? "0" : StrengthRatio[0, 4]);
            Results[0, 5] = St[Convert.ToInt32(float.Parse(StrengthRatio[0, 5] == null ? "0" : StrengthRatio[0, 5]))].M3;
            Results[0, 6] = St[Convert.ToInt32(float.Parse(StrengthRatio[0, 5] == null ? "0" : StrengthRatio[0, 5]))].V2;
            Results[0, 7] = St[Convert.ToInt32(float.Parse(StrengthRatio[0, 5] == null ? "0" : StrengthRatio[0, 5]))].T;
            Results[0, 8] = St[Convert.ToInt32(float.Parse(StrengthRatio[0, 5] == null ? "0" : StrengthRatio[0, 5]))].P;
            Results[0, 10] = float.Parse(StrengthRatio[0, 5] == null ? "0" : StrengthRatio[0, 5]);

            for (i = 1; i <= 3; i++)
            {
                if (i == 1 && (StrengthRatio[i + 1, 0] != null || StrengthRatio[i, 0] != null))
                {
                    if (float.Parse(StrengthRatio[i + 1, 0] == null ? "0" : StrengthRatio[i + 1, 0]) > float.Parse(StrengthRatio[i, 0] == null ? "0" : StrengthRatio[i, 0]))
                    {
                        Results[1, 0] = float.Parse(StrengthRatio[i + 1, 0] == null ? "0" : StrengthRatio[i + 1, 0]);
                        Results[1, 1] = float.Parse(StrengthRatio[i + 1, 1] == null ? "0" : StrengthRatio[i + 1, 1]);
                        Results[1, 2] = float.Parse(StrengthRatio[i + 1, 2] == null ? "0" : StrengthRatio[i + 1, 2]);
                        Results[1, 3] = float.Parse(StrengthRatio[i + 1, 3] == null ? "0" : StrengthRatio[i + 1, 3]);
                        Results[1, 4] = float.Parse(StrengthRatio[i + 1, 4] == null ? "0" : StrengthRatio[i + 1, 4]);
                        Results[1, 5] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].M3;
                        Results[1, 6] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].V2;
                        Results[1, 7] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].T;
                        Results[1, 8] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].P;
                        Results[1, 9] = i + 1;
                        Results[1, 10] = float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]);
                    }
                    else
                    {
                        Results[1, 0] = float.Parse(StrengthRatio[i, 0] == null ? "0" : StrengthRatio[i, 0]);
                        Results[1, 1] = float.Parse(StrengthRatio[i, 1] == null ? "0" : StrengthRatio[i, 1]);
                        Results[1, 2] = float.Parse(StrengthRatio[i, 2] == null ? "0" : StrengthRatio[i, 2]);
                        Results[1, 3] = float.Parse(StrengthRatio[i, 3] == null ? "0" : StrengthRatio[i, 3]);
                        Results[1, 4] = float.Parse(StrengthRatio[i, 4] == null ? "0" : StrengthRatio[i, 4]);
                        Results[1, 5] = St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].M3;
                        Results[1, 6] = St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].V2;
                        Results[1, 7] = St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].T;
                        Results[1, 8] = St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].P;
                        Results[1, 9] = i;
                        Results[i, 10] = float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]);
                    }
                }
                else if (float.Parse(StrengthRatio[i + 1, 0] == null ? "0" : StrengthRatio[i + 1, 0]) > float.Parse(StrengthRatio[i, 0] == null ? "0" : StrengthRatio[i, 0]) && (StrengthRatio[i + 1, 0] != null || StrengthRatio[i, 0] != null))
                {
                    Results[1, 0] = float.Parse(StrengthRatio[i + 1, 0] == null ? "0" : StrengthRatio[i + 1, 0]);
                    Results[1, 1] = float.Parse(StrengthRatio[i + 1, 1] == null ? "0" : StrengthRatio[i + 1, 1]);
                    Results[1, 2] = float.Parse(StrengthRatio[i + 1, 2] == null ? "0" : StrengthRatio[i + 1, 2]);
                    Results[1, 3] = float.Parse(StrengthRatio[i + 1, 3] == null ? "0" : StrengthRatio[i + 1, 3]);
                    Results[1, 4] = float.Parse(StrengthRatio[i + 1, 4] == null ? "0" : StrengthRatio[i + 1, 4]);
                    Results[1, 5] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].M3;
                    Results[1, 6] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].V2;
                    Results[1, 7] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].T;
                    Results[1, 8] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].P;
                    Results[1, 9] = i + 1;
                    Results[1, 10] = float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]);
                }
            }



            for (i = 5; i <= 7; i++)
            {
                if (i == 5 && (StrengthRatio[i + 1, 0] != null || StrengthRatio[i, 0] != null))
                {
                    if (float.Parse(StrengthRatio[i + 1, 0] == null ? "0" : StrengthRatio[i + 1, 0]) > float.Parse(StrengthRatio[i, 0] == null ? "0" : StrengthRatio[i, 0]))
                    {
                        Results[2, 0] = float.Parse(StrengthRatio[i + 1, 0] == null ? "0" : StrengthRatio[i + 1, 0]);
                        Results[2, 1] = float.Parse(StrengthRatio[i + 1, 1] == null ? "0" : StrengthRatio[i + 1, 1]);
                        Results[2, 2] = float.Parse(StrengthRatio[i + 1, 2] == null ? "0" : StrengthRatio[i + 1, 2]);
                        Results[2, 3] = float.Parse(StrengthRatio[i + 1, 3] == null ? "0" : StrengthRatio[i + 1, 3]);
                        Results[2, 4] = float.Parse(StrengthRatio[i + 1, 4] == null ? "0" : StrengthRatio[i + 1, 4]);
                        Results[2, 5] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].M3;
                        Results[2, 6] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].V2;
                        Results[2, 7] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].T;
                        Results[2, 8] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].P;
                        Results[2, 9] = i - 3;
                        Results[2, 10] = float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]);
                    }
                    else
                    {
                        Results[2, 0] = float.Parse(StrengthRatio[i, 0] == null ? "0" : StrengthRatio[i, 0]);
                        Results[2, 1] = float.Parse(StrengthRatio[i, 1] == null ? "0" : StrengthRatio[i, 1]);
                        Results[2, 2] = float.Parse(StrengthRatio[i, 2] == null ? "0" : StrengthRatio[i, 2]);
                        Results[2, 3] = float.Parse(StrengthRatio[i, 3] == null ? "0" : StrengthRatio[i, 3]);
                        Results[2, 4] = float.Parse(StrengthRatio[i, 4] == null ? "0" : StrengthRatio[i, 4]);
                        Results[2, 5] = St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].M3;
                        Results[2, 6] = St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].V2;
                        Results[2, 7] = St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].T;
                        Results[2, 8] = St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].P;
                        Results[2, 9] = i - 4;
                        Results[2, 10] = float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]);
                    }
                }
                else if (float.Parse(StrengthRatio[i + 1, 0] == null ? "0" : StrengthRatio[i + 1, 0]) > float.Parse(StrengthRatio[i, 0] == null ? "0" : StrengthRatio[i, 0]) && (StrengthRatio[i + 1, 0] != null || StrengthRatio[i, 0] != null))
                {
                    Results[2, 0] = float.Parse(StrengthRatio[i + 1, 0] == null ? "0" : StrengthRatio[i + 1, 0]);
                    Results[2, 1] = float.Parse(StrengthRatio[i + 1, 1] == null ? "0" : StrengthRatio[i + 1, 1]);
                    Results[2, 2] = float.Parse(StrengthRatio[i + 1, 2] == null ? "0" : StrengthRatio[i + 1, 2]);
                    Results[2, 3] = float.Parse(StrengthRatio[i + 1, 3] == null ? "0" : StrengthRatio[i + 1, 3]);
                    Results[2, 4] = float.Parse(StrengthRatio[i + 1, 4] == null ? "0" : StrengthRatio[i + 1, 4]);
                    Results[2, 5] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].M3;
                    Results[2, 6] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].V2;
                    Results[2, 7] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].T;
                    Results[2, 8] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].P;
                    Results[2, 9] = i - 3;
                    Results[2, 10] = float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]);
                }
            }


            if (StrengthRatio[9, 5] != null)
            {
                Results[3, 0] = float.Parse(StrengthRatio[9, 0] == null ? "0" : StrengthRatio[9, 0]);
                Results[3, 1] = float.Parse(StrengthRatio[9, 1] == null ? "0" : StrengthRatio[9, 1]);
                Results[3, 2] = float.Parse(StrengthRatio[9, 2] == null ? "0" : StrengthRatio[9, 2]);
                Results[3, 3] = float.Parse(StrengthRatio[9, 3] == null ? "0" : StrengthRatio[9, 3]);
                Results[3, 4] = float.Parse(StrengthRatio[9, 4] == null ? "0" : StrengthRatio[9, 4]);
                Results[3, 5] = St[Convert.ToInt32(float.Parse(StrengthRatio[9, 5] == null ? "0" : StrengthRatio[9, 5]))].M3;
                Results[3, 6] = St[Convert.ToInt32(float.Parse(StrengthRatio[9, 5] == null ? "0" : StrengthRatio[9, 5]))].V2;
                Results[3, 7] = St[Convert.ToInt32(float.Parse(StrengthRatio[9, 5] == null ? "0" : StrengthRatio[9, 5]))].T;
                Results[3, 8] = St[Convert.ToInt32(float.Parse(StrengthRatio[9, 5] == null ? "0" : StrengthRatio[9, 5]))].P;
                Results[3, 10] = float.Parse(StrengthRatio[9, 5] == null ? "0" : StrengthRatio[9, 5]);
            }


            for (i = 10; i <= 12; i++)
            {
                if (i == 10 && (StrengthRatio[i + 1, 0] != null || StrengthRatio[i, 0] != null))
                {
                    if (float.Parse(StrengthRatio[i + 1, 0] == null ? "0" : StrengthRatio[i + 1, 0]) > float.Parse(StrengthRatio[i, 0] == null ? "0" : StrengthRatio[i, 0]))
                    {
                        Results[4, 0] = float.Parse(StrengthRatio[i + 1, 0] == null ? "0" : StrengthRatio[i + 1, 0]);
                        Results[4, 1] = float.Parse(StrengthRatio[i + 1, 1] == null ? "0" : StrengthRatio[i + 1, 1]);
                        Results[4, 2] = float.Parse(StrengthRatio[i + 1, 2] == null ? "0" : StrengthRatio[i + 1, 2]);
                        Results[4, 3] = float.Parse(StrengthRatio[i + 1, 3] == null ? "0" : StrengthRatio[i + 1, 3]);
                        Results[4, 4] = float.Parse(StrengthRatio[i + 1, 4] == null ? "0" : StrengthRatio[i + 1, 4]);
                        Results[4, 5] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].M3;
                        Results[4, 6] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].V2;
                        Results[4, 7] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].T;
                        Results[4, 8] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].P;
                        Results[4, 9] = i - 8;
                        Results[4, 10] = float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]);
                    }
                    else
                    {
                        Results[4, 0] = float.Parse(StrengthRatio[i, 0] == null ? "0" : StrengthRatio[i, 0]);
                        Results[4, 1] = float.Parse(StrengthRatio[i, 1] == null ? "0" : StrengthRatio[i, 1]);
                        Results[4, 2] = float.Parse(StrengthRatio[i, 2] == null ? "0" : StrengthRatio[i, 2]);
                        Results[4, 3] = float.Parse(StrengthRatio[i, 3] == null ? "0" : StrengthRatio[i, 3]);
                        Results[4, 4] = float.Parse(StrengthRatio[i, 4] == null ? "0" : StrengthRatio[i, 4]);
                        Results[4, 5] = St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].M3;
                        Results[4, 6] = St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].V2;
                        Results[4, 7] = St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].T;
                        Results[4, 8] = St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].P;
                        Results[4, 9] = i - 9;
                        Results[4, 10] = float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]);
                    }
                }
                else if (float.Parse(StrengthRatio[i + 1, 0] == null ? "0" : StrengthRatio[i + 1, 0]) > float.Parse(StrengthRatio[i, 0] == null ? "0" : StrengthRatio[i, 0]) && (StrengthRatio[i + 1, 0] != null || StrengthRatio[i, 0] != null))
                {
                    Results[4, 0] = float.Parse(StrengthRatio[i + 1, 0] == null ? "0" : StrengthRatio[i + 1, 0]);
                    Results[4, 1] = float.Parse(StrengthRatio[i + 1, 1] == null ? "0" : StrengthRatio[i + 1, 1]);
                    Results[4, 2] = float.Parse(StrengthRatio[i + 1, 2] == null ? "0" : StrengthRatio[i + 1, 2]);
                    Results[4, 3] = float.Parse(StrengthRatio[i + 1, 3] == null ? "0" : StrengthRatio[i + 1, 3]);
                    Results[4, 4] = float.Parse(StrengthRatio[i + 1, 4] == null ? "0" : StrengthRatio[i + 1, 4]);
                    Results[4, 5] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].M3;
                    Results[4, 6] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].V2;
                    Results[4, 7] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].T;
                    Results[4, 8] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].P;
                    Results[4, 9] = i - 8;
                    Results[4, 10] = float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]);
                }
            }



            for (i = 14; i <= 16; i++)
            {
                if (i == 14 && (StrengthRatio[i + 1, 0] != null || StrengthRatio[i, 0] != null))
                {
                    if (float.Parse(StrengthRatio[i + 1, 0] == null ? "0" : StrengthRatio[i + 1, 0]) > float.Parse(StrengthRatio[i, 0] == null ? "0" : StrengthRatio[i, 0]))
                    {
                        Results[5, 0] = float.Parse(StrengthRatio[i + 1, 0] == null ? "0" : StrengthRatio[i + 1, 0]);
                        Results[5, 1] = float.Parse(StrengthRatio[i + 1, 1] == null ? "0" : StrengthRatio[i + 1, 1]);
                        Results[5, 2] = float.Parse(StrengthRatio[i + 1, 2] == null ? "0" : StrengthRatio[i + 1, 2]);
                        Results[5, 3] = float.Parse(StrengthRatio[i + 1, 3] == null ? "0" : StrengthRatio[i + 1, 3]);
                        Results[5, 4] = float.Parse(StrengthRatio[i + 1, 4] == null ? "0" : StrengthRatio[i + 1, 4]);
                        Results[5, 5] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].M3;
                        Results[5, 6] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].V2;
                        Results[5, 7] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].T;
                        Results[5, 8] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].P;
                        Results[5, 9] = i - 12;
                        Results[5, 10] = float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]);
                    }
                    else
                    {
                        Results[5, 0] = float.Parse(StrengthRatio[i, 0] == null ? "0" : StrengthRatio[i, 0]);
                        Results[5, 1] = float.Parse(StrengthRatio[i, 1] == null ? "0" : StrengthRatio[i, 1]);
                        Results[5, 2] = float.Parse(StrengthRatio[i, 2] == null ? "0" : StrengthRatio[i, 2]);
                        Results[5, 3] = float.Parse(StrengthRatio[i, 3] == null ? "0" : StrengthRatio[i, 3]);
                        Results[5, 4] = float.Parse(StrengthRatio[i, 4] == null ? "0" : StrengthRatio[i, 4]);
                        Results[5, 5] = St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].M3;
                        Results[5, 6] = St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].V2;
                        Results[5, 7] = St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].T;
                        Results[5, 8] = St[Convert.ToInt32(float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]))].P;
                        Results[5, 9] = i - 13;
                        Results[5, 10] = float.Parse(StrengthRatio[i, 5] == null ? "0" : StrengthRatio[i, 5]);
                    }
                }
                else if (float.Parse(StrengthRatio[i + 1, 0] == null ? "0" : StrengthRatio[i + 1, 0]) > float.Parse(StrengthRatio[i, 0] == null ? "0" : StrengthRatio[i, 0]) && (StrengthRatio[i + 1, 0] != null || StrengthRatio[i, 0] != null))
                {
                    Results[5, 0] = float.Parse(StrengthRatio[i + 1, 0] == null ? "0" : StrengthRatio[i + 1, 0]);
                    Results[5, 1] = float.Parse(StrengthRatio[i + 1, 1] == null ? "0" : StrengthRatio[i + 1, 1]);
                    Results[5, 2] = float.Parse(StrengthRatio[i + 1, 2] == null ? "0" : StrengthRatio[i + 1, 2]);
                    Results[5, 3] = float.Parse(StrengthRatio[i + 1, 3] == null ? "0" : StrengthRatio[i + 1, 3]);
                    Results[5, 4] = float.Parse(StrengthRatio[i + 1, 4] == null ? "0" : StrengthRatio[i + 1, 4]);
                    Results[5, 5] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].M3;
                    Results[5, 6] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].V2;
                    Results[5, 7] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].T;
                    Results[5, 8] = St[Convert.ToInt32(float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]))].P;
                    Results[5, 9] = i - 12;
                    Results[5, 10] = float.Parse(StrengthRatio[i + 1, 5] == null ? "0" : StrengthRatio[i + 1, 5]);
                }
            }

            Results[6, 0] = float.Parse(StrengthRatio[18, 0] == null ? "0" : StrengthRatio[18, 0]);
            Results[6, 1] = float.Parse(StrengthRatio[18, 1] == null ? "0" : StrengthRatio[18, 1]);
            Results[6, 2] = float.Parse(StrengthRatio[18, 2] == null ? "0" : StrengthRatio[18, 2]);
            Results[6, 3] = float.Parse(StrengthRatio[18, 3] == null ? "0" : StrengthRatio[18, 3]);
            Results[6, 4] = float.Parse(StrengthRatio[18, 4] == null ? "0" : StrengthRatio[18, 4]);
            Results[6, 5] = St[Convert.ToInt32(float.Parse(StrengthRatio[18, 5] == null ? "0" : StrengthRatio[18, 5]))].M3;
            Results[6, 6] = St[Convert.ToInt32(float.Parse(StrengthRatio[18, 5] == null ? "0" : StrengthRatio[18, 5]))].V2;
            Results[6, 7] = St[Convert.ToInt32(float.Parse(StrengthRatio[18, 5] == null ? "0" : StrengthRatio[18, 5]))].T;
            Results[6, 8] = St[Convert.ToInt32(float.Parse(StrengthRatio[18, 5] == null ? "0" : StrengthRatio[18, 5]))].P;
            Results[6, 10] = float.Parse(StrengthRatio[18, 5] == null ? "0" : StrengthRatio[18, 5]);



            Results[7, 0] = float.Parse(StrengthRatio[19, 0] == null ? "0" : StrengthRatio[19, 0]);
            Results[7, 1] = float.Parse(StrengthRatio[19, 1] == null ? "0" : StrengthRatio[19, 1]);
            Results[7, 2] = float.Parse(StrengthRatio[19, 2] == null ? "0" : StrengthRatio[19, 2]);
            Results[7, 3] = float.Parse(StrengthRatio[19, 3] == null ? "0" : StrengthRatio[19, 3]);
            Results[7, 4] = float.Parse(StrengthRatio[19, 4] == null ? "0" : StrengthRatio[19, 4]);
            Results[7, 5] = St[Convert.ToInt32(float.Parse(StrengthRatio[19, 5] == null ? "0" : StrengthRatio[19, 5]))].M3;
            Results[7, 6] = St[Convert.ToInt32(float.Parse(StrengthRatio[19, 5] == null ? "0" : StrengthRatio[19, 5]))].V2;
            Results[7, 7] = St[Convert.ToInt32(float.Parse(StrengthRatio[19, 5] == null ? "0" : StrengthRatio[19, 5]))].T;
            Results[7, 8] = St[Convert.ToInt32(float.Parse(StrengthRatio[19, 5] == null ? "0" : StrengthRatio[19, 5]))].P;
            if (StrengthRatio[19, 3] == null && StrengthRatio[19, 4] == null)
            {
                Results[7, 9] = float.Parse(St[Convert.ToInt32(float.Parse(StrengthRatio[0, 1] == null ? "0" : StrengthRatio[0, 1]))].Vconc);
                Results[7, 10] = ShearRatio[0, 1];
            }
            else
            {
                Results[7, 10] = float.Parse(StrengthRatio[19, 5] == null ? "0" : StrengthRatio[19, 5]);
            }



            if (StrengthRatio[20, 5] != null)
            {
                Results[8, 0] = float.Parse(StrengthRatio[20, 0] == null ? "0" : StrengthRatio[20, 0]);
                Results[8, 1] = float.Parse(StrengthRatio[20, 1] == null ? "0" : StrengthRatio[20, 1]);
                Results[8, 2] = float.Parse(StrengthRatio[20, 2] == null ? "0" : StrengthRatio[20, 2]);
                Results[8, 3] = float.Parse(StrengthRatio[20, 3] == null ? "0" : StrengthRatio[20, 3]);
                Results[8, 4] = float.Parse(StrengthRatio[20, 4] == null ? "0" : StrengthRatio[20, 4]);
                Results[8, 5] = St[Convert.ToInt32(float.Parse(StrengthRatio[20, 5] == null ? "0" : StrengthRatio[20, 5]))].M3;
                Results[8, 6] = St[Convert.ToInt32(float.Parse(StrengthRatio[20, 5] == null ? "0" : StrengthRatio[20, 5]))].V2;
                Results[8, 7] = St[Convert.ToInt32(float.Parse(StrengthRatio[20, 5] == null ? "0" : StrengthRatio[20, 5]))].T;
                Results[8, 8] = St[Convert.ToInt32(float.Parse(StrengthRatio[20, 5] == null ? "0" : StrengthRatio[20, 5]))].P;
                if (StrengthRatio[20, 3] == null && StrengthRatio[20, 4] == null)
                {
                    Results[8, 9] = float.Parse(St[Convert.ToInt32(float.Parse(StrengthRatio[1, 1] == null ? "0" : StrengthRatio[1, 1]))].Vconc);
                    Results[8, 10] = ShearRatio[1, 1];
                }
                else
                {
                    Results[8, 10] = float.Parse(StrengthRatio[20, 5] == null ? "0" : StrengthRatio[20, 5]);
                }
            }


            if (StrengthRatio[21, 5] != null)
            {
                Results[9, 0] = float.Parse(StrengthRatio[21, 0] == null ? "0" : StrengthRatio[21, 0]);
                Results[9, 1] = float.Parse(StrengthRatio[21, 1] == null ? "0" : StrengthRatio[21, 1]);
                Results[9, 2] = float.Parse(StrengthRatio[21, 2] == null ? "0" : StrengthRatio[21, 2]);
                Results[9, 3] = float.Parse(StrengthRatio[21, 3] == null ? "0" : StrengthRatio[21, 3]);
                Results[9, 4] = float.Parse(StrengthRatio[21, 4] == null ? "0" : StrengthRatio[21, 4]);
                Results[9, 5] = St[Convert.ToInt32(StrengthRatio[21, 5])].M3;
                Results[9, 6] = St[Convert.ToInt32(StrengthRatio[21, 5])].V2;
                Results[9, 7] = St[Convert.ToInt32(StrengthRatio[21, 5])].T;
                Results[9, 8] = St[Convert.ToInt32(StrengthRatio[21, 5])].P;
                if (StrengthRatio[21, 3] == null && StrengthRatio[21, 4] == null)
                {
                    Results[9, 9] = float.Parse(St[Convert.ToInt32(float.Parse(StrengthRatio[2, 1] == null ? "0" : StrengthRatio[2, 1]))].Vconc);

                    Results[9, 10] = ShearRatio[2, 1];
                }
                else
                {
                    Results[9, 10] = float.Parse(StrengthRatio[21, 5] == null ? "0" : StrengthRatio[21, 5]);
                }
            }


            Results[10, 0] = float.Parse(StrengthRatio[22, 0] == null ? "0" : StrengthRatio[22, 0]);
            Results[10, 1] = float.Parse(StrengthRatio[22, 1] == null ? "0" : StrengthRatio[22, 1]);
            Results[10, 2] = float.Parse(StrengthRatio[22, 2] == null ? "0" : StrengthRatio[22, 2]);
            Results[10, 3] = float.Parse(StrengthRatio[22, 3] == null ? "0" : StrengthRatio[22, 3]);
            Results[10, 4] = float.Parse(StrengthRatio[22, 4] == null ? "0" : StrengthRatio[22, 4]);
            Results[10, 5] = St[Convert.ToInt32(StrengthRatio[22, 5])].M3;
            Results[10, 6] = St[Convert.ToInt32(StrengthRatio[22, 5])].V2;
            Results[10, 7] = St[Convert.ToInt32(StrengthRatio[22, 5])].T;
            Results[10, 8] = St[Convert.ToInt32(StrengthRatio[22, 5])].P;
            if (StrengthRatio[22, 3] == null && StrengthRatio[22, 4] == null)
            {
                Results[10, 9] = float.Parse(St[Convert.ToInt32(ShearRatio[3, 1])].Vconc);
                Results[10, 10] = ShearRatio[3, 1];
            }
            else
            {
                Results[10, 10] = float.Parse(StrengthRatio[22, 5]);
            }




            if (DesignAll == true)
            {
                Results[11, 0] = float.Parse(StrengthRatio[23, 0]);
                Results[11, 1] = float.Parse(StrengthRatio[23, 1]);
                Results[11, 2] = float.Parse(StrengthRatio[23, 2]);
                Results[11, 3] = float.Parse(StrengthRatio[23, 3]);
                Results[11, 4] = float.Parse(StrengthRatio[23, 4]);
                Results[11, 5] = St[Convert.ToInt32(StrengthRatio[23, 5])].M3;
                Results[11, 6] = St[Convert.ToInt32(StrengthRatio[23, 5])].V2;
                Results[11, 7] = St[Convert.ToInt32(StrengthRatio[23, 5])].T;
                Results[11, 8] = St[Convert.ToInt32(StrengthRatio[23, 5])].P;
                Results[11, 10] = St[Convert.ToInt32(StrengthRatio[23, 5])].Station;
                Results[12, 0] = float.Parse(StrengthRatio[24, 0]);
                Results[12, 1] = float.Parse(StrengthRatio[24, 1]);
                Results[12, 2] = float.Parse(StrengthRatio[24, 2]);
                Results[12, 3] = float.Parse(StrengthRatio[24, 3]);
                Results[12, 4] = float.Parse(StrengthRatio[24, 4]);
                Results[12, 5] = St[Convert.ToInt32(StrengthRatio[24, 5])].M3;
                Results[12, 6] = St[Convert.ToInt32(StrengthRatio[24, 5])].V2;
                Results[12, 7] = St[Convert.ToInt32(StrengthRatio[24, 5])].T;
                Results[12, 8] = St[Convert.ToInt32(StrengthRatio[24, 5])].P;
                if (StrengthRatio[24, 3] == null && StrengthRatio[24, 4] == null)
                {
                    Results[12, 9] = float.Parse(St[Convert.ToInt32(StrengthRatio[24, 5])].Vconc);
                }
                Results[12, 10] = St[Convert.ToInt32(StrengthRatio[24, 5])].Station;
            }


            Results[13, 0] = float.Parse(StrengthRatio[25, 0] == null ? "0" : StrengthRatio[25, 0]);
            Results[13, 1] = float.Parse(StrengthRatio[25, 1] == null ? "0" : StrengthRatio[25, 1]);
            Results[13, 2] = float.Parse(StrengthRatio[25, 2] == null ? "0" : StrengthRatio[25, 2]);



            return Results;
        }

        public Single[,] FindDevelopmentLength(Single fy, Single fcp, Single[,] Steel)
        {
            Single[,] DevLength = new Single[15, 4];
            for (int i = 1; i <= 15; i++)
            {
                if (Steel[i, 1] != 0)
                {
                    if (Steel[i, 1] >= 20)
                    {
                        DevLength[i - 1, 0] = float.Parse((fy * 1.3 * Steel[i, 1] / (5.3 * Math.Sqrt(fcp))).ToString());
                    }
                    else
                    {
                        DevLength[i - 1, 0] = float.Parse((fy * 1.3 * Steel[i, 1] / (6.6 * Math.Sqrt(fcp))).ToString());
                    }
                    if (Steel[i, 1] >= 20)
                    {
                        DevLength[i - 1, 1] = float.Parse((fy * Steel[i, 1] / (5.3 * Math.Sqrt(fcp))).ToString());
                    }
                    else
                    {
                        DevLength[i - 1, 1] = float.Parse((fy * Steel[i, 1] / (6.6 * Math.Sqrt(fcp))).ToString());
                    }
                    DevLength[i - 1, 2] = float.Parse((Math.Max(0.075 * fy * Steel[i, 1] / Math.Sqrt(fcp), 0.0044 * fy / Steel[i, 1])).ToString());
                    DevLength[i - 1, 3] = float.Parse((Math.Max(Math.Max(0.075 * fy * Steel[i, 1] / Math.Sqrt(fcp), 8 * Steel[i, 1]), 150)).ToString());
                }
            }
            return DevLength;
        }

        public void FindSecFor()
        {
            int mid = Convert.ToInt32(Session["mid"]);
            int aid = Convert.ToInt32(Session["aid"]);
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            string query2 = "DELETE FROM Stress where accessFileID = " + aid;
            SqlCommand sqlcomm2 = new SqlCommand(query2, sqlconn);
            String query3 = "INSERT INTO Stress (projectID,accessFileID,Mem,Station,[Case],M3,V2,T,P) SELECT ElementForcesFrames.projectID,ElementForcesFrames.accessFileID,ElementForcesFrames.Frame,ElementForcesFrames.Station,ElementForcesFrames.OutputCase,ElementForcesFrames.M3,ElementForcesFrames.V2,ElementForcesFrames.T,ElementForcesFrames.P FROM ElementForcesFrames  where ElementForcesFrames.accessFileID = " + aid + " AND ElementForcesFrames.Frame = " + mid;
            SqlCommand sqlcomm3 = new SqlCommand(query3, sqlconn);
            sqlcomm2.ExecuteNonQuery();
            sqlcomm3.ExecuteNonQuery();
        }

        public Single[,] FindMemList(string MemL)
        {
            RC_Beam rc = new RC_Beam();
            int aid = Convert.ToInt32(Session["aid"]);
            int mid = Convert.ToInt32(Session["mid"]);
            string memlist = MemL;
            string a = "";
            int B = 0;
            int d = 0;
            int i = 1;
            Single[] e;
            Single[,] f;
            List<Connectivity___Frame> MemGT = (List<Connectivity___Frame>)TempData["MemGT"];
            e = new Single[1];
            e[0] = mid;
            a = memlist;
            if (a != " ")
            {
                a = a + "/";
            }
            d = a.Length;
            i = 1;
            while ((d - 1) != 0)
            {
                B = a.IndexOf("/");
                Array.Resize(ref e, i + 1);
                e[i] = float.Parse((a.Substring(0, B)).ToString());
                a = a.Substring(B, d - B);
                d = a.Length;
                i = i + 1;
            }
            int k = e.GetUpperBound(0);
            f = new Single[k + 1, 4];
            for (int x = 0; x <= k; x++)
            {
                f[x, 0] = e[x];
            }
            int m = f.GetUpperBound(0);
            for (i = 0; i <= m; i++)
            {
                foreach (var mem in MemGT)
                {
                    if (f[i, 0] == mem.Frame)
                    {
                        f[i, 1] = mem.JointI;
                        f[i, 2] = mem.JointJ;
                        f[i, 3] = mem.Length;
                    }
                }
            }
            return f;
        }

        public int CheckMemList(Single[,] memlist)
        {
            int a = 0;
            for (int i = 0; i <= memlist.GetUpperBound(0); i++)
            {
                if (memlist[i, 3] == 0)
                {
                    a = -1;
                    return a;
                }
            }
            for (int i = 1; i <= memlist.GetUpperBound(0); i++)
            {
                if (memlist[i, 1] != memlist[(i - 1), 2])
                {
                    a = -2;
                    return a;
                }
            }
            return a;
        }

        public Single FindLt(Single[,] memlist)
        {
            Single FindLt;
            Single Lt = 0;
            List<Connectivity___Frame> MemGT = (List<Connectivity___Frame>)TempData["MemGT"];
            for (int i = 0; i <= memlist.GetUpperBound(0); i++)
            {
                foreach (var mem in MemGT)
                {
                    if (memlist[i, 0] == mem.Frame)
                    {
                        float length = mem.Length;
                        Lt = Lt + length;
                    }
                }
            }
            FindLt = float.Parse(String.Format("{0:0.000}", Lt));
            return FindLt;
        }

        public Single[,] FindSteel(RC_Beam rc)
        {
            Single[,] Rebar = new Single[16, 2];
            Rebar[1, 0] = rc.LN1;
            Rebar[1, 1] = rc.LD1;
            Rebar[2, 0] = rc.LN2;
            Rebar[2, 1] = rc.LD2;
            Rebar[3, 0] = rc.RN2;
            Rebar[3, 1] = rc.RD2;
            Rebar[4, 0] = rc.LN3;
            Rebar[4, 1] = rc.LD3;
            Rebar[5, 0] = rc.RN3;
            Rebar[5, 1] = rc.RD3;
            Rebar[6, 0] = rc.LN4;
            Rebar[6, 1] = rc.LD4;
            Rebar[7, 0] = rc.RN4;
            Rebar[7, 1] = rc.RD4;
            Rebar[8, 0] = rc.LN5;
            Rebar[8, 1] = rc.LD5;
            Rebar[9, 0] = rc.RN5;
            Rebar[9, 1] = rc.RD5;
            Rebar[10, 0] = rc.N1;
            Rebar[10, 1] = rc.D1;
            Rebar[11, 0] = rc.N2;
            Rebar[11, 1] = rc.D2;
            Rebar[12, 0] = rc.N3;
            Rebar[12, 1] = rc.D3;
            Rebar[13, 0] = rc.N4;
            Rebar[13, 1] = rc.D4;
            Rebar[14, 0] = rc.N5;
            Rebar[14, 1] = rc.D5;
            Rebar[15, 0] = rc.N11;
            Rebar[15, 1] = rc.D11;

            int Ld1, Ld2, Ld3, Ld4, Ld5, Rd2, Rd3, Rd4, Rd5, D1, D2, D3, D4, D5;
            Ld1 = rc.LD1;
            Ld2 = rc.LD2;
            Ld3 = rc.LD3;
            Ld4 = rc.LD4;
            Ld5 = rc.LD5;
            Rd2 = rc.RD2;
            Rd3 = rc.RD3;
            Rd4 = rc.RD4;
            Rd5 = rc.RD5;
            D1 = rc.D1;
            D2 = rc.D2;
            D3 = rc.D3;
            D4 = rc.D4;
            D5 = rc.D5;
            Rebar[0, 0] = Math.Max(Ld1, Math.Max(Ld2, Math.Max(Ld3, Math.Max(Ld4, Math.Max(Ld5, Math.Max(Rd2, Math.Max(Rd3, Math.Max(Rd4, Math.Max(Rd5, Math.Max(D1, Math.Max(D2, Math.Max(D3, Math.Max(D4, D5)))))))))))));
            Rebar[0, 1] = rc.RS2;

            return Rebar;
        }

        public int[] FindShearSteel(RC_Beam rc)
        {
            int[] ShearRebar = new int[12];
            ShearRebar[0] = rc.Ds;
            ShearRebar[1] = rc.Nsi;
            ShearRebar[2] = rc.Nso;
            ShearRebar[3] = rc.Nsli;
            ShearRebar[4] = rc.Nslo;
            ShearRebar[5] = rc.Nsri;
            ShearRebar[6] = rc.Nsro;
            ShearRebar[7] = rc.Stln;
            ShearRebar[8] = rc.Stls;
            ShearRebar[9] = rc.Sts;
            ShearRebar[10] = rc.Strn;
            ShearRebar[11] = rc.Strs;

            return ShearRebar;
        }
        public int Finddd(Single[,] Steel, RC_Beam rc)
        {
            int dd = 0;
            int Ld1 = rc.LD1;
            int D1 = rc.D1;
            int Ct = rc.Ct;
            int Cb = rc.Cb;
            int Ds = rc.Ds;
            int T = Convert.ToInt32(rc.T.ToString());
            Single st0 = Steel[0, 0];
            double max1 = (T - ((Cb + Ds + D1 / 2.0) / 10.0));
            double max2 = (T - (Ct + Ds + Ld1 / 2.0) / 10.0);
            double max3 = (12.0 * st0 / 10.0);
            double ddd = Math.Round(10 * (Math.Max(max1, Math.Max(max2, max3))), 0);
            dd = Convert.ToInt32(ddd);
            return dd;
        }

        public Single[] CheckLenExtra(Single Lt, int dd, RC_Beam rc)
        {
            Single[] Len = new Single[24];
            Single[] Lenextra;
            Len[0] = 0;
            Lt = Lt * 1000;

            Len[1] = rc.LL2;
            Len[2] = rc.LL3;
            Len[3] = rc.LL4;
            Len[4] = rc.LL5;
            Len[5] = rc.RL2;
            Len[6] = rc.RL3;
            Len[7] = rc.RL4;
            Len[8] = rc.RL5;
            Len[9] = rc.L2L;
            Len[10] = rc.L3L;
            Len[11] = rc.L4L;
            Len[12] = rc.L5L;
            Len[13] = rc.L2R;
            Len[14] = rc.L3R;
            Len[15] = rc.L4R;
            Len[16] = rc.L5R;
            Len[17] = rc.Stln;
            Len[18] = rc.Stls;
            Len[19] = rc.Sts;
            Len[20] = rc.Strn;
            Len[21] = rc.Strs;
            Len[22] = rc.TL;
            Len[23] = rc.TR;

            if (rc.IsCheck3 == false)
            {
                if (rc.IsCheck4 == false)
                {
                    if (rc.IsCheck5 == true)
                    {
                        Len[0] = -1;
                        return Len;
                    }
                    if (rc.IsCheck6 == true)
                    {
                        Len[0] = -1;
                        return Len;
                    }
                    else if (rc.IsCheck5 == false)
                    {
                        if (rc.IsCheck6 == true)
                        {
                            Len[0] = -1;
                            return Len;
                        }
                    }
                }
            }
            else
            {
                if (rc.IsCheck4 == false)
                {
                    if (rc.IsCheck5 == true)
                    {
                        Len[0] = -1;
                        return Len;
                    }
                    if (rc.IsCheck6 == true)
                    {
                        Len[0] = -1;
                        return Len;
                    }
                }
                else
                {
                    if (rc.IsCheck5 == false)
                    {
                        if (rc.IsCheck6 == true)
                        {
                            Len[0] = -1;
                            return Len;
                        }
                    }
                }
            }

            if (rc.IsCheck7 == false)
            {
                if (rc.IsCheck8 == false)
                {
                    if (rc.IsCheck9 == true)
                    {
                        Len[0] = -1;
                        return Len;
                    }
                    if (rc.IsCheck10 == true)
                    {
                        Len[0] = -1;
                        return Len;
                    }
                    else if (rc.IsCheck9 == false)
                    {
                        if (rc.IsCheck10 == true)
                        {
                            Len[0] = -1;
                            return Len;
                        }
                    }
                }
            }
            else
            {
                if (rc.IsCheck8 == false)
                {
                    if (rc.IsCheck9 == true)
                    {
                        Len[0] = -1;
                        return Len;
                    }
                    if (rc.IsCheck10 == true)
                    {
                        Len[0] = -1;
                        return Len;
                    }
                }
                else
                {
                    if (rc.IsCheck9 == false)
                    {
                        if (rc.IsCheck10 == true)
                        {
                            Len[0] = -1;
                            return Len;
                        }
                    }
                }
            }

            if (rc.LL2 < rc.LL3 && rc.LL2 != 0)
            {
                Len[0] = -1;
                return Len;
            }
            else
            {
                if (rc.LL3 < rc.LL4 && rc.IsCheck4 == false)
                {
                    Len[0] = -1;
                    return Len;
                }
                else
                {
                    if (rc.LL4 < rc.LL5 && rc.IsCheck5 == false)
                    {
                        Len[0] = -1;
                        return Len;
                    }
                }
            }

            if (rc.RL2 < rc.RL3 && rc.RL2 != 0)
            {
                Len[0] = -1;
                return Len;
            }
            else
            {
                if (rc.RL3 < rc.RL4 && rc.IsCheck4 == false)
                {
                    Len[0] = -1;
                    return Len;
                }
                else
                {
                    if (rc.RL4 < rc.RL5 && rc.IsCheck5 == false)
                    {
                        Len[0] = -1;
                        return Len;
                    }
                }
            }

            if (rc.L2L > rc.L3L && rc.IsCheck7 == false && rc.L3L != 0)
            {
                Len[0] = -1;
                return Len;
            }
            else
            {
                if (rc.L3L > rc.L4L && rc.IsCheck8 == false && rc.L4L != 0)
                {
                    Len[0] = -1;
                    return Len;
                }
                else
                {
                    if (rc.L4L > rc.L5L && rc.IsCheck9 == false && rc.L5L != 0)
                    {
                        Len[0] = -1;
                        return Len;
                    }
                }
            }

            if (rc.L2R > rc.L3R && rc.IsCheck7 == false && rc.L3R != 0)
            {
                Len[0] = -1;
                return Len;
            }
            else
            {
                if (rc.L3R > rc.L4R && rc.IsCheck8 == false && rc.L4R != 0)
                {
                    Len[0] = -1;
                    return Len;
                }
                else
                {
                    if (rc.L4R > rc.L5R && rc.IsCheck9 == false && rc.L5R != 0)
                    {
                        Len[0] = -1;
                        return Len;
                    }
                }
            }

            if (rc.IsCheck3 == false)
            {
                if ((rc.LL2 + rc.RL2) >= Lt)
                {
                    Len[0] = -1;
                    return Len;
                }
            }

            if (rc.IsCheck4 == false)
            {
                if ((rc.LL3 + rc.RL3) >= Lt)
                {
                    Len[0] = -1;
                    return Len;
                }
            }

            if (rc.IsCheck5 == false)
            {
                if ((rc.LL4 + rc.RL4) >= Lt)
                {
                    Len[0] = -1;
                    return Len;
                }
            }

            if (rc.IsCheck6 == false)
            {
                if ((rc.LL5 + rc.RL5) >= Lt)
                {
                    Len[0] = -1;
                    return Len;
                }
            }

            if (rc.IsCheck7 == false)
            {
                if ((rc.L2L + rc.L2R) >= Lt)
                {
                    Len[0] = -1;
                    return Len;
                }
            }

            if (rc.IsCheck8 == false)
            {
                if ((rc.L3L + rc.L3R) >= Lt)
                {
                    Len[0] = -1;
                    return Len;
                }
            }

            if (rc.IsCheck9 == false)
            {
                if ((rc.L4L + rc.L4R) >= Lt)
                {
                    Len[0] = -1;
                    return Len;
                }
            }

            if (rc.IsCheck10 == false)
            {
                if ((rc.L5L + rc.L5R) >= Lt)
                {
                    Len[0] = -1;
                    return Len;
                }
            }

            if ((rc.LL2 != 0) && (rc.LL2 <= (dd + rc.TL) / 2))
            {
                Len[0] = -1;
                return Len;
            }

            if ((rc.LL3 != 0) && (rc.LL3 <= (dd + rc.TL) / 2))
            {
                Len[0] = -1;
                return Len;
            }

            if ((rc.LL4 != 0) && (rc.LL4 <= (dd + rc.TL) / 2))
            {
                Len[0] = -1;
                return Len;
            }

            if ((rc.LL5 != 0) && (rc.LL5 <= (dd + rc.TL) / 2))
            {
                Len[0] = -1;
                return Len;
            }

            if ((rc.RL2 != 0) && (rc.RL2 <= (dd + rc.TL) / 2))
            {
                Len[0] = -1;
                return Len;
            }

            if ((rc.RL3 != 0) && (rc.RL3 <= (dd + rc.TL) / 2))
            {
                Len[0] = -1;
                return Len;
            }

            if ((rc.RL4 != 0) && (rc.RL4 <= (dd + rc.TL) / 2))
            {
                Len[0] = -1;
                return Len;
            }

            if ((rc.RL5 != 0) && (rc.RL5 <= (dd + rc.TL) / 2))
            {
                Len[0] = -1;
                return Len;
            }

            if ((rc.L2L != 0 || rc.L2R != 0) && ((Lt - (rc.L2L + rc.L2R)) <= (2 * dd)))
            {
                Len[0] = -1;
                return Len;
            }

            if ((rc.L3L != 0 || rc.L3R != 0) && ((Lt - (rc.L3L + rc.L3R)) <= (2 * dd)))
            {
                Len[0] = -1;
                return Len;
            }

            if ((rc.L4L != 0 || rc.L4R != 0) && ((Lt - (rc.L4L + rc.L4R)) <= (2 * dd)))
            {
                Len[0] = -1;
                return Len;
            }

            if ((rc.L5L != 0 || rc.L5R != 0) && ((Lt - (rc.L5L + rc.L5R)) <= (2 * dd)))
            {
                Len[0] = -1;
                return Len;
            }

            if (rc.Stln != 0 && rc.Strn == 0)
            {
                if (((rc.Stln - 1) * (rc.Stls + rc.Sts + 100 + rc.TL / 2 + rc.TR / 2)) > Lt)
                {
                    Len[0] = -1;
                    return Len;
                }
            }
            else if (rc.Stln == 0 && rc.Strn != 0)
            {
                if (((rc.Strn - 1) * (rc.Strs + rc.Sts + 100 + rc.TL / 2 + rc.TR / 2)) > Lt)
                {
                    Len[0] = -1;
                    return Len;
                }
            }
            else if (rc.Stln != 0 && rc.Strn != 0)
            {
                if (((rc.Stln - 1) * (rc.Stls + (rc.Strn - 1)) * (rc.Strs + rc.TL / 2 + rc.TR / 2 + rc.Sts + 100)) > Lt)
                {
                    Len[0] = -1;
                    return Len;
                }
            }

            return Len;
        }

        [HttpPost]
        public ActionResult BtnGpRun_Click(string Rowmaster, string Mark)
        {
            int Gpalert;
            int aid = Convert.ToInt32(Session["aid"]);
            List<RC_Beam> list1 = new List<RC_Beam>();
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            String query2 = "SELECT * FROM RCBeam where accessFileID = " + aid + " ORDER BY Mem ASC";
            SqlCommand sqlcomm2 = new SqlCommand(query2, sqlconn);
            SqlDataReader rd = sqlcomm2.ExecuteReader();
            while (rd.Read())
            {
                if (DBNull.Value.Equals(rd["Ct"]))
                {
                    list1.Add(new RC_Beam
                    {
                        Mem = Convert.ToInt32(rd["Mem"]),
                        Mark = rd["Mark"].ToString(),
                        B = rd["B"].ToString(),
                        T = rd["T"].ToString(),
                        L = float.Parse(rd["L"].ToString()),
                        LT = float.Parse(rd["LT"].ToString()),
                        Name = rd["Name"].ToString(),

                    });

                }
                else
                {
                    list1.Add(new RC_Beam
                    {
                        Mem = Convert.ToInt32(rd["Mem"]),
                        Mark = rd["Mark"].ToString(),
                        B = rd["B"].ToString(),
                        T = rd["T"].ToString(),
                        L = float.Parse(rd["L"].ToString()),
                        LT = float.Parse(rd["LT"].ToString()),
                        Name = rd["Name"].ToString(),
                        Ct = Convert.ToInt32(rd["Ct"]),
                        Cb = Convert.ToInt32(rd["Cb"]),
                        CLT = Convert.ToChar(rd["CLT"]),
                        CRT = Convert.ToChar(rd["CRT"]),
                        CL = Convert.ToChar(rd["CL"]),
                        CR = Convert.ToChar(rd["CR"]),
                        CLH = Convert.ToChar(rd["CLH"]),
                        CRH = Convert.ToChar(rd["CRH"]),
                        AXL = Convert.ToChar(rd["AXL"]),
                        FL = Convert.ToChar(rd["FL"]),
                        FR = Convert.ToChar(rd["FR"]),
                        TL = Convert.ToInt32(rd["TL"]),
                        TR = Convert.ToInt32(rd["TR"]),
                        N1 = Convert.ToInt32(rd["N1"]),
                        D1 = Convert.ToInt32(rd["D1"]),
                        N2 = Convert.ToInt32(rd["N2"]),
                        D2 = Convert.ToInt32(rd["D2"]),
                        L2L = Convert.ToInt32(rd["L2L"]),
                        L2R = Convert.ToInt32(rd["L2R"]),
                        N3 = Convert.ToInt32(rd["N3"]),
                        D3 = Convert.ToInt32(rd["D3"]),
                        L3L = Convert.ToInt32(rd["L3L"]),
                        L3R = Convert.ToInt32(rd["L3R"]),
                        N4 = Convert.ToInt32(rd["N4"]),
                        D4 = Convert.ToInt32(rd["D4"]),
                        L4L = Convert.ToInt32(rd["L4L"]),
                        L4R = Convert.ToInt32(rd["L4R"]),
                        N5 = Convert.ToInt32(rd["N5"]),
                        D5 = Convert.ToInt32(rd["D5"]),
                        L5L = Convert.ToInt32(rd["L5L"]),
                        L5R = Convert.ToInt32(rd["L5R"]),
                        LN1 = Convert.ToInt32(rd["LN1"]),
                        LD1 = Convert.ToInt32(rd["LD1"]),
                        LN2 = Convert.ToInt32(rd["LN2"]),
                        LD2 = Convert.ToInt32(rd["LD2"]),
                        LL2 = Convert.ToInt32(rd["LL2"]),
                        LN3 = Convert.ToInt32(rd["LN3"]),
                        LD3 = Convert.ToInt32(rd["LD3"]),
                        LL3 = Convert.ToInt32(rd["LL3"]),
                        LN4 = Convert.ToInt32(rd["LN4"]),
                        LD4 = Convert.ToInt32(rd["LD4"]),
                        LL4 = Convert.ToInt32(rd["LL4"]),
                        LN5 = Convert.ToInt32(rd["LN5"]),
                        LD5 = Convert.ToInt32(rd["LD5"]),
                        LL5 = Convert.ToInt32(rd["LL5"]),
                        RN2 = Convert.ToInt32(rd["RN2"]),
                        RD2 = Convert.ToInt32(rd["RD2"]),
                        RL2 = Convert.ToInt32(rd["RL2"]),
                        RN3 = Convert.ToInt32(rd["RN3"]),
                        RD3 = Convert.ToInt32(rd["RD3"]),
                        RL3 = Convert.ToInt32(rd["RL3"]),
                        RN4 = Convert.ToInt32(rd["RN4"]),
                        RD4 = Convert.ToInt32(rd["RD4"]),
                        RL4 = Convert.ToInt32(rd["RL4"]),
                        RN5 = Convert.ToInt32(rd["RN5"]),
                        RD5 = Convert.ToInt32(rd["RD5"]),
                        RL5 = Convert.ToInt32(rd["RL5"]),
                        N11 = Convert.ToInt32(rd["N11"]),
                        D11 = Convert.ToInt32(rd["D11"]),
                        RS2 = Convert.ToInt32(rd["RS2"]),
                        Cot2 = Convert.ToChar(rd["Cot2"]),
                        Cot3 = Convert.ToChar(rd["Cot3"]),
                        Cot4 = Convert.ToChar(rd["Cot4"]),
                        Cot5 = Convert.ToChar(rd["Cot5"]),
                        Cob2 = Convert.ToChar(rd["Cob2"]),
                        Cob3 = Convert.ToChar(rd["Cob3"]),
                        Cob4 = Convert.ToChar(rd["Cob4"]),
                        Cob5 = Convert.ToChar(rd["Cob5"]),
                        Ds = Convert.ToInt32(rd["Ds"]),
                        Nso = Convert.ToInt32(rd["Nso"]),
                        Nsi = Convert.ToInt32(rd["Nsi"]),
                        Nslo = Convert.ToInt32(rd["Nslo"]),
                        Nsli = Convert.ToInt32(rd["Nsli"]),
                        Nsro = Convert.ToInt32(rd["Nsro"]),
                        Nsri = Convert.ToInt32(rd["Nsri"]),
                        Stln = Convert.ToInt32(rd["Stln"]),
                        Stls = Convert.ToInt32(rd["Stls"]),
                        Sts = Convert.ToInt32(rd["Sts"]),
                        Strn = Convert.ToInt32(rd["Strn"]),
                        Strs = Convert.ToInt32(rd["Strs"]),
                        MEMLIST = rd["MEMLIST"].ToString()

                    });
                }

            }
            rd.Close();
            ViewBag.rc = list1;
            ViewBag.Message = "RC";

            return Json(new { });
        }

        [HttpPost]
        public ActionResult btnSave_Click(RC_Beam formContainer)
        {
            int rid = Convert.ToInt32(Session["rid"]);
            int aid = Convert.ToInt32(Session["aid"]);
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();

            if (formContainer.IsCheck)
            {
                formContainer.FL = 'T';
            }
            else
            {
                formContainer.FL = ' ';
            }
            if (formContainer.IsCheck2)
            {
                formContainer.FR = 'T';
            }
            else
            {
                formContainer.FR = ' ';
            }
            if (formContainer.IsCheck3)
            {
                formContainer.Cot2 = 'T';
            }
            else
            {
                formContainer.Cot2 = ' ';
            }
            if (formContainer.IsCheck4)
            {
                formContainer.Cot3 = 'T';
            }
            else
            {
                formContainer.Cot3 = ' ';
            }
            if (formContainer.IsCheck5)
            {
                formContainer.Cot4 = 'T';
            }
            else
            {
                formContainer.Cot4 = ' ';
            }
            if (formContainer.IsCheck6)
            {
                formContainer.Cot5 = 'T';
            }
            else
            {
                formContainer.Cot5 = ' ';
            }
            if (formContainer.IsCheck7)
            {
                formContainer.Cob2 = 'T';
            }
            else
            {
                formContainer.Cob2 = ' ';
            }
            if (formContainer.IsCheck8)
            {
                formContainer.Cob3 = 'T';
            }
            else
            {
                formContainer.Cob3 = ' ';
            }
            if (formContainer.IsCheck9)
            {
                formContainer.Cob4 = 'T';
            }
            else
            {
                formContainer.Cob4 = ' ';
            }
            if (formContainer.IsCheck10)
            {
                formContainer.Cob5 = 'T';
            }
            else
            {
                formContainer.Cob5 = ' ';
            }

            String query10 = "UPDATE RCBeam SET Ct = @Ct , Cb = @Cb , CLT = @CLT , CRT = @CRT , CL = @CL , CR = @CR , CLH = @CLH , CRH = @CRH , AXL = @AXL , FL = @FL , FR = @FR , TL = @TL , TR = @TR , N1 = @N1 , D1 = @D1 , N2 = @N2 , D2 = @D2 , L2L = @L2L , L2R = @L2R , N3 = @N3 , D3 = @D3 , L3L = @L3L , L3R = @L3R , N4 = @N4 , D4 = @D4 , L4L = @L4L , L4R = @L4R , N5 = @N5 , D5 = @D5 , L5L = @L5L , L5R = @L5R , LN1 = @LN1 , LD1 = @LD1 , LN2 = @LN2 , LD2 = @LD2 , LL2 = @LL2 , LN3 = @LN3 , LD3 = @LD3 , LL3 = @LL3 , LN4 = @LN4 , LD4 = @LD4 , LL4 = @LL4 , LN5 = @LN5 , LD5 = @LD5 , LL5 = @LL5 , RN2 = @RN2 , RD2 = @RD2 , RL2 = @RL2 , RN3 = @RN3 , RD3 = @RD3 , RL3 = @RL3 , RN4 = @RN4 , RD4 = @RD4 , RL4 = @RL4 , RN5 = @RN5 , RD5 = @RD5 , RL5 = @RL5 , N11 = @N11 , D11 = @D11 , RS2 = @RS2 , Cot2 = @Cot2 , Cot3 = @Cot3 , Cot4 = @Cot4 , Cot5 = @Cot5 , Cob2 = @Cob2 , Cob3 = @Cob3 , Cob4 = @Cob4 , Cob5 = @Cob5 , Ds = @Ds , Nso = @Nso , Nsi = @Nsi , Nslo = @Nslo , Nsli = @Nsli , Nsro = @Nsro , Nsri = @Nsri , Stln = @Stln , Stls = @Stls , Sts = @Sts , Strn = @Strn , Strs = @Strs , MEMLIST = @MEMLIST where accessFileID = " + aid + " AND Id = " + rid;
            //String query10 = "UPDATE RCBeam SET Ct = @Ct , Cb = @Cb , CLT = @CLT , CRT = @CRT , CL = @CL , CR = @CR , CLH = @CLH , CRH = @CRH , AXL = @AXL , FL = @FL , FR = @FR , TL = @TL , TR = @TR , N1 = @N1 , D1 = @D1 , N2 = @N2 , D2 = @D2 , L2L = @L2L , L2R = @L2R , N3 = @N3 , D3 = @D3 , L3L = @L3L , L3R = @L3R , N4 = @N4 , D4 = @D4 , L4L = @L4L , L4R = @L4R , N5 = @N5 , D5 = @D5 , L5L = @L5L , L5R = @L5R , LN1 = @LN1 , LD1 = @LD1 , LN2 = @LN2 , LD2 = @LD2 , LL2 = @LL2 , LN3 = @LN3 , LD3 = @LD3 , LL3 = @LL3 , LN4 = @LN4 , LD4 = @LD4 , LL4 = @LL4 , LN5 = @LN5 , LD5 = @LD5 , LL5 = @LL5 , RN2 = @RN2 , RD2 = @RD2 , RL2 = @RL2 , RN3 = @RN3 , RD3 = @RD3 , RL3 = @RL3 , RN4 = @RN4 , RD4 = @RD4 , RL4 = @RL4 , RN5 = @RN5 , RD5 = @RD5 , RL5 = @RL5 , N11 = @N11 , D11 = @D11 , RS2 = @RS2 , Ds = @Ds , Nso = @Nso , Nsi = @Nsi , Nslo = @Nslo , Nsli = @Nsli , Nsro = @Nsro , Nsri = @Nsri , Stln = @Stln , Stls = @Stls , Sts = @Sts , Strn = @Strn , Strs = @Strs , MEMLIST = @MEMLIST where accessFileID = " + aid + " AND Id = " + rid;
            SqlCommand sqlcomm10 = new SqlCommand(query10, sqlconn);
            sqlcomm10.Parameters.AddWithValue("@Ct", formContainer.Ct);
            sqlcomm10.Parameters.AddWithValue("@Cb", formContainer.Cb);
            sqlcomm10.Parameters.AddWithValue("@CLT", formContainer.CLT);
            sqlcomm10.Parameters.AddWithValue("@CRT", formContainer.CRT);
            sqlcomm10.Parameters.AddWithValue("@CL", formContainer.CL);
            sqlcomm10.Parameters.AddWithValue("@CR", formContainer.CR);
            sqlcomm10.Parameters.AddWithValue("@CLH", formContainer.CLH);
            sqlcomm10.Parameters.AddWithValue("@CRH", formContainer.CRH);
            sqlcomm10.Parameters.AddWithValue("@AXL", formContainer.AXL);
            sqlcomm10.Parameters.AddWithValue("@FL", formContainer.FL);
            sqlcomm10.Parameters.AddWithValue("@FR", formContainer.FR);
            sqlcomm10.Parameters.AddWithValue("@TL", formContainer.TL);
            sqlcomm10.Parameters.AddWithValue("@TR", formContainer.TR);
            sqlcomm10.Parameters.AddWithValue("@N1", formContainer.N1);
            sqlcomm10.Parameters.AddWithValue("@D1", formContainer.D1);
            sqlcomm10.Parameters.AddWithValue("@N2", formContainer.N2);
            sqlcomm10.Parameters.AddWithValue("@D2", formContainer.D2);
            sqlcomm10.Parameters.AddWithValue("@L2L", formContainer.L2L);
            sqlcomm10.Parameters.AddWithValue("@L2R", formContainer.L2R);
            sqlcomm10.Parameters.AddWithValue("@N3", formContainer.N3);
            sqlcomm10.Parameters.AddWithValue("@D3", formContainer.D3);
            sqlcomm10.Parameters.AddWithValue("@L3L", formContainer.L3L);
            sqlcomm10.Parameters.AddWithValue("@L3R", formContainer.L3R);
            sqlcomm10.Parameters.AddWithValue("@N4", formContainer.N4);
            sqlcomm10.Parameters.AddWithValue("@D4", formContainer.D4);
            sqlcomm10.Parameters.AddWithValue("@L4L", formContainer.L4L);
            sqlcomm10.Parameters.AddWithValue("@L4R", formContainer.L4R);
            sqlcomm10.Parameters.AddWithValue("@N5", formContainer.N5);
            sqlcomm10.Parameters.AddWithValue("@D5", formContainer.D5);
            sqlcomm10.Parameters.AddWithValue("@L5L", formContainer.L5L);
            sqlcomm10.Parameters.AddWithValue("@L5R", formContainer.L5R);
            sqlcomm10.Parameters.AddWithValue("@LN1", formContainer.LN1);
            sqlcomm10.Parameters.AddWithValue("@LD1", formContainer.LD1);
            sqlcomm10.Parameters.AddWithValue("@LN2", formContainer.LN2);
            sqlcomm10.Parameters.AddWithValue("@LD2", formContainer.LD2);
            sqlcomm10.Parameters.AddWithValue("@LL2", formContainer.LL2);
            sqlcomm10.Parameters.AddWithValue("@LN3", formContainer.LN3);
            sqlcomm10.Parameters.AddWithValue("@LD3", formContainer.LD3);
            sqlcomm10.Parameters.AddWithValue("@LL3", formContainer.LL3);
            sqlcomm10.Parameters.AddWithValue("@LN4", formContainer.LN4);
            sqlcomm10.Parameters.AddWithValue("@LD4", formContainer.LD4);
            sqlcomm10.Parameters.AddWithValue("@LL4", formContainer.LL4);
            sqlcomm10.Parameters.AddWithValue("@LN5", formContainer.LN5);
            sqlcomm10.Parameters.AddWithValue("@LD5", formContainer.LD5);
            sqlcomm10.Parameters.AddWithValue("@LL5", formContainer.LL5);
            sqlcomm10.Parameters.AddWithValue("@RN2", formContainer.RN2);
            sqlcomm10.Parameters.AddWithValue("@RD2", formContainer.RD2);
            sqlcomm10.Parameters.AddWithValue("@RL2", formContainer.RL2);
            sqlcomm10.Parameters.AddWithValue("@RN3", formContainer.RN3);
            sqlcomm10.Parameters.AddWithValue("@RD3", formContainer.RD3);
            sqlcomm10.Parameters.AddWithValue("@RL3", formContainer.RL3);
            sqlcomm10.Parameters.AddWithValue("@RN4", formContainer.RN4);
            sqlcomm10.Parameters.AddWithValue("@RD4", formContainer.RD4);
            sqlcomm10.Parameters.AddWithValue("@RL4", formContainer.RL4);
            sqlcomm10.Parameters.AddWithValue("@RN5", formContainer.RN5);
            sqlcomm10.Parameters.AddWithValue("@RD5", formContainer.RD5);
            sqlcomm10.Parameters.AddWithValue("@RL5", formContainer.RL5);
            sqlcomm10.Parameters.AddWithValue("@N11", formContainer.N11);
            sqlcomm10.Parameters.AddWithValue("@D11", formContainer.D11);
            sqlcomm10.Parameters.AddWithValue("@RS2", formContainer.RS2);
            sqlcomm10.Parameters.AddWithValue("@Cot2", formContainer.Cot2);
            sqlcomm10.Parameters.AddWithValue("@Cot3", formContainer.Cot3);
            sqlcomm10.Parameters.AddWithValue("@Cot4", formContainer.Cot4);
            sqlcomm10.Parameters.AddWithValue("@Cot5", formContainer.Cot5);
            sqlcomm10.Parameters.AddWithValue("@Cob2", formContainer.Cob2);
            sqlcomm10.Parameters.AddWithValue("@Cob3", formContainer.Cob3);
            sqlcomm10.Parameters.AddWithValue("@Cob4", formContainer.Cob4);
            sqlcomm10.Parameters.AddWithValue("@Cob5", formContainer.Cob5);
            sqlcomm10.Parameters.AddWithValue("@Ds", formContainer.Ds);
            sqlcomm10.Parameters.AddWithValue("@Nso", formContainer.Nso);
            sqlcomm10.Parameters.AddWithValue("@Nsi", formContainer.Nsi);
            sqlcomm10.Parameters.AddWithValue("@Nslo", formContainer.Nslo);
            sqlcomm10.Parameters.AddWithValue("@Nsli", formContainer.Nsli);
            sqlcomm10.Parameters.AddWithValue("@Nsro", formContainer.Nsro);
            sqlcomm10.Parameters.AddWithValue("@Nsri", formContainer.Nsri);
            sqlcomm10.Parameters.AddWithValue("@Stln", formContainer.Stln);
            sqlcomm10.Parameters.AddWithValue("@Stls", formContainer.Stls);
            sqlcomm10.Parameters.AddWithValue("@Sts", formContainer.Sts);
            sqlcomm10.Parameters.AddWithValue("@Strn", formContainer.Strn);
            sqlcomm10.Parameters.AddWithValue("@Strs", formContainer.Strs);
            sqlcomm10.Parameters.AddWithValue("@MEMLIST", formContainer.MEMLIST);
            sqlcomm10.ExecuteNonQuery();
            sqlconn.Close();
            ViewBag.Message = "save";
            return Json(new { });
        }
        [HttpPost]
        public ActionResult btnOK_Click()
        {
            int aid = Convert.ToInt32(Session["aid"]);
            List<RC_Beam> list1 = new List<RC_Beam>();
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            //SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=StructureEn;Integrated Security=True");
            sqlconn.Open();
            String query2 = "SELECT * FROM RCBeam where accessFileID = " + aid + " ORDER BY Mem ASC";
            SqlCommand sqlcomm2 = new SqlCommand(query2, sqlconn);
            SqlDataReader rd = sqlcomm2.ExecuteReader();
            while (rd.Read())
            {
                list1.Add(new RC_Beam
                {
                    Mem = Convert.ToInt32(rd["Mem"]),
                    B = rd["B"].ToString(),
                    T = rd["T"].ToString(),
                    Ct = Convert.ToInt32(rd["Ct"]),
                    Cb = Convert.ToInt32(rd["Cb"]),
                    N1 = Convert.ToInt32(rd["N1"]),
                    D1 = Convert.ToInt32(rd["D1"]),
                    N2 = Convert.ToInt32(rd["N2"]),
                    D2 = Convert.ToInt32(rd["D2"]),
                    L2L = Convert.ToInt32(rd["L2L"]),
                    L2R = Convert.ToInt32(rd["L2R"]),
                    N3 = Convert.ToInt32(rd["N3"]),
                    D3 = Convert.ToInt32(rd["D3"]),
                    L3L = Convert.ToInt32(rd["L3L"]),
                    L3R = Convert.ToInt32(rd["L3R"]),
                    N4 = Convert.ToInt32(rd["N4"]),
                    D4 = Convert.ToInt32(rd["D4"]),
                    L4L = Convert.ToInt32(rd["L4L"]),
                    L4R = Convert.ToInt32(rd["L4R"]),
                    N5 = Convert.ToInt32(rd["N5"]),
                    D5 = Convert.ToInt32(rd["D5"]),
                    L5L = Convert.ToInt32(rd["L5L"]),
                    L5R = Convert.ToInt32(rd["L5R"]),
                    LN1 = Convert.ToInt32(rd["LN1"]),
                    LD1 = Convert.ToInt32(rd["LD1"]),
                    LN2 = Convert.ToInt32(rd["LN2"]),
                    LD2 = Convert.ToInt32(rd["LD2"]),
                    LL2 = Convert.ToInt32(rd["LL2"]),
                    LN3 = Convert.ToInt32(rd["LN3"]),
                    LD3 = Convert.ToInt32(rd["LD3"]),
                    LL3 = Convert.ToInt32(rd["LL3"]),
                    LN4 = Convert.ToInt32(rd["LN4"]),
                    LD4 = Convert.ToInt32(rd["LD4"]),
                    LL4 = Convert.ToInt32(rd["LL4"]),
                    LN5 = Convert.ToInt32(rd["LN5"]),
                    LD5 = Convert.ToInt32(rd["LD5"]),
                    LL5 = Convert.ToInt32(rd["LL5"]),
                    RN2 = Convert.ToInt32(rd["RN2"]),
                    RD2 = Convert.ToInt32(rd["RD2"]),
                    RL2 = Convert.ToInt32(rd["RL2"]),
                    RN3 = Convert.ToInt32(rd["RN3"]),
                    RD3 = Convert.ToInt32(rd["RD3"]),
                    RL3 = Convert.ToInt32(rd["RL3"]),
                    RN4 = Convert.ToInt32(rd["RN4"]),
                    RD4 = Convert.ToInt32(rd["RD4"]),
                    RL4 = Convert.ToInt32(rd["RL4"]),
                    RN5 = Convert.ToInt32(rd["RN5"]),
                    RD5 = Convert.ToInt32(rd["RD5"]),
                    RL5 = Convert.ToInt32(rd["RL5"]),
                    N11 = Convert.ToInt32(rd["N11"]),
                    D11 = Convert.ToInt32(rd["D11"]),
                    RS2 = Convert.ToInt32(rd["RS2"]),
                    Cot2 = Convert.ToChar(rd["Cot2"]),
                    Cot3 = Convert.ToChar(rd["Cot3"]),
                    Cot4 = Convert.ToChar(rd["Cot4"]),
                    Cot5 = Convert.ToChar(rd["Cot5"]),
                    Cob2 = Convert.ToChar(rd["Cob2"]),
                    Cob3 = Convert.ToChar(rd["Cob3"]),
                    Cob4 = Convert.ToChar(rd["Cob4"]),
                    Cob5 = Convert.ToChar(rd["Cob5"]),
                    Ds = Convert.ToInt32(rd["Ds"]),
                    Nso = Convert.ToInt32(rd["Nso"]),
                    Nsi = Convert.ToInt32(rd["Nsi"]),
                    Nslo = Convert.ToInt32(rd["Nslo"]),
                    Nsli = Convert.ToInt32(rd["Nsli"]),
                    Nsro = Convert.ToInt32(rd["Nsro"]),
                    Nsri = Convert.ToInt32(rd["Nsri"]),
                    Stln = Convert.ToInt32(rd["Stln"]),
                    Stls = Convert.ToInt32(rd["Stls"]),
                    Sts = Convert.ToInt32(rd["Sts"]),
                    Strn = Convert.ToInt32(rd["Strn"]),
                    Strs = Convert.ToInt32(rd["Strs"]),
                });
            }
            rd.Close();
            return Json(new { list = list1 });
        }

        [HttpPost]
        public ActionResult BtnDesignAll_Click(RC_Beam formContainer)
        {
            int aid = Convert.ToInt32(Session["aid"]);
            List<RC_Beam> list1 = new List<RC_Beam>();
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            //SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=StructureEn;Integrated Security=True");
            sqlconn.Open();
            String query2 = "SELECT * FROM RCBeam where accessFileID = " + aid + " ORDER BY Mem ASC";
            SqlCommand sqlcomm2 = new SqlCommand(query2, sqlconn);
            SqlDataReader rd = sqlcomm2.ExecuteReader();
            while (rd.Read())
            {
                list1.Add(new RC_Beam
                {
                    ID = Convert.ToInt32(rd["Id"]),
                    Mem = Convert.ToInt32(rd["Mem"]),
                });
            }
            int[] RunAllError = new int[1];
            Boolean DesignAll = true;
            List<RC_Beam> designallList = new List<RC_Beam>();
            int txtRownum = 0;
            int i = 0;
            string msg = "";
            string MemErrorList = "";
            for (i = 0; i < list1.Count; i++)
            {
                txtRownum = list1[i].ID;
                designallList = DesignAll_Data(txtRownum);
                //Design(formContainer);
            }
            if (RunAllError.GetUpperBound(0) != 0)
            {
                for (i = 1; i < RunAllError.GetUpperBound(0); i++)
                {
                    if (i != RunAllError.GetUpperBound(0))
                    {
                        MemErrorList = "MemErrorList" + RunAllError[i].ToString() + ",";
                    }
                    else
                    {
                        MemErrorList = "MemErrorList" + RunAllError[i].ToString();
                    }
                }
                if (RunAllError.GetUpperBound(0) == 1)
                {
                    msg = "Member number " + MemErrorList + " was not designed due to error.";
                }
                else
                {
                    msg = "Member number " + MemErrorList + " were not designed due to error.";
                }
            }
            DesignAll = false;
            return Json(new { txtRowNumber = txtRownum, MSG = msg, list = designallList });
        }

        public List<RC_Beam> DesignAll_Data(int id)
        {
            int aid = Convert.ToInt32(Session["aid"]);
            List<RC_Beam> list1 = new List<RC_Beam>();
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            //SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=StructureEn;Integrated Security=True");
            sqlconn.Open();
            String query2 = "SELECT * FROM RCBeam where accessFileID = " + aid + " AND Id = " + id + " ORDER BY Mem ASC";
            SqlCommand sqlcomm2 = new SqlCommand(query2, sqlconn);
            SqlDataReader rd = sqlcomm2.ExecuteReader();
            while (rd.Read())
            {
                list1.Add(new RC_Beam
                {
                    Mem = Convert.ToInt32(rd["Mem"]),
                    B = rd["B"].ToString(),
                    T = rd["T"].ToString(),
                    Ct = Convert.ToInt32(rd["Ct"]),
                    Cb = Convert.ToInt32(rd["Cb"]),
                    N1 = Convert.ToInt32(rd["N1"]),
                    D1 = Convert.ToInt32(rd["D1"]),
                    N2 = Convert.ToInt32(rd["N2"]),
                    D2 = Convert.ToInt32(rd["D2"]),
                    L2L = Convert.ToInt32(rd["L2L"]),
                    L2R = Convert.ToInt32(rd["L2R"]),
                    N3 = Convert.ToInt32(rd["N3"]),
                    D3 = Convert.ToInt32(rd["D3"]),
                    L3L = Convert.ToInt32(rd["L3L"]),
                    L3R = Convert.ToInt32(rd["L3R"]),
                    N4 = Convert.ToInt32(rd["N4"]),
                    D4 = Convert.ToInt32(rd["D4"]),
                    L4L = Convert.ToInt32(rd["L4L"]),
                    L4R = Convert.ToInt32(rd["L4R"]),
                    N5 = Convert.ToInt32(rd["N5"]),
                    D5 = Convert.ToInt32(rd["D5"]),
                    L5L = Convert.ToInt32(rd["L5L"]),
                    L5R = Convert.ToInt32(rd["L5R"]),
                    LN1 = Convert.ToInt32(rd["LN1"]),
                    LD1 = Convert.ToInt32(rd["LD1"]),
                    LN2 = Convert.ToInt32(rd["LN2"]),
                    LD2 = Convert.ToInt32(rd["LD2"]),
                    LL2 = Convert.ToInt32(rd["LL2"]),
                    LN3 = Convert.ToInt32(rd["LN3"]),
                    LD3 = Convert.ToInt32(rd["LD3"]),
                    LL3 = Convert.ToInt32(rd["LL3"]),
                    LN4 = Convert.ToInt32(rd["LN4"]),
                    LD4 = Convert.ToInt32(rd["LD4"]),
                    LL4 = Convert.ToInt32(rd["LL4"]),
                    LN5 = Convert.ToInt32(rd["LN5"]),
                    LD5 = Convert.ToInt32(rd["LD5"]),
                    LL5 = Convert.ToInt32(rd["LL5"]),
                    RN2 = Convert.ToInt32(rd["RN2"]),
                    RD2 = Convert.ToInt32(rd["RD2"]),
                    RL2 = Convert.ToInt32(rd["RL2"]),
                    RN3 = Convert.ToInt32(rd["RN3"]),
                    RD3 = Convert.ToInt32(rd["RD3"]),
                    RL3 = Convert.ToInt32(rd["RL3"]),
                    RN4 = Convert.ToInt32(rd["RN4"]),
                    RD4 = Convert.ToInt32(rd["RD4"]),
                    RL4 = Convert.ToInt32(rd["RL4"]),
                    RN5 = Convert.ToInt32(rd["RN5"]),
                    RD5 = Convert.ToInt32(rd["RD5"]),
                    RL5 = Convert.ToInt32(rd["RL5"]),
                    N11 = Convert.ToInt32(rd["N11"]),
                    D11 = Convert.ToInt32(rd["D11"]),
                    RS2 = Convert.ToInt32(rd["RS2"]),
                    Cot2 = Convert.ToChar(rd["Cot2"]),
                    Cot3 = Convert.ToChar(rd["Cot3"]),
                    Cot4 = Convert.ToChar(rd["Cot4"]),
                    Cot5 = Convert.ToChar(rd["Cot5"]),
                    Cob2 = Convert.ToChar(rd["Cob2"]),
                    Cob3 = Convert.ToChar(rd["Cob3"]),
                    Cob4 = Convert.ToChar(rd["Cob4"]),
                    Cob5 = Convert.ToChar(rd["Cob5"]),
                    Ds = Convert.ToInt32(rd["Ds"]),
                    Nso = Convert.ToInt32(rd["Nso"]),
                    Nsi = Convert.ToInt32(rd["Nsi"]),
                    Nslo = Convert.ToInt32(rd["Nslo"]),
                    Nsli = Convert.ToInt32(rd["Nsli"]),
                    Nsro = Convert.ToInt32(rd["Nsro"]),
                    Nsri = Convert.ToInt32(rd["Nsri"]),
                    Stln = Convert.ToInt32(rd["Stln"]),
                    Stls = Convert.ToInt32(rd["Stls"]),
                    Sts = Convert.ToInt32(rd["Sts"]),
                    Strn = Convert.ToInt32(rd["Strn"]),
                    Strs = Convert.ToInt32(rd["Strs"]),
                });
            }
            rd.Close();
            return list1;
        }
        public ActionResult CalSheet()
        {
            int aid = Convert.ToInt32(Session["aid"]);
            List<RC_Beam> list1 = new List<RC_Beam>();
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            String query2 = "SELECT * FROM RCBeam where accessFileID = " + aid + " ORDER BY Mem ASC";
            SqlCommand sqlcomm2 = new SqlCommand(query2, sqlconn);
            SqlDataReader rd = sqlcomm2.ExecuteReader();
            while (rd.Read())
            {
                list1.Add(new RC_Beam
                {
                    Mem = Convert.ToInt32(rd["Mem"]),
                    Mark = rd["Mark"].ToString(),
                    B = rd["B"].ToString(),
                    T = rd["T"].ToString(),
                    L = float.Parse(rd["L"].ToString()),
                    LT = float.Parse(rd["LT"].ToString()),
                    Name = rd["Name"].ToString(),
                    Ct = Convert.ToInt32(rd["Ct"]),
                    Cb = Convert.ToInt32(rd["Cb"]),
                    CLT = Convert.ToChar(rd["CLT"]),
                    CRT = Convert.ToChar(rd["CRT"]),
                    CL = Convert.ToChar(rd["CL"]),
                    CR = Convert.ToChar(rd["CR"]),
                    CLH = Convert.ToChar(rd["CLH"]),
                    CRH = Convert.ToChar(rd["CRH"]),
                    AXL = Convert.ToChar(rd["AXL"]),
                    FL = Convert.ToChar(rd["FL"]),
                    FR = Convert.ToChar(rd["FR"]),
                    TL = Convert.ToInt32(rd["TL"]),
                    TR = Convert.ToInt32(rd["TR"]),
                    N1 = Convert.ToInt32(rd["N1"]),
                    D1 = Convert.ToInt32(rd["D1"]),
                    N2 = Convert.ToInt32(rd["N2"]),
                    D2 = Convert.ToInt32(rd["D2"]),
                    L2L = Convert.ToInt32(rd["L2L"]),
                    L2R = Convert.ToInt32(rd["L2R"]),
                    N3 = Convert.ToInt32(rd["N3"]),
                    D3 = Convert.ToInt32(rd["D3"]),
                    L3L = Convert.ToInt32(rd["L3L"]),
                    L3R = Convert.ToInt32(rd["L3R"]),
                    N4 = Convert.ToInt32(rd["N4"]),
                    D4 = Convert.ToInt32(rd["D4"]),
                    L4L = Convert.ToInt32(rd["L4L"]),
                    L4R = Convert.ToInt32(rd["L4R"]),
                    N5 = Convert.ToInt32(rd["N5"]),
                    D5 = Convert.ToInt32(rd["D5"]),
                    L5L = Convert.ToInt32(rd["L5L"]),
                    L5R = Convert.ToInt32(rd["L5R"]),
                    LN1 = Convert.ToInt32(rd["LN1"]),
                    LD1 = Convert.ToInt32(rd["LD1"]),
                    LN2 = Convert.ToInt32(rd["LN2"]),
                    LD2 = Convert.ToInt32(rd["LD2"]),
                    LL2 = Convert.ToInt32(rd["LL2"]),
                    LN3 = Convert.ToInt32(rd["LN3"]),
                    LD3 = Convert.ToInt32(rd["LD3"]),
                    LL3 = Convert.ToInt32(rd["LL3"]),
                    LN4 = Convert.ToInt32(rd["LN4"]),
                    LD4 = Convert.ToInt32(rd["LD4"]),
                    LL4 = Convert.ToInt32(rd["LL4"]),
                    LN5 = Convert.ToInt32(rd["LN5"]),
                    LD5 = Convert.ToInt32(rd["LD5"]),
                    LL5 = Convert.ToInt32(rd["LL5"]),
                    RN2 = Convert.ToInt32(rd["RN2"]),
                    RD2 = Convert.ToInt32(rd["RD2"]),
                    RL2 = Convert.ToInt32(rd["RL2"]),
                    RN3 = Convert.ToInt32(rd["RN3"]),
                    RD3 = Convert.ToInt32(rd["RD3"]),
                    RL3 = Convert.ToInt32(rd["RL3"]),
                    RN4 = Convert.ToInt32(rd["RN4"]),
                    RD4 = Convert.ToInt32(rd["RD4"]),
                    RL4 = Convert.ToInt32(rd["RL4"]),
                    RN5 = Convert.ToInt32(rd["RN5"]),
                    RD5 = Convert.ToInt32(rd["RD5"]),
                    RL5 = Convert.ToInt32(rd["RL5"]),
                    N11 = Convert.ToInt32(rd["N11"]),
                    D11 = Convert.ToInt32(rd["D11"]),
                    RS2 = Convert.ToInt32(rd["RS2"]),
                    Cot2 = Convert.ToChar(rd["Cot2"]),
                    Cot3 = Convert.ToChar(rd["Cot3"]),
                    Cot4 = Convert.ToChar(rd["Cot4"]),
                    Cot5 = Convert.ToChar(rd["Cot5"]),
                    Cob2 = Convert.ToChar(rd["Cob2"]),
                    Cob3 = Convert.ToChar(rd["Cob3"]),
                    Cob4 = Convert.ToChar(rd["Cob4"]),
                    Cob5 = Convert.ToChar(rd["Cob5"]),
                    Ds = Convert.ToInt32(rd["Ds"]),
                    Nso = Convert.ToInt32(rd["Nso"]),
                    Nsi = Convert.ToInt32(rd["Nsi"]),
                    Nslo = Convert.ToInt32(rd["Nslo"]),
                    Nsli = Convert.ToInt32(rd["Nsli"]),
                    Nsro = Convert.ToInt32(rd["Nsro"]),
                    Nsri = Convert.ToInt32(rd["Nsri"]),
                    Stln = Convert.ToInt32(rd["Stln"]),
                    Stls = Convert.ToInt32(rd["Stls"]),
                    Sts = Convert.ToInt32(rd["Sts"]),
                    Strn = Convert.ToInt32(rd["Strn"]),
                    Strs = Convert.ToInt32(rd["Strs"]),
                    MEMLIST = rd["MEMLIST"].ToString(),
                    M3X0 = float.Parse(rd["M3X0"].ToString()),
                    TX0 = float.Parse(rd["TX0"].ToString()),
                    SX0 = float.Parse(rd["SX0"].ToString()),
                    V2XV0 = float.Parse(rd["V2XV0"].ToString()),
                    XV0 = float.Parse(rd["XV0"].ToString()),
                    M3XM = float.Parse(rd["M3XM"].ToString()),
                    TXM = float.Parse(rd["TXM"].ToString()),
                    SXM = float.Parse(rd["SXM"].ToString()),
                    M3X10 = float.Parse(rd["M3X10"].ToString()),
                    TX10 = float.Parse(rd["TX10"].ToString()),
                    SX10 = float.Parse(rd["SX10"].ToString()),
                    V2XV10 = float.Parse(rd["V2XV10"].ToString()),
                    XV10 = float.Parse(rd["XV10"].ToString())
                });
            }

            rd.Close();
            int lastRow = list1.Count;
            List<calSheet> calS = new List<calSheet>();
            for (int i = 0; i < lastRow; i++)
            {
                if (list1[i].MEMLIST == " ")
                {
                    calS.Add(new calSheet
                    {
                        MemberNo = list1[i].Mem.ToString()
                    });
                }
                else
                {
                    calS.Add(new calSheet
                    {
                        MemberNo = list1[i].Mem.ToString() + "/" + list1[i].MEMLIST
                    });
                }
                calS[i].Ds = list1[i].Ds.ToString();
                calS[i].Mark = list1[i].Mark;
                calS[i].LTB1 = list1[i].LN1.ToString() + "-" + list1[i].LD1.ToString();
                calS[i].LTB2 = list1[i].LN2.ToString() + "-" + list1[i].LD2.ToString();
                calS[i].LTB3 = list1[i].LN3.ToString() + "-" + list1[i].LD3.ToString();
                calS[i].LTB4 = list1[i].LN4.ToString() + "-" + list1[i].LD4.ToString();
                calS[i].LTB5 = list1[i].LN5.ToString() + "-" + list1[i].LD5.ToString();
                if (list1[i].Stls == 0)
                {
                    if (list1[i].Nsi == 0)
                    {
                        calS[i].LTBStls = list1[i].Nso.ToString() + "@" + list1[i].Sts.ToString();
                    }
                    else
                    {
                        calS[i].LTBStls = list1[i].Nso.ToString() + "," + list1[i].Nsi.ToString() + "@" + list1[i].Sts.ToString();
                    }
                }
                else
                {
                    if (list1[i].Nsli == 0)
                    {
                        calS[i].LTBStls = list1[i].Nslo.ToString() + "@" + list1[i].Stls.ToString();
                    }
                    else
                    {
                        calS[i].LTBStls = list1[i].Nslo.ToString() + "," + list1[i].Nsli.ToString() + "@" + list1[i].Stls.ToString();
                    }
                }
                calS[i].MBB1 = list1[i].N1.ToString() + "-" + list1[i].D1.ToString();
                calS[i].MBB2 = list1[i].N2.ToString() + "-" + list1[i].D2.ToString();
                calS[i].MBB3 = list1[i].N3.ToString() + "-" + list1[i].D3.ToString();
                calS[i].MBB4 = list1[i].N4.ToString() + "-" + list1[i].D4.ToString();
                calS[i].MBB5 = list1[i].N5.ToString() + "-" + list1[i].D5.ToString();
                if (list1[i].Nsi == 0)
                {
                    calS[i].MBBSts = list1[i].Nso.ToString() + "@" + list1[i].Sts.ToString();
                }
                else
                {
                    calS[i].MBBSts = list1[i].Nso.ToString() + "," + list1[i].Nsi.ToString() + "@" + list1[i].Sts.ToString();
                }
                calS[i].RTB1 = list1[i].LN1.ToString() + "-" + list1[i].LD1.ToString();
                calS[i].RTB2 = list1[i].RN2.ToString() + "-" + list1[i].RD2.ToString();
                calS[i].RTB3 = list1[i].RN3.ToString() + "-" + list1[i].RD3.ToString();
                calS[i].RTB4 = list1[i].RN4.ToString() + "-" + list1[i].RD4.ToString();
                calS[i].RTB5 = list1[i].RN5.ToString() + "-" + list1[i].RD5.ToString();
                if (list1[i].Strs == 0)
                {
                    if (list1[i].Nsi == 0)
                    {
                        calS[i].RTBStrs = list1[i].Nso.ToString() + "@" + list1[i].Sts.ToString();
                    }
                    else
                    {
                        calS[i].RTBStrs = list1[i].Nso.ToString() + "," + list1[i].Nsi.ToString() + "@" + list1[i].Sts.ToString();
                    }
                }
                else
                {
                    if (list1[i].Nslo == 0)
                    {
                        calS[i].RTBStrs = list1[i].Nsro.ToString() + "@" + list1[i].Strs.ToString();
                    }
                    else
                    {
                        calS[i].RTBStrs = list1[i].Nsro.ToString() + "," + list1[i].Nsri.ToString() + "@" + list1[i].Strs.ToString();
                    }
                }
                calS[i].Cm = list1[i].B + "x" + list1[i].T;
                calS[i].Mzl = list1[i].M3X0.ToString();
                calS[i].Mxl = list1[i].TX0.ToString();
                calS[i].SX0 = list1[i].SX0.ToString();
                calS[i].VX0 = list1[i].V2XV0.ToString();
                calS[i].SV0 = list1[i].XV0.ToString();
                calS[i].Mzm = list1[i].M3XM.ToString();
                calS[i].Mxm = list1[i].TXM.ToString();
                calS[i].Sxm = list1[i].SXM.ToString();
                calS[i].Mzr = list1[i].M3X10.ToString();
                calS[i].Mxr = list1[i].TX10.ToString();
                calS[i].SX10 = list1[i].SX10.ToString();
                calS[i].VX10 = list1[i].V2XV10.ToString();
                calS[i].SV10 = list1[i].XV10.ToString();
            }
            ViewBag.CalSheet = calS;
            Session["Cals"] = calS;
            return View();
        }
        public void ExportToExcel()
        {
            List<calSheet> cals = Session["Cals"] as List<calSheet>;
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("CalSheet");
            ws.Cells["A:AZ"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Cells["A:AZ"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            ws.Cells[1, 1].Value = "RCBEAM DESIGN RESULTS";
            ws.Cells[1, 1].Style.Font.Bold = true;
            ws.Cells[1, 1].Style.Font.UnderLine = true;
            ws.Cells[2, 1, 3, 1].Merge = true;
            ws.Cells[2, 1].Value = "Member No.";
            ws.Cells[2, 2, 3, 2].Merge = true;
            ws.Cells[2, 2].Value = "Ds";
            ws.Cells[2, 3, 3, 3].Merge = true;
            ws.Cells[2, 3].Value = "Mark";
            ws.Cells[2, 4, 2, 9].Merge = true;
            ws.Cells[2, 4].Value = "Left Top Bar";
            ws.Cells[2, 10, 2, 15].Merge = true;
            ws.Cells[2, 10].Value = "Middle Bottom Bar";
            ws.Cells[2, 16, 2, 21].Merge = true;
            ws.Cells[2, 16].Value = "Right Top Bar";
            ws.Cells[3, 4].Value = "1st";
            ws.Cells[3, 5].Value = "2nd-ex";
            ws.Cells[3, 6].Value = "3rd-ex";
            ws.Cells[3, 7].Value = "4th-ex";
            ws.Cells[3, 8].Value = "5th-ex";
            ws.Cells[3, 9].Value = "Stls";
            ws.Cells[3, 10].Value = "1st";
            ws.Cells[3, 11].Value = "2nd-ex";
            ws.Cells[3, 12].Value = "3rd-ex";
            ws.Cells[3, 13].Value = "4th-ex";
            ws.Cells[3, 14].Value = "5th-ex";
            ws.Cells[3, 15].Value = "Sts";
            ws.Cells[3, 16].Value = "1st";
            ws.Cells[3, 17].Value = "2nd-ex";
            ws.Cells[3, 18].Value = "3rd-ex";
            ws.Cells[3, 19].Value = "4th-ex";
            ws.Cells[3, 20].Value = "5th-ex";
            ws.Cells[3, 21].Value = "Strs";
            ws.Cells[4, 2].Value = "mm";
            ws.Cells[4, 3].Value = "cm";
            ws.Cells[4, 4].Value = "Mzl";
            ws.Cells[4, 5].Value = "Mxl";
            ws.Cells[4, 6].Value = "SX0";
            ws.Cells[4, 7].Value = "VX0";
            ws.Cells[4, 8].Value = "SV0";
            ws.Cells[4, 9].Value = "mm";
            ws.Cells[4, 10].Value = "Mzm";
            ws.Cells[4, 11].Value = "Mxm";
            ws.Cells[4, 12].Value = "SXm";
            ws.Cells[4, 15].Value = "mm";
            ws.Cells[4, 16].Value = "Mzr";
            ws.Cells[4, 17].Value = "Mxr";
            ws.Cells[4, 18].Value = "SX10";
            ws.Cells[4, 19].Value = "VX10";
            ws.Cells[4, 20].Value = "SV10";
            ws.Cells[4, 21].Value = "mm";
            int j = 5;
            for (int i = 0; i < cals.Count; i++)
            {
                ws.Cells[j, 1].Value = cals[i].MemberNo;
                ws.Cells[j, 2].Value = cals[i].Ds;
                ws.Cells[j, 3].Value = cals[i].Mark;
                ws.Cells[j, 4].Value = cals[i].LTB1;
                ws.Cells[j, 5].Value = cals[i].LTB2;
                ws.Cells[j, 6].Value = cals[i].LTB3;
                ws.Cells[j, 7].Value = cals[i].LTB4;
                ws.Cells[j, 8].Value = cals[i].LTB5;
                ws.Cells[j, 9].Value = cals[i].LTBStls;
                ws.Cells[j, 10].Value = cals[i].MBB1;
                ws.Cells[j, 11].Value = cals[i].MBB2;
                ws.Cells[j, 12].Value = cals[i].MBB3;
                ws.Cells[j, 13].Value = cals[i].MBB4;
                ws.Cells[j, 14].Value = cals[i].MBB5;
                ws.Cells[j, 15].Value = cals[i].MBBSts;
                ws.Cells[j, 16].Value = cals[i].RTB1;
                ws.Cells[j, 17].Value = cals[i].RTB2;
                ws.Cells[j, 18].Value = cals[i].RTB3;
                ws.Cells[j, 19].Value = cals[i].RTB4;
                ws.Cells[j, 20].Value = cals[i].RTB5;
                ws.Cells[j, 21].Value = cals[i].RTBStrs;
                ws.Cells[j + 1, 3].Value = cals[i].Cm;
                ws.Cells[j + 1, 4].Value = cals[i].Mzl;
                ws.Cells[j + 1, 5].Value = cals[i].Mxl;
                ws.Cells[j + 1, 6].Value = cals[i].SX0;
                ws.Cells[j + 1, 7].Value = cals[i].VX0;
                ws.Cells[j + 1, 8].Value = cals[i].SV0;
                ws.Cells[j + 1, 10].Value = cals[i].Mzm;
                ws.Cells[j + 1, 11].Value = cals[i].Mxm;
                ws.Cells[j + 1, 12].Value = cals[i].Sxm;
                ws.Cells[j + 1, 16].Value = cals[i].Mzr;
                ws.Cells[j + 1, 17].Value = cals[i].Mxr;
                ws.Cells[j + 1, 18].Value = cals[i].SX10;
                ws.Cells[j + 1, 19].Value = cals[i].VX10;
                ws.Cells[j + 1, 20].Value = cals[i].SV10;
                j += 2;
            }
            ws.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename" + "CalSheet.xlsx");
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
        public ActionResult BeamDetail()
        {
            int aid = Convert.ToInt32(Session["aid"]);
            List<RC_Beam> list1 = new List<RC_Beam>();
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            String query2 = "SELECT * FROM RCBeam where accessFileID = " + aid + " ORDER BY Mem ASC";
            SqlCommand sqlcomm2 = new SqlCommand(query2, sqlconn);
            SqlDataReader rd = sqlcomm2.ExecuteReader();
            while (rd.Read())
            {
                list1.Add(new RC_Beam
                {
                    Mem = Convert.ToInt32(rd["Mem"]),
                    Mark = rd["Mark"].ToString(),
                    B = rd["B"].ToString(),
                    T = rd["T"].ToString(),
                    L = float.Parse(rd["L"].ToString()),
                    LT = float.Parse(rd["LT"].ToString()),
                    Name = rd["Name"].ToString(),
                    Ct = Convert.ToInt32(rd["Ct"]),
                    Cb = Convert.ToInt32(rd["Cb"]),
                    CLT = Convert.ToChar(rd["CLT"]),
                    CRT = Convert.ToChar(rd["CRT"]),
                    CL = Convert.ToChar(rd["CL"]),
                    CR = Convert.ToChar(rd["CR"]),
                    CLH = Convert.ToChar(rd["CLH"]),
                    CRH = Convert.ToChar(rd["CRH"]),
                    AXL = Convert.ToChar(rd["AXL"]),
                    FL = Convert.ToChar(rd["FL"]),
                    FR = Convert.ToChar(rd["FR"]),
                    TL = Convert.ToInt32(rd["TL"]),
                    TR = Convert.ToInt32(rd["TR"]),
                    N1 = Convert.ToInt32(rd["N1"]),
                    D1 = Convert.ToInt32(rd["D1"]),
                    N2 = Convert.ToInt32(rd["N2"]),
                    D2 = Convert.ToInt32(rd["D2"]),
                    L2L = Convert.ToInt32(rd["L2L"]),
                    L2R = Convert.ToInt32(rd["L2R"]),
                    N3 = Convert.ToInt32(rd["N3"]),
                    D3 = Convert.ToInt32(rd["D3"]),
                    L3L = Convert.ToInt32(rd["L3L"]),
                    L3R = Convert.ToInt32(rd["L3R"]),
                    N4 = Convert.ToInt32(rd["N4"]),
                    D4 = Convert.ToInt32(rd["D4"]),
                    L4L = Convert.ToInt32(rd["L4L"]),
                    L4R = Convert.ToInt32(rd["L4R"]),
                    N5 = Convert.ToInt32(rd["N5"]),
                    D5 = Convert.ToInt32(rd["D5"]),
                    L5L = Convert.ToInt32(rd["L5L"]),
                    L5R = Convert.ToInt32(rd["L5R"]),
                    LN1 = Convert.ToInt32(rd["LN1"]),
                    LD1 = Convert.ToInt32(rd["LD1"]),
                    LN2 = Convert.ToInt32(rd["LN2"]),
                    LD2 = Convert.ToInt32(rd["LD2"]),
                    LL2 = Convert.ToInt32(rd["LL2"]),
                    LN3 = Convert.ToInt32(rd["LN3"]),
                    LD3 = Convert.ToInt32(rd["LD3"]),
                    LL3 = Convert.ToInt32(rd["LL3"]),
                    LN4 = Convert.ToInt32(rd["LN4"]),
                    LD4 = Convert.ToInt32(rd["LD4"]),
                    LL4 = Convert.ToInt32(rd["LL4"]),
                    LN5 = Convert.ToInt32(rd["LN5"]),
                    LD5 = Convert.ToInt32(rd["LD5"]),
                    LL5 = Convert.ToInt32(rd["LL5"]),
                    RN2 = Convert.ToInt32(rd["RN2"]),
                    RD2 = Convert.ToInt32(rd["RD2"]),
                    RL2 = Convert.ToInt32(rd["RL2"]),
                    RN3 = Convert.ToInt32(rd["RN3"]),
                    RD3 = Convert.ToInt32(rd["RD3"]),
                    RL3 = Convert.ToInt32(rd["RL3"]),
                    RN4 = Convert.ToInt32(rd["RN4"]),
                    RD4 = Convert.ToInt32(rd["RD4"]),
                    RL4 = Convert.ToInt32(rd["RL4"]),
                    RN5 = Convert.ToInt32(rd["RN5"]),
                    RD5 = Convert.ToInt32(rd["RD5"]),
                    RL5 = Convert.ToInt32(rd["RL5"]),
                    N11 = Convert.ToInt32(rd["N11"]),
                    D11 = Convert.ToInt32(rd["D11"]),
                    RS2 = Convert.ToInt32(rd["RS2"]),
                    Cot2 = Convert.ToChar(rd["Cot2"]),
                    Cot3 = Convert.ToChar(rd["Cot3"]),
                    Cot4 = Convert.ToChar(rd["Cot4"]),
                    Cot5 = Convert.ToChar(rd["Cot5"]),
                    Cob2 = Convert.ToChar(rd["Cob2"]),
                    Cob3 = Convert.ToChar(rd["Cob3"]),
                    Cob4 = Convert.ToChar(rd["Cob4"]),
                    Cob5 = Convert.ToChar(rd["Cob5"]),
                    Ds = Convert.ToInt32(rd["Ds"]),
                    Nso = Convert.ToInt32(rd["Nso"]),
                    Nsi = Convert.ToInt32(rd["Nsi"]),
                    Nslo = Convert.ToInt32(rd["Nslo"]),
                    Nsli = Convert.ToInt32(rd["Nsli"]),
                    Nsro = Convert.ToInt32(rd["Nsro"]),
                    Nsri = Convert.ToInt32(rd["Nsri"]),
                    Stln = Convert.ToInt32(rd["Stln"]),
                    Stls = Convert.ToInt32(rd["Stls"]),
                    Sts = Convert.ToInt32(rd["Sts"]),
                    Strn = Convert.ToInt32(rd["Strn"]),
                    Strs = Convert.ToInt32(rd["Strs"]),
                    MEMLIST = rd["MEMLIST"].ToString(),
                    M3X0 = float.Parse(rd["M3X0"].ToString()),
                    //TX0 = float.Parse(rd["TX0"].ToString()),
                    //SX0 = float.Parse(rd["SX0"].ToString()),
                    //V2XV0 = float.Parse(rd["V2XV0"].ToString()),
                    ////XV0 = float.Parse(rd["XV0"].ToString()),
                    //M3XM = float.Parse(rd["M3XM"].ToString()),
                    //TXM = float.Parse(rd["TXM"].ToString()),
                    //SXM = float.Parse(rd["SXM"].ToString()),
                    //M3X10 = float.Parse(rd["M3X10"].ToString()),
                    //TX10 = float.Parse(rd["TX10"].ToString()),
                    //SX10 = float.Parse(rd["SX10"].ToString()),
                    //V2XV10 = float.Parse(rd["V2XV10"].ToString()),
                    //XV10 = float.Parse(rd["XV10"].ToString())
                });
            }

            rd.Close();
            int lastRow = list1.Count;
            List<BeamDetails> beamDetails = new List<BeamDetails>();
            List<Single> rowUsed = new List<Single>();
            List<string> MarkUsed = new List<string>();
            int i = 0;
            for (int j = 0; j < lastRow; j++)
            {
                rowUsed.Add(j);
                MarkUsed.Add(list1[j].Mark);
                i++;
            }
            for (int n = 0; n < rowUsed.Count; n++)
            {
                beamDetails.Add(new BeamDetails() { BeamNo = MarkUsed[n] });
                beamDetails[n].Size = list1[n].B + "x" + list1[n].T;
                beamDetails[n].Layer1 = new Details();
                beamDetails[n].Layer2 = new Details();
                beamDetails[n].Layer3 = new Details();
                beamDetails[n].Layer4 = new Details();
                beamDetails[n].Layer5 = new Details();
                //Layer 1
                beamDetails[n].Layer1.LTB1 = list1[n].LN1 + "-" + list1[n].LD1;
                if (list1[n].N5 != 0)
                {
                    if (list1[n].Cob5 != 'T')
                    {
                        beamDetails[n].Layer1.BB1 = list1[n].L5L != 0 ? list1[n].L5L.ToString() : "-";
                        beamDetails[n].Layer1.BB2 = list1[n].N5 != 0 && list1[n].D5 != 0 ? list1[n].N5.ToString() + "-" + list1[n].D5.ToString() : "-";
                        beamDetails[n].Layer1.BB3 = list1[n].L5R != 0 ? list1[n].L5R.ToString() : "-";
                    }
                    else
                    {
                        beamDetails[n].Layer1.BB1 = "CONT";
                        beamDetails[n].Layer1.BB2 = list1[n].N5 != 0 && list1[n].D5 != 0 ? list1[n].N5.ToString() + "-" + list1[n].D5.ToString() : "-";
                        beamDetails[n].Layer1.BB3 = "CONT";
                    }
                }
                else
                {
                    beamDetails[n].Layer1.BB1 = "-";
                    beamDetails[n].Layer1.BB2 = "-";
                    beamDetails[n].Layer1.BB3 = "-";
                }
                beamDetails[n].Layer1.RTB2 = list1[n].LN1.ToString() + "-" + list1[n].LD1.ToString();

                //Layer 2
                if (list1[n].LN2 != 0)
                {
                    if (list1[n].Cot2 != 'T')
                    {
                        beamDetails[n].Layer2.LTB1 = list1[n].LN2 != 0 && list1[n].LD2 != 0 ? list1[n].LN2.ToString() + "-" + list1[n].LD2.ToString() : "-";
                        beamDetails[n].Layer2.LTB2 = list1[n].LL2 != 0 ? list1[n].LL2.ToString() : "-";
                    }
                    else
                    {
                        beamDetails[n].Layer2.LTB1 = list1[n].LN2 != 0 && list1[n].LD2 != 0 ? list1[n].LN2.ToString() + "-" + list1[n].LD2.ToString() : "-";
                        beamDetails[n].Layer2.LTB2 = "CONT";
                        beamDetails[n].Layer2.RTB1 = "CONT";
                        beamDetails[n].Layer2.RTB2 = list1[n].LN2 != 0 && list1[n].LD2 != 0 ? list1[n].LN2.ToString() + "-" + list1[n].LD2.ToString() : "-";
                    }
                }
                else
                {
                    beamDetails[n].Layer2.LTB1 = "-";
                    beamDetails[n].Layer2.LTB2 = "-";
                }

                if (list1[n].N4 != 0)
                {
                    if (list1[n].Cob4 != 'T')
                    {
                        beamDetails[n].Layer2.BB1 = list1[n].L4L != 0 ? list1[n].L4L.ToString() : "-";
                        beamDetails[n].Layer2.BB2 = list1[n].N4 != 0 && list1[n].D4 != 0 ? list1[n].N4.ToString() + "-" + list1[n].D4.ToString() : "-";
                        beamDetails[n].Layer2.BB3 = list1[n].L4R != 0 ? list1[n].L4R.ToString() : "-";
                    }
                    else
                    {
                        beamDetails[n].Layer2.BB1 = "CONT";
                        beamDetails[n].Layer2.BB2 = list1[n].N4 != 0 && list1[n].D4 != 0 ? list1[n].N4.ToString() + "-" + list1[n].D4.ToString() : "-";
                        beamDetails[n].Layer2.BB3 = "CONT";
                    }
                }
                else
                {
                    beamDetails[n].Layer2.BB1 = "-";
                    beamDetails[n].Layer2.BB2 = "-";
                    beamDetails[n].Layer2.BB3 = "-";
                }
                if (list1[n].Cot2 != 'T')
                {
                    if (list1[n].RN2 != 0)
                    {
                        beamDetails[n].Layer2.RTB1 = list1[n].RL2 != 0 ? list1[n].RL2.ToString() : "-"; ;
                        beamDetails[n].Layer2.RTB2 = list1[n].RN2 != 0 && list1[n].RD2 != 0 ? list1[n].RN2.ToString() + "-" + list1[n].RD2.ToString() : "-"; ;
                    }
                    else
                    {
                        beamDetails[n].Layer2.RTB1 = "-"; ;
                        beamDetails[n].Layer2.RTB2 = "-"; ;
                    }
                }

                //Layer 3
                if (list1[n].LN3 != 0)
                {
                    if (list1[n].Cot3 != 'T')
                    {
                        beamDetails[n].Layer3.LTB1 = list1[n].LN3 != 0 && list1[n].LD3 != 0 ? list1[n].LN3.ToString() + "-" + list1[n].LD3.ToString() : "-";
                        beamDetails[n].Layer3.LTB2 = list1[n].LL3 != 0 ? list1[n].LL3.ToString() : "-";
                    }
                    else
                    {
                        beamDetails[n].Layer3.LTB1 = list1[n].LN3 != 0 && list1[n].LD3 != 0 ? list1[n].LN3.ToString() + "-" + list1[n].LD3.ToString() : "-";
                        beamDetails[n].Layer3.LTB2 = "CONT";
                        beamDetails[n].Layer3.RTB1 = "CONT";
                        beamDetails[n].Layer3.RTB2 = list1[n].LN3 != 0 && list1[n].LD3 != 0 ? list1[n].LN3.ToString() + "-" + list1[n].LD3.ToString() : "-";
                    }
                }
                else
                {
                    beamDetails[n].Layer3.LTB1 = "-";
                    beamDetails[n].Layer3.LTB2 = "-";
                }

                if (list1[n].N3 != 0)
                {
                    if (list1[n].Cob3 != 'T')
                    {
                        beamDetails[n].Layer3.BB1 = list1[n].L3L != 0 ? list1[n].L3L.ToString() : "-";
                        beamDetails[n].Layer3.BB2 = list1[n].N3 != 0 && list1[n].D3 != 0 ? list1[n].N3.ToString() + "-" + list1[n].D3.ToString() : "-";
                        beamDetails[n].Layer3.BB3 = list1[n].L3R != 0 ? list1[n].L3R.ToString() : "-";
                    }
                    else
                    {
                        beamDetails[n].Layer3.BB1 = "CONT";
                        beamDetails[n].Layer3.BB2 = list1[n].N3 != 0 && list1[n].D3 != 0 ? list1[n].N3.ToString() + "-" + list1[n].D3.ToString() : "-";
                        beamDetails[n].Layer3.BB3 = "CONT";
                    }
                }
                else
                {
                    beamDetails[n].Layer3.BB1 = "-";
                    beamDetails[n].Layer3.BB2 = "-";
                    beamDetails[n].Layer3.BB3 = "-";
                }
                if (list1[n].Cot3 != 'T')
                {
                    if (list1[n].RN3 != 0)
                    {
                        beamDetails[n].Layer3.RTB1 = list1[n].RL3 != 0 ? list1[n].RL3.ToString() : "-"; ;
                        beamDetails[n].Layer3.RTB2 = list1[n].RN3 != 0 && list1[n].RD3 != 0 ? list1[n].RN3.ToString() + "-" + list1[n].RD3.ToString() : "-"; ;
                    }
                    else
                    {
                        beamDetails[n].Layer3.RTB1 = "-"; ;
                        beamDetails[n].Layer3.RTB2 = "-"; ;
                    }
                }


                //Layer 4
                if (list1[n].LN4 != 0)
                {
                    if (list1[n].Cot4 != 'T')
                    {
                        beamDetails[n].Layer4.LTB1 = list1[n].LN4 != 0 && list1[n].LD4 != 0 ? list1[n].LN4.ToString() + "-" + list1[n].LD4.ToString() : "-";
                        beamDetails[n].Layer4.LTB2 = list1[n].LL4 != 0 ? list1[n].LL4.ToString() : "-";
                    }
                    else
                    {
                        beamDetails[n].Layer4.LTB1 = list1[n].LN4 != 0 && list1[n].LD4 != 0 ? list1[n].LN4.ToString() + "-" + list1[n].LD4.ToString() : "-";
                        beamDetails[n].Layer4.LTB2 = "CONT";
                        beamDetails[n].Layer4.RTB1 = "CONT";
                        beamDetails[n].Layer4.RTB2 = list1[n].LN4 != 0 && list1[n].LD4 != 0 ? list1[n].LN4.ToString() + "-" + list1[n].LD4.ToString() : "-";
                    }
                }
                else
                {
                    beamDetails[n].Layer4.LTB1 = "-";
                    beamDetails[n].Layer4.LTB2 = "-";
                }

                if (list1[n].N4 != 0)
                {
                    if (list1[n].Cob4 != 'T')
                    {
                        beamDetails[n].Layer4.BB1 = list1[n].L4L != 0 ? list1[n].L4L.ToString() : "-";
                        beamDetails[n].Layer4.BB2 = list1[n].N4 != 0 && list1[n].D4 != 0 ? list1[n].N4.ToString() + "-" + list1[n].D4.ToString() : "-";
                        beamDetails[n].Layer4.BB3 = list1[n].L4R != 0 ? list1[n].L4R.ToString() : "-";
                    }
                    else
                    {
                        beamDetails[n].Layer4.BB1 = "CONT";
                        beamDetails[n].Layer4.BB2 = list1[n].N4 != 0 && list1[n].D4 != 0 ? list1[n].N4.ToString() + "-" + list1[n].D4.ToString() : "-";
                        beamDetails[n].Layer4.BB3 = "CONT";
                    }
                }
                else
                {
                    beamDetails[n].Layer4.BB1 = "-";
                    beamDetails[n].Layer4.BB2 = "-";
                    beamDetails[n].Layer4.BB3 = "-";
                }
                if (list1[n].Cot4 != 'T')
                {
                    if (list1[n].RN4 != 0)
                    {
                        beamDetails[n].Layer4.RTB1 = list1[n].RL4 != 0 ? list1[n].RL4.ToString() : "-"; ;
                        beamDetails[n].Layer4.RTB2 = list1[n].RN4 != 0 && list1[n].RD4 != 0 ? list1[n].RN4.ToString() + "-" + list1[n].RD4.ToString() : "-"; ;
                    }
                    else
                    {
                        beamDetails[n].Layer4.RTB1 = "-"; ;
                        beamDetails[n].Layer4.RTB2 = "-"; ;
                    }
                }

                //Layer 5
                if (list1[n].LN5 != 0)
                {
                    if (list1[n].Cot5 != 'T')
                    {
                        beamDetails[n].Layer5.LTB1 = list1[n].LN5 != 0 && list1[n].LD5 != 0 ? list1[n].LN5.ToString() + "-" + list1[n].LD5.ToString() : "-";
                        beamDetails[n].Layer5.LTB2 = list1[n].LL5 != 0 ? list1[n].LL5.ToString() : "-";
                    }
                    else
                    {
                        beamDetails[n].Layer5.LTB1 = list1[n].LN5 != 0 && list1[n].LD5 != 0 ? list1[n].LN5.ToString() + "-" + list1[n].LD5.ToString() : "-";
                        beamDetails[n].Layer5.LTB2 = "CONT";
                        beamDetails[n].Layer5.RTB1 = "CONT";
                        beamDetails[n].Layer5.RTB2 = list1[n].LN5 != 0 && list1[n].LD5 != 0 ? list1[n].LN5.ToString() + "-" + list1[n].LD5.ToString() : "-";
                    }
                }
                else
                {
                    beamDetails[n].Layer5.LTB1 = "-";
                    beamDetails[n].Layer5.LTB2 = "-";
                }

                if (list1[n].N5 != 0)
                {
                    if (list1[n].Cob5 != 'T')
                    {

                        beamDetails[n].Layer5.BB2 = list1[n].N5 != 0 && list1[n].D5 != 0 ? list1[n].N5.ToString() + "-" + list1[n].D5.ToString() : "-";
                    }
                    else
                    {

                        beamDetails[n].Layer5.BB2 = list1[n].N5 != 0 && list1[n].D5 != 0 ? list1[n].N5.ToString() + "-" + list1[n].D5.ToString() : "-";
                    }
                }
                else
                {

                    beamDetails[n].Layer5.BB2 = "-";
                }
                if (list1[n].Cot5 != 'T')
                {
                    if (list1[n].RN5 != 0)
                    {
                        beamDetails[n].Layer5.RTB1 = list1[n].RL5 != 0 ? list1[n].RL5.ToString() : "-"; ;
                        beamDetails[n].Layer5.RTB2 = list1[n].RN5 != 0 && list1[n].RD5 != 0 ? list1[n].RN5.ToString() + "-" + list1[n].RD5.ToString() : "-";
                    }
                    else
                    {
                        beamDetails[n].Layer5.RTB1 = "-"; ;
                        beamDetails[n].Layer5.RTB2 = "-"; ;
                    }
                }

                //Torsion reinforcement
                if (list1[n].N11 != 0)
                {
                    beamDetails[n].TorReinf = list1[n].N11 != 0 && list1[n].D11 != 0 ? list1[n].N11.ToString() + "-" + list1[n].D11.ToString() : "-";
                }
                else
                {
                    beamDetails[n].TorReinf = "-";
                }
                string rebar = "";
                //Stirrup Reinforcement
                switch (list1[n].Ds)
                {
                    case 6: case 9: rebar = "RB"; break;
                    case 12: rebar = "DB"; break;
                }
                if (list1[n].Nsi == 0)
                {
                    beamDetails[n].Stirrup2 = list1[n].Nso.ToString() + "-" + rebar + list1[n].Ds.ToString() + "@" + list1[n].Sts.ToString();
                }
                else
                {
                    beamDetails[n].Stirrup2 = "(" + list1[n].Nso.ToString() + "+" + list1[n].Nsi.ToString() + ")-" + rebar + list1[n].Ds.ToString() + "@" + list1[n].Sts.ToString();
                }
                if (list1[n].Nslo != 0)
                {
                    if (list1[n].Nsli == 0)
                    {
                        beamDetails[n].Stirrup1 = list1[n].Nslo.ToString() + "-" + list1[n].Stln.ToString() + rebar + list1[n].Ds.ToString() + "@" + list1[n].Stls.ToString();
                    }
                    else
                    {
                        beamDetails[n].Stirrup1 = "(" + list1[n].Nslo.ToString() + list1[n].Nsli.ToString() + ")-" + list1[n].Stln.ToString() + rebar + list1[n].Ds.ToString() + "@" + list1[n].Stls.ToString();
                    }
                }
                else
                {
                    beamDetails[n].Stirrup1 = "-";
                }
                if (list1[n].Nsro != 0)
                {
                    if (list1[n].Nsri == 0)
                    {
                        beamDetails[n].Stirrup3 = list1[n].Nsro.ToString() + "-" + list1[n].Strn.ToString() + rebar + list1[n].Ds.ToString() + "@" + list1[n].Strs.ToString();
                    }
                    else
                    {
                        beamDetails[n].Stirrup3 = list1[n].Nsro.ToString() + list1[n].Nsri.ToString() + ")-" + list1[n].Strn.ToString() + rebar + list1[n].Ds.ToString() + "@" + list1[n].Strs.ToString();
                    }
                }
                else
                {
                    beamDetails[n].Stirrup3 = "-";
                }

            }
            ViewBag.beamDetails = beamDetails;
            Session["beamDetails"] = beamDetails;
            return View();
        }
        public void ExportBeamDetailsToExcel()
        {
            List<BeamDetails> beam = Session["beamDetails"] as List<BeamDetails>;
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("BeamDetails");
            ws.Cells["A:AZ"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Cells["A:AZ"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            using (ExcelRange range = ws.Cells[1, 1, 1, 9])
            {
                range.Merge = true;
                ws.Cells[1, 1].Style.Font.Size = 12;
                ws.Cells[1, 1].Style.Font.Bold = true;
                range.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                ws.Cells[1, 1].Value = "BEAM REINFORCEMENT DETAILS";
                range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                range.Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            }

            using (ExcelRange range = ws.Cells[1, 10])
            {
                range.Merge = true;
                ws.Cells[1, 10].Style.Font.Bold = true;
                ws.Cells[1, 10].Style.Font.Size = 13;
                range.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                ws.Cells[1, 10].Value = "PROJECT";
                range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                range.Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            }

            using (ExcelRange range = ws.Cells[1, 12])
            {
                range.Merge = true;
                ws.Cells[1, 12].Style.Font.Bold = true;
                ws.Cells[1, 12].Style.Font.Size = 13;
                range.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                ws.Cells[1, 12].Value = "BLDG.";
                range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                range.Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            }
            using (ExcelRange range = ws.Cells[1, 13])
            {
                range.Merge = true;
                range.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                range.Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            }






            using (ExcelRange range = ws.Cells[2, 1])
            {
                ws.Cells[2, 1].Style.Font.Bold = true;
                range.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                ws.Cells[2, 1].Value = "Beam No.";
                range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                range.Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            }

            using (ExcelRange range = ws.Cells[2, 2])
            {
                range.Style.Font.Bold = true;
                range.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                range.Value = "Size";
                range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                range.Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            }
            using (ExcelRange range = ws.Cells[2, 3, 2, 4])
            {
                range.Merge = true;
                range.Style.Font.Bold = true;
                range.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                ws.Cells[2, 3].Value = "Left Top Bars";
                range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                range.Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            }
            using (ExcelRange range = ws.Cells[2, 5, 2, 7])
            {
                range.Merge = true;
                range.Style.Font.Bold = true;
                range.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                ws.Cells[2, 5].Value = "Bottom Bars";
                range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                range.Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            }
            using (ExcelRange range = ws.Cells[2, 8, 2, 9])
            {
                range.Merge = true;
                range.Style.Font.Bold = true;
                range.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                ws.Cells[2, 8].Value = "Right Top Bars";
                range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                range.Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            }
            using (ExcelRange range = ws.Cells[2, 10, 2, 12])
            {
                range.Merge = true;
                range.Style.Font.Bold = true;
                range.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                ws.Cells[2, 10].Value = "Stirrup";
                range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                range.Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            }

            using (ExcelRange range = ws.Cells[2, 13])
            {
                range.Style.Font.Bold = true;
                range.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                ws.Cells[2, 13].Value = "Tor. Reinf";
                range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                range.Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            }


            for (int n = 0; n < beam.Count; n++)
            {
                ws.Cells[6 * n + 4, 1].Value = beam[n].BeamNo;
                ws.Cells[6 * n + 4, 2].Value = beam[n].Size;
                ws.Cells[6 * n + 4, 3].Value = beam[n].Layer1.LTB1;
                ws.Cells[6 * n + 4, 5].Value = beam[n].Layer1.BB1;
                ws.Cells[6 * n + 4, 6].Value = beam[n].Layer1.BB2;
                ws.Cells[6 * n + 4, 7].Value = beam[n].Layer1.BB3;
                ws.Cells[6 * n + 4, 9].Value = beam[n].Layer1.RTB2;
                ws.Cells[6 * n + 4, 10].Value = beam[n].Stirrup1;
                ws.Cells[6 * n + 4, 11].Value = beam[n].Stirrup2;
                ws.Cells[6 * n + 4, 12].Value = beam[n].Stirrup3;
                ws.Cells[6 * n + 4, 13].Value = beam[n].TorReinf;


                ws.Cells[6 * n + 5, 3].Value = beam[n].Layer2.LTB1;
                ws.Cells[6 * n + 5, 4].Value = beam[n].Layer2.LTB2;
                ws.Cells[6 * n + 5, 5].Value = beam[n].Layer2.BB1;
                ws.Cells[6 * n + 5, 6].Value = beam[n].Layer2.BB2;
                ws.Cells[6 * n + 5, 7].Value = beam[n].Layer2.BB3;
                ws.Cells[6 * n + 5, 8].Value = beam[n].Layer2.RTB1;
                ws.Cells[6 * n + 5, 9].Value = beam[n].Layer2.RTB2;

                ws.Cells[6 * n + 6, 3].Value = beam[n].Layer3.LTB1;
                ws.Cells[6 * n + 6, 4].Value = beam[n].Layer3.LTB2;
                ws.Cells[6 * n + 6, 5].Value = beam[n].Layer3.BB1;
                ws.Cells[6 * n + 6, 6].Value = beam[n].Layer3.BB2;
                ws.Cells[6 * n + 6, 7].Value = beam[n].Layer3.BB3;
                ws.Cells[6 * n + 6, 8].Value = beam[n].Layer3.RTB1;
                ws.Cells[6 * n + 6, 9].Value = beam[n].Layer3.RTB2;

                ws.Cells[6 * n + 7, 3].Value = beam[n].Layer4.LTB1;
                ws.Cells[6 * n + 7, 4].Value = beam[n].Layer4.LTB2;
                ws.Cells[6 * n + 7, 5].Value = beam[n].Layer4.BB1;
                ws.Cells[6 * n + 7, 6].Value = beam[n].Layer4.BB2;
                ws.Cells[6 * n + 7, 7].Value = beam[n].Layer4.BB3;
                ws.Cells[6 * n + 7, 8].Value = beam[n].Layer4.RTB1;
                ws.Cells[6 * n + 7, 9].Value = beam[n].Layer4.RTB2;

                ws.Cells[6 * n + 8, 3].Value = beam[n].Layer5.LTB1;
                ws.Cells[6 * n + 8, 4].Value = beam[n].Layer5.LTB2;
                ws.Cells[6 * n + 8, 6].Value = beam[n].Layer5.BB2;
                ws.Cells[6 * n + 8, 8].Value = beam[n].Layer5.RTB1;
                ws.Cells[6 * n + 8, 9].Value = beam[n].Layer5.RTB2;

                ws.Cells[6 * n + 8, 1, 6 * n + 8, 13].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            }


            ws.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename" + "BeamDetails.xlsx");
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }

        public void fillSheet(Single[,] Results, int rid, bool chkDesignAll, decimal utf)
        {
            List<Stress> St = new List<Stress>();
            int aid = Convert.ToInt32(Session["aid"]);

            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            String stque = "select * from stress where accessFileID = " + aid + " Order By [Case], [Station]";
            SqlCommand sqlcomst = new SqlCommand(stque, sqlconn);
            SqlDataReader cf = sqlcomst.ExecuteReader();
            while (cf.Read())
            {
                St.Add(new Stress
                {
                    Case = cf["Case"].ToString(),
                });
            }
            sqlconn.Close();

            sqlconn.Open();
            String fsquery = "UPDATE RCBeam SET SX0 = @SX0 , BX0 = @BX0 , AX0 = @AX0 , CaseX0 = @CaseX0 , M3X0 = @M3X0 , V2X0 = @V2X0 , TX0 = @TX0 , PX0 = @PX0 , SX1 = @SX1 , StaX1 = @StaX1 , BX1 = @BX1 , AX1 = @AX1 , CaseX1 = @CaseX1 , M3X1 = @M3X1 , V2X1 = @V2X1 , TX1 = @TX1 , PX1 = @PX1 , SX2 = @SX2 , StaX2 = @StaX2 , BX2 = @BX2 , AX2 = @AX2 , CaseX2 = @CaseX2 , M3X2 = @M3X2 , V2X2 = @V2X2 , TX2 = @TX2 , PX2 = @PX2 , SXM = @SXM , BXM = @BXM , AXM = @AXM , CaseXM = @CaseXM , M3XM = @M3XM , V2XM = @V2XM , TXM = @TXM , PXM = @PXM , SX8 = @SX8 , StaX8 = @StaX8 , BX8 = @BX8 , AX8 = @AX8 , CaseX8 = @CaseX8 , M3X8 = @M3X8 , V2X8 = @V2X8 , TX8 = @TX8 , PX8 = @PX8 , SX9 = @SX9 , StaX9 = @StaX9 , BX9 = @BX9 , AX9 = @AX9 , CaseX9 = @CaseX9 , M3X9 = @M3X9 , V2X9 = @V2X9 , TX9 = @TX9 , PX9 = @PX9 , SX10 = @SX10 , BX10 = @BX10 , AX10 = @AX10 , CaseX10 = @CaseX10 , M3X10 = @M3X10 , V2X10 = @V2X10 , TX10 = @TX10 , PX10 = @PX10 , XV0 = @XV0 , XT0 = @XT0 , XVc0 = @XVc0 , CaseXV0 = @CaseXV0 , M3XV0 = @M3XV0 , V2XV0 = @V2XV0 , TXV0 = @TXV0 , PXV0 = @PXV0 , XV1 = @XV1 , XT1 = @XT1 , XVc1 = @XVc1 , CaseXV1 = @CaseXV1 , M3XV1 = @M3XV1 , V2XV1 = @V2XV1 , TXV1 = @TXV1 , PXV1 = @PXV1 , XV9 = @XV9 , XT9 = @XT9 , XVc9 = @XVc9 , CaseXV9 = @CaseXV9 , M3XV9 = @M3XV9 , V2XV9 = @V2XV9 , TXV9 = @TXV9 , PXV9 = @PXV9 , XV10 = @XV10 , XT10 = @XT10 , XVc10 = @XVc10 , CaseXV10 = @CaseXV10 , M3XV10 = @M3XV10 , V2XV10 = @V2XV10 , TXV10 = @TXV10 , PXV10 = @PXV10 , SXMax = @SXMax , BXMax = @BXMax , AXMax = @AXMax , SMaxSta = @SMaxSta , M3XMax = @M3XMax , V2XMax = @V2XMax , TXMax = @TXMax , PXMax = @PXMax , XVMax = @XVMax , XTMax = @XTMax , XVcMax = @XVcMax , VMaxSta = @VMaxSta , M3XVMax = @M3XVMax , V2XVMax = @V2XVMax ,TXVMax = @TXVMax ,PXVMax = @PXVMax ,BL = @BL ,BR = @BR ,AsMin = @AsMin where Id = " + rid;
            SqlCommand fscommand = new SqlCommand(fsquery, sqlconn);
            //X0
            int cell = Convert.ToInt32(Results[0, 10]);
            fscommand.Parameters.AddWithValue("@SX0", Results[0, 0]);
            fscommand.Parameters.AddWithValue("@BX0", Results[0, 1]);
            fscommand.Parameters.AddWithValue("@AX0", Results[0, 2]);
            fscommand.Parameters.AddWithValue("@CaseX0", St[cell].Case);
            fscommand.Parameters.AddWithValue("@M3X0", Results[0, 5]);
            fscommand.Parameters.AddWithValue("@V2X0", Results[0, 6]);
            fscommand.Parameters.AddWithValue("@TX0", Results[0, 7]);
            fscommand.Parameters.AddWithValue("@PX0", Results[0, 8]);
            //X1
            if (Results[1, 10] != 0)
            {
                int cell1 = Convert.ToInt32(Results[1, 10]);
                fscommand.Parameters.AddWithValue("@SX1", Results[1, 0]);
                switch (Results[1, 9])
                {
                    case 1: fscommand.Parameters.AddWithValue("@StaX1", "X1A"); break;
                    case 2: fscommand.Parameters.AddWithValue("@StaX1", "X1B"); break;
                    case 3: fscommand.Parameters.AddWithValue("@StaX1", "X1C"); break;
                    case 4: fscommand.Parameters.AddWithValue("@StaX1", "X1D"); break;
                }
                fscommand.Parameters.AddWithValue("@BX1", Results[1, 1]);
                fscommand.Parameters.AddWithValue("@AX1", Results[1, 2]);
                fscommand.Parameters.AddWithValue("@CaseX1", St[cell1].Case);
                fscommand.Parameters.AddWithValue("@M3X1", Results[1, 5]);
                fscommand.Parameters.AddWithValue("@V2X1", Results[1, 6]);
                fscommand.Parameters.AddWithValue("@TX1", Results[1, 7]);
                fscommand.Parameters.AddWithValue("@PX1", Results[1, 8]);
            }
            else
            {
                fscommand.Parameters.AddWithValue("@SX1", DBNull.Value);
                fscommand.Parameters.AddWithValue("@StaX1", "");
                fscommand.Parameters.AddWithValue("@BX1", DBNull.Value);
                fscommand.Parameters.AddWithValue("@AX1", DBNull.Value);
                fscommand.Parameters.AddWithValue("@CaseX1", "");
                fscommand.Parameters.AddWithValue("@M3X1", DBNull.Value);
                fscommand.Parameters.AddWithValue("@V2X1", DBNull.Value);
                fscommand.Parameters.AddWithValue("@TX1", DBNull.Value);
                fscommand.Parameters.AddWithValue("@PX1", DBNull.Value);
            }
            //X2
            if (Results[2, 10] != 0)
            {
                int cell2 = Convert.ToInt32(Results[2, 10]);
                fscommand.Parameters.AddWithValue("@SX2", Results[2, 0]);
                switch (Results[2, 9])
                {
                    case 1: fscommand.Parameters.AddWithValue("@StaX2", "X2A"); break;
                    case 2: fscommand.Parameters.AddWithValue("@StaX2", "X2B"); break;
                    case 3: fscommand.Parameters.AddWithValue("@StaX2", "X2C"); break;
                    case 4: fscommand.Parameters.AddWithValue("@StaX2", "X2D"); break;
                }
                fscommand.Parameters.AddWithValue("@BX2", Results[2, 1]);
                fscommand.Parameters.AddWithValue("@AX2", Results[2, 2]);
                fscommand.Parameters.AddWithValue("@CaseX2", St[cell2].Case);
                fscommand.Parameters.AddWithValue("@M3X2", Results[2, 5]);
                fscommand.Parameters.AddWithValue("@V2X2", Results[2, 6]);
                fscommand.Parameters.AddWithValue("@TX2", Results[2, 7]);
                fscommand.Parameters.AddWithValue("@PX2", Results[2, 8]);
            }
            else
            {
                fscommand.Parameters.AddWithValue("@SX2", DBNull.Value);
                fscommand.Parameters.AddWithValue("@StaX2", "");
                fscommand.Parameters.AddWithValue("@BX2", DBNull.Value);
                fscommand.Parameters.AddWithValue("@AX2", DBNull.Value);
                fscommand.Parameters.AddWithValue("@CaseX2", "");
                fscommand.Parameters.AddWithValue("@M3X2", DBNull.Value);
                fscommand.Parameters.AddWithValue("@V2X2", DBNull.Value);
                fscommand.Parameters.AddWithValue("@TX2", DBNull.Value);
                fscommand.Parameters.AddWithValue("@PX2", DBNull.Value);
            }
            //XM
            if (Results[3, 10] != 0)
            {
                int cell3 = Convert.ToInt32(Results[3, 10]);
                fscommand.Parameters.AddWithValue("@SXM", Results[3, 0]);
                fscommand.Parameters.AddWithValue("@BXM", Results[3, 1]);
                fscommand.Parameters.AddWithValue("@AXM", Results[3, 2]);
                fscommand.Parameters.AddWithValue("@CaseXM", St[cell3].Case);
                fscommand.Parameters.AddWithValue("@M3XM", Results[3, 5]);
                fscommand.Parameters.AddWithValue("@V2XM", Results[3, 6]);
                fscommand.Parameters.AddWithValue("@TXM", Results[3, 7]);
                fscommand.Parameters.AddWithValue("@PXM", Results[3, 8]);
            }
            else
            {
                fscommand.Parameters.AddWithValue("@SXM", DBNull.Value);
                fscommand.Parameters.AddWithValue("@BXM", DBNull.Value);
                fscommand.Parameters.AddWithValue("@AXM", DBNull.Value);
                fscommand.Parameters.AddWithValue("@CaseXM", "");
                fscommand.Parameters.AddWithValue("@M3XM", DBNull.Value);
                fscommand.Parameters.AddWithValue("@V2XM", DBNull.Value);
                fscommand.Parameters.AddWithValue("@TXM", DBNull.Value);
                fscommand.Parameters.AddWithValue("@PXM", DBNull.Value);
            }
            //X8
            if (Results[4, 10] != 0)
            {
                int cell4 = Convert.ToInt32(Results[4, 10]);
                fscommand.Parameters.AddWithValue("@SX8", Results[4, 0]);
                switch (Results[4, 9])
                {
                    case 1: fscommand.Parameters.AddWithValue("@StaX8", "X8A"); break;
                    case 2: fscommand.Parameters.AddWithValue("@StaX8", "X8B"); break;
                    case 3: fscommand.Parameters.AddWithValue("@StaX8", "X8C"); break;
                    case 4: fscommand.Parameters.AddWithValue("@StaX8", "X8D"); break;
                }
                fscommand.Parameters.AddWithValue("@BX8", Results[4, 1]);
                fscommand.Parameters.AddWithValue("@AX8", Results[4, 2]);
                fscommand.Parameters.AddWithValue("@CaseX8", St[cell4].Case);
                fscommand.Parameters.AddWithValue("@M3X8", Results[4, 5]);
                fscommand.Parameters.AddWithValue("@V2X8", Results[4, 6]);
                fscommand.Parameters.AddWithValue("@TX8", Results[4, 7]);
                fscommand.Parameters.AddWithValue("@PX8", Results[4, 8]);
            }
            else
            {
                fscommand.Parameters.AddWithValue("@SX8", DBNull.Value);
                fscommand.Parameters.AddWithValue("@StaX8", "");
                fscommand.Parameters.AddWithValue("@BX8", DBNull.Value);
                fscommand.Parameters.AddWithValue("@AX8", DBNull.Value);
                fscommand.Parameters.AddWithValue("@CaseX8", "");
                fscommand.Parameters.AddWithValue("@M3X8", DBNull.Value);
                fscommand.Parameters.AddWithValue("@V2X8", DBNull.Value);
                fscommand.Parameters.AddWithValue("@TX8", DBNull.Value);
                fscommand.Parameters.AddWithValue("@PX8", DBNull.Value);
            }
            //X9
            if (Results[5, 10] != 0)
            {
                int cell5 = Convert.ToInt32(Results[5, 10]);
                fscommand.Parameters.AddWithValue("@SX9", Results[5, 0]);
                switch (Results[5, 9])
                {
                    case 1: fscommand.Parameters.AddWithValue("@StaX9", "X9A"); break;
                    case 2: fscommand.Parameters.AddWithValue("@StaX9", "X9B"); break;
                    case 3: fscommand.Parameters.AddWithValue("@StaX9", "X9C"); break;
                    case 4: fscommand.Parameters.AddWithValue("@StaX9", "X9D"); break;
                }
                fscommand.Parameters.AddWithValue("@BX9", Results[5, 1]);
                fscommand.Parameters.AddWithValue("@AX9", Results[5, 2]);
                fscommand.Parameters.AddWithValue("@CaseX9", St[cell5].Case);
                fscommand.Parameters.AddWithValue("@M3X9", Results[5, 5]);
                fscommand.Parameters.AddWithValue("@V2X9", Results[5, 6]);
                fscommand.Parameters.AddWithValue("@TX9", Results[5, 7]);
                fscommand.Parameters.AddWithValue("@PX9", Results[5, 8]);
            }
            else
            {
                fscommand.Parameters.AddWithValue("@SX9", DBNull.Value);
                fscommand.Parameters.AddWithValue("@StaX9", "");
                fscommand.Parameters.AddWithValue("@BX9", DBNull.Value);
                fscommand.Parameters.AddWithValue("@AX9", DBNull.Value);
                fscommand.Parameters.AddWithValue("@CaseX9", "");
                fscommand.Parameters.AddWithValue("@M3X9", DBNull.Value);
                fscommand.Parameters.AddWithValue("@V2X9", DBNull.Value);
                fscommand.Parameters.AddWithValue("@TX9", DBNull.Value);
                fscommand.Parameters.AddWithValue("@PX9", DBNull.Value);
            }
            //X10
            int cell6 = Convert.ToInt32(Results[6, 10]);
            fscommand.Parameters.AddWithValue("@SX10", Results[6, 0]);
            fscommand.Parameters.AddWithValue("@BX10", Results[6, 1]);
            fscommand.Parameters.AddWithValue("@AX10", Results[6, 2]);
            fscommand.Parameters.AddWithValue("@CaseX10", St[cell6].Case);
            fscommand.Parameters.AddWithValue("@M3X10", Results[6, 5]);
            fscommand.Parameters.AddWithValue("@V2X10", Results[6, 6]);
            fscommand.Parameters.AddWithValue("@TX10", Results[6, 7]);
            fscommand.Parameters.AddWithValue("@PX10", Results[6, 8]);

            //XV0
            int cell7 = Convert.ToInt32(Results[7, 10]);
            fscommand.Parameters.AddWithValue("@XV0", Results[7, 4]);
            fscommand.Parameters.AddWithValue("@XT0", Results[7, 3]);
            if (Results[7, 9] != 0)
            {
                fscommand.Parameters.AddWithValue("@XVc0", Results[7, 9]);
            }
            else
            {
                fscommand.Parameters.AddWithValue("@XVc0", DBNull.Value);
            }
            fscommand.Parameters.AddWithValue("@CaseXV0", St[cell7].Case);
            fscommand.Parameters.AddWithValue("@M3XV0", Results[7, 5]);
            fscommand.Parameters.AddWithValue("@V2XV0", Results[7, 6]);
            fscommand.Parameters.AddWithValue("@TXV0", Results[7, 7]);
            fscommand.Parameters.AddWithValue("@PXV0", Results[7, 8]);
            //XV1
            if (Results[8, 10] != 0)
            {
                int cell8 = Convert.ToInt32(Results[8, 10]);
                fscommand.Parameters.AddWithValue("@XV1", Results[8, 4]);
                fscommand.Parameters.AddWithValue("@XT1", Results[8, 3]);
                if (Results[8, 9] != 0)
                {
                    fscommand.Parameters.AddWithValue("@XVc1", Results[8, 9]);
                }
                else
                {
                    fscommand.Parameters.AddWithValue("@XVc1", DBNull.Value);
                }
                fscommand.Parameters.AddWithValue("@CaseXV1", St[cell8].Case);
                fscommand.Parameters.AddWithValue("@M3XV1", Results[8, 5]);
                fscommand.Parameters.AddWithValue("@V2XV1", Results[8, 6]);
                fscommand.Parameters.AddWithValue("@TXV1", Results[8, 7]);
                fscommand.Parameters.AddWithValue("@PXV1", Results[8, 8]);
            }
            else
            {
                fscommand.Parameters.AddWithValue("@XV1", DBNull.Value);
                fscommand.Parameters.AddWithValue("@XT1", DBNull.Value);
                fscommand.Parameters.AddWithValue("@XVc1", DBNull.Value);
                fscommand.Parameters.AddWithValue("@CaseXV1", "");
                fscommand.Parameters.AddWithValue("@M3XV1", DBNull.Value);
                fscommand.Parameters.AddWithValue("@V2XV1", DBNull.Value);
                fscommand.Parameters.AddWithValue("@TXV1", DBNull.Value);
                fscommand.Parameters.AddWithValue("@PXV1", DBNull.Value);
            }
            //XV9
            if (Results[9, 10] != 0)
            {
                int cell9 = Convert.ToInt32(Results[9, 10]);
                fscommand.Parameters.AddWithValue("@XV9", Results[9, 4]);
                fscommand.Parameters.AddWithValue("@XT9", Results[9, 3]);
                if (Results[9, 9] != 0)
                {
                    fscommand.Parameters.AddWithValue("@XVc9", Results[9, 9]);
                }
                else
                {
                    fscommand.Parameters.AddWithValue("@XVc9", DBNull.Value);
                }
                fscommand.Parameters.AddWithValue("@CaseXV9", St[cell9].Case);
                fscommand.Parameters.AddWithValue("@M3XV9", Results[9, 5]);
                fscommand.Parameters.AddWithValue("@V2XV9", Results[9, 6]);
                fscommand.Parameters.AddWithValue("@TXV9", Results[9, 7]);
                fscommand.Parameters.AddWithValue("@PXV9", Results[9, 8]);
            }
            else
            {
                fscommand.Parameters.AddWithValue("@XV9", DBNull.Value);
                fscommand.Parameters.AddWithValue("@XT9", DBNull.Value);
                fscommand.Parameters.AddWithValue("@XVc9", DBNull.Value);
                fscommand.Parameters.AddWithValue("@CaseXV9", "");
                fscommand.Parameters.AddWithValue("@M3XV9", DBNull.Value);
                fscommand.Parameters.AddWithValue("@V2XV9", DBNull.Value);
                fscommand.Parameters.AddWithValue("@TXV9", DBNull.Value);
                fscommand.Parameters.AddWithValue("@PXV9", DBNull.Value);
            }
            //XV10
            int cell10 = Convert.ToInt32(Results[10, 10]);
            fscommand.Parameters.AddWithValue("@XV10", Results[10, 4]);
            fscommand.Parameters.AddWithValue("@XT10", Results[10, 3]);
            if (Results[10, 9] != 0)
            {
                fscommand.Parameters.AddWithValue("@XVc10", Results[10, 9]);
            }
            else
            {
                fscommand.Parameters.AddWithValue("@XVc10", DBNull.Value);
            }
            fscommand.Parameters.AddWithValue("@CaseXV10", St[cell10].Case);
            fscommand.Parameters.AddWithValue("@M3XV10", Results[10, 5]);
            fscommand.Parameters.AddWithValue("@V2XV10", Results[10, 6]);
            fscommand.Parameters.AddWithValue("@TXV10", Results[10, 7]);
            fscommand.Parameters.AddWithValue("@PXV10", Results[10, 8]);
            //Max
            if (chkDesignAll == true)
            {
                fscommand.Parameters.AddWithValue("@SXMax", Results[11, 0]);
                fscommand.Parameters.AddWithValue("@BXMax", Results[11, 1]);
                fscommand.Parameters.AddWithValue("@AXMax", Results[11, 2]);
                fscommand.Parameters.AddWithValue("@SMaxSta", Results[11, 10]);
                fscommand.Parameters.AddWithValue("@M3XMax", Results[11, 5]);
                fscommand.Parameters.AddWithValue("@V2XMax", Results[11, 6]);
                fscommand.Parameters.AddWithValue("@TXMax", Results[11, 7]);
                fscommand.Parameters.AddWithValue("@PXMax", Results[11, 8]);
                fscommand.Parameters.AddWithValue("@XVMax", Results[12, 4]);
                fscommand.Parameters.AddWithValue("@XTMax", Results[12, 3]);
                if (Results[12, 9] != 0)
                {
                    fscommand.Parameters.AddWithValue("@XVcMax", Results[12, 9]);
                }
                else
                {
                    fscommand.Parameters.AddWithValue("@XVcMax", DBNull.Value);
                }
                fscommand.Parameters.AddWithValue("@VMaxSta", Results[12, 10]);
                fscommand.Parameters.AddWithValue("@M3XVMax", Results[12, 5]);
                fscommand.Parameters.AddWithValue("@V2XVMax", Results[12, 6]);
                fscommand.Parameters.AddWithValue("@TXVMax", Results[12, 7]);
                fscommand.Parameters.AddWithValue("@PXVMax", Results[12, 8]);
            }
            else
            {
                fscommand.Parameters.AddWithValue("@SXMax", DBNull.Value);
                fscommand.Parameters.AddWithValue("@BXMax", DBNull.Value);
                fscommand.Parameters.AddWithValue("@AXMax", DBNull.Value);
                fscommand.Parameters.AddWithValue("@SMaxSta", DBNull.Value);
                fscommand.Parameters.AddWithValue("@M3XMax", DBNull.Value);
                fscommand.Parameters.AddWithValue("@V2XMax", DBNull.Value);
                fscommand.Parameters.AddWithValue("@TXMax", DBNull.Value);
                fscommand.Parameters.AddWithValue("@PXMax", DBNull.Value);
                fscommand.Parameters.AddWithValue("@XVMax", DBNull.Value);
                fscommand.Parameters.AddWithValue("@XTMax", DBNull.Value);
                fscommand.Parameters.AddWithValue("@XVcMax", DBNull.Value);
                fscommand.Parameters.AddWithValue("@VMaxSta", DBNull.Value);
                fscommand.Parameters.AddWithValue("@M3XVMax", DBNull.Value);
                fscommand.Parameters.AddWithValue("@V2XVMax", DBNull.Value);
                fscommand.Parameters.AddWithValue("@TXVMax", DBNull.Value);
                fscommand.Parameters.AddWithValue("@PXVMax", DBNull.Value);
            }

            if (Results[13, 0] == 1)
            {
                fscommand.Parameters.AddWithValue("@BL", "T");
            }
            else
            {
                fscommand.Parameters.AddWithValue("@BL", "");
            }
            if (Results[13, 1] == 1)
            {
                fscommand.Parameters.AddWithValue("@BR", "T");
            }
            else
            {
                fscommand.Parameters.AddWithValue("@BR", "");
            }
            if (Results[13, 2] == 1)
            {
                fscommand.Parameters.AddWithValue("@AsMin", "T");
            }
            else
            {
                fscommand.Parameters.AddWithValue("@AsMin", "");
            }
            fscommand.ExecuteNonQuery();
        }
    }
}