﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Application_for_Structure_Engineering.Models;

namespace Web_Application_for_Structure_Engineering.Controllers
{
    public class datafileController : Controller
    {
        // GET: datafile
        public ActionResult RCBeam()
        {

            return View();
        }
        [HttpPost]
        public ActionResult RCBeam(String submit)
        {
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            switch (submit)
            {
                case "Element Forces - Frames":


                    String query1 = "INSERT INTO RCBeam (projectID,accessID,Mem,Name,L,LT,B,T) SELECT DISTINCT ElementForcesFrames.projectID,ElementForcesFrames.accessID,ElementForcesFrames.Frame,FrameSectionAssignments.AnalSect,ConnectivityFrame.Length,ConnectivityFrame.Length,SUBSTRING(FrameSectionAssignments.AnalSect, 3, 2),SUBSTRING('B 30x50', 6, 2) AS ExtractString FROM ElementForcesFrames INNER JOIN FrameSectionAssignments ON ElementForcesFrames.Frame = FrameSectionAssignments.Frame INNER JOIN ConnectivityFrame ON ElementForcesFrames.Frame = ConnectivityFrame.Frame";
                    SqlCommand sqlcomm1 = new SqlCommand(query1, sqlconn);

                    sqlcomm1.ExecuteNonQuery();
                    TempData["buttonval"] = "datafile";
                    break;
                default:
                    TempData["buttonval"] = "ไม่ได้";
                    break;
            }

            return RedirectToAction("RCBeamDesign", "Home");
        }
        public ActionResult RCBeamShow()
        {
            List<RCBDesign> list1 = new List<RCBDesign>();
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");


            sqlconn.Open();
            String query2 = "SELECT * FROM RCBeam";
            SqlCommand sqlcomm2 = new SqlCommand(query2, sqlconn);
            SqlDataReader rd = sqlcomm2.ExecuteReader();
            while (rd.Read())
            {
                list1.Add(new RCBDesign
                {
                    Mem = rd["Mem"].ToString(),
                    Mark = rd["Mark"].ToString(),
                    B = rd["B"].ToString(),
                    T = rd["T"].ToString(),
                    L = float.Parse(rd["L"].ToString()),
                    LT = float.Parse(rd["LT"].ToString()),
                    Name = rd["Name"].ToString()

                });
                ViewBag.list1 = list1;
            }
            sqlconn.Close();

            return View();
        }
    }
}