﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Application_for_Structure_Engineering.Models;

namespace Web_Application_for_Structure_Engineering.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Login()
        {
            return Json(new { });
        }

        [HttpPost]
        public ActionResult SignIn(string uid, string name, string email, string imgUrl)
        {
            List<Member> list = new List<Member>();
            Session["uid"] = uid;
            
            SqlConnection sqlconn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=web4st_StructureEn;Integrated Security=True; Max Pool Size=100; Connection Timeout=10;");
            //SqlConnection sqlconn = new SqlConnection("Data Source = web4structureen.97.double.in.th; Initial Catalog = web4st_StructureEn; User ID = web4st_admin; Password = 4StructureEn; Max Pool Size=100; Connection Timeout=10;");
            sqlconn.Open();
            string sqlquery = "select Id , email from Member where User_Id = " + uid;
            SqlCommand sqlcomm = new SqlCommand(sqlquery);
            sqlcomm.Connection = sqlconn;
            SqlDataReader dr = sqlcomm.ExecuteReader();
            while (dr.Read())
            {
                list.Add(new Member
                {
                    Id = Convert.ToInt32(dr["Id"]),
                    email = dr["email"].ToString(),
                });
            }
            sqlconn.Close();

            for (int i = 0; i <= list.Count; i++)
            {
                if (list.Count > 0)
                {
                    sqlconn.Open();
                    String queryUpdate = "UPDATE Member SET email = @email , Img_path = @Img_path , Name = @Name where User_Id = " + uid;
                    SqlCommand sqlcommUp = new SqlCommand(queryUpdate, sqlconn);
                    sqlcommUp.Parameters.AddWithValue("@email", email);
                    sqlcommUp.Parameters.AddWithValue("@Img_path", imgUrl);
                    sqlcommUp.Parameters.AddWithValue("@Name", name);
                    sqlcommUp.ExecuteNonQuery();
                    sqlconn.Close();
                }
                else
                {
                    sqlconn.Open();
                    string sqlquery2 = "INSERT INTO Member(email,User_Id,Img_path,Name)  VALUES(@email,@User_Id,@Img_path,@Name)";
                    SqlCommand sqlcomm2 = new SqlCommand(sqlquery2, sqlconn); ;
                    sqlcomm2.Parameters.AddWithValue("@email", email);
                    sqlcomm2.Parameters.AddWithValue("@User_Id", uid);
                    sqlcomm2.Parameters.AddWithValue("@Img_path", imgUrl);
                    sqlcomm2.Parameters.AddWithValue("@Name", name);
                    sqlcomm2.ExecuteNonQuery();
                    sqlconn.Close();
                }
            }
            return Json(new { ImgUrl = imgUrl, Email = email });
        }

        [HttpPost]
        public ActionResult SignOut()
        {
            Session.Remove("uid");
            return Json(new { });
        }
    }
}