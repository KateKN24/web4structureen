﻿function onSignIn(googleUser) {
    
        var profile = googleUser.getBasicProfile();

        var uid = profile.getId();
        var name = profile.getName();
        var email = profile.getEmail();
        var imgUrl = profile.getImageUrl();
        var id_token = googleUser.getAuthResponse().id_token;

        $.ajax({
            type: 'post',
            url: '/Login/SignIn',
            async: false,
            data: {
                uid: uid,
                name: name,
                email: email,
                imgUrl: imgUrl
            },
            success: function (data) {
                $("#logoutbtn").show();
                $("#loginbtn").hide();
                $("#img").show();
                $("#email").show();
                $("#img").attr('src', data.ImgUrl);
                document.getElementById("email").innerHTML = data.Email;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
}

