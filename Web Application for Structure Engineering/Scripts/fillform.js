﻿function fillForm(Results, chkDesignAll, utf) {
    checkDesignAll
    var checkDesignAll = document.getElementById("checkDesignAll").value
    document.getElementById("lblSX0").innerHTML = Results[0].toFixed(2);
    document.getElementById("lblBX0").innerHTML = Results[1].toFixed(2);
    document.getElementById("lblAX0").innerHTML = Results[2].toFixed(2);

    if (Results[21] != 0) {
        document.getElementById("lblSX1").innerHTML = Results[11].toFixed(2);
        document.getElementById("lblBX1").innerHTML = Results[12].toFixed(2);
        document.getElementById("lblAX1").innerHTML = Results[13].toFixed(2);
    }
    else {
        document.getElementById("lblSX1").innerHTML = "";
        document.getElementById("lblBX1").innerHTML = "";
        document.getElementById("lblAX1").innerHTML = "";
    }
    if (Results[32] != 0) {
        document.getElementById("lblSX2").innerHTML = Results[22].toFixed(2);
        document.getElementById("lblBX2").innerHTML = Results[23].toFixed(2);
        document.getElementById("lblAX2").innerHTML = Results[24].toFixed(2);
    }
    else {
        document.getElementById("lblSX2").innerHTML = "";
        document.getElementById("lblBX2").innerHTML = "";
        document.getElementById("lblAX2").innerHTML = "";
    }
    if (Results[43] != 0) {
        document.getElementById("lblSXM").innerHTML = Results[33].toFixed(2);
        document.getElementById("lblBXM").innerHTML = Results[34].toFixed(2);
        document.getElementById("lblAXM").innerHTML = Results[35].toFixed(2);
    }
    else {
        document.getElementById("lblSXM").innerHTML = "";
        document.getElementById("lblBXM").innerHTML = "";
        document.getElementById("lblAXM").innerHTML = "";
    }
    if (Results[54] != 0) {
        document.getElementById("lblSX8").innerHTML = Results[44].toFixed(2);
        document.getElementById("lblBX8").innerHTML = Results[45].toFixed(2);
        document.getElementById("lblAX8").innerHTML = Results[46].toFixed(2);
    }
    else {
        document.getElementById("lblSX8").innerHTML = "";
        document.getElementById("lblBX8").innerHTML = "";
        document.getElementById("lblAX8").innerHTML = "";
    }
    if (Results[65] != 0) {
        document.getElementById("lblSX9").innerHTML = Results[55].toFixed(2);
        document.getElementById("lblBX9").innerHTML = Results[56].toFixed(2);
        document.getElementById("lblAX9").innerHTML = Results[57].toFixed(2);
    }
    else {
        document.getElementById("lblSX9").innerHTML = "";
        document.getElementById("lblBX9").innerHTML = "";
        document.getElementById("lblAX9").innerHTML = "";
    }
    document.getElementById("lblSX10").innerHTML = Results[66].toFixed(2);
    document.getElementById("lblBX10").innerHTML = Results[67].toFixed(2);
    document.getElementById("lblAX10").innerHTML = Results[68].toFixed(2);

    document.getElementById("lblShearXV0").innerHTML = Results[81].toFixed(2);
    document.getElementById("lblTorsionXV0").innerHTML = Results[80].toFixed(2);

    if (Results[80] == 0 && Results[81] == 0) {
        document.getElementById("lblVcXV0").innerHTML = Results[86].toFixed(2);
    } else {
        document.getElementById("lblVcXV0").innerHTML = "";
    }

    if (Results[98] != 0) {
        document.getElementById("lblShearXV1").innerHTML = Results[92].toFixed(2);
        document.getElementById("lblTorsionXV1").innerHTML = Results[91].toFixed(2);
        if (Results[91] == 0 && Results[92] == 0) {
            document.getElementById("lblVcXV1").innerHTML = Results[97].toFixed(2);
        } else {
            document.getElementById("lblVcXV1").innerHTML = "";
        }
    }
    else {
        document.getElementById("lblShearXV1").innerHTML = "";
        document.getElementById("lblTorsionXV1").innerHTML = "";
        document.getElementById("lblVcXV1").innerHTML = "";
    }

    if (Results[109] != 0) {
        document.getElementById("lblShearXV9").innerHTML = Results[103].toFixed(2);
        document.getElementById("lblTorsionXV9").innerHTML = Results[102].toFixed(2);
        if (Results[102] == 0 && Results[103] == 0) {
            document.getElementById("lblVcXV9").innerHTML = Results[108].toFixed(2);
        } else {
            document.getElementById("lblVcXV9").innerHTML = "";
        }
    }
    else {
        document.getElementById("lblShearXV9").innerHTML = "";
        document.getElementById("lblTorsionXV9").innerHTML = "";
        document.getElementById("lblVcXV9").innerHTML = "";
    }

    document.getElementById("lblShearXV10").innerHTML = Results[114].toFixed(2);
    document.getElementById("lblTorsionXV10").innerHTML = Results[113].toFixed(2);
    if (Results[113] == 0 && Results[114] == 0) {
        document.getElementById("lblVcXV10").innerHTML = Results[119].toFixed(2);
    } else {
        document.getElementById("lblVcXV10").innerHTML = "";
    }
    if (chkDesignAll == true) {
        document.getElementById("lblXMax").innerHTML = "MAX at L=" + Results[131].toFixed(2);
        document.getElementById("lblSXMax").innerHTML = Results[121].toFixed(2);
        document.getElementById("lblBXMax").innerHTML = Results[122].toFixed(2);
        document.getElementById("lblAXMax").innerHTML = Results[123].toFixed(2);
        document.getElementById("lblXVMax").innerHTML = "MAX at L=" + Results[142].toFixed(2);
        document.getElementById("lblShearXVMax").innerHTML = Results[136].toFixed(2);
        document.getElementById("lblTorsionXVMax").innerHTML = Results[135].toFixed(2);
        if (Results[136] == 0 && Results[135] == 0) {
            document.getElementById("lblVcXVMax").innerHTML = Results[141].toFixed(2);
        } else {
            document.getElementById("lblVcXVMax").innerHTML = "";
        }
    } else {
        document.getElementById("lblXMax").innerHTML = "";
        document.getElementById("lblSXMax").innerHTML = "";
        document.getElementById("lblBXMax").innerHTML = "";
        document.getElementById("lblAXMax").innerHTML = "";
        document.getElementById("lblXVMax").innerHTML = "";
        document.getElementById("lblShearXVMax").innerHTML = "";
        document.getElementById("lblTorsionXVMax").innerHTML = "";
        document.getElementById("lblVcXVMax").innerHTML = "";
    }
    switch (Results[20]) {
        case 1: document.getElementById("lblX1").innerHTML = "1A"; break;
        case 2: document.getElementById("lblX1").innerHTML = "1B"; break;
        case 3: document.getElementById("lblX1").innerHTML = "1C"; break;
        case 4: document.getElementById("lblX1").innerHTML = "1D"; break;
        default: document.getElementById("lblX1").innerHTML = "1"; break;
    }
    switch (Results[31]) {
        case 1: document.getElementById("lblX2").innerHTML = "2A"; break;
        case 2: document.getElementById("lblX2").innerHTML = "2B"; break;
        case 3: document.getElementById("lblX2").innerHTML = "2C"; break;
        case 4: document.getElementById("lblX2").innerHTML = "2D"; break;
        default: document.getElementById("lblX2").innerHTML = "2"; break;
    }
    switch (Results[53]) {
        case 1: document.getElementById("lblX8").innerHTML = "8A"; break;
        case 2: document.getElementById("lblX8").innerHTML = "8B"; break;
        case 3: document.getElementById("lblX8").innerHTML = "8C"; break;
        case 4: document.getElementById("lblX8").innerHTML = "8D"; break;
        default: document.getElementById("lblX8").innerHTML = "8"; break;
    }
    switch (Results[64]) {
        case 1: document.getElementById("lblX9").innerHTML = "9A"; break;
        case 2: document.getElementById("lblX9").innerHTML = "9B"; break;
        case 3: document.getElementById("lblX9").innerHTML = "9C"; break;
        case 4: document.getElementById("lblX9").innerHTML = "9D"; break;
        default: document.getElementById("lblX9").innerHTML = "9"; break;
    }


    if (Results[0] > utf) {
        document.getElementById("lblSX0").style.color = "red";
    }
    else {
        document.getElementById("lblSX0").style.color = "black";
    }
    if (Results[1] > utf) {
        document.getElementById("lblBX0").style.color = "red";
    }
    else {
        document.getElementById("lblBX0").style.color = "black";
    }
    if (Results[2] > utf) {
        document.getElementById("lblAX0").style.color = "red";
    }
    else {
        document.getElementById("lblAX0").style.color = "black";
    }

    if (Results[11] > utf) {
        document.getElementById("lblSX1").style.color = "red";
    }
    else {
        document.getElementById("lblSX1").style.color = "black";
    }
    if (Results[12] > utf) {
        document.getElementById("lblBX1").style.color = "red";
    }
    else {
        document.getElementById("lblBX1").style.color = "black";
    }
    if (Results[13] > utf) {
        document.getElementById("lblAX1").style.color = "red";
    }
    else {
        document.getElementById("lblAX1").style.color = "black";
    }

    if (Results[22] > utf) {
        document.getElementById("lblSX2").style.color = "red";
    }
    else {
        document.getElementById("lblSX2").style.color = "black";
    }
    if (Results[23] > utf) {
        document.getElementById("lblBX2").style.color = "red";
    }
    else {
        document.getElementById("lblBX2").style.color = "black";
    }
    if (Results[24] > utf) {
        document.getElementById("lblAX2").style.color = "red";
    }
    else {
        document.getElementById("lblAX2").style.color = "black";
    }

    if (Results[33] > utf) {
        document.getElementById("lblSXM").style.color = "red";
    }
    else {
        document.getElementById("lblSXM").style.color = "black";
    }
    if (Results[34] > utf) {
        document.getElementById("lblBXM").style.color = "red";
    }
    else {
        document.getElementById("lblBXM").style.color = "black";
    }
    if (Results[35] > utf) {
        document.getElementById("lblAXM").style.color = "red";
    }
    else {
        document.getElementById("lblAXM").style.color = "black";
    }

    if (Results[44] > utf) {
        document.getElementById("lblSX8").style.color = "red";
    }
    else {
        document.getElementById("lblSX8").style.color = "black";
    }
    if (Results[45] > utf) {
        document.getElementById("lblBX8").style.color = "red";
    }
    else {
        document.getElementById("lblBX8").style.color = "black";
    }
    if (Results[46] > utf) {
        document.getElementById("lblAX8").style.color = "red";
    }
    else {
        document.getElementById("lblAX8").style.color = "black";
    }

    if (Results[55] > utf) {
        document.getElementById("lblSX9").style.color = "red";
    }
    else {
        document.getElementById("lblSX9").style.color = "black";
    }
    if (Results[56] > utf) {
        document.getElementById("lblBX9").style.color = "red";
    }
    else {
        document.getElementById("lblBX9").style.color = "black";
    }
    if (Results[57] > utf) {
        document.getElementById("lblAX9").style.color = "red";
    }
    else {
        document.getElementById("lblAX9").style.color = "black";
    }

    if (Results[66] > utf) {
        document.getElementById("lblSX10").style.color = "red";
    }
    else {
        document.getElementById("lblSX10").style.color = "black";
    }
    if (Results[67] > utf) {
        document.getElementById("lblBX10").style.color = "red";
    }
    else {
        document.getElementById("lblBX10").style.color = "black";
    }
    if (Results[68] > utf) {
        document.getElementById("lblAX10").style.color = "red";
    }
    else {
        document.getElementById("lblAX10").style.color = "black";
    }

    if (Results[81] > utf) {
        document.getElementById("lblShearXV0").style.color = "red";
    }
    else {
        document.getElementById("lblShearXV0").style.color = "black";
    }
    if (Results[80] > utf) {
        document.getElementById("lblTorsionXV0").style.color = "red";
    }
    else {
        document.getElementById("lblTorsionXV0").style.color = "black";
    }

    if (Results[92] > utf) {
        document.getElementById("lblShearXV1").style.color = "red";
    }
    else {
        document.getElementById("lblShearXV1").style.color = "black";
    }
    if (Results[91] > utf) {
        document.getElementById("lblTorsionXV1").style.color = "red";
    }
    else {
        document.getElementById("lblTorsionXV1").style.color = "black";
    }

    if (Results[103] > utf) {
        document.getElementById("lblShearXV9").style.color = "red";
    }
    else {
        document.getElementById("lblShearXV9").style.color = "black";
    }
    if (Results[102] > utf) {
        document.getElementById("lblTorsionXV9").style.color = "red";
    }
    else {
        document.getElementById("lblTorsionXV9").style.color = "black";
    }

    if (Results[114] > utf) {
        document.getElementById("lblShearXV10").style.color = "red";
    }
    else {
        document.getElementById("lblShearXV10").style.color = "black";
    }
    if (Results[113] > utf) {
        document.getElementById("lblTorsionXV10").style.color = "red";
    }
    else {
        document.getElementById("lblTorsionXV10").style.color = "black";
    }

    if (checkDesignAll == true) {
        if (Results[121] > utf) {
            document.getElementById("lblSXMax").style.color = "red";
        }
        else {
            document.getElementById("lblSXMax").style.color = "black";
        }
        if (Results[122] > utf) {
            document.getElementById("lblBXMax").style.color = "red";
        }
        else {
            document.getElementById("lblBXMax").style.color = "black";
        }
        if (Results[123] > utf) {
            document.getElementById("lblAXMax").style.color = "red";
        }
        else {
            document.getElementById("lblAXMax").style.color = "black";
        }

        if (Results[136] > utf) {
            document.getElementById("lblShearXVMax").style.color = "red";
        }
        else {
            document.getElementById("lblShearXVMax").style.color = "black";
        }
        if (Results[135] > utf) {
            document.getElementById("lblTorsionXVMax").style.color = "red";
        }
        else {
            document.getElementById("lblTorsionXVMax").style.color = "black";
        }
    }

    if (Results[143] == 1) {
        document.getElementById("M3Left").value = "TRUE";
        document.getElementById("M3Left").style.color = "red";
    } else {
        document.getElementById("M3Left").value = "FALSE";
        document.getElementById("M3Left").style.color = "black";
    }
    if (Results[144] == 1) {
        document.getElementById("M3Right").value = "TRUE";
        document.getElementById("M3Right").style.color = "red";
    } else {
        document.getElementById("M3Right").value = "FALSE";
        document.getElementById("M3Right").style.color = "black";
    }
    if (Results[145] == 1) {
        document.getElementById("AsMin").value = "TRUE";
        document.getElementById("AsMin").style.color = "red";
    } else {
        document.getElementById("AsMin").value = "FALSE";
        document.getElementById("AsMin").style.color = "black";
    }
    if (Results[81] == 7) {
        alert("ERR 7.00 : Provided stirrup reinforcement is less than required by torsion.");
    }
    if (Results[81] == 8 || Results[80] == 8) {
        alert("ERR 8.00 : Spacing of stirrup reinforcement is more than allowable maximum spacing.");
    }
    if (Results[81] == 9 || Results[80] == 9) {
        alert("ERR 9.00 : Provided stirrup reinforcement is less than minimum reinforcement.");
    }

}