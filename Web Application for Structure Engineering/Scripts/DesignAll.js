﻿function designall() {
    var infoF = $("#infoForm");
    return designallRun(infoF);
}
function designallRun(formContainer) {
    $.ajax({
        type: 'post',
        url: '/Home/BtnDesignAll_Click',
        async: false,
        data: formContainer.serialize(),
        success: function (data) {
            document.getElementById("txtRownumber").value = data.txtRowNumber;
            var i = 0;
            for (i = 0; i < data.list.length; i++) {
                    document.getElementById("Mem").value = data.list[i].Mem;
                    document.getElementById("B").value = data.list[i].B;
                    document.getElementById("T").value = data.list[i].T;
                    document.getElementById("Ct").value = data.list[i].Ct;
                    document.getElementById("Cb").value = data.list[i].Cb;
                    document.getElementById("N1").value = data.list[i].N1;
                    document.getElementById("D1").value = data.list[i].D1;
                    document.getElementById("N2").value = data.list[i].N2;
                    document.getElementById("D2").value = data.list[i].D2;
                    document.getElementById("L2L").value = data.list[i].L2L;
                    document.getElementById("L2R").value = data.list[i].L2R;
                    document.getElementById("N3").value = data.list[i].N3;
                    document.getElementById("D3").value = data.list[i].D3;
                    document.getElementById("L3L").value = data.list[i].L3L;
                    document.getElementById("L3R").value = data.list[i].L3R;
                    document.getElementById("N4").value = data.list[i].N4;
                    document.getElementById("D4").value = data.list[i].D4;
                    document.getElementById("L4L").value = data.list[i].L4L;
                    document.getElementById("L4R").value = data.list[i].L4R;
                    document.getElementById("N5").value = data.list[i].N5;
                    document.getElementById("D5").value = data.list[i].D5;
                    document.getElementById("L5L").value = data.list[i].L5L;
                    document.getElementById("L5R").value = data.list[i].L5R;
                    document.getElementById("LN1").value = data.list[i].LN1;
                    document.getElementById("LD1").value = data.list[i].LD1;
                    document.getElementById("LN2").value = data.list[i].LN2;
                    document.getElementById("LD2").value = data.list[i].LD2;
                    document.getElementById("LL2").value = data.list[i].LL2;
                    document.getElementById("LN3").value = data.list[i].LN3;
                    document.getElementById("LD3").value = data.list[i].LD3;
                    document.getElementById("LL3").value = data.list[i].LL3;
                    document.getElementById("LN4").value = data.list[i].LN4;
                    document.getElementById("LD4").value = data.list[i].LD4;
                    document.getElementById("LL4").value = data.list[i].LL4;
                    document.getElementById("LN5").value = data.list[i].LN5;
                    document.getElementById("LD5").value = data.list[i].LD5;
                    document.getElementById("LL5").value = data.list[i].LL5;
                    document.getElementById("RN2").value = data.list[i].RN2;
                    document.getElementById("RD2").value = data.list[i].RD2;
                    document.getElementById("RL2").value = data.list[i].RL2;
                    document.getElementById("RN3").value = data.list[i].RN3;
                    document.getElementById("RD3").value = data.list[i].RD3;
                    document.getElementById("RL3").value = data.list[i].RL3;
                    document.getElementById("RN4").value = data.list[i].RN4;
                    document.getElementById("RD4").value = data.list[i].RD4;
                    document.getElementById("RL4").value = data.list[i].RL4;
                    document.getElementById("RN5").value = data.list[i].RN5;
                    document.getElementById("RD5").value = data.list[i].RD5;
                    document.getElementById("RL5").value = data.list[i].RL5;
                    document.getElementById("N11").value = data.list[i].N11;
                    document.getElementById("D11").value = data.list[i].D11;
                    document.getElementById("RS2").value = data.list[i].RS2;
                    if (data.list[i].Cot2 == 'T') {
                        document.getElementById("Cot2").value = true;
                        document.getElementById("Cot2").checked = true;
                    }
                    else {
                        document.getElementById("Cot2").value = false;
                        document.getElementById("Cot2").checked = false;
                    }
                    if (data.list[i].Cot3 == 'T') {
                        document.getElementById("Cot3").value = true;
                        document.getElementById("Cot3").checked = true;
                    }
                    else {
                        document.getElementById("Cot3").value = false;
                        document.getElementById("Cot3").checked = false;
                    }
                    if (data.list[i].Cot4 == 'T') {
                        document.getElementById("Cot4").value = true;
                        document.getElementById("Cot4").checked = true;
                    }
                    else {
                        document.getElementById("Cot4").value = false;
                        document.getElementById("Cot4").checked = false;
                    }
                    if (data.list[i].Cot5 == 'T') {
                        document.getElementById("Cot5").value = true;
                        document.getElementById("Cot5").checked = true;
                    }
                    else {
                        document.getElementById("Cot5").value = false;
                        document.getElementById("Cot5").checked = false;
                    }
                    if (data.list[i].Cob2 == 'T') {
                        document.getElementById("Cob2").value = true;
                        document.getElementById("Cob2").checked = true;
                    }
                    else {
                        document.getElementById("Cob2").value = false;
                        document.getElementById("Cob2").checked = false;
                    }
                    if (data.list[i].Cob3 == 'T') {
                        document.getElementById("Cob3").value = true;
                        document.getElementById("Cob3").checked = true;
                    }
                    else {
                        document.getElementById("Cob3").value = false;
                        document.getElementById("Cob3").checked = false;
                    }
                    if (data.list[i].Cob4 == 'T') {
                        document.getElementById("Cob4").value = true;
                        document.getElementById("Cob4").checked = true;
                    }
                    else {
                        document.getElementById("Cob4").value = false;
                        document.getElementById("Cob4").checked = false;
                    }
                    if (data.list[i].Cob5 == 'T') {
                        document.getElementById("Cob5").value = true;
                        document.getElementById("Cob5").checked = true;
                    }
                    else {
                        document.getElementById("Cob5").value = false;
                        document.getElementById("Cob5").checked = false;
                    }
                    document.getElementById("Ds").value = data.list[i].Ds;
                    document.getElementById("Nso").value = data.list[i].Nso;
                    document.getElementById("Nsi").value = data.list[i].Nsi;
                    document.getElementById("Nslo").value = data.list[i].Nslo;
                    document.getElementById("Nsli").value = data.list[i].Nsli;
                    document.getElementById("Nsro").value = data.list[i].Nsro;
                    document.getElementById("Nsri").value = data.list[i].Nsri;
                    document.getElementById("Stln").value = data.list[i].Stln;
                    document.getElementById("Stls").value = data.list[i].Stls;
                    document.getElementById("Sts").value = data.list[i].Sts;
                    document.getElementById("Strn").value = data.list[i].Strn;
                    document.getElementById("Strs").value = data.list[i].Strs;
                    check();
            }
            if (data.MSG != "") {
                alert(data.MSG);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}