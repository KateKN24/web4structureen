﻿var btnSaveErr;
var GrpDesign;
var GPRunError = [];
var GPRunResults = [[], []];
function btnSave() {
    var formContainer = $("#infoForm");
    var txtRownumber = document.getElementById("txtRownumber").value
    if (txtRownumber == 0) {
        if (GrpDesign == true) {
            btnSaveErr = true;
        }
        else {
            alert("Illegal Input Data. ");
        }
    } else {

        var check = 0;

        var N1 = document.getElementById("N1").value;
        var D1 = document.getElementById("D1").value;
        var LN1 = document.getElementById("LN1").value;
        var LD1 = document.getElementById("LD1").value;
        if (N1 == 0 || D1 == "" || LN1 == "" || LD1 == "" || N1 == "" || LN1 == 0 || LD1 == 0) {
            document.getElementById("N1").focus();
            alert("Extra reinforcement is not valid.");
            check = 1;
        }
        var RS2 = document.getElementById("RS2").value;
        if (RS2 == 0) {
            document.getElementById("RS2").value = 25;
        }

        var N11 = document.getElementById("N11").value;
        if (N11 != "") {
            if (N11 % 2 != 0) {
                document.getElementById("N11").focus();
                alert("Number of extra reinforcement for torsion is not valid.");
                check = 1;
            }
        }
        var Ct = document.getElementById("Ct").value;
        if (Ct == "" || Ct == 0) {
            document.getElementById("Ct").focus();
            alert("Covering is not valid.");
            check = 1;
        }
        var Cb = document.getElementById("Cb").value;
        if (Cb == "" || Cb == 0) {
            document.getElementById("Cb").focus();
            alert("Covering is not valid.");
            check = 1;
        }
        var FL = document.getElementById("FL");
        var TL = document.getElementById("TL").value;
        if (FL.checked != true || FL.checked == null) {
            if (TL == "" || TL <= 50) {
                document.getElementById("TL").focus();
                alert("Support width is not valid.");
                check = 1;
            }
        }
        var FR = document.getElementById("FR");
        var TR = document.getElementById("TR").value;
        if (FR.checked != true || FR.checked == null) {
            if (TR == "" || TR <= 50) {
                document.getElementById("TR").focus();
                alert("Support width is not valid.");
                check = 1;
            }
        }

        var Cot2 = document.getElementById("Cot2");
        var LN2 = document.getElementById("LN2").value;
        var LD2 = document.getElementById("LD2").value;
        var LL2 = document.getElementById("LL2").value;
        var RN2 = document.getElementById("RN2").value;
        var RD2 = document.getElementById("RD2").value;
        var RL2 = document.getElementById("RL2").value;
        if (Cot2.checked != true || Cot2.checked == null) {
            if (LN2 == "" || LN2 == 0 || LD2 == "" || LD2 == 0 || LL2 == "" || LL2 == 0) {
                document.getElementById("LN2").value = "";
                document.getElementById("LD2").value = "";
                document.getElementById("LL2").value = "";
            }
            if (RN2 == "" || RN2 == 0 || RD2 == "" || RD2 == 0 || RL2 == "" || RL2 == 0) {
                document.getElementById("RN2").value = "";
                document.getElementById("RD2").value = "";
                document.getElementById("RL2").value = "";
            }
        }
        else {
            if (LN2 == "" || LN2 == 0) {
                document.getElementById("LN2").focus();
                alert("Extra reinforcement is not valid.");
                check = 1;
            }
            if (LD2 == "" || LD2 == 0) {
                document.getElementById("LD2").focus();
                alert("Extra reinforcement is not valid.");
                check = 1;
            }
        }
        var Cot3 = document.getElementById("Cot3");
        var LN3 = document.getElementById("LN3").value;
        var LD3 = document.getElementById("LD3").value;
        var LL3 = document.getElementById("LL3").value;
        var RN3 = document.getElementById("RN3").value;
        var RD3 = document.getElementById("RD3").value;
        var RL3 = document.getElementById("RL3").value;
        if (Cot3.checked != true || Cot3.checked == null) {
            if (LN3 == "" || LN3 == 0 || LD3 == "" || LD3 == 0 || LL3 == "" || LL3 == 0) {
                document.getElementById("LN3").value = "";
                document.getElementById("LD3").value = "";
                document.getElementById("LL3").value = "";
            }
            if (RN3 == "" || RN3 == 0 || RD3 == "" || RD3 == 0 || RL3 == "" || RL3 == 0) {
                document.getElementById("RN3").value = "";
                document.getElementById("RD3").value = "";
                document.getElementById("RL3").value = "";
            }
        }
        else {
            if (LN3 == "" || LN3 == 0) {
                document.getElementById("LN3").focus();
                alert("Extra reinforcement is not valid.3");
                check = 1;
            }
            if (LD3 == "" || LD3 == 0) {
                document.getElementById("LD3").focus();
                alert("Extra reinforcement is not valid.3");
                check = 1;
            }
        }
        var Cot4 = document.getElementById("Cot4");
        var LN4 = document.getElementById("LN4").value;
        var LD4 = document.getElementById("LD4").value;
        var LL4 = document.getElementById("LL4").value;
        var RN4 = document.getElementById("RN4").value;
        var RD4 = document.getElementById("RD4").value;
        var RL4 = document.getElementById("RL4").value;
        if (Cot4.checked != true || Cot4.checked == null) {
            if (LN4 == "" || LN4 == 0 || LD4 == "" || LD4 == 0 || LL4 == "" || LL4 == 0) {
                document.getElementById("LN4").value = "";
                document.getElementById("LD4").value = "";
                document.getElementById("LL4").value = "";
            }
            if (RN4 == "" || RN4 == 0 || RD4 == "" || RD4 == 0 || RL4 == "" || RL4 == 0) {
                document.getElementById("RN4").value = "";
                document.getElementById("RD4").value = "";
                document.getElementById("RL4").value = "";
            }
        }
        else {
            if (LN4 == "" || LN4 == 0) {
                document.getElementById("LN4").focus();
                alert("Extra reinforcement is not valid.");
                check = 1;
            }
            if (LD4 == "" || LD4 == 0) {
                document.getElementById("LD4").focus();
                alert("Extra reinforcement is not valid.");
                check = 1;
            }
        }
        var Cot5 = document.getElementById("Cot5");
        var LN5 = document.getElementById("LN5").value;
        var LD5 = document.getElementById("LD5").value;
        var LL5 = document.getElementById("LL5").value;
        var RN5 = document.getElementById("RN5").value;
        var RD5 = document.getElementById("RD5").value;
        var RL5 = document.getElementById("RL5").value;
        if (Cot5.checked != true || Cot5.checked == null) {
            if (LN5 == "" || LN5 == 0 || LD5 == "" || LD5 == 0 || LL5 == "" || LL5 == 0) {
                document.getElementById("LN5").value = "";
                document.getElementById("LD5").value = "";
                document.getElementById("LL5").value = "";
            }
            if (RN5 == "" || RN5 == 0 || RD5 == "" || RD5 == 0 || RL5 == "" || RL5 == 0) {
                document.getElementById("RN5").value = "";
                document.getElementById("RD5").value = "";
                document.getElementById("RL5").value = "";
            }
        }
        else {
            if (LN5 == "" || LN5 == 0) {
                document.getElementById("LN5").focus();
                alert("Extra reinforcement is not valid.");
                check = 1;
            }
            if (LD5 == "" || LD5 == 0) {
                document.getElementById("LD5").focus();
                alert("Extra reinforcement is not valid.");
                check = 1;
            }
        }
        var Cob2 = document.getElementById("Cob2");
        var N2 = document.getElementById("N2").value;
        var D2 = document.getElementById("D2").value;
        var L2L = document.getElementById("L2L").value;
        var L2R = document.getElementById("L2L").value;
        if (Cob2.checked != true || Cob2.checked == null) {
            if (N2 == "" || N2 == 0 || D2 == "" || D2 == 0 || L2L == "" || L2R == "") {
                document.getElementById("N2").value = "";
                document.getElementById("D2").value = "";
                document.getElementById("L2L").value = "";
                document.getElementById("L2R").value = "";
            }
        }
        else {
            if (N2 == "" || N2 == 0) {
                document.getElementById("N2").focus();
                alert("Extra reinforcement is not valid.");
                check = 1;
            }
            if (D2 == "" || D2 == 0) {
                document.getElementById("D2").focus();
                alert("Extra reinforcement is not valid.");
                check = 1;
            }
        }
        var Cob3 = document.getElementById("Cob3");
        var N3 = document.getElementById("N3").value;
        var D3 = document.getElementById("D3").value;
        var L3L = document.getElementById("L3L").value;
        var L3R = document.getElementById("L3L").value;
        if (Cob3.checked != true || Cob3.checked == null) {
            if (N3 == "" || N3 == 0 || D3 == "" || D3 == 0 || L3L == "" || L3R == "") {
                document.getElementById("N3").value = "";
                document.getElementById("D3").value = "";
                document.getElementById("L3L").value = "";
                document.getElementById("L3R").value = "";
            }
        }
        else {
            if (N3 == "" || N3 == 0) {
                document.getElementById("N3").focus();
                alert("Extra reinforcement is not valid.");
                check = 1;
            }
            if (D3 == "" || D3 == 0) {
                document.getElementById("D3").focus();
                alert("Extra reinforcement is not valid.");
                check = 1;
            }
        }
        var Cob4 = document.getElementById("Cob4");
        var N4 = document.getElementById("N4").value;
        var D4 = document.getElementById("D4").value;
        var L4L = document.getElementById("L4L").value;
        var L4R = document.getElementById("L4L").value;
        if (Cob4.checked != true || Cob4.checked == null) {
            if (N4 == "" || N4 == 0 || D4 == "" || D4 == 0 || L4L == "" || L4R == "") {
                document.getElementById("N4").value = "";
                document.getElementById("D4").value = "";
                document.getElementById("L4L").value = "";
                document.getElementById("L4R").value = "";
            }
        }
        else {
            if (N4 == "" || N4 == 0) {
                document.getElementById("N4").focus();
                alert("Extra reinforcement is not valid.");
                check = 1;
            }
            if (D4 == "" || D4 == 0) {
                document.getElementById("D4").focus();
                alert("Extra reinforcement is not valid.");
                check = 1;
            }
        }
        var Cob5 = document.getElementById("Cob5");
        var N5 = document.getElementById("N5").value;
        var D5 = document.getElementById("D5").value;
        var L5L = document.getElementById("L5L").value;
        var L5R = document.getElementById("L5L").value;
        if (Cob5.checked != true || Cob5.checked == null) {
            if (N5 == "" || N5 == 0 || D5 == "" || D5 == 0 || L5L == "" || L5R == "") {
                document.getElementById("N5").value = "";
                document.getElementById("D5").value = "";
                document.getElementById("L5L").value = "";
                document.getElementById("L5R").value = "";
            }
        }
        else {
            if (N5 == "" || N5 == 0) {
                document.getElementById("N5").focus();
                alert("Extra reinforcement is not valid.");
                check = 1;
            }
            if (D5 == "" || D5 == 0) {
                document.getElementById("D5").focus();
                alert("Extra reinforcement is not valid.");
                check = 1;
            }
        }

        var Nslin = document.getElementById("Nsli").value;
        var Nslout = document.getElementById("Nslo").value;
        var Stln = document.getElementById("Stln").value;
        var Stls = document.getElementById("Stls").value;
        var Nsrin = document.getElementById("Nsri").value;
        var Nsrout = document.getElementById("Nsro").value;
        var Strn = document.getElementById("Strn").value;
        var Strs = document.getElementById("Strs").value;
        var Nsi = document.getElementById("Nsi").value;
        var Nso = document.getElementById("Nso").value;
        var Sts = document.getElementById("Sts").value;
        if (((Nslin == "" || Nslin == 0) && (Nslout == "" || Nslout == 0)) || Stln == "" || Stln == 0 || Stls == "" || Stls == 0) {
            document.getElementById("Nsli").value = "";
            document.getElementById("Nslo").value = "";
            document.getElementById("Stln").value = "";
            document.getElementById("Stls").value = "";
        }
        if (((Nsrin == "" || Nsrin == 0) && (Nsrout == "" || Nsrout == 0)) || Strn == "" || Strn == 0 || Strs == "" || Strs == 0) {
            document.getElementById("Nsri").value = "";
            document.getElementById("Nsro").value = "";
            document.getElementById("Strn").value = "";
            document.getElementById("Strs").value = "";
        }
        if (((Nsi == "" || Nsi == 0) && (Nso == "" || Nso == 0)) || Sts == "" || Sts == 0) {
            document.getElementById("Nsi").value = "";
            document.getElementById("Nso").value = "";
            document.getElementById("Sts").value = "";
        }

        if (check == 0) {
            $.ajax({
                url: '/Home/btnSave_Click',
                async: false,
                type: 'post',
                data: formContainer.serialize(),
                success: function (data) {
                    alert("Save Success");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
        }
    }
}
function btnGpRun() {
    var Mark = document.getElementById("Mark").value;
    $.ajax({
        url: '/Home/BtnGpRun_Click',
        type: 'post',
        async: false,
        data: {
            Row: document.getElementById("txtRownumber").value,
            Mark: document.getElementById("Mark").value
        },
        success: function (data) {
            GpSuccess(Mark);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}

function GpSuccess(Mark) {
    if (Mark == "") {
        alert("Mark for this member is not specified.");
        return;
    }
    else {
        '<%Session["Mark"] = "' + Mark + '"; %>';
        btnSaveErr = false;
        GrpDesign = true;
        GPRunError.length = 1;
        GPRunResults.length = 12;
        var i, j;
        for (i = 0; i < GPRunError.length; i++) {
            if (GPRunError[i] == null) {
                GPRunError[i] = [];
            }
        }
        for (i = 0; i < GPRunResults.length; i++) {
            if (GPRunResults[i] != null) {
                GPRunResults[i].length = 10;
                for (j = 0; j < GPRunResults[i].length; j++) {
                    if (GPRunResults[i][j] == null) {
                        GPRunResults[i][j] = new Array(1);
                    }
                }
            }
            else {
                GPRunResults[i] = new Array(10);
            }
        }
        btnSave();
        btnSaveErr = false;
        if (btnSaveErr == true) {
            alert("Illegal input data.")
            return;
        }
        if (GPRunError.length != 1) {
            var MemErrorList = "";
            alert(GPRunError.length);
            for (i = 1; i < GPRunError.length; i++) {
                if (i != GPRunError.length - 1) {
                    MemErrorList = MemErrorList + GPRunError[i].toString() + ", ";
                }
                else {
                    MemErrorList = MemErrorList + GPRunError[i].toString();
                }
            }
            if (GPRunError.length == 1) {
                alert("Member number " + MemErrorList + " was not designed due to error.");
            }
            else {
                alert("Member number " + MemErrorList + " were not designed due to error.");
            }
        }
        GrpDesign = false;
        document.getElementById("Mark1").value = Mark;
        var infoF = $("#infoForm");
        SubmitGP(infoF);
        $("#myModal").modal();

    }
}
function SubmitGP(formContainer) {
    $.ajax({
        type: 'post',
        url: '/Home/Design',
        data: formContainer.serialize(),
        success: function (data) {
            GPForm(data.results);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}

function GPForm(Results) {
    document.getElementById("GPlblSX0").innerHTML = Results[0, 0];
    document.getElementById("GPlblBX0").innerHTML = Results[0, 1];
    document.getElementById("GPlblAX0").innerHTML = Results[0, 2];

    if (Results[1, 10] != 0) {
        document.getElementById("GPlblSX1").innerHTML = Results[1, 0];
        document.getElementById("GPlblBX1").innerHTML = Results[1, 1];
        document.getElementById("GPlblAX1").innerHTML = Results[1, 2];
    }
    else {
        document.getElementById("GPlblSX1").innerHTML = "";
        document.getElementById("GPlblBX1").innerHTML = "";
        document.getElementById("GPlblAX1").innerHTML = "";
    }
    if (Results[2, 10] != 0) {
        document.getElementById("GPlblSX2").innerHTML = Results[2, 0];
        document.getElementById("GPlblBX2").innerHTML = Results[2, 1];
        document.getElementById("GPlblAX2").innerHTML = Results[2, 2];
    }
    else {
        document.getElementById("GPlblSX2").innerHTML = "";
        document.getElementById("GPlblBX2").innerHTML = "";
        document.getElementById("GPlblAX2").innerHTML = "";
    }
    if (Results[3, 10] != 0) {
        document.getElementById("GPlblSXM").innerHTML = Results[3, 0];
        document.getElementById("GPlblBXM").innerHTML = Results[3, 1];
        document.getElementById("GPlblAXM").innerHTML = Results[3, 2];
    }
    else {
        document.getElementById("GPlblSXM").innerHTML = "";
        document.getElementById("GPlblBXM").innerHTML = "";
        document.getElementById("GPlblAXM").innerHTML = "";
    }
    if (Results[4, 10] != 0) {
        document.getElementById("GPlblSX8").innerHTML = Results[4, 0];
        document.getElementById("GPlblBX8").innerHTML = Results[4, 1];
        document.getElementById("GPlblAX8").innerHTML = Results[4, 2];
    }
    else {
        document.getElementById("GPlblSX8").innerHTML = "";
        document.getElementById("GPlblBX8").innerHTML = "";
        document.getElementById("GPlblAX8").innerHTML = "";
    }
    if (Results[5, 10] != 0) {
        document.getElementById("GPlblSX9").innerHTML = Results[5, 0];
        document.getElementById("GPlblBX9").innerHTML = Results[5, 1];
        document.getElementById("GPlblAX9").innerHTML = Results[5, 2];
    }
    else {
        document.getElementById("GPlblSX9").innerHTML = "";
        document.getElementById("GPlblBX9").innerHTML = "";
        document.getElementById("GPlblAX9").innerHTML = "";
    }
    document.getElementById("GPlblSX10").innerHTML = Results[6, 0];
    document.getElementById("GPlblBX10").innerHTML = Results[6, 1];
    document.getElementById("GPlblAX10").innerHTML = Results[6, 2];

    document.getElementById("GPlblShearXV0").innerHTML = Results[7, 4];
    document.getElementById("GPlblTorsionXV0").innerHTML = Results[7, 3];

    if (Results[7, 3] == 0 && Results[7, 4] == 0) {
        document.getElementById("GPlblVcXV0").innerHTML = Results[7, 9];
    } else {
        document.getElementById("GPlblVcXV0").innerHTML = "";
    }

    if (Results[8, 10] != 0) {
        document.getElementById("GPlblShearXV1").innerHTML = Results[8, 4];
        document.getElementById("GPlblTorsionXV1").innerHTML = Results[8, 3];
        if (Results[8, 3] == 0 && Results[8, 4] == 0) {
            document.getElementById("GPlblVcXV1").innerHTML = Results[8, 9];
        } else {
            document.getElementById("GPlblVcXV1").innerHTML = "";
        }
    }
    else {
        document.getElementById("GPlblShearXV1").innerHTML = "";
        document.getElementById("GPlblTorsionXV1").innerHTML = "";
        document.getElementById("GPlblVcXV1").innerHTML = "";
    }

    if (Results[9, 10] != 0) {
        document.getElementById("GPlblShearXV9").innerHTML = Results[9, 4];
        document.getElementById("GPlblTorsionXV9").innerHTML = Results[9, 3];
        if (Results[9, 3] == 0 && Results[9, 4] == 0) {
            document.getElementById("GPlblVcXV9").innerHTML = Results[9, 9];
        } else {
            document.getElementById("GPlblVcXV9").innerHTML = "";
        }
    }
    else {
        document.getElementById("GPlblShearXV9").innerHTML = "";
        document.getElementById("GPlblTorsionXV9").innerHTML = "";
        document.getElementById("GPlblVcXV9").innerHTML = "";
    }

    document.getElementById("GPlblShearXV10").innerHTML = Results[10, 4];
    document.getElementById("GPlblTorsionXV10").innerHTML = Results[10, 3];
    if (Results[10, 3] == 0 && Results[10, 4] == 0) {
        document.getElementById("GPlblVcXV10").innerHTML = Results[10, 9];
    } else {
        document.getElementById("GPlblVcXV10").innerHTML = "";
    }
    if (document.getElementById("GPcheckDesignAll").value) {
        document.getElementById("GPlblXMax").innerHTML = "MAX at L=" + Results[11, 10];
        document.getElementById("GPlblSXMax").innerHTML = Results[11, 0];
        document.getElementById("GPlblBXMax").innerHTML = Results[11, 1];
        document.getElementById("GPlblAXMax").innerHTML = Results[11, 2];
        document.getElementById("GPlblXVMax").innerHTML = "MAX at L=" + Results[12, 10];
        document.getElementById("GPlblShearXVMax").innerHTML = Results[12, 4];
        document.getElementById("GPlblTorsionXVMax").innerHTML = Results[12, 3];
        if (Results[12, 4] == 0 && Results[12, 3] == 0) {
            document.getElementById("GPlblVcXVMax").innerHTML = Results[12, 9];
        } else {
            document.getElementById("GPlblVcXVMax").innerHTML = "";
        }
    } else {
        document.getElementById("GPlblXMax").innerHTML = "";
        document.getElementById("GPlblSXMax").innerHTML = "";
        document.getElementById("GPlblBXMax").innerHTML = "";
        document.getElementById("GPlblAXMax").innerHTML = "";
        document.getElementById("GPlblXVMax").innerHTML = "";
        document.getElementById("GPlblShearXVMax").innerHTML = "";
        document.getElementById("GPlblTorsionXVMax").innerHTML = "";
        document.getElementById("GPlblVcXVMax").innerHTML = "";
    }
    switch (Results[1, 9]) {
        case 1: document.getElementById("GPlblX1").innerHTML = "1A"; break;
        case 2: document.getElementById("GPlblX1").innerHTML = "1B"; break;
        case 3: document.getElementById("GPlblX1").innerHTML = "1C"; break;
        case 4: document.getElementById("GPlblX1").innerHTML = "1D"; break;
        default: document.getElementById("GPlblX1").innerHTML = "1"; break;
    }
    switch (Results[2, 9]) {
        case 1: document.getElementById("GPlblX2").innerHTML = "2A"; break;
        case 2: document.getElementById("GPlblX2").innerHTML = "2B"; break;
        case 3: document.getElementById("GPlblX2").innerHTML = "2C"; break;
        case 4: document.getElementById("GPlblX2").innerHTML = "2D"; break;
        default: document.getElementById("GPlblX2").innerHTML = "2"; break;
    }
    switch (Results[4, 9]) {
        case 1: document.getElementById("GPlblX8").innerHTML = "8A"; break;
        case 2: document.getElementById("GPlblX8").innerHTML = "8B"; break;
        case 3: document.getElementById("GPlblX8").innerHTML = "8C"; break;
        case 4: document.getElementById("GPlblX8").innerHTML = "8D"; break;
        default: document.getElementById("GPlblX8").innerHTML = "8"; break;
    }
    switch (Results[4, 9]) {
        case 1: document.getElementById("GPlblX9").innerHTML = "9A"; break;
        case 2: document.getElementById("GPlblX9").innerHTML = "9B"; break;
        case 3: document.getElementById("GPlblX9").innerHTML = "9C"; break;
        case 4: document.getElementById("GPlblX9").innerHTML = "9D"; break;
        default: document.getElementById("GPlblX9").innerHTML = "9"; break;
    }
}
