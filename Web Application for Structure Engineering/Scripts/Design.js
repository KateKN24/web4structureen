﻿function check() {
    var infoF = $("#infoForm");
    return SubmitInf(infoF);
}
function SubmitInf(formContainer) {
    var check = 0;

    var N1 = document.getElementById("N1").value;
    var D1 = document.getElementById("D1").value;
    var LN1 = document.getElementById("LN1").value;
    var LD1 = document.getElementById("LD1").value;
    if (N1 == 0 || D1 == "" || LN1 == "" || LD1 == "" || N1 == "" || LN1 == 0 || LD1 == 0) {
        document.getElementById("N1").focus();
        alert("Extra reinforcement is not valid.");
        check = 1;
    }
    var RS2 = document.getElementById("RS2").value;
    if (RS2 == 0) {
        document.getElementById("RS2").value = 25;
    }

    var N11 = document.getElementById("N11").value;
    if (N11 != "") {
        if (N11 % 2 != 0) {
            document.getElementById("N11").focus();
            alert("Number of extra reinforcement for torsion is not valid.");
            check = 1;
        }
    }
    var Ct = document.getElementById("Ct").value;
    if (Ct == "" || Ct == 0) {
        document.getElementById("Ct").focus();
        alert("Covering is not valid.");
        check = 1;
    }
    var Cb = document.getElementById("Cb").value;
    if (Cb == "" || Cb == 0) {
        document.getElementById("Cb").focus();
        alert("Covering is not valid.");
        check = 1;
    }
    var FL = document.getElementById("FL");
    var TL = document.getElementById("TL").value;
    if (FL.checked != true || FL.checked == null) {
        if (TL == "" || TL <= 50) {
            document.getElementById("TL").focus();
            alert("Support width is not valid.");
            check = 1;
        }
    }
    var FR = document.getElementById("FR");
    var TR = document.getElementById("TR").value;
    if (FR.checked != true || FR.checked == null) {
        if (TR == "" || TR <= 50) {
            document.getElementById("TR").focus();
            alert("Support width is not valid.");
            check = 1;
        }
    }

    var Cot2 = document.getElementById("Cot2");
    var LN2 = document.getElementById("LN2").value;
    var LD2 = document.getElementById("LD2").value;
    var LL2 = document.getElementById("LL2").value;
    var RN2 = document.getElementById("RN2").value;
    var RD2 = document.getElementById("RD2").value;
    var RL2 = document.getElementById("RL2").value;
    if (Cot2.checked != true || Cot2.checked == null) {
        if (LN2 == "" || LN2 == 0 || LD2 == "" || LD2 == 0 || LL2 == "" || LL2 == 0) {
            document.getElementById("LN2").value = "";
            document.getElementById("LD2").value = "";
            document.getElementById("LL2").value = "";
        }
        if (RN2 == "" || RN2 == 0 || RD2 == "" || RD2 == 0 || RL2 == "" || RL2 == 0) {
            document.getElementById("RN2").value = "";
            document.getElementById("RD2").value = "";
            document.getElementById("RL2").value = "";
        }
    }
    else {
        if (LN2 == "" || LN2 == 0) {
            document.getElementById("LN2").focus();
            alert("Extra reinforcement is not valid.");
            check = 1;
        }
        if (LD2 == "" || LD2 == 0) {
            document.getElementById("LD2").focus();
            alert("Extra reinforcement is not valid.");
            check = 1;
        }
    }
    var Cot3 = document.getElementById("Cot3");
    var LN3 = document.getElementById("LN3").value;
    var LD3 = document.getElementById("LD3").value;
    var LL3 = document.getElementById("LL3").value;
    var RN3 = document.getElementById("RN3").value;
    var RD3 = document.getElementById("RD3").value;
    var RL3 = document.getElementById("RL3").value;
    if (Cot3.checked != true || Cot3.checked == null) {
        if (LN3 == "" || LN3 == 0 || LD3 == "" || LD3 == 0 || LL3 == "" || LL3 == 0) {
            document.getElementById("LN3").value = "";
            document.getElementById("LD3").value = "";
            document.getElementById("LL3").value = "";
        }
        if (RN3 == "" || RN3 == 0 || RD3 == "" || RD3 == 0 || RL3 == "" || RL3 == 0) {
            document.getElementById("RN3").value = "";
            document.getElementById("RD3").value = "";
            document.getElementById("RL3").value = "";
        }
    }
    else {
        if (LN3 == "" || LN3 == 0) {
            document.getElementById("LN3").focus();
            alert("Extra reinforcement is not valid.3");
            check = 1;
        }
        if (LD3 == "" || LD3 == 0) {
            document.getElementById("LD3").focus();
            alert("Extra reinforcement is not valid.3");
            check = 1;
        }
    }
    var Cot4 = document.getElementById("Cot4");
    var LN4 = document.getElementById("LN4").value;
    var LD4 = document.getElementById("LD4").value;
    var LL4 = document.getElementById("LL4").value;
    var RN4 = document.getElementById("RN4").value;
    var RD4 = document.getElementById("RD4").value;
    var RL4 = document.getElementById("RL4").value;
    if (Cot4.checked != true || Cot4.checked == null) {
        if (LN4 == "" || LN4 == 0 || LD4 == "" || LD4 == 0 || LL4 == "" || LL4 == 0) {
            document.getElementById("LN4").value = "";
            document.getElementById("LD4").value = "";
            document.getElementById("LL4").value = "";
        }
        if (RN4 == "" || RN4 == 0 || RD4 == "" || RD4 == 0 || RL4 == "" || RL4 == 0) {
            document.getElementById("RN4").value = "";
            document.getElementById("RD4").value = "";
            document.getElementById("RL4").value = "";
        }
    }
    else {
        if (LN4 == "" || LN4 == 0) {
            document.getElementById("LN4").focus();
            alert("Extra reinforcement is not valid.");
            check = 1;
        }
        if (LD4 == "" || LD4 == 0) {
            document.getElementById("LD4").focus();
            alert("Extra reinforcement is not valid.");
            check = 1;
        }
    }
    var Cot5 = document.getElementById("Cot5");
    var LN5 = document.getElementById("LN5").value;
    var LD5 = document.getElementById("LD5").value;
    var LL5 = document.getElementById("LL5").value;
    var RN5 = document.getElementById("RN5").value;
    var RD5 = document.getElementById("RD5").value;
    var RL5 = document.getElementById("RL5").value;
    if (Cot5.checked != true || Cot5.checked == null) {
        if (LN5 == "" || LN5 == 0 || LD5 == "" || LD5 == 0 || LL5 == "" || LL5 == 0) {
            document.getElementById("LN5").value = "";
            document.getElementById("LD5").value = "";
            document.getElementById("LL5").value = "";
        }
        if (RN5 == "" || RN5 == 0 || RD5 == "" || RD5 == 0 || RL5 == "" || RL5 == 0) {
            document.getElementById("RN5").value = "";
            document.getElementById("RD5").value = "";
            document.getElementById("RL5").value = "";
        }
    }
    else {
        if (LN5 == "" || LN5 == 0) {
            document.getElementById("LN5").focus();
            alert("Extra reinforcement is not valid.");
            check = 1;
        }
        if (LD5 == "" || LD5 == 0) {
            document.getElementById("LD5").focus();
            alert("Extra reinforcement is not valid.");
            check = 1;
        }
    }
    var Cob2 = document.getElementById("Cob2");
    var N2 = document.getElementById("N2").value;
    var D2 = document.getElementById("D2").value;
    var L2L = document.getElementById("L2L").value;
    var L2R = document.getElementById("L2L").value;
    if (Cob2.checked != true || Cob2.checked == null) {
        if (N2 == "" || N2 == 0 || D2 == "" || D2 == 0 || L2L == "" || L2R == "") {
            document.getElementById("N2").value = "";
            document.getElementById("D2").value = "";
            document.getElementById("L2L").value = "";
            document.getElementById("L2R").value = "";
        }
    }
    else {
        if (N2 == "" || N2 == 0) {
            document.getElementById("N2").focus();
            alert("Extra reinforcement is not valid.");
            check = 1;
        }
        if (D2 == "" || D2 == 0) {
            document.getElementById("D2").focus();
            alert("Extra reinforcement is not valid.");
            check = 1;
        }
    }
    var Cob3 = document.getElementById("Cob3");
    var N3 = document.getElementById("N3").value;
    var D3 = document.getElementById("D3").value;
    var L3L = document.getElementById("L3L").value;
    var L3R = document.getElementById("L3L").value;
    if (Cob3.checked != true || Cob3.checked == null) {
        if (N3 == "" || N3 == 0 || D3 == "" || D3 == 0 || L3L == "" || L3R == "") {
            document.getElementById("N3").value = "";
            document.getElementById("D3").value = "";
            document.getElementById("L3L").value = "";
            document.getElementById("L3R").value = "";
        }
    }
    else {
        if (N3 == "" || N3 == 0) {
            document.getElementById("N3").focus();
            alert("Extra reinforcement is not valid.");
            check = 1;
        }
        if (D3 == "" || D3 == 0) {
            document.getElementById("D3").focus();
            alert("Extra reinforcement is not valid.");
            check = 1;
        }
    }
    var Cob4 = document.getElementById("Cob4");
    var N4 = document.getElementById("N4").value;
    var D4 = document.getElementById("D4").value;
    var L4L = document.getElementById("L4L").value;
    var L4R = document.getElementById("L4L").value;
    if (Cob4.checked != true || Cob4.checked == null) {
        if (N4 == "" || N4 == 0 || D4 == "" || D4 == 0 || L4L == "" || L4R == "") {
            document.getElementById("N4").value = "";
            document.getElementById("D4").value = "";
            document.getElementById("L4L").value = "";
            document.getElementById("L4R").value = "";
        }
    }
    else {
        if (N4 == "" || N4 == 0) {
            document.getElementById("N4").focus();
            alert("Extra reinforcement is not valid.");
            check = 1;
        }
        if (D4 == "" || D4 == 0) {
            document.getElementById("D4").focus();
            alert("Extra reinforcement is not valid.");
            check = 1;
        }
    }
    var Cob5 = document.getElementById("Cob5");
    var N5 = document.getElementById("N5").value;
    var D5 = document.getElementById("D5").value;
    var L5L = document.getElementById("L5L").value;
    var L5R = document.getElementById("L5L").value;
    if (Cob5.checked != true || Cob5.checked == null) {
        if (N5 == "" || N5 == 0 || D5 == "" || D5 == 0 || L5L == "" || L5R == "") {
            document.getElementById("N5").value = "";
            document.getElementById("D5").value = "";
            document.getElementById("L5L").value = "";
            document.getElementById("L5R").value = "";
        }
    }
    else {
        if (N5 == "" || N5 == 0) {
            document.getElementById("N5").focus();
            alert("Extra reinforcement is not valid.");
            check = 1;
        }
        if (D5 == "" || D5 == 0) {
            document.getElementById("D5").focus();
            alert("Extra reinforcement is not valid.");
            check = 1;
        }
    }

    var Nslin = document.getElementById("Nsli").value;
    var Nslout = document.getElementById("Nslo").value;
    var Stln = document.getElementById("Stln").value;
    var Stls = document.getElementById("Stls").value;
    var Nsrin = document.getElementById("Nsri").value;
    var Nsrout = document.getElementById("Nsro").value;
    var Strn = document.getElementById("Strn").value;
    var Strs = document.getElementById("Strs").value;
    var Nsi = document.getElementById("Nsi").value;
    var Nso = document.getElementById("Nso").value;
    var Sts = document.getElementById("Sts").value;
    if (((Nslin == "" || Nslin == 0) && (Nslout == "" || Nslout == 0)) || Stln == "" || Stln == 0 || Stls == "" || Stls == 0) {
        document.getElementById("Nsli").value = "";
        document.getElementById("Nslo").value = "";
        document.getElementById("Stln").value = "";
        document.getElementById("Stls").value = "";
    }
    if (((Nsrin == "" || Nsrin == 0) && (Nsrout == "" || Nsrout == 0)) || Strn == "" || Strn == 0 || Strs == "" || Strs == 0) {
        document.getElementById("Nsri").value = "";
        document.getElementById("Nsro").value = "";
        document.getElementById("Strn").value = "";
        document.getElementById("Strs").value = "";
    }
    if (((Nsi == "" || Nsi == 0) && (Nso == "" || Nso == 0)) || Sts == "" || Sts == 0) {
        document.getElementById("Nsi").value = "";
        document.getElementById("Nso").value = "";
        document.getElementById("Sts").value = "";
    }

    if (check == 0) {
        $.ajax({
            type: 'post',
            url: '/Home/Design',
            data: formContainer.serialize(),
            success: function (data) {
                if (data.checkk == -1) {
                    document.getElementById("MEMLIST").focus();
                    alert("At least one member in memlist has no data.");
                }
                else if (data.checkk == -2) {
                    document.getElementById("MEMLIST").focus();
                    alert("At least one member in memlist has invalid incidence.");
                }
                else if (data.Lenexx == -1) {
                    alert("Extra reinforcement is invalid.");
                }
                else {
                    fillForm(data.results, data.chkDeAll, data.utf);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    }
    else {

    }
}

function checkFLFR() {
    var FL = document.getElementById("FL");
    if (FL.checked == true) {
        document.getElementById("TL").disabled = true;
        document.getElementById("TL").value = "";
        document.getElementById("CL").value = "F";
        document.getElementById("CLT").value = "F";
        document.getElementById("CL").disabled = true;
        document.getElementById("CLT").disabled = true;
    }
    else {
        document.getElementById("TL").disabled = false;
        document.getElementById("CL").disabled = false;
        document.getElementById("CLT").disabled = false;
    }
    var FR = document.getElementById("FR");
    if (FR.checked == true) {
        document.getElementById("TR").disabled = true;
        document.getElementById("TR").value = "";
        document.getElementById("CR").value = "F";
        document.getElementById("CRT").value = "F";
        document.getElementById("CR").disabled = true;
        document.getElementById("CRT").disabled = true;
    }
    else {
        document.getElementById("TR").disabled = false;
        document.getElementById("CR").disabled = false;
        document.getElementById("CRT").disabled = false;
    }
}

function checkCotCob() {
    var Cot2 = document.getElementById("Cot2");
    if (Cot2.checked == true) {
        document.getElementById("LL2").disabled = true;
        document.getElementById("LL2").value = "";
        document.getElementById("RN2").disabled = true;
        document.getElementById("RN2").value = "";
        document.getElementById("RD2").disabled = true;
        document.getElementById("RD2").value = 0;
        document.getElementById("RL2").disabled = true;
        document.getElementById("RL2").value = "";
    }
    else {
        document.getElementById("LL2").disabled = false;
        document.getElementById("RN2").disabled = false;
        document.getElementById("RD2").disabled = false;
        document.getElementById("RL2").disabled = false;
    }
    var Cot3 = document.getElementById("Cot3");
    if (Cot3.checked == true) {
        document.getElementById("LL3").disabled = true;
        document.getElementById("LL3").value = "";
        document.getElementById("RN3").disabled = true;
        document.getElementById("RN3").value = "";
        document.getElementById("RD3").disabled = true;
        document.getElementById("RD3").value = 0;
        document.getElementById("RL3").disabled = true;
        document.getElementById("RL3").value = "";
    }
    else {
        document.getElementById("LL3").disabled = false;
        document.getElementById("RN3").disabled = false;
        document.getElementById("RD3").disabled = false;
        document.getElementById("RL3").disabled = false;
    }
    var Cot4 = document.getElementById("Cot4");
    if (Cot4.checked == true) {
        document.getElementById("LL4").disabled = true;
        document.getElementById("LL4").value = "";
        document.getElementById("RN4").disabled = true;
        document.getElementById("RN4").value = "";
        document.getElementById("RD4").disabled = true;
        document.getElementById("RD4").value = 0;
        document.getElementById("RL4").disabled = true;
        document.getElementById("RL4").value = "";
    }
    else {
        document.getElementById("LL4").disabled = false;
        document.getElementById("RN4").disabled = false;
        document.getElementById("RD4").disabled = false;
        document.getElementById("RL4").disabled = false;
    }
    var Cot5 = document.getElementById("Cot5");
    if (Cot5.checked == true) {
        document.getElementById("LL5").disabled = true;
        document.getElementById("LL5").value = "";
        document.getElementById("RN5").disabled = true;
        document.getElementById("RN5").value = "";
        document.getElementById("RD5").disabled = true;
        document.getElementById("RD5").value = 0;
        document.getElementById("RL5").disabled = true;
        document.getElementById("RL5").value = "";
    }
    else {
        document.getElementById("LL5").disabled = false;
        document.getElementById("RN5").disabled = false;
        document.getElementById("RD5").disabled = false;
        document.getElementById("RL5").disabled = false;
    }


    var Cob2 = document.getElementById("Cob2");
    if (Cob2.checked == true) {
        document.getElementById("L2L").disabled = true;
        document.getElementById("L2L").value = "";
        document.getElementById("L2R").disabled = true;
        document.getElementById("L2R").value = "";
    }
    else {
        document.getElementById("L2L").disabled = false;
        document.getElementById("L2R").disabled = false;
    }
    var Cob3 = document.getElementById("Cob3");
    if (Cob3.checked == true) {
        document.getElementById("L3L").disabled = true;
        document.getElementById("L3L").value = "";
        document.getElementById("L3R").disabled = true;
        document.getElementById("L3R").value = "";
    }
    else {
        document.getElementById("L3L").disabled = false;
        document.getElementById("L3R").disabled = false;
    }
    var Cob4 = document.getElementById("Cob4");
    if (Cob4.checked == true) {
        document.getElementById("L4L").disabled = true;
        document.getElementById("L4L").value = "";
        document.getElementById("L4R").disabled = true;
        document.getElementById("L4R").value = "";
    }
    else {
        document.getElementById("L4L").disabled = false;
        document.getElementById("L4R").disabled = false;
    }
    var Cob5 = document.getElementById("Cob5");
    if (Cob5.checked == true) {
        document.getElementById("L5L").disabled = true;
        document.getElementById("L5L").value = "";
        document.getElementById("L5R").disabled = true;
        document.getElementById("L5R").value = "";
    }
    else {
        document.getElementById("L5L").disabled = false;
        document.getElementById("L5R").disabled = false;
    }
}