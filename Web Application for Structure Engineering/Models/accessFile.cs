﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;


namespace Web_Application_for_Structure_Engineering.Models
{
    public class accessFile
    {
        [DisplayName("Upload File")]

        public string Front { get; set; }
        public HttpPostedFileBase ImgFile1 { get; set; }

        public int accessFileID { get; set; }
        public string accessFileName { get; set; }
        public int projectID { get; set; }
    }
}