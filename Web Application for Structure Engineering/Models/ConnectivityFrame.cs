﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Application_for_Structure_Engineering.Models
{
    public class Connectivity___Frame
    {
        public int projectID { get; set; }
        public int accessFileID { get; set; }
        public int Id { get; set; }
        public int Frame { get; set; }
        public float JointI { get; set; }
        public float JointJ { get; set; }
        public string IsCurved { get; set; }
        public float Length { get; set; }
        public float CentroidX { get; set; }
        public float CentroidY { get; set; }
        public float CentroidZ { get; set; }
        public string GUID { get; set; }
    }
}