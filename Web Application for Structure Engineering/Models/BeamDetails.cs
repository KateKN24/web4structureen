﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Application_for_Structure_Engineering.Models
{
    public class BeamDetails
    {
        public string BeamNo { get; set; }
        public string Size { get; set; }
        public string Stirrup1 { get; set; }
        public string Stirrup2 { get; set; }
        public string Stirrup3 { get; set; }
        public string TorReinf { get; set; }
        public Details Layer1 { get; set; }
        public Details Layer2 { get; set; }
        public Details Layer3 { get; set; }
        public Details Layer4 { get; set; }
        public Details Layer5 { get; set; }

    }
    public class Details
    {
        public string LTB1 { get; set; }
        public string LTB2 { get; set; }
        public string BB1 { get; set; }
        public string BB2 { get; set; }
        public string BB3 { get; set; }
        public string RTB1 { get; set; }
        public string RTB2 { get; set; }
        public string RTB3 { get; set; }
    }
}