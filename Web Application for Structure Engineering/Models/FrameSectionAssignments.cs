﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Application_for_Structure_Engineering.Models
{
    public class Frame_Section_Assignments
    {
        public int projectID { get; set; }
        public int accessFileID { get; set; }
        public int ID { get; set; }
        public int Frame { get; set; }
        public string SectionType { get; set; }
        public string AutoSelect { get; set; }
        public string AnalSect    { get; set; }
        public string DesignSect { get; set; }
        public string MatProp { get; set; }
    }
}