﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Application_for_Structure_Engineering.Models
{
    public class Member
    {
        public int Id { get; set; }
        public string email { get; set; }
        public string User_Id { get; set; }
        public string Name { get; set; }
        public string Img_path { get; set; }
    }
}