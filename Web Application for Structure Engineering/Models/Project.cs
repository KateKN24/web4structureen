﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Application_for_Structure_Engineering.Models
{
    public class Project
    {
        public int projectID { get; set; }
        public string projectName { get; set; }
        public int Uid { get; set; }
    }
}