﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Application_for_Structure_Engineering.Models
{
    public class calSheet
    {
        public string MemberNo { get; set; }
        public string Ds { get; set; }
        public string Mark { get; set; }
        public string LTB1 { get; set; }
        public string LTB2 { get; set; }
        public string LTB3 { get; set; }
        public string LTB4 { get; set; }
        public string LTB5 { get; set; }
        public string LTBStls { get; set; }
        public string MBB1 { get; set; }
        public string MBB2 { get; set; }
        public string MBB3 { get; set; }
        public string MBB4 { get; set; }
        public string MBB5 { get; set; }
        public string MBBSts { get; set; }
        public string RTB1 { get; set; }
        public string RTB2 { get; set; }
        public string RTB3 { get; set; }
        public string RTB4 { get; set; }
        public string RTB5 { get; set; }
        public string RTBStrs { get; set; }
        public string Cm { get; set; }
        public string Mzl { get; set; }
        public string Mxl { get; set; }
        public string SX0 { get; set; }
        public string VX0 { get; set; }
        public string SV0 { get; set; }
        public string Mzm { get; set; }
        public string Mxm { get; set; }
        public string Sxm { get; set; }
        public string Mzr { get; set; }
        public string Mxr { get; set; }
        public string SX10 { get; set; }
        public string VX10 { get; set; }
        public string SV10 { get; set; }

    }
}