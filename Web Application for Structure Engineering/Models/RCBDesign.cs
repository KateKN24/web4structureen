﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Application_for_Structure_Engineering.Models
{
    public class RCBDesign
    {
        public int projectID { get; set; }
        public int accessFileID { get; set; }
        public int RCBeamID { get; set; }
        public string Mem { get; set; }
        public string Mark { get; set; }
        public string B { get; set; }
        public string T { get; set; }
        public float L { get; set; }
        public float LT { get; set; }
        public string Name { get; set; }
        public int EFFID { get; set; }
        public string Frame { get; set; }
        public float Station { get; set; }
        public string OutputCase { get; set; }
        public string CaseType { get; set; }
        public float P { get; set; }
        public float V2 { get; set; }
        public float V3 { get; set; }
        public float M2 { get; set; }
        public float M3 { get; set; }
    }
}